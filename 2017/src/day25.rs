use aoc::{self, Day, Result};
use std::collections::VecDeque;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day25 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub start: char,
    pub diagnostic: usize,
    pub machine: Vec<State>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day25 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day25::Part1;
    type Part2 = crate::day25::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let (start, diagnostic, machine) = parse_input(input.lines().collect()).unwrap();

        Ok(Self { part1: None, part2: None, start, diagnostic, machine })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut tape = VecDeque::new();
        let mut cursor = 0;
        let mut current_state = self.start;
        tape.push_back(false);

        for _ in 0..self.diagnostic {
            let state = state(current_state, &self.machine);

            let action = if tape[cursor] { &state.on_one } else { &state.on_zero };
            tape[cursor] = action.write;

            match action.direction {
                Direction::Right => {
                    if cursor == tape.len() - 1 {
                        tape.push_back(false);
                    }
                    cursor += 1;
                },
                Direction::Left => {
                    if cursor > 0 {
                        cursor -= 1;
                    } else {
                        tape.push_front(false);
                    }
                }
            }
            current_state = action.next;
        }


        self.part1 = Some(tape.iter().filter(|&c| *c).count());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn state(name: char, machine: &[State]) -> &State {
    for state in machine {
        if state.name == name {
            return state;
        }
    }
    panic!("State not found {}", name)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn parse_input(input: Vec<&str>) -> Option<(char, usize, Vec<State>)> {
    let start_state = input[0].trim().split(' ').last()?.chars().next()?;
    let diagnostic = input[1].trim().split(' ').nth(5)?.parse().unwrap();

    let mut state_offset = 3;
    let mut machine = vec![];

    while state_offset < input.len() {
        let name = input[state_offset].trim().split(' ').last()?.chars().next()?;

        let on_zero = parse_action(&input, state_offset + 1)?;
        let on_one = parse_action(&input, state_offset + 5)?;

        machine.push(State { name, on_zero, on_one });

        state_offset += 10;
    }

    Some((start_state, diagnostic, machine))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn parse_action(input: &[&str], action_offset: usize) -> Option<Action> {
    let write = input[action_offset + 1].trim().split(' ').last()? == "1.";
    let direction = match input[action_offset + 2].trim().split(' ').last()? {
        "left." => Direction::Left,
        "right." => Direction::Right,
        _ => panic!("Invalid direction")
    };
    let next = input[action_offset + 3].trim().split(' ').last()?.chars().next()?;

    Some(Action { write, direction, next })
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, PartialEq)]
pub enum Direction {
    Right,
    Left
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Action {
    write: bool,
    direction: Direction,
    next: char,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct State {
    name: char,
    on_zero: Action,
    on_one: Action
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day25::{Day25, parse_input, Direction};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const TEST_INPUT: &str =
        "Begin in state A.
         Perform a diagnostic checksum after 6 steps.

         In state A:
           If the current value is 0:
             - Write the value 1.
             - Move one slot to the right.
             - Continue with state B.
           If the current value is 1:
             - Write the value 0.
             - Move one slot to the left.
             - Continue with state B.

         In state B:
           If the current value is 0:
             - Write the value 1.
             - Move one slot to the left.
             - Continue with state A.
           If the current value is 1:
             - Write the value 1.
             - Move one slot to the right.
             - Continue with state A.";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_parsing() {
        let (start, diagnostic, machine) = parse_input(TEST_INPUT.lines().collect()).unwrap();
        assert_eq!(start, 'A');
        assert_eq!(diagnostic, 6);

        assert_eq!(machine.len(), 2);

        let state = &machine[0];
        assert_eq!(state.name, 'A');
        assert_eq!(state.on_zero.write, true);
        assert_eq!(state.on_zero.direction, Direction::Right);
        assert_eq!(state.on_zero.next, 'B');
        assert_eq!(state.on_one.write, false);
        assert_eq!(state.on_one.direction, Direction::Left);
        assert_eq!(state.on_one.next, 'B');

        let state = &machine[1];
        assert_eq!(state.name, 'B');
        assert_eq!(state.on_zero.write, true);
        assert_eq!(state.on_zero.direction, Direction::Left);
        assert_eq!(state.on_zero.next, 'A');
        assert_eq!(state.on_one.write, true);
        assert_eq!(state.on_one.direction, Direction::Right);
        assert_eq!(state.on_one.next, 'A');

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let mut day = Day25::setup(TEST_INPUT).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day25.txt");

        let mut day = Day25::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2474));
    }
}
