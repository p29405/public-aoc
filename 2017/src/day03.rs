use aoc::{self, Day, Result, CenteredMap, Coord, GridElement, Map};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day03::Part1;
    type Part2 = crate::day03::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim().parse()?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let coord = to_coord(self.input);

        self.part1 = Some(coord.x.abs() + coord.y.abs());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut map : CenteredMap<Square> = CenteredMap::default();

        for i in 1.. {
            let coord = to_coord(i);
            let value = coord.neighbors().map(|p| map.get(&p).value()).sum();

            if value > self.input {
                self.part2 = Some(value);
                return;
            }

            map.set(&coord, Square::Value(if i == 1 { 1 } else { value }));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn to_coord(index: i64) -> Coord {
    let (ul, lr) = corners(index);

    let ul_value = ul.1.value();
    let lr_value = lr.1.value();

    if index == ul_value {
        ul.0
    } else if index == lr_value {
        lr.0
    } else if index < ul_value {
        let diff = ul_value - index;
        let width = -ul.0.x * 2;

        if diff <= width {
            Coord::new(ul.0.x + diff, ul.0.y)
        } else {
            let y_offset = diff - width;
            Coord::new(-ul.0.x, ul.0.y - y_offset)
        }
    } else {
        let diff = index - ul_value;
        let height = ul.0.y * 2;

        if diff <= height {
            Coord::new(ul.0.x, ul.0.y - diff)
        } else {
            let x_offset = diff - height;
            Coord::new(ul.0.x + x_offset, -ul.0.y)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn corners(input: i64) -> ((Coord, Square), (Coord, Square)) {

    let mut sqrt = (input as f64).sqrt() as i64;

    if sqrt & 0x1 == 0x0 {
        
        let ul = (Coord::new(-sqrt / 2, sqrt / 2), Square::Value(sqrt * sqrt + 1));
        sqrt += if input == sqrt * sqrt { -1 } else { 1 };
        let lr = (Coord::new(sqrt / 2, -sqrt / 2), Square::Value(sqrt * sqrt));

        (ul, lr)
    } else {
        let lr = (Coord::new(sqrt / 2, -sqrt / 2), Square::Value(sqrt * sqrt));
        sqrt += 1;
        let ul = (Coord::new(-sqrt / 2, sqrt / 2), Square::Value(sqrt * sqrt + 1));
        (ul, lr)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Unknown,
    Value(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    fn value(&self) -> i64 {

        if let Square::Value(val) = self {
            *val
        } else {
            0
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    fn default() -> Self {
        Square::Unknown
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Unknown => write!(f, "."),
            Square::Value(c) => write!(f, "{}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day, Coord};
    use crate::day03::{Day03, to_coord};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_to_coord() {
        assert_eq!(to_coord(1), Coord::new(0, 0));
        assert_eq!(to_coord(2), Coord::new(1, 0));
        assert_eq!(to_coord(3), Coord::new(1, 1));
        assert_eq!(to_coord(4), Coord::new(0, 1));
        assert_eq!(to_coord(5), Coord::new(-1, 1));
        assert_eq!(to_coord(6), Coord::new(-1, 0));
        assert_eq!(to_coord(7), Coord::new(-1, -1));
        assert_eq!(to_coord(8), Coord::new(0, -1));
        assert_eq!(to_coord(9), Coord::new(1, -1));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("1", 0),
                ("12", 3),
                ("21", 4),
                ("22", 3),
                ("23", 2),
                ("25", 4),
                ("26", 5),
                ("49", 6),
                ("1024", 31)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(552));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("9", 10),
                ("100", 122),
                ("500", 747)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(330_785));
    }
}
