
////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct KnotHash;

////////////////////////////////////////////////////////////////////////////////////////////////////
impl KnotHash {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn compute(string: &str) -> u128 {
        let mut lengths = string.chars().map(|c| c as u8).collect::<Vec<_>>();
        lengths.append(&mut Vec::from([17u8, 31u8, 73u8, 47u8, 23u8]));

        let mut start = 0;
        let mut skip = 0usize;
        let mut list= (0u8..=255).collect::<Vec<_>>();

        for _ in 0..64 {
            for length in lengths.iter() {
                Self::step(&mut list[..], start, *length as usize);

                start += *length as usize + skip;
                skip += 1;
            }

            start %= list.len();
            skip %= list.len();
        }

        list.chunks(16)
            .map(|chunk| chunk.iter().fold(0, |accum, v| v ^ accum))
            .fold(0, |accum, v| accum << 8 | v as u128)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn compute_hex(string: &str) -> String {
        format!("{:032x}", Self::compute(string))
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(list: &mut [u8], start: usize, length: usize) {
        for (i, j) in (0..length / 2).map(|i| (start + i, start + length - 1 - i)) {
            let temp = list[i % list.len()];
            list[i % list.len()] = list[j % list.len()];
            list[j % list.len()] = temp;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use crate::knot::KnotHash;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_step() {
        let mut list = [0, 1, 2, 3, 4];
        KnotHash::step(&mut list, 0, 3);
        assert_eq!(list, [2, 1, 0, 3, 4]);
        KnotHash::step(&mut list, 3, 4);
        assert_eq!(list, [4, 3, 0, 1, 2]);
        KnotHash::step(&mut list, 8, 1);
        assert_eq!(list, [4, 3, 0, 1, 2]);
        KnotHash::step(&mut list, 11, 5);
        assert_eq!(list, [3, 4, 2, 1, 0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example1() {
        let inputs =
            [
                ("", "a2582a3a0e66e6e86e3812dcb672a272"),
                ("AoC 2017", "33efeb34ea91902bb2f59c9920caa6cd"),
                ("1,2,3", "3efbe78a8d82f29979031a4aa0b16a9d"),
                ("1,2,4", "63960835bcdc130f0b66d7ff4f6a5a8e")
            ];

        for (input, answer) in inputs {
            assert_eq!(KnotHash::compute_hex(input), answer.to_owned(), "For input: {}", input);
        }
    }

}