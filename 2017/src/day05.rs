use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day05 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day05 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day05::Part1;
    type Part2 = crate::day05::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut jumps = self.input.to_vec();
        let mut i = 0;
        let mut steps = 0;

        while i < jumps.len() {
            let cur = i;
            i = (i as i32 + jumps[i]) as usize;
            jumps[cur] += 1;
            steps += 1;
        }

        self.part1 = Some(steps);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut jumps = self.input.to_vec();
        let mut i = 0;
        let mut steps = 0;

        while i < jumps.len() {
            let cur = i;
            let offset = jumps[i];
            i = (i as i32 + offset) as usize;
            jumps[cur] += if offset >= 3 { -1 } else { 1 };
            steps += 1;
        }

        self.part2 = Some(steps);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day05::Day05;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "0\n3\n0\n1\n-3";

        let mut day = Day05::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(343_364));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "0\n3\n0\n1\n-3";

        let mut day = Day05::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(10));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(25_071_947));
    }
}
