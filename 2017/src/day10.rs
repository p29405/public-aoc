use aoc::{self, Day, Result};
use crate::knot::KnotHash;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<usize>,
    pub input_2: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(string: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(string, ",")?;

        Ok(Self { part1: None, part2: None, input, input_2: string.to_owned() })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut start = 0;
        let mut skip = 0;
        let mut list = [0u8; 256];

        for i in 0u8..=255 {
            list[i as usize] = i;
        }

        for length in self.input.iter() {
            KnotHash::step(&mut list[..], start, *length);

            start += length + skip;
            skip += 1;
        }


        self.part1 = Some(list[0] as usize * list[1] as usize);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(KnotHash::compute_hex(&self.input_2));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.to_owned() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day10::Day10;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4114));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("2f8c3d2100fdd57cec130d928b0fd2dd".into()));
    }
}