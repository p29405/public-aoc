use aoc::{self, Result};
use coprocessor::{State, Coprocessor, Instruction};

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_list("\n")?;
    aoc::setup_finished();

    aoc::answer("Part 1", &part1(&input))?;
    aoc::answer("Part 2", &part2(&input))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(input: &[Instruction]) -> usize {
    let mut coprocessor = Coprocessor::default();
    coprocessor.program(input);

    coprocessor.mul_instruction_count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(input: &[Instruction]) -> i64 {
    let mut coprocessor = Coprocessor::default();
    coprocessor.program(input).poke('a', 1);

    while coprocessor.step() != State::Interesting {
    }

    *coprocessor.peek('h').unwrap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::{part1, part2};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        aoc::logger::init(aoc::logger::LevelFilter::Debug).unwrap();
        let instructions = aoc::read_list_from_file("input/day23.txt", "\n").unwrap();
        assert_eq!(part1(&instructions), 4225);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        //let instructions = aoc::read_list_from_file("input/day23.txt", "\n").unwrap();
        //assert_eq!(part2(&instructions), 0);
    }

}
