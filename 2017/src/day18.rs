use aoc::{self, Result};
use crate::duet::{DuetState, DuetVM, Instruction};

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_list("\n")?;
    aoc::setup_finished();

    aoc::answer("Part 1", &part1(&input))?;
    aoc::answer("Part 2", &part2(&input))?;

    Ok(())
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(input: &[Instruction]) -> i64 {
    let mut duet = DuetVM::default();
    duet.program(input).day18_part1();

    loop {
        match duet.step() {
            DuetState::Halt => break,
            DuetState::Send(_) => continue,
            DuetState::Receive => return *duet.output().last().unwrap(),
            DuetState::Continue => continue,
        };
    }

    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(input: &[Instruction]) -> usize {
    let mut duet0 = DuetVM::default();
    let mut duet1 = DuetVM::default();

    duet0.program(input).program_id(0);
    duet1.program(input).program_id(1);

    while !duet0.terminal_state() || !duet1.terminal_state() {
        loop {
            match duet0.step() {
                DuetState::Receive | DuetState::Halt => break,
                DuetState::Send(value) => duet1.input(value),
                DuetState::Continue => ()
            };
        }

        loop {
            match duet1.step() {
                DuetState::Receive | DuetState::Halt => break,
                DuetState::Send(value) => duet0.input(value),
                DuetState::Continue => ()
            };
        }
    }

    duet1.output().len()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::day18::{part1, part2};
    use crate::duet::Instruction;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let input = aoc::read_list_from_file::<Instruction>("input/day18.txt","\n").unwrap();

        assert_eq!(part1(&input), 2_951);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let input = aoc::read_list_from_file::<Instruction>("input/day18.txt","\n").unwrap();

        assert_eq!(part2(&input), 7_366);
    }
}
