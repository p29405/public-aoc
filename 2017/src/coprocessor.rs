use log::debug;
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Value {
    Immediate { value: i64 },
    Register { register: char }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Value {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.parse::<i64>() {
            Ok(value) => Ok(Value::Immediate { value }),
            _ => Ok(Value::Register { register: s.parse().unwrap() })
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Instruction {
    Set { op1: Value, op2: Value },
    Sub { op1: Value, op2: Value },
    Mul { op1: Value, op2: Value },
    Jnz { op1: Value, op2: Value }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        match parts[0] {
            "set" => Ok(Instruction::Set {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "sub" => Ok(Instruction::Sub {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "mul" => Ok(Instruction::Mul {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "jnz" => Ok(Instruction::Jnz {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            _ => Err(aoc::Error::new(&format!("Unknown opcode {}", parts[0])))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub enum State {
    Halt,
    Continue,
    Interesting
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub enum ExecResult {
    Jump(i64),
    RegisterUpdate(char)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug)]
pub struct Coprocessor {
    registers: HashMap<char, i64>,
    ip: usize,
    program: Vec<Instruction>,
    pub mul_instruction_count: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Coprocessor {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[Instruction]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn poke(&mut self, register: char, value: i64) -> &mut Self {
        self.registers.insert(register, value);
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn peek(&mut self, register: char) -> Option<&i64> {
        self.registers.get(&register)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(&mut self) -> State {
        if self.ip >= self.program.len() {
            return State::Halt;
        }


        debug!("Coprocessor: Before: ip: {} {:?}: {:?}", self.ip, self.program[self.ip], self.registers);

        let result = self.execute(self.program[self.ip]);

        if let Some(result) = result {
            match result {
                ExecResult::Jump(jump) => {
                    debug!("Coprocessor: After: ip: {} {:?}: Jump to {}", self.ip, self.program[self.ip], self.ip as i64 + jump);
                    self.ip = (self.ip as i64 + jump) as usize;
                },
                ExecResult::RegisterUpdate(r) => {
                    let value = self.fetch(Value::Register{ register: r});
                    debug!("Coprocessor: After: ip: {} {:?}: Update: {} = {}", self.ip, self.program[self.ip], r, value);
                    self.ip += 1;
                }
            }
        } else {
            self.ip += 1;
        }

        debug!("==========================");

        if self.ip == 24 {
            State::Interesting
        } else {
            State::Continue
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn register(&mut self, register: char) -> &mut i64 {
        self.registers.entry(register).or_insert(0)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fetch(&mut self, value: Value) -> i64 {
        match value {
            Value::Immediate { value} => value,
            Value::Register { register } => *self.register(register)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: Instruction) -> Option<ExecResult> {
        match instruction {
            Instruction::Set { op1, op2 } => {
                match op1 {
                    Value::Register { register} => {
                        *self.register(register) = self.fetch(op2);
                        Some(ExecResult::RegisterUpdate(register))
                    },
                    _ => unreachable!()
                }
            },
            Instruction::Sub { op1, op2 } => {
                match op1 {
                    Value::Register { register} => {
                        *self.register(register) -= self.fetch(op2);
                        Some(ExecResult::RegisterUpdate(register))
                    },
                    _ => unreachable!()
                }
            },
            Instruction::Mul { op1, op2 } => {
                self.mul_instruction_count += 1;
                match op1 {
                    Value::Register { register} => {
                        *self.register(register) *= self.fetch(op2);
                        Some(ExecResult::RegisterUpdate(register))
                    },
                    _ => unreachable!()
                }
            },
            Instruction::Jnz { op1, op2 } => {
                if self.fetch(op1) != 0 {
                    Some(ExecResult::Jump(self.fetch(op2)))
                } else {
                    None
                }
            }
        }
    }
}
