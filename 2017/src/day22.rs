use aoc::{self, Grid, GridElement, Result, Map, Coord, CenteredMap, Direction};
use log::debug;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let grid = aoc::read_grid()?;
    aoc::setup_finished();

    aoc::answer("Part 1", &part1(&grid))?;
    aoc::answer("Part 2", &part2(&grid))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(grid: &Grid<Node>) -> usize {
    let center = center(&grid);

    let mut map = centered_map(center, &grid);
    let mut carrier = Carrier { position: Coord::default(), infections: 0, direction: Direction::North };

    for _ in 0..10_000 {
        burst_part1(&mut carrier, &mut map);
    }

    debug!("{}", map);

    carrier.infections
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(grid: &Grid<Node>) -> usize {
    let center = center(&grid);

    let mut map = centered_map(center, &grid);
    let mut carrier = Carrier { position: Coord::default(), infections: 0, direction: Direction::North };

    for _ in 0..10_000_000 {
        burst_part2(&mut carrier, &mut map);
    }

    debug!("{}", map);

    carrier.infections
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn center(grid: &Grid<Node>) -> Coord {
    Coord::new(grid.columns / 2, grid.rows / 2)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn centered_map(center: Coord, grid: &Grid<Node>) -> CenteredMap<Node> {
    let mut map = CenteredMap::default();

    for (coord, node) in grid.iter() {
        let pos = Coord::new(coord.x - center.x, coord.y - center.y);
        map.set(&pos, node);
    }

    map
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn burst_part1(carrier: &mut Carrier, map: &mut CenteredMap<Node>) {
    match map.get(&carrier.position) {
        Node::Clean => {
            carrier.direction = carrier.direction.turn_left();
            map.set(&carrier.position, Node::Infected);
            carrier.infections += 1;
        },
        Node::Infected => {
            carrier.direction = carrier.direction.turn_right();
            map.set(&carrier.position, Node::Clean);
        },
        _ => ()
    };

    carrier.position = carrier.position.go_inverted(carrier.direction);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn burst_part2(carrier: &mut Carrier, map: &mut CenteredMap<Node>) {
    match map.get(&carrier.position) {
        Node::Clean => {
            carrier.direction = carrier.direction.turn_left();
            map.set(&carrier.position, Node::Weakened);
        },
        Node::Infected => {
            carrier.direction = carrier.direction.turn_right();
            map.set(&carrier.position, Node::Flagged);
        },
        Node::Weakened => {
            map.set(&carrier.position, Node::Infected);
            carrier.infections += 1;
        },
        Node::Flagged => {
            carrier.direction = carrier.direction.one_eighty();
            map.set(&carrier.position, Node::Clean);

        }
    };

    carrier.position = carrier.position.go_inverted(carrier.direction);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub struct Carrier {
    pub position: Coord,
    pub infections: usize,
    pub direction: Direction
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Node {
    Clean,
    Infected,
    Weakened,
    Flagged
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Node {
    fn default() -> Self {
        Node::Clean
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Node {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Node::Clean => write!(f, "."),
            Node::Infected => write!(f, "#"),
            Node::Weakened => write!(f, "W"),
            Node::Flagged => write!(f, "F")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Node {
    fn to_element(c: char) -> Self {
        match c {
            '#' => Node::Infected,
            '.' => Node::Clean,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::{Node, part1, part2, center, centered_map, Carrier, burst_part1, burst_part2};
    use aoc::{Coord, Direction};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input =
            "..#
             #..
             ...";
        let grid = aoc::read_grid_from_string::<Node>(input).unwrap();
        let center = center(&grid);

        assert_eq!(center, Coord::new(1, 1));

        let mut map = centered_map(center, &grid);

        let mut carrier = Carrier { position: Coord::default(), infections: 0, direction: Direction::North };

        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);
        burst_part1(&mut carrier, &mut map);

        assert_eq!(carrier.infections, 5);

        for _ in 7..70 {
            burst_part1(&mut carrier, &mut map);
        }
        println!("{}", map);

        assert_eq!(carrier.infections, 41);

        for _ in 70..10_000 {
            burst_part1(&mut carrier, &mut map);
        }

        assert_eq!(carrier.infections, 5_587);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input =
            "..#
             #..
             ...";
        let grid = aoc::read_grid_from_string::<Node>(input).unwrap();
        let center = center(&grid);

        assert_eq!(center, Coord::new(1, 1));

        let mut map = centered_map(center, &grid);

        let mut carrier = Carrier { position: Coord::default(), infections: 0, direction: Direction::North };

        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);
        burst_part2(&mut carrier, &mut map);

        for _ in 7..100 {
            burst_part2(&mut carrier, &mut map);
        }
        println!("{}", map);

        assert_eq!(carrier.infections, 26);

        for _ in 100..10_000_000 {
            burst_part2(&mut carrier, &mut map);
        }

        assert_eq!(carrier.infections, 2_511_944);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let grid = aoc::read_grid_from_file::<Node>("input/day22.txt").unwrap();

        assert_eq!(part1(&grid), 5_404);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let grid = aoc::read_grid_from_file::<Node>("input/day22.txt").unwrap();

        assert_eq!(part2(&grid), 2_511_672);
    }
}
