use log::debug;
use std::str::FromStr;
use std::collections::{HashMap, VecDeque};

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Value {
    Immediate { value: i64 },
    Register { register: char }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Value {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.parse::<i64>() {
            Ok(value) => Ok(Value::Immediate { value }),
            _ => Ok(Value::Register { register: s.parse().unwrap() })
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Instruction {
    Snd { op1: Value },
    Set { op1: Value, op2: Value },
    Add { op1: Value, op2: Value },
    Mul { op1: Value, op2: Value },
    Mod { op1: Value, op2: Value },
    Rcv { op1: Value },
    Jgz { op1: Value, op2: Value }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Instruction {
    pub fn is_receive(&self) -> bool {
        matches!(self, Instruction::Rcv { .. })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        match parts[0] {
            "snd" => Ok(Instruction::Snd {
                op1: parts[1].parse().unwrap()
            }),
            "set" => Ok(Instruction::Set {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "add" => Ok(Instruction::Add {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "mul" => Ok(Instruction::Mul {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "mod" => Ok(Instruction::Mod {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "rcv" => Ok(Instruction::Rcv {
                op1: parts[1].parse().unwrap()
            }),
            "jgz" => Ok(Instruction::Jgz {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            _ => Err(aoc::Error::new(&format!("Unknown opcode {}", parts[0])))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub enum DuetState {
    Halt,
    Receive,
    Send(i64),
    Continue,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
enum ExecResult {
    Send(i64),
    Receive,
    Jump(usize)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug)]
pub struct DuetVM {
    registers: HashMap<char, i64>,
    ip: usize,
    program: Vec<Instruction>,
    output: Vec<i64>,
    input: VecDeque<i64>,
    day18_part1: bool,
    program_id: Option<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl DuetVM {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[Instruction]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn day18_part1(&mut self) -> &mut Self {
        self.day18_part1 = true;
        self
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program_id(&mut self, id: i64) -> &mut Self {
        self.program_id = Some(id);
        *self.register('p') = id;
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn input(&mut self, value: i64) {
        self.input.push_back(value);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn terminal_state(&mut self) -> bool {
        self.ip >= self.program.len() || (self.program[self.ip].is_receive() && self.input.is_empty())
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn output(&self) -> &[i64] {
        &self.output
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(&mut self) -> DuetState {
        if self.ip >= self.program.len() {
            return DuetState::Halt;
        }

        if !self.day18_part1 && self.program[self.ip].is_receive() && self.input.is_empty() {
            return DuetState::Receive;
        }

        debug!("Duet {:?}: Before: ip: {} {:?} {:?}", self.program_id, self.ip, self.registers, self.program[self.ip]);

        let result = self.execute(self.program[self.ip]);

        debug!("Duet {:?}: After: ip: {} {:?} {:?}", self.program_id, self.ip, self.registers, self.program[self.ip]);

        self.ip = self.ip.overflowing_add(if let Some(ExecResult::Jump(jump)) = result { jump } else { 1 }).0;

        match result {
            Some(ExecResult::Send(output)) => {
                self.output.push(output);
                DuetState::Send(output)
            },
            Some(ExecResult::Receive) => DuetState::Receive,
            _ => DuetState::Continue
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[allow(dead_code)]
    fn peek(&mut self, register: char) -> Option<&i64> {
        self.registers.get(&register)
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn register(&mut self, register: char) -> &mut i64 {
        self.registers.entry(register).or_insert(0)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fetch(&mut self, value: Value) -> i64 {
        match value {
            Value::Immediate { value} => value,
            Value::Register { register } => *self.register(register)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: Instruction) -> Option<ExecResult> {
        match instruction {
            Instruction::Snd { op1 } => Some(ExecResult::Send(self.fetch(op1))),
            Instruction::Set { op1, op2 } => {
                match op1 {
                    Value::Register { register} => *self.register(register) = self.fetch(op2),
                    _ => unreachable!()
                };
                None
            },
            Instruction::Add { op1, op2 } => {
                match op1 {
                    Value::Register { register} => *self.register(register) += self.fetch(op2),
                    _ => unreachable!()
                };
                None
            },
            Instruction::Mul { op1, op2 } => {
                match op1 {
                    Value::Register { register} => *self.register(register) *= self.fetch(op2),
                    _ => unreachable!()
                };
                None
            },
            Instruction::Mod { op1, op2 } => {
                match op1 {
                    Value::Register { register} => *self.register(register) %= self.fetch(op2),
                    _ => unreachable!()
                };
                None
            },
            Instruction::Rcv { op1 } => {
                if self.day18_part1 {
                    if self.fetch(op1) != 0 {
                        Some(ExecResult::Receive)
                    } else {
                        None
                    }
                } else {
                    match op1 {
                        Value::Register { register} => *self.register(register) = self.input.pop_front().unwrap(),
                        _ => unreachable!()
                    };
                    None
                }
            },
            Instruction::Jgz { op1, op2 } => {
                if self.fetch(op1) > 0 {
                    Some(ExecResult::Jump(self.fetch(op2) as usize))
                } else {
                    None
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod duet_day18 {
    use aoc::read_list_from_string;
    use crate::duet::{DuetState, DuetVM, Instruction, Value};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "set a 1
             add a 2
             mul a a
             mod a 5
             snd a
             set a 0
             rcv a
             jgz a -1
             set a 1
             jgz a -2";

        let instructions = read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions.len(), 10);
        assert_eq!(instructions[0], Instruction::Set { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: 1 } });
        assert_eq!(instructions[1], Instruction::Add { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: 2 } });
        assert_eq!(instructions[2], Instruction::Mul { op1: Value::Register { register: 'a' }, op2: Value::Register { register: 'a' } });
        assert_eq!(instructions[3], Instruction::Mod { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: 5 } });
        assert_eq!(instructions[4], Instruction::Snd { op1: Value::Register { register: 'a' } });
        assert_eq!(instructions[5], Instruction::Set { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: 0 } });
        assert_eq!(instructions[6], Instruction::Rcv { op1: Value::Register { register: 'a' } });
        assert_eq!(instructions[7], Instruction::Jgz { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: -1 } });
        assert_eq!(instructions[8], Instruction::Set { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: 1 } });
        assert_eq!(instructions[9], Instruction::Jgz { op1: Value::Register { register: 'a' }, op2: Value::Immediate { value: -2 } });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn day18_example() {
        let input =
            "set a 1
             add a 2
             mul a a
             mod a 5
             snd a
             set a 0
             rcv a
             jgz a -1
             set a 1
             jgz a -2";

        let instructions = read_list_from_string::<Instruction>(input, "\n").unwrap();
        let mut duet = DuetVM::default();
        duet.program(&instructions).day18_part1();

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&1));

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&3));

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&9));

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&4));

        assert_eq!(duet.step(), DuetState::Send(4));
        assert_eq!(duet.peek('a'), Some(&4));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&0));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&0));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&0));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&1));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&1));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Continue);
        assert_eq!(duet.peek('a'), Some(&1));
        assert_eq!(duet.output(), &[4]);

        assert_eq!(duet.step(), DuetState::Receive);
        assert_eq!(duet.peek('a'), Some(&1));
        assert_eq!(duet.output(), &[4]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn day18_example_2() {
        let input =
            "snd 1
             snd 2
             snd p
             rcv a
             rcv b
             rcv c
             rcv d";

        let instructions = read_list_from_string::<Instruction>(input, "\n").unwrap();

        let mut duet0 = DuetVM::default();
        let mut duet1 = DuetVM::default();

        duet0.program(&instructions).program_id(0);
        duet1.program(&instructions).program_id(1);


        assert_eq!(duet0.step(), DuetState::Send(1));
        assert_eq!(duet0.output(), &[1]);

        assert_eq!(duet0.step(), DuetState::Send(2));
        assert_eq!(duet0.output(), &[1, 2]);

        assert_eq!(duet0.step(), DuetState::Send(0));
        assert_eq!(duet0.output(), &[1, 2, 0]);

        assert_eq!(duet1.step(), DuetState::Send(1));
        assert_eq!(duet1.output(), &[1]);

        assert_eq!(duet1.step(), DuetState::Send(2));
        assert_eq!(duet1.output(), &[1, 2]);

        assert_eq!(duet1.step(), DuetState::Send(1));
        assert_eq!(duet1.output(), &[1, 2, 1]);

        assert_eq!(duet0.step(), DuetState::Receive);
        assert_eq!(duet1.step(), DuetState::Receive);

        duet0.output().iter().for_each(|&output| duet1.input(output));
        duet1.output().iter().for_each(|&output| duet0.input(output));

        assert_eq!(duet0.step(), DuetState::Continue);
        assert_eq!(duet0.input, vec![2, 1]);
        assert_eq!(duet0.peek('a'), Some(&1));

        assert_eq!(duet1.step(), DuetState::Continue);
        assert_eq!(duet1.input, vec![2, 0]);
        assert_eq!(duet1.peek('a'), Some(&1));

        assert_eq!(duet0.step(), DuetState::Continue);
        assert_eq!(duet0.input, vec![1]);
        assert_eq!(duet0.peek('b'), Some(&2));

        assert_eq!(duet1.step(), DuetState::Continue);
        assert_eq!(duet1.input, vec![0]);
        assert_eq!(duet1.peek('b'), Some(&2));

        assert_eq!(duet0.step(), DuetState::Continue);
        assert_eq!(duet0.input, vec![]);
        assert_eq!(duet0.peek('c'), Some(&1));

        assert_eq!(duet1.step(), DuetState::Continue);
        assert_eq!(duet1.input, vec![]);
        assert_eq!(duet1.peek('c'), Some(&0));

        assert_eq!(duet0.step(), DuetState::Receive);

        assert_eq!(duet1.step(), DuetState::Receive);

    }
}
