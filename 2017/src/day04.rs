use aoc::{self, Day, Result};
use std::collections::HashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day04 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Vec<String>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day04 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day04::Part1;
    type Part2 = crate::day04::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_lists_from_string(input, " ")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter()
            .map(|p| (p.len(), p))
            .map(|(l, p)| {
              let mut set = HashSet::new();
              p.iter().for_each(|w| { set.insert(w); });

              (l, set.len())
            })
            .filter(|(l, s)| l == s)
            .count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .map(|p| (p.len(), p))
            .map(|(l, p)| {
                  let mut set = HashSet::new();
                  for w in p {
                      let mut chars = w.chars().collect::<Vec<_>>();
                      chars.sort();
                      let s: String = chars.into_iter().collect();
                      set.insert(s);
                  }

                  (l, set)
            })
            .filter(|(l, set)| *l == set.len())
            .count();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day04::Day04;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(455));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(186));
    }
}
