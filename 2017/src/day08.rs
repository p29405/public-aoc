use aoc::{self, Day, Result};
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut registers:  HashMap<String, i64> = HashMap::new();
        let mut max = 0;
        for i in self.input.iter() {
            if i.condition.check(&registers) {
                let register = i.register.clone();
                let entry = registers.entry(register).or_insert(0);
                if i.increase {
                    *entry += i.amount;
                } else {
                    *entry -= i.amount;
                }

                if *entry > max {
                    max = *entry;
                }
            }
        }
        self.part1 = Some(registers.iter().map(|(_, value)| *value).max().unwrap());
        self.part2 = Some(max);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        if self.part2.is_none() {
            self.run_part1();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub enum Compare {
    Equal,
    NotEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Compare {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "==" => Ok(Compare::Equal),
            "!=" => Ok(Compare::NotEqual),
            ">" => Ok(Compare::Greater),
            ">=" => Ok(Compare::GreaterEqual),
            "<" => Ok(Compare::Less),
            "<=" => Ok(Compare::LessEqual),
            _ => Err(aoc::Error::new(&format!("Unknown operation {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub struct Condition {
    register: String,
    compare: Compare,
    amount: i64,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Condition {
    fn check(&self, registers: &HashMap<String, i64>) -> bool {
        let value = *registers.get(&self.register).unwrap_or(&0);
        match self.compare {
            Compare::Equal => value == self.amount,
            Compare::NotEqual => value != self.amount,
            Compare::Greater => value > self.amount,
            Compare::GreaterEqual => value >= self.amount,
            Compare::Less => value < self.amount,
            Compare::LessEqual => value <= self.amount,
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Eq, PartialEq)]
pub struct Instruction {
    register: String,
    increase: bool,
    amount: i64,
    condition: Condition
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        Ok(Self {
            register: parts[0].into(),
            increase: parts[1] == "inc",
            amount: parts[2].parse().unwrap(),
            condition: Condition {
                register: parts[4].into(),
                compare: parts[5].parse()?,
                amount: parts[6].parse().unwrap()
            }
        })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::Day08;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "b inc 5 if a > 1
             a inc 1 if b < 5
             c dec -10 if a >= 1
             c inc -20 if c == 10";

        let mut day = Day08::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1));
        assert_eq!(day.part2_answer(), Some(10));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5102));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6056));
    }
}