use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<char>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day09::Part1;
    type Part2 = crate::day09::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim().chars().collect();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(score(&self.input, 0, 0).0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(count_garbage(&self.input));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

fn count_garbage(stream: &[char]) -> i64 {
    let mut index = 0;
    let mut count = 0;
    let mut garbage = false;

    while index < stream.len() {
        match stream[index] {
            '<' => if garbage { count += 1; } else { garbage = true; },
            '>' => garbage = false,
            '!' => index += 1,
            _   => if garbage { count += 1; }
        };

        index += 1;
    }

    count
}

fn score(stream: &[char], start: usize, depth: i64) -> (i64, usize) {
    let mut index = start;
    let mut count = 0;
    let mut garbage = false;

    while index < stream.len() {
        match stream[index] {
            '<' => garbage = true,
            '>' => garbage = false,
            '{' => if !garbage {
                let result = score(stream, index + 1, depth + 1);
                count += result.0;
                index = result.1
            },
            '}' => if !garbage { return (count + depth, index); },
            '!' => index += 1,
            _   => ()
        };

        index += 1;
    }

    return (count, stream.len());

}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("{}", 1),
                ("{{{}}}", 6),
                ("{{},{}}", 5),
                ("{{{},{},{{}}}}", 16),
                ("{<a>,<a>,<a>,<a>}", 1),
                ("{{<ab>},{<ab>},{<ab>},{<ab>}}", 9),
                ("{{<!!>},{<!!>},{<!!>},{<!!>}}", 9),
                ("{{<a!>},{<a!>},{<a!>},{<ab>}}", 3)
            ];

        for (input, answer) in inputs {
            let mut day = Day09::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "For input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(12396));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("<>", 0),
                ("<random characters>", 17),
                ("<<<<>", 3),
                ("<{!>}>", 2),
                ("<!!>", 0),
                ("<!!!>>", 0),
                ("<{o\"i!a,<{i<a>", 10),
            ];

        for (input, answer) in inputs {
            let mut day = Day09::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(answer), "For input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6346));
    }
}