use aoc::{self, Grid, Pixel, Result};
use std::fmt;
use std::str::FromStr;
use std::fmt::Display;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let rules = aoc::read_list("\n")?;
    let expanded_rules = expand_rules(&rules);
    let initial_grid =
        ".#.
         ..#
         ###";

    let grid = aoc::read_grid_from_string::<Pixel>(initial_grid).unwrap();
    aoc::setup_finished();

    aoc::answer("Part 1", &part1(&grid, &expanded_rules))?;
    aoc::answer("Part 2", &part2(&grid, &expanded_rules))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(grid: &Grid<Pixel>, rules: &[Rule]) -> usize {
    let mut target = grid.clone();
    for _ in 0..5 {
        target = apply(&target, rules);
    }

    target.elements.iter().filter(|&p| p == &Pixel::On).count()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(grid: &Grid<Pixel>,rules: &[Rule]) -> usize {
    let mut target = grid.clone();
    for _ in 0..18 {
        target = apply(&target, rules);
    }

    target.elements.iter().filter(|&p| p == &Pixel::On).count()

}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn expand_rules(rules: &[Rule]) -> Vec<Rule> {
    let mut expanded_rules = vec![];

    for rule in rules {
        let mut current = rule.clone();
        for _ in 0..4 {
            if find_rule(&current.pattern, &expanded_rules).is_none() {
                expanded_rules.push(current.clone());
            }
            current.pattern = current.pattern.rotate_right();
        }

        current.pattern = current.pattern.flip_horizontal();

        for _ in 0..4 {
            if find_rule(&current.pattern, &expanded_rules).is_none() {
                expanded_rules.push(current.clone());
            }
            current.pattern = current.pattern.rotate_right();
        }
    }

    expanded_rules
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn split(grid: &Grid<Pixel>) -> Vec<Grid<Pixel>> {
    if grid.columns % 2 == 0 && grid.rows % 2 == 0 {
        grid.split(2, 2)
    } else {
        grid.split(3, 3)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn find_rule<'a>(grid: &Grid<Pixel>, rules: &'a [Rule]) -> Option<&'a Rule> {
    rules.iter().find(|&r| r.pattern == *grid)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn apply(grid: &Grid<Pixel>, rules: &[Rule]) -> Grid<Pixel> {
    let grids = split(grid);
    let mut next_grids = vec![];
    let num_grids = grids.len() as f64;

    for g in grids {
        let rule = find_rule(&g, rules).unwrap();

        next_grids.push(rule.result.clone());
    }

    Grid::combine(&next_grids, num_grids.sqrt() as i64, num_grids.sqrt() as i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone)]
pub struct Rule {
    pattern: Grid<Pixel>,
    result: Grid<Pixel>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Rule {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split("=>").collect::<Vec<&str>>();

        let pattern_lines = parts[0].trim().split('/').collect::<Vec<&str>>();
        let pattern = aoc::read_grid_from_string::<Pixel>(&pattern_lines.join("\n")).unwrap();

        let result_lines = parts[1].trim().split('/').collect::<Vec<&str>>();
        let result = aoc::read_grid_from_string::<Pixel>(&result_lines.join("\n")).unwrap();

        Ok(Rule { pattern, result })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Rule {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", self.pattern)?;
        writeln!(f)?;
        writeln!(f, "{}", self.result)?;
        Ok(())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::{part1, part2, Rule, Pixel, apply, expand_rules};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "../.# => ##./#../...
             .#./..#/### => #..#/..../..../#..#";

        let rules = aoc::read_list_from_string::<Rule>(input, "\n").unwrap();

        rules.iter().for_each(|rule| println!("{}", rule));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let initial_grid =
            ".#.
             ..#
             ###";

        let grid = aoc::read_grid_from_string::<Pixel>(initial_grid).unwrap();

        let input =
            "../.# => ##./#../...
             .#./..#/### => #..#/..../..../#..#";

        let rules = aoc::read_list_from_string::<Rule>(input, "\n").unwrap();
        let expanded_rules = expand_rules(&rules);

        let first = apply(&grid, &expanded_rules);

        let second = apply(&first, &expanded_rules);

        let lights_on = second.elements.iter().filter(|&p| p == &Pixel::On).count();

        assert_eq!(lights_on, 12)

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let initial_grid =
            ".#.
             ..#
             ###";

        let grid = aoc::read_grid_from_string::<Pixel>(initial_grid).unwrap();

        let rules = aoc::read_list_from_file::<Rule>("input/day21.txt", "\n").unwrap();
        let expanded_rules = expand_rules(&rules);

        assert_eq!(part1(&grid, &expanded_rules), 139);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let initial_grid =
            ".#.
             ..#
             ###";

        let grid = aoc::read_grid_from_string::<Pixel>(initial_grid).unwrap();

        let rules = aoc::read_list_from_file::<Rule>("input/day21.txt", "\n").unwrap();
        let expanded_rules = expand_rules(&rules);

        assert_eq!(part2(&grid, &expanded_rules), 1_857_134);
    }
}
