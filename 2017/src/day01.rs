use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = u32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day01 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<u32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day01 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day01::Part1;
    type Part2 = crate::day01::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut sum = 0;
        for i in 0..self.input.len() {
            let next = (i + 1) % self.input.len();
            if self.input[i] == self.input[next] {
                sum += self.input[i];
            }
        }
        
        self.part1 = Some(sum);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut sum = 0;
        for i in 0..self.input.len() {
            let next = (i + self.input.len() / 2) % self.input.len();
            if self.input[i] == self.input[next] {
                sum += self.input[i];
            }
        }

        self.part2 = Some(sum);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day01::Day01;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs: [(&str, u32); 4] = [
            ("1122", 3),
            ("1111", 4),
            ("1234", 0),
            ("91212129", 9)
        ];

        for (input, answer) in inputs.iter() {
            let mut day = Day01::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "For input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs: [(&str, u32); 5] = [
            ("1212", 6),
            ("1221", 0),
            ("123425", 4),
            ("123123", 12),
            ("12131415", 4)
        ];

        for (input, answer) in inputs.iter() {
            let mut day = Day01::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer), "For input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1044));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1054));
    }
}