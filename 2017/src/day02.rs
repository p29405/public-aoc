use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Vec<i64>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_lists_from_string(input, "\t")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().map(|row| { row.iter().max().unwrap() - row.iter().min().unwrap() }).sum();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .map(|row| {
                for i in 0..row.len() {
                    for j in 0..row.len() {
                        if row[i] > row[j] && row[i] % row[j] == 0 {
                            return row[i] / row[j]
                        }
                    }
                }
                panic!("No pair found in {:?}", row);
            })
            .sum();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "5\t1\t9\t5
             7\t5\t3
             2\t4\t6\t8";

        let mut day = Day02::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(18));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(32020));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "5\t9\t2\t8
             9\t4\t7\t3
             3\t8\t6\t5";

        let mut day = Day02::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(9));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(236));
    }
}