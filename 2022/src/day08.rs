use std::fmt;
use aoc::{self, Coord, Day, Direction, Grid, GridElement, Map, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Tree>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.input.coord_iter().map(|c| visible(c, &self.input) as usize).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = self.input.coord_iter().map(|c| score(c, &self.input)).max();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn visible(c: Coord, grid: &Grid<Tree>) -> bool {
    let height = grid.get(&c).0;
    let mut visible = false;
    for d in [Direction::North, Direction::East, Direction::South, Direction::West] {
        let mut check = c.go(d);
        let mut current = true;
        while grid.contains(check) {
            if grid.get(&check).0 >= height {
                current = false;
                break;
            }
            check = check.go(d);
        }
        visible |= current;

        if visible { return visible; }
    }

    visible
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn score(c: Coord, grid: &Grid<Tree>) -> usize {
    let height = grid.get(&c).0;
    let mut score = [0; 4];
    for (i, &d) in [Direction::North, Direction::East, Direction::South, Direction::West].iter().enumerate() {
        let mut check = c.go(d);
        while grid.contains(check) {
            score[i] += 1;
            if grid.get(&check).0 >= height {
                break;
            }
            check = check.go(d);
        }
    }

    score.iter().product()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
pub struct Tree(i64);

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Tree {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Tree {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        Tree(format!("{}", c).parse().unwrap())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::Day08;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "30373
25512
65332
33549
35390";

        let mut day = Day08::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(21));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1679));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "30373
25512
65332
33549
35390";

        let mut day = Day08::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(8));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(536625));
    }
}