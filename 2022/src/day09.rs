use std::str::FromStr;
use fnv::FnvHashSet;
use aoc::{self, Coord, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Move>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut visited: FnvHashSet<Coord> = FnvHashSet::default();
        let mut head = Coord::default();
        let mut tail = Coord::default();
        visited.insert(tail);

        for &m in self.input.iter() {
            for _ in 0..m.distance {
                head.x += m.dx;
                head.y += m.dy;
                if (head.x - tail.x).abs() > 1 || (head.y - tail.y).abs() > 1 {
                    let dx = (head.x - tail.x).signum();
                    let dy = (head.y - tail.y).signum();
                    tail.x += dx;
                    tail.y += dy;
                }
                visited.insert(tail);

            }
        }
        self.part1 = Some(visited.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut visited: FnvHashSet<Coord> = FnvHashSet::default();
        let mut rope = [Coord::default(); 10];
        for &m in self.input.iter() {
            for _ in 0..m.distance {
                rope[0].x += m.dx;
                rope[0].y += m.dy;
                for i in 1..rope.len() {
                    if (rope[i -1].x -  rope[i].x).abs() > 1 || (rope[i -1].y -  rope[i].y).abs() > 1 {
                        let dx = (rope[i -1].x -  rope[i].x).signum();
                        let dy = (rope[i -1].y -  rope[i].y).signum();
                        rope[i].x += dx;
                        rope[i].y += dy;
                    }
                }
                visited.insert(rope[9]);
            }
        }

        self.part2 = Some(visited.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug,)]
pub struct Move {
    pub dx: i64,
    pub dy: i64,
    pub distance: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Move {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');
        let direction = parts.next().unwrap();
        let distance = parts.next().unwrap().parse().unwrap();

        let dx;
        let dy;
        match direction {
            "U" => { dx = 0; dy = 1; },
            "R" => { dx = 1; dy = 0; },
            "D" => { dx = 0; dy = -1; },
            "L" => { dx = -1; dy = 0; },
            _ => panic!("Unknown direction: {}", direction)
        };

        Ok(Self { dx, dy, distance })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";

        let mut day = Day09::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(13));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5619));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";

        let mut day = Day09::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(36));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2376));
    }
}