use std::cmp::Ordering;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<(Vec<Token>, Vec<Token>)>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input: Vec<(Vec<Token>, Vec<Token>)> = input.split("\n\n").map(|p| {
            let mut pairs = p.split('\n');
            (tokenize(pairs.next().unwrap()), tokenize(pairs.next().unwrap()))
        }).collect();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().enumerate()
            .filter(|(_, (left, right))| compare(left, right) == Ordering::Less)
            .fold(0, |accum, (i, _)| accum + i + 1);
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let locator1 = tokenize("[[2]]");
        let locator2 = tokenize("[[6]]");

        let mut index1 = 1;
        let mut index2 = 2;

        for (left, right) in self.input.iter() {
            if compare(left, &locator1) == Ordering::Less  {
                index1 += 1;
                index2 += 1;
            } else if compare(left, & locator2) == Ordering::Less {
                index2 += 1;
            }

            if compare(right, &locator1) == Ordering::Less  {
                index1 += 1;
                index2 += 1;
            } else if compare(right, & locator2) == Ordering::Less {
                index2 += 1;
            }
        }

        self.part2 = Some(index1 * index2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Token {
    Open,
    Close,
    Number(u8)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn tokenize(s: &str) -> Vec<Token> {
    let chars = s.chars().collect::<Vec<char>>();

    let mut i = 0;
    let mut tokens = vec![];
    tokens.reserve(chars.len());
    while i < chars.len() {
        match chars[i] {
            '[' => tokens.push(Token::Open),
            ']' => tokens.push(Token::Close),
            ',' => (),
            c =>
                if c.is_ascii_digit() {
                    if chars[i + 1].is_ascii_digit() {
                        let tmp = format!("{}{}", c, chars[i + 1]);
                        tokens.push(Token::Number(tmp.parse().unwrap()));
                        i += 1;
                    } else {
                        let tmp = format!("{}", c);
                        tokens.push(Token::Number(tmp.parse().unwrap()));
                    }
                } else {
                    panic!("Unknown char {}", c);
                }
        };

        i += 1;
    }
    tokens
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn compare(left: &[Token], right: &[Token]) -> Ordering {
    compare_token(left, right, 0, 0)
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn compare_token(left: &[Token], right: &[Token], i: usize, j: usize) -> Ordering {
    if i >= left.len() && j >= right.len() {
        return Ordering::Equal;
    }
    if i >= left.len() {
        return Ordering::Less;
    }
    if j >= right.len() {
        return Ordering::Greater;
    }

    match (left[i], right[j]) {
        (l, r)  if l == r => compare_token(left, right, i + 1, j + 1),
        (_, Token::Close) => Ordering::Greater,
        (Token::Close, _) => Ordering::Less,
        (Token::Number(l), Token::Number(r)) if l < r =>  Ordering::Less,
        (Token::Number(l), Token::Number(r)) if l > r =>  Ordering::Greater,
        (l@Token::Number(_), Token::Open) =>  compare_token(&[l, Token::Close], right, 0, j + 1),
        (Token::Open, r@Token::Number(_)) =>  compare_token(left, &[r, Token::Close], i + 1, 0),
        (l, r) => unreachable!("Unhandled {:?} vs {:?}", l, r)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use std::cmp::Ordering;
    use aoc::Day;
    use crate::day13::{compare, Day13, tokenize};

    const TEST_INPUT: &str = "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_compare() {
        let pairs = [
            (("[1,1,3,1,1]", "[1,1,5,1,1]"), Ordering::Less),
            (("[[1],[2,3,4]]", "[[1],4]"), Ordering::Less),
            (("[9]", "[[8,7,6]]"), Ordering::Greater),
            (("[[4,4],4,4]", "[[4,4],4,4,4]"), Ordering::Less),
            (("[1,[2,[3,[4,[5,6,7]]]],8,9]", "[1,[2,[3,[4,[5,6,0]]]],8,9]"), Ordering::Greater),
        ];

        for ((left_str, right_str), order) in pairs {
            println!("Test {} vs {}", left_str, right_str);
            let left = tokenize(left_str);
            let right = tokenize(right_str);

            assert_eq!(compare(&left, &right), order, "{} vs {}", left_str, right_str);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {

        let mut day = Day13::setup(TEST_INPUT).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(13));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(6086));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let mut day = Day13::setup(TEST_INPUT).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(140));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(27_930));
    }
}