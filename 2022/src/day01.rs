use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day01 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day01 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day01::Part1;
    type Part2 = crate::day01::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let supplies: Vec<String> = aoc::read_list_from_string(input, "\n\n")?;
        let mut input: Vec<i64> = vec!();
        input.reserve(supplies.len());
        for s in supplies {
            input.push(aoc::read_list_from_string::<i64>(&s, "\n")?.iter().sum());
        }

        input.sort();
        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = self.input.last().copied();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(self.input.iter().rev().take(3).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day01::Day01;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000";

        let mut day = Day01::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(24000));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(71506));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000";

        let mut day = Day01::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(45000));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(209603));
    }
}