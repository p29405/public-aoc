use std::fmt;
use aoc::{self, Coord, Day, find_paths, Grid, GridElement, Map, Path, Result};
use crate::day12::Square::{Elevation, End, Start};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day12 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Square>,
    pub start: Coord,
    pub end: Coord
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day12 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;
        let start = input.coord_iter().find(|c| input.get(c) == Start).unwrap();
        let end = input.coord_iter().find(|c| input.get(c) == End).unwrap();

        Ok(Self { part1: None, part2: None, input, start, end})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = shortest_path(&[self.start], self.end, &self.input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let starts = self.input.coord_iter().filter(|c| self.input.get(c).elevation() == 0).collect::<Vec<_>>();

        self.part2 = shortest_path(&starts, self.end, &self.input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn shortest_path(start: &[Coord], end: Coord, grid: &Grid<Square>) -> Option<usize> {
    let path = find_paths(start, &[end], grid.columns, grid.rows,
                          |from, to| grid.contains(*to) && grid.get(to).elevation() <= grid.get(from).elevation() + 1);

    match path.get(&end) {
        Path::Distance(i) => Some(i),
        _ => None
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Elevation(i64),
    Start,
    End
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    pub fn elevation(&self) -> i64 {
        match self {
            Elevation(e) => *e,
            Start => 0,
            End => 25
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Elevation(0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Start => write!(f, "S |"),
            End => write!(f, "E |"),
            Elevation(i) => write!(f, "{:02}|", i)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            'S' => Start,
            'E' => End,
            c => Elevation(c as i64 - 'a' as i64)
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day12::Day12;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "Sabqponm
             abcryxxl
             accszExk
             acctuvwj
             abdefghi";

        let mut day = Day12::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(31));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(391));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "Sabqponm
             abcryxxl
             accszExk
             acctuvwj
             abdefghi";

        let mut day = Day12::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(29));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(386));
    }
}