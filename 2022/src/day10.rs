use std::str::FromStr;
use aoc::{self, Day, Grid, Pixel, Result, to_string};
use aoc::AocFontType::Aoc2016;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Command>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut x = 1;
        let mut cycles = 1;
        let mut interesting = 20;
        let mut answer = 0;

        for &command in self.input.iter() {
            match command {
                Command::Noop => {
                    cycles += 1;
                },
                Command::Addx(v) => {
                    cycles += 1;
                    if cycles == interesting {
                        answer += cycles * x;
                        interesting += 40;
                    }
                    cycles += 1;
                    x += v;
                }
            };
            if cycles == interesting {
                answer += cycles * x;
                interesting += 40;
            }
        }
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let grid = crt(&self.input);
        self.part2 = to_string(&grid, Pixel::On, Aoc2016);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn crt(commands: &[Command]) -> Grid<Pixel> {
    let mut x = 1;
    let mut cursor = 0;
    let mut grid: Grid<Pixel> = Grid::new(40, 6);

    for &command in commands {
        match command {
            Command::Noop => {
                if (cursor % 40)  >= x - 1 && (cursor % 40)  <= x + 1 {
                    grid.elements[cursor as usize] = Pixel::On;
                }
                cursor += 1;
                if cursor == 240 { break; }
            },
            Command::Addx(v) => {
                if (cursor % 40)  >= x - 1 && (cursor % 40)  <= x + 1 {
                    grid.elements[cursor as usize] = Pixel::On;
                }
                cursor += 1;
                if cursor == 240 { break; }
                if (cursor % 40)  >= x - 1 && (cursor % 40)  <= x + 1 {
                    grid.elements[cursor as usize] = Pixel::On;
                }
                cursor += 1;
                if cursor == 240 { break; }
                x += v;
            }
        };
    }
    grid
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug,)]
pub enum Command {
    Noop,
    Addx(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Command {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');

        let command = match parts.next().unwrap() {
            "noop" => Self::Noop,
            "addx" => Self::Addx(parts.next().unwrap().parse().unwrap()),
            c => panic!("Unknown command: {}", c)
        };

        Ok(command)
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day, Grid, Pixel};
    use crate::day10::{crt, Day10};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = aoc::get_input_from_file("input/day10_example.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(13_140));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(13_720));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = aoc::get_input_from_file("input/day10_example.txt");
        let expected_output =
            "##..##..##..##..##..##..##..##..##..##..
             ###...###...###...###...###...###...###.
             ####....####....####....####....####....
             #####.....#####.....#####.....#####.....
             ######......######......######......####
             #######.......#######.......#######.....";

        let day = Day10::setup(&input).unwrap();
        let output = crt(&day.input);
        let expected: Grid<Pixel> = aoc::read_grid_from_string(expected_output).unwrap();
        assert_eq!(output, expected);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("FBURHZCH".to_owned()));
    }
}