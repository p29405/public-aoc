use std::collections::VecDeque;
use std::str::FromStr;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day05 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub stacks: Stacks,
    pub moves: Vec<Move>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day05 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day05::Part1;
    type Part2 = crate::day05::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<String>(input, "\n\n")?;
        let stacks = input[0].parse()?;
        let moves = aoc::read_list_from_string(&input[1], "\n")?;

        Ok(Self { part1: None, part2: None, stacks, moves})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut stacks = Stacks::new(&self.stacks);

        for m in self.moves.iter() {
            stacks.move_crates(m.quantity, m.from, m.to);
        }

        let mut answer = String::new();
        for s in stacks.stacks {
            if let Some(c) = s.back() {
                answer = format!("{}{}", answer, c);
            }
        }
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> {
        self.part1.clone()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut stacks = Stacks::new(&self.stacks);

        for m in self.moves.iter() {
            stacks.move_crates_p2(m.quantity, m.from, m.to);
        }

        let mut answer = String::new();
        for s in stacks.stacks {
            if let Some(c) = s.back() {
                answer = format!("{}{}", answer, c);
            }
        }

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> {
        self.part2.clone()
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug,)]
pub struct Stacks {
    pub stacks: Vec<VecDeque<char>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Stacks {
    fn new(other: &Self) -> Self {
        let mut stacks = vec![];
        for stack in other.stacks.iter() {
            stacks.push(stack.clone());
        }
        Self { stacks }
    }

    fn move_crates(&mut self, quantity: u32, from: usize, to: usize) {
        for _ in 0..quantity {
            let c = self.stacks[from].pop_back().unwrap();
            self.stacks[to].push_back(c);
        }
    }
    fn move_crates_p2(&mut self, quantity: u32, from: usize, to: usize) {
        let mut crates = vec![];
        for _ in 0..quantity {
            let c = self.stacks[from].pop_back().unwrap();
            crates.push(c);
        }

        for &c in crates.iter().rev() {
            self.stacks[to].push_back(c);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Stacks {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let lines = s.split('\n');
        let mut stacks: Vec<VecDeque<char>> = vec![];
        for line in lines {
            let mut parts = line.trim().split(' ');
            // skip
            parts.next();

            stacks.push(parts.map(|p| p.chars().nth(1).unwrap()).collect());
        }

        Ok(Self { stacks })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug,)]
pub struct Move {
    pub quantity: u32,
    pub from: usize,
    pub to: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Move {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');
        parts.next();
        let quantity = parts.next().unwrap().parse().unwrap();
        parts.next();
        let from: usize = parts.next().unwrap().parse().unwrap();
        parts.next();
        let to: usize = parts.next().unwrap().parse().unwrap();

        Ok(Self { quantity, from: from - 1, to: to - 1})
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day05::Day05;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "
             1 [Z] [N]
             2 [M] [C] [D]
             3 [P]

             move 1 from 2 to 1
             move 3 from 1 to 3
             move 2 from 2 to 1
             move 1 from 1 to 2";

        let mut day = Day05::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("CMZ".to_owned()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("DHBJQJCCW".to_owned()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "
             1 [Z] [N]
             2 [M] [C] [D]
             3 [P]

             move 1 from 2 to 1
             move 3 from 1 to 3
             move 2 from 2 to 1
             move 1 from 1 to 2";

        let mut day = Day05::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("MCD".to_owned()));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("WJVRLSJJT".to_owned()));
    }
}