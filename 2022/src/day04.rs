use std::str::FromStr;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day04 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Sections>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day04 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().filter(|s| s.enclosing()).count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter().filter(|s| s.overlap()).count();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug,)]
pub struct Sections {
    pub first: (i64, i64),
    pub second: (i64, i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Sections {
    fn enclosing(&self) -> bool {
        self.first.0 <= self.second.0 && self.first.1 >= self.second.1 ||
            self.second.0 <= self.first.0 && self.second.1 >= self.first.1
    }

    fn overlap(&self) -> bool {
        self.first.0 <= self.second.1 && self.first.1 >= self.second.0
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Sections {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut items = s.split(',');

        let first_section = items.next().unwrap();
        let second_section = items.next().unwrap();

        items = first_section.split('-');
        let first = (items.next().unwrap().parse::<i64>().unwrap(), items.next().unwrap().parse::<i64>().unwrap());
        items = second_section.split('-');
        let second = (items.next().unwrap().parse::<i64>().unwrap(), items.next().unwrap().parse::<i64>().unwrap());

        Ok(Self { first, second })
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day04::Day04;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "2-4,6-8
             2-3,4-5
             5-7,7-9
             2-8,3-7
             6-6,4-6
             2-6,4-8";

        let mut day = Day04::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(471));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "2-4,6-8
             2-3,4-5
             5-7,7-9
             2-8,3-7
             6-6,4-6
             2-6,4-8";

        let mut day = Day04::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(888));
    }
}