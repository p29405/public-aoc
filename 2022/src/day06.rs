use std::collections::VecDeque;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.chars().map(|c| c as usize - 'a' as usize).collect();
        Ok(Self { part1: None, part2: None, input })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut chars = self.input.iter();
        let mut packet: VecDeque<usize> = VecDeque::new();
        let mut uniqueness = [0; 26];
        (0..4).for_each(|_| {
            let &c = chars.next().unwrap();
            uniqueness[c] += 1;
            packet.push_back(c);
        });

        let mut answer = 4;
        for &c in chars {
            if uniq(&uniqueness) {
                break;
            }
            uniqueness[c] += 1;
            packet.push_back(c);
            uniqueness[packet.pop_front().unwrap()] -= 1;
            answer += 1
        }

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut chars = self.input.iter();
        let mut packet: VecDeque<usize> = VecDeque::new();
        let mut uniqueness = [0; 26];
        (0..14).for_each(|_| {
            let &c = chars.next().unwrap();
            uniqueness[c] += 1;
            packet.push_back(c);
        });

        let mut answer = 14;
        for &c in chars {
            if uniq(&uniqueness) {
                break;
            }
            uniqueness[c] += 1;
            packet.push_back(c);
            uniqueness[packet.pop_front().unwrap()] -= 1;
            answer += 1
        }

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn uniq(packet: &[i32]) -> bool {
    packet.iter().all(|&p| p < 2)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::Day06;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";

        let mut day = Day06::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(7));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";

        let mut day = Day06::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(19));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1042));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2980));
    }
}