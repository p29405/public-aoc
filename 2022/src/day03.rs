use std::str::FromStr;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Rucksack>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let total = self.input.iter()
            .map(|s| common_items(s.front & s.back))
            .sum();

        self.part1 = Some(total);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.chunks(3)
            .map(|group| group.iter().fold(-1i64, |accum, rucksack| accum & rucksack.items))
            .map(common_items)
            .sum();

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn priority(c: char) -> i64 {
    let offset = if c >= 'a' { 'a' as i64 } else { 'A' as i64 - 26 };
    c as i64 - offset
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn common_items(common: i64) -> i64 {
    (0..52).filter(|i| common | (1 << i) == (1 << i))
        .fold(0i64, |accum, i| accum + i + 1)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug,)]
pub struct Rucksack {
    pub items: i64,
    pub front: i64,
    pub back: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Rucksack {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let items = s.split_at(s.len() / 2);
        let front = items.0.chars().fold(0i64, |accum, c| accum | (1 << priority(c)));
        let back = items.1.chars().fold(0i64, |accum, c| accum | (1 << priority(c)));

        Ok(Self { items: front | back, front, back })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day03::Day03;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "vJrwpWtwJgWrhcsFMMfFFhFp
             jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
             PmmdzqPrVvPwwTWBwg
             wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
             ttgJtRGJQctTZtZT
             CrZsJsPPZsGzwwsLwLmpwMDw";

        let mut day = Day03::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(157));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(7850));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "vJrwpWtwJgWrhcsFMMfFFhFp
             jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
             PmmdzqPrVvPwwTWBwg
             wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
             ttgJtRGJQctTZtZT
             CrZsJsPPZsGzwwsLwLmpwMDw";

        let mut day = Day03::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(70));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2581));
    }
}