use std::fmt;
use aoc::{self, CenteredMap, Coord, Day, GridElement, Map, Result, Velocity};
use crate::day14::Square::{Air, Rock, Sand};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: CenteredMap<Square>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut lines = vec![];
        for line in input.lines() {
            lines.push(line.split(" -> ").map(|c| c.parse::<Coord>().unwrap()).collect::<Vec<Coord>>());
        }
        let mut map: CenteredMap<Square> = CenteredMap::default();
        // set rock formation
        for line in lines.iter() {
            let mut start = line[0];
            for i in 1..line.len() {
                let end = line[i];

                let coords = if start < end {
                    Coord::range_inclusive(start, end)
                } else {
                    Coord::range_inclusive(end, start)
                };

                for coord in coords {
                    map.set(&coord, Rock);
                }

                start = end;
            }
        }
        Ok(Self { part1: None, part2: None, input: map })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut map = self.input.clone();
        // drop sand
        let max = map.lower_right.y;

        let mut amount = 0;
        let drops = [Velocity::new(0, 1), Velocity::new(-1, 1), Velocity::new(1, 1)];

        'outer: loop {
            let mut sand = Coord::new(500, 0);

            while let Some(v) = drops.iter().find(|v| map.get(&sand.next(v)) == Air) {
                sand = sand.next(v);

                if sand.y >= max {
                    break 'outer;
                }
            }
            map.set(&sand, Sand);
            amount += 1;
        }
        self.part1 = Some(amount);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut map = self.input.clone();

        // drop sand
        let max = map.lower_right.y + 2;

        let mut amount = 0;
        let drops = [Velocity::new(0, 1), Velocity::new(-1, 1), Velocity::new(1, 1)];

        'outer: loop {
            let mut sand = Coord::new(500, 0);

            while let Some(v) = drops.iter().find(|v| map.get(&sand.next(v)) == Air) {
                sand = sand.next(v);

                if sand.y == max - 1 {
                    break;
                }

            }
            map.set(&sand, Sand);
            amount += 1;
            if sand ==  Coord::new(500, 0) {
                break 'outer;
            }

        }
        self.part2 = Some(amount);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Rock,
    Air,
    Sand
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Air
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Rock => write!(f, "#"),
            Air => write!(f, "."),
            Sand => write!(f, "o")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::Day14;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9";

        let mut day = Day14::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(24));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(805));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9";

        let mut day = Day14::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(93));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(25_161));
    }
}