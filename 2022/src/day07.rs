use std::collections::HashMap;
use std::str::FromStr;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day07 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Directory
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day07 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.parse()?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.files.values().filter(|&&size| size <= 100_000).sum();

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let unused = 70_000_000 - self.input.files.get("/").unwrap();
        self.part2 = self.input.files.values().filter(|&&size| unused + size >= 30_000_000).copied().min();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug,)]
pub struct Directory {
    pub files: HashMap<String, usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Directory {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Directory {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut directory = Self { files: HashMap::new() };
        let lines = s.lines();
        let mut cwd: Vec<String> = vec!();
        for line in lines {
            if line.starts_with("$ cd") {
                let dir = line.split(' ').nth(2).unwrap();
                if dir == "/" {
                    cwd = vec![ "/".to_owned() ];
                } else if dir == ".." {
                    cwd.pop();
                } else {
                    cwd.push(dir.to_owned());
                }
            } else if !line.starts_with('$') && !line.starts_with("dir") {
                let mut splits = line.split(' ');
                let size: usize = splits.next().unwrap().parse().unwrap();

                for include in 0..cwd.len() {
                    let entry = directory.files.entry(cwd[0..include+1].join("")).or_insert(0);
                    *entry += size
                }
            }
        }

        Ok(directory)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day07::Day07;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";

        let mut day = Day07::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(95437));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1427048));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2940614));
    }
}