use std::str::FromStr;
use aoc::{self, Day, Result};
use crate::day02::Outcome::{Draw, Lose, Win};
use crate::day02::Throw::{Paper, Rock, Scissors};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Round>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.input.iter().map( |r|  r.value_p1()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(self.input.iter().map( |r|  r.value_p2()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Outcome {
    Lose,
    Draw,
    Win
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Outcome {
    pub fn from_char(c: char) -> Self {
        match c {
            'X' => Lose,
            'Y' => Draw,
            'Z' => Win,
            unknown => unreachable!("Illegal character in output {}", unknown)
        }
    }

    pub fn value(&self) -> i64 {
        match self {
            Win => 6,
            Draw => 3,
            Lose => 0
        }
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Throw {
    Rock,
    Paper,
    Scissors
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Throw {
    pub fn from_char(c: char) -> Self {
        match c {
            'A' | 'X' => Rock,
            'B' | 'Y' => Paper,
            'C' | 'Z' => Scissors,
            unknown => unreachable!("Illegal character in output {}", unknown)
        }
    }

    pub fn value(&self) -> i64 {
        match self {
            Rock => 1,
            Paper => 2,
            Scissors => 3
        }
    }

    pub fn outcome(&self, other: Self) -> Outcome {
        match self {
            Rock => match other {
                Rock => Draw,
                Paper =>  Lose,
                Scissors => Win
            },
            Paper => match other {
                Rock => Win,
                Paper =>  Draw,
                Scissors => Lose
            },
            Scissors => match other {
                Rock => Lose,
                Paper =>  Win,
                Scissors => Draw
            },
        }
    }

    pub fn my_move(&self, outcome: Outcome) -> Self {
        match self {
            Rock => match outcome {
                Win => Paper,
                Draw => Rock,
                Lose => Scissors
            },
            Paper => match outcome {
                Win => Scissors,
                Draw => Paper,
                Lose => Rock
            },
            Scissors => match outcome {
                Win => Rock,
                Draw => Scissors,
                Lose => Paper
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Round {
    pub opponent: Throw,
    pub mine: Throw,
    pub desired_outcome: Outcome
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Round {
    fn value_p1(&self) -> i64 {
        self.mine.value() + self.mine.outcome(self.opponent).value()
    }

    fn value_p2(&self) -> i64 {
        self.opponent.my_move(self.desired_outcome).value() + self.desired_outcome.value()
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Round {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.chars();
        let opponent = Throw::from_char(parts.next().unwrap());
        parts.next();
        let second = parts.next().unwrap();
        let mine = Throw::from_char(second);
        let desired_outcome = Outcome::from_char(second);

        Ok(Self { opponent, mine, desired_outcome })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "A Y\nB X\nC Z";

        let mut day = Day02::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(15));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(10994));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "A Y\nB X\nC Z";

        let mut day = Day02::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(12));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(12526));
    }
}