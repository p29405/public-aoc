use std::collections::VecDeque;
use std::str::FromStr;
use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day11 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Monkey>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day11 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut monkeys = self.input.clone();

        for _ in 0..20 {
            round(&mut monkeys, None);
        }
        let mut sorted = monkeys.iter().map(|m| m.inspected).collect::<Vec<_>>();

        sorted.sort();

        self.part1 = Some(sorted.iter().rev().take(2).product());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut monkeys = self.input.clone();

        let modulus: i64 = self.input.iter().map(|m| m.divisible).product();

        for _ in 0..10_000 {
            round(&mut monkeys, Some(modulus));
        }
        let mut sorted = monkeys.iter().map(|m| m.inspected).collect::<Vec<_>>();

        sorted.sort();

        self.part2 = Some(sorted.iter().rev().take(2).product());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn round(monkeys: &mut Vec<Monkey>, modulus: Option<i64>) {
    for i in 0..monkeys.len() {
        while !monkeys[i].items.is_empty() {
            if let Some(item) = monkeys[i].items.pop_front() {
                let mut worry = match monkeys[i].operation {
                    Operation::Square => item * item,
                    Operation::Multiply(i) => item * i,
                    Operation::Add(i) => item + i
                };

                if let Some(modulus) = modulus {
                    worry %= modulus;
                } else {
                    worry /= 3;
                }
                let next = if worry % monkeys[i].divisible == 0 {
                    monkeys[i].next_on_true
                } else {
                    monkeys[i].next_on_false
                };

                monkeys[next].items.push_back(worry);
                monkeys[i].inspected += 1;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug,)]
pub enum Operation {
    Square,
    Multiply(i64),
    Add(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug,)]
pub struct Monkey {
    pub items: VecDeque<i64>,
    pub operation: Operation,
    pub divisible: i64,
    pub next_on_true: usize,
    pub next_on_false: usize,
    pub inspected: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Monkey {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut lines = s.lines();

        lines.next();
        let (_, starting_items) = lines.next().unwrap().split_at(17);
        let items: Vec<i64> = aoc::read_list_from_string(starting_items, ",").unwrap();
        let (_, operation_string) = lines.next().unwrap().split_at(23);
        let mut chars = operation_string.chars();
        let op = chars.next().unwrap();
        let oparg = chars.skip(1).fold(String::new(), |s, c| format!("{}{}", s, c));
        let operation = match (op, oparg.parse::<i64>()) {
            ('*', result) => if let Ok(i) = result {
                Operation::Multiply(i)
            } else {
                Operation::Square
            },
            ('+', Ok(i)) => Operation::Add(i),
            _ => panic!("Unknown operation")
        };
        let divisible = lines.next().unwrap().split_at(21).1.parse::<i64>().unwrap();
        let next_on_true = lines.next().unwrap().split_at(29).1.parse::<usize>().unwrap();
        let next_on_false = lines.next().unwrap().split_at(30).1.parse::<usize>().unwrap();
        Ok(Self { items: items.into(), operation, divisible, next_on_true, next_on_false, inspected: 0 })
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day11::Day11;

    const EXAMPLE_DATA: &str = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let mut day = Day11::setup(EXAMPLE_DATA).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(10_605));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(66_802));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let mut day = Day11::setup(EXAMPLE_DATA).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2_713_310_158));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(21_800_916_620));
    }
}