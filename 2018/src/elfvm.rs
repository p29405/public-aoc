use log::debug;
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub enum ElfInstruction {
    Addr { op1: usize, op2: usize, dest: usize, },
    Addi { op1: usize, op2: i64, dest: usize, },
    Mulr { op1: usize, op2: usize, dest: usize, },
    Muli { op1: usize, op2: i64, dest: usize, },
    Banr { op1: usize, op2: usize, dest: usize, },
    Bani { op1: usize, op2: i64, dest: usize, },
    Borr { op1: usize, op2: usize, dest: usize, },
    Bori { op1: usize, op2: i64, dest: usize, },
    Setr { op1: usize, op2: usize, dest: usize, },
    Seti { op1: i64, op2: usize, dest: usize, },
    Gtir { op1: i64, op2: usize, dest: usize, },
    Gtri { op1: usize, op2: i64, dest: usize, },
    Gtrr { op1: usize, op2: usize, dest: usize, },
    Eqir { op1: i64, op2: usize, dest: usize, },
    Eqri { op1: usize, op2: i64, dest: usize, },
    Eqrr { op1: usize, op2: usize, dest: usize, },
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl ElfInstruction {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new_part1_loop(instruction: usize, r1: usize, r2: usize, r3: usize) -> Self {
        match instruction {
            0  => ElfInstruction::Addr { op1: r1, op2: r2, dest: r3},
            1  => ElfInstruction::Addi { op1: r1, op2: r2 as i64, dest: r3},
            2  => ElfInstruction::Mulr { op1: r1, op2: r2, dest: r3},
            3  => ElfInstruction::Muli { op1: r1, op2: r2 as i64, dest: r3},
            4  => ElfInstruction::Banr { op1: r1, op2: r2, dest: r3},
            5  => ElfInstruction::Bani { op1: r1, op2: r2 as i64, dest: r3},
            6  => ElfInstruction::Borr { op1: r1, op2: r2, dest: r3},
            7  => ElfInstruction::Bori { op1: r1, op2: r2 as i64, dest: r3},
            8  => ElfInstruction::Setr { op1: r1, op2: r2, dest: r3},
            9  => ElfInstruction::Seti { op1: r1 as i64, op2: r2, dest: r3},
            10 => ElfInstruction::Gtir { op1: r1 as i64, op2: r2, dest: r3},
            11 => ElfInstruction::Gtri { op1: r1, op2: r2 as i64, dest: r3},
            12 => ElfInstruction::Gtrr { op1: r1, op2: r2, dest: r3},
            13 => ElfInstruction::Eqir { op1: r1 as i64, op2: r2, dest: r3},
            14 => ElfInstruction::Eqri { op1: r1, op2: r2 as i64, dest: r3},
            15 => ElfInstruction::Eqrr { op1: r1, op2: r2, dest: r3},
            _ => unreachable!("Invalid instruction index {}", instruction)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn opcode(&self) -> usize {
        match self {
            ElfInstruction::Addr { .. } => 0,
            ElfInstruction::Addi { .. } => 1,
            ElfInstruction::Mulr { .. } => 2,
            ElfInstruction::Muli { .. } => 3,
            ElfInstruction::Banr { .. } => 4,
            ElfInstruction::Bani { .. } => 5,
            ElfInstruction::Borr { .. } => 6,
            ElfInstruction::Bori { .. } => 7,
            ElfInstruction::Setr { .. } => 8,
            ElfInstruction::Seti { .. } => 9,
            ElfInstruction::Gtir { .. } => 10,
            ElfInstruction::Gtri { .. } => 11,
            ElfInstruction::Gtrr { .. } => 12,
            ElfInstruction::Eqir { .. } => 13,
            ElfInstruction::Eqri { .. } => 14,
            ElfInstruction::Eqrr { .. } => 15,
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for ElfInstruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        match parts[0] {
            "addr" => Ok(ElfInstruction::Addr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "addi" => Ok(ElfInstruction::Addi {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "mulr" => Ok(ElfInstruction::Mulr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "muli" => Ok(ElfInstruction::Muli {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "banr" => Ok(ElfInstruction::Banr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "bani" => Ok(ElfInstruction::Bani {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "borr" => Ok(ElfInstruction::Borr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "bori" => Ok(ElfInstruction::Bori {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "setr" => Ok(ElfInstruction::Setr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "seti" => Ok(ElfInstruction::Seti {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "gtir" => Ok(ElfInstruction::Gtir {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "gtri" => Ok(ElfInstruction::Gtri {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "gtrr" => Ok(ElfInstruction::Gtrr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "eqir" => Ok(ElfInstruction::Eqir {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "eqri" => Ok(ElfInstruction::Eqri {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            "eqrr" => Ok(ElfInstruction::Eqrr {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap(),
                dest: parts[3].parse().unwrap()
            }),
            _ => Err(aoc::Error::new(&format!("Unknown opcode {}", parts[0])))
        }
    }
}

pub trait ElfOptimization {
    fn execute(&self, registers: &mut [i64; 6]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
pub struct ElfComputer {
    pub registers: [i64; 6],
    pub ip: usize,
    program: Vec<ElfInstruction>,
    optimizations: HashMap<usize, Box<dyn ElfOptimization>>
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl ElfComputer {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    pub fn new() -> Self {
        ElfComputer { registers: [0, 0, 0, 0, 0, 0], ip: 0, program: vec![], optimizations: HashMap::new() }
    }
    */

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn instruction_pointer_register(&mut self, ip: usize) -> &mut Self {
        self.ip = ip;
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[ElfInstruction]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn optimization(&mut self, instruction: usize, optimization: Box<dyn ElfOptimization>) -> &mut Self {
        self.optimizations.insert(instruction, optimization);
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn set_registers_day16(&mut self, registers: &[i64; 4]) {
        self.registers[..4].clone_from_slice(&registers[..4])
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn set_registers(&mut self, registers: &[i64; 6]) {
        self.registers[..].clone_from_slice(&registers[..])
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn halted(&self) -> bool {
        self.registers[self.ip] as usize >= self.program.len()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(&mut self) {
        let ip = self.registers[self.ip] as usize;

        if let Some(optimization) = self.optimizations.get(&ip) {
            optimization.execute(&mut self.registers);
        } else {
            let before = self.registers.to_vec();
            let instruction = self.program[ip];
            self.execute(instruction);
            debug!("ip={:02}, {:?} {:?} {:?}", ip, before, instruction, self.registers);
        }

        self.registers[self.ip] += 1;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn execute(&mut self, instruction: ElfInstruction) {
        match instruction {
            ElfInstruction::Addr { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] + self.registers[op2]
            },
            ElfInstruction::Addi { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] + op2
            },
            ElfInstruction::Mulr { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] * self.registers[op2]
            },
            ElfInstruction::Muli { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] * op2
            },
            ElfInstruction::Banr { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] & self.registers[op2]
            },
            ElfInstruction::Bani { op1, op2  , dest } => {
                self.registers[dest] = self.registers[op1] & op2
            },
            ElfInstruction::Borr { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] | self.registers[op2]
            },
            ElfInstruction::Bori { op1, op2, dest } => {
                self.registers[dest] = self.registers[op1] | op2
            },
            ElfInstruction::Setr { op1, dest, .. } => {
                self.registers[dest] = self.registers[op1]
            },
            ElfInstruction::Seti { op1, dest, .. } => {
                self.registers[dest] = op1
            },
            ElfInstruction::Gtir { op1, op2, dest } => {
                self.registers[dest] = (op1 > self.registers[op2]) as i64
            },
            ElfInstruction::Gtri { op1, op2, dest } => {
                self.registers[dest] = (self.registers[op1] > op2) as i64
            },
            ElfInstruction::Gtrr { op1, op2, dest } => {
                self.registers[dest] = (self.registers[op1] > self.registers[op2]) as i64
            },
            ElfInstruction::Eqir { op1, op2, dest } => {
                self.registers[dest] = (op1 == self.registers[op2]) as i64
            },
            ElfInstruction::Eqri { op1, op2, dest } => {
                self.registers[dest] = (self.registers[op1] == op2) as i64
            },
            ElfInstruction::Eqrr { op1, op2, dest } => {
                self.registers[dest] = (self.registers[op1] == self.registers[op2]) as i64
            },
        }
    }
}