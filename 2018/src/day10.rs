use aoc::{self, Day, Result, GridElement, Coord, Velocity, CenteredMap, Map, to_string, AocFontType};
use lazy_static::lazy_static;
use regex::Regex;
use std::fmt;
use std::str::FromStr;
use std::cmp::{min, max};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Light>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut seconds = 0;
        let mut min_area = i64::MAX;

        loop {
            let (ll, ur) = next(&mut self.input);

            let current_area = area(ll, ur);
            if min_area < current_area {
                break;
            }
            seconds += 1;
            min_area = current_area;
        }

        let mut sky = CenteredMap::default();
        prev(&mut self.input);
        self.input.iter().for_each(|&light| sky.set(&light.position(), light));

        self.part1 = to_string(&sky.into(), Light::On { pos: Coord::default(), vel: Velocity::default() }, AocFontType::Aoc2018);
        self.part2 = Some(seconds);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        // no-op
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn area(lower_left: Coord, upper_right: Coord) -> i64 {
    (upper_right.x - lower_left.x).abs() * (upper_right.y - lower_left.y).abs()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn next(sky: &mut Vec<Light>) -> (Coord, Coord) {
    let mut min_x = i64::MAX;
    let mut min_y = i64::MAX;
    let mut max_x = i64::MIN;
    let mut max_y = i64::MIN;

    for light in sky.iter_mut() {
        *light = Light::On { pos: light.position().next(&light.velocity()), vel: light.velocity() };

        max_x = max(light.position().x, max_x);
        max_y = max(light.position().y, max_y);
        min_x = min(light.position().x, min_x);
        min_y = min(light.position().y, min_y);
    }

    (Coord::new(min_x, min_y), Coord::new(max_x, max_y))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn prev(sky: &mut Vec<Light>) {
    for light in sky.iter_mut() {
        *light = Light::On { pos: light.position().prev(&light.velocity()), vel: light.velocity() };
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub enum Light {
    Off,
    On { pos: Coord, vel: Velocity }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Light {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn position(&self) -> Coord {
        match self {
            Light::Off => unreachable!(),
            Light::On { pos, .. } => *pos
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn velocity(&self) -> Velocity {
        match self {
            Light::Off => unreachable!(),
            Light::On { vel, .. } => *vel
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Light {
    fn default() -> Self {
        Light::Off
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl PartialEq for Light {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn eq(&self, other: &Self) -> bool {
        match self {
            Light::On { .. } => matches!(other, Light ::On { .. }),
            _ => false
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Eq for Light {}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Light {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Light::Off => write!(f, "."),
            Light::On { .. } => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Light {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Light {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^position=<\s*(?P<x>[-\d]+),\s+(?P<y>[-\d]+)> velocity=<\s*(?P<dx>[-\d]+),\s+(?P<dy>[-\d]+)>$").unwrap();
        }

        if let Some(captures) =  RE.captures(s) {
            let x = captures["x"].parse().unwrap();
            let dx = captures["dx"].parse().unwrap();
            let y = captures["y"].parse().unwrap();
            let dy = captures["dy"].parse().unwrap();

            return Ok(Self::On { pos: Coord::new(x, y), vel: Velocity::new(dx, dy) });
        }

        Err(aoc::Error::new(&format!("Failed to parse Light: {}", s)))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day10::Day10;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parts() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("ZAEKAJGC".to_owned()));
        assert_eq!(day.part2_answer(), Some(10_577));
    }
}
/*
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        // This isn't well tested...
        let input =
            "position=< 9,  1> velocity=< 0,  2>
             position=< 7,  0> velocity=<-1,  0>
             position=< 3, -2> velocity=<-1,  1>
             position=< 6, 10> velocity=<-2, -1>
             position=< 2, -4> velocity=< 2,  2>
             position=<-6, 10> velocity=< 2, -2>
             position=< 1,  8> velocity=< 1, -1>
             position=< 1,  7> velocity=< 1,  0>
             position=<-3, 11> velocity=< 1, -2>
             position=< 7,  6> velocity=<-1, -1>
             position=<-2,  3> velocity=< 1,  0>
             position=<-4,  3> velocity=< 2,  0>
             position=<10, -3> velocity=<-1,  1>
             position=< 5, 11> velocity=< 1, -2>
             position=< 4,  7> velocity=< 0, -1>
             position=< 8, -2> velocity=< 0,  1>
             position=<15,  0> velocity=<-2,  0>
             position=< 1,  6> velocity=< 1,  0>
             position=< 8,  9> velocity=< 0, -1>
             position=< 3,  3> velocity=<-1,  1>
             position=< 0,  5> velocity=< 0, -1>
             position=<-2,  2> velocity=< 2,  0>
             position=< 5, -2> velocity=< 1,  2>
             position=< 1,  4> velocity=< 2,  1>
             position=<-2,  7> velocity=< 2, -2>
             position=< 3,  6> velocity=<-1, -1>
             position=< 5,  0> velocity=< 1,  0>
             position=<-6,  0> velocity=< 2,  0>
             position=< 5,  9> velocity=< 1, -2>
             position=<14,  7> velocity=<-2,  0>
             position=<-3,  6> velocity=< 2, -1>";

        let mut lights = aoc::read_list_from_string::<Light>(input, "\n").unwrap();

        assert_eq!(lights[0].position(), Coord::new(9, 1));
        assert_eq!(lights[0].velocity().dx, 0);
        assert_eq!(lights[0].velocity().dy, 2);

        let mut sky = CenteredMap::default();

        lights.iter().for_each(|&light| sky.set(&light.position(), light));
        println!("{}", sky);

        lights = next(&lights).0;
        sky = CenteredMap::default();
        lights.iter().for_each(|&light| sky.set(&light.position(), light));
        println!("{}", sky);

        lights = next(&lights).0;
        sky = CenteredMap::default();
        lights.iter().for_each(|&light| sky.set(&light.position(), light));
        println!("{}", sky);

        lights = next(&lights).0;
        sky = CenteredMap::default();
        lights.iter().for_each(|&light| sky.set(&light.position(), light));
        println!("{}", sky);

        lights = next(&lights).0;
        sky = CenteredMap::default();
        lights.iter().for_each(|&light| sky.set(&light.position(), light));
        println!("{}", sky);

    }
*/
