use aoc::{self, Coord, Day, Grid, GridElement, Map, Result, Direction, find_paths, Path};
use log::debug;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day15 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Square>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day15 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day15::Part1;
    type Part2 = crate::day15::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut map = self.input.clone();
        update_combatant(None, &mut map);
        let mut rounds = 0;

        while turn(&mut map) {
            rounds += 1;
        }
        let hp = map.elements.iter().map(|s| s.combatant()).map(|c| c.map_or(0, |c| c.hit_points)).sum::<i32>();
        debug!("Rounds = {} Total HP = {}", rounds, hp);

        self.part1 = Some(rounds * hp);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let elves = elf_count(&self.input);
        let mut map = self.input.clone();
        let mut rounds= 0;

        for boost in 4.. {
            debug!("Strength = {}", boost);
            map = self.input.clone();
            update_combatant(Some(boost), &mut map);
            rounds = 0;

            debug!("Initial\n{}", map);
            debug!("Round 1");
            while turn(&mut map) && elf_count(&map) == elves {
                rounds += 1;
                debug!("{}", map);
                debug!("Round {}", rounds + 1);
            }

            if elf_count(&map) == elves {
                debug!("{}", map);
                break;
            }
        }

        let hp = map.elements.iter().map(|s| s.combatant()).map(|c| c.map_or(0, |c| c.hit_points)).sum::<i32>();
        debug!("Rounds = {} Total HP = {}", rounds, hp);

        self.part2 = Some(rounds * hp);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn elf_count(map: &Grid<Square>) -> usize {
    map.elements.iter()
        .filter(|s| match s {
            Square::Combatant(c) => c.combatant_type == CombatantType::Elf,
            _ => false
        })
        .count()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn update_combatant(elf_boost: Option<i32>, map: &mut Grid<Square>) {
    let mut id = 0;
    for (position, c) in map.iter().filter(|(_, c)| c.combatant().is_some()).map(|(p, c)| (p, c.combatant().unwrap())) {
        let mut updated = c;
        updated.id = id;
        updated.position = position;
        if let Some(elf_boost) = elf_boost {
            if updated.combatant_type == CombatantType::Elf {
                updated.attack = elf_boost;
            }
        }
        map.set(&position, Square::Combatant(updated));
        id += 1;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn attackable(combatant: &Combatant, map: &Grid<Square>) -> Option<Combatant> {
    let mut enemy : Option<Combatant> = None;
    for direction in &[Direction::North, Direction::West, Direction::East, Direction::South] {
        let position = combatant.position.go_inverted(*direction);
        if let Square::Combatant(target) = map.get(&position) {
            if target.combatant_type == combatant.enemy() && enemy.map_or(true, |e| target.hit_points < e.hit_points) {
                enemy = Some(target);
            }
        }
    }
    enemy
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn turn(map: &mut Grid<Square>) -> bool {
    let order = attack_order(map);
    for combatant in order {
        if let Some(current) = map.get(&combatant.position).combatant() {
            let mut updated_position = current.position;
            if attackable(&combatant, map).is_none() {
                let enemies = enemies(current.enemy(), &attack_order(map));

                if enemies.is_empty() {
                    return false;
                }
                let in_range = in_range(&enemies, &map);
                let reachable = reachable(current.position, &in_range, &map);

                if let Some((_, position)) = reachable {
                    map.set(&current.position, Square::Open);
                    map.set(&position, Square::Combatant(current.position(&position)));
                    updated_position = position;
                    //debug!("{} moves to @{},{}", current, updated_position.x, updated_position.y);
                } else {
                    //debug!("{} can't move", current);
                }
            }

            let current = current.position(&updated_position);

            if let Some(enemy) = attackable(&current, map) {
                let hp = enemy.hit_points - combatant.attack;

                //debug!("{} attacks {} health={}", current, enemy, if hp > 0 { hp } else { 0 });
                if hp > 0 {
                    map.set(&enemy.position, Square::Combatant(enemy.hit_points(hp)))
                } else {
                    map.set(&enemy.position, Square::Open);
                }
            }
        }
    }

    true
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn attack_order(map: &Grid<Square>) -> Vec<Combatant> {
    map.elements.iter()
        .map(|e| e.combatant())
        .filter(|c| c.is_some())
        .map(|c| c.unwrap())
        .collect()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn enemies(enemy_type: CombatantType, combatants: &[Combatant]) -> Vec<Combatant> {
    combatants.iter()
        .filter(|c| c.combatant_type == enemy_type)
        .copied()
        .collect()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn in_range(enemies: &[Combatant], map: &Grid<Square>) -> Vec<Coord> {
    let mut in_range = vec![];

    enemies.iter()
        .for_each(|e| {
        let position = e.position;

        position.adjacent()
            .filter(|p| map.get(p).open())
            .for_each(|p| in_range.push(p));
    });

    in_range
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn reachable(start: Coord, in_range: &[Coord], map: &Grid<Square>) -> Option<(usize, Coord)> {
    let paths = find_paths(in_range, &[start],
                                      map.columns, map.rows,
                           |_, p| p == &start || map.get(p).open());
    let mut step : Option<(usize, Coord)> = None;

    if paths.get(&start) != Path::Unreachable {
        for direction in &[Direction::North, Direction::West, Direction::East, Direction::South] {
            let position = start.go_inverted(*direction);
            if let Path::Distance(d) = paths.get(&position) {
                if step.map_or(true, |s| d < s.0) {
                    step = Some((d, position));
                }
            }
        }
    }

    step
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CombatantType {
    Elf,
    Goblin
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Combatant {
    pub id: i32,
    pub combatant_type: CombatantType,
    pub attack: i32,
    pub hit_points: i32,
    pub position: Coord
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Combatant {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(combatant_type: CombatantType) -> Self {
        Self { id: 0, combatant_type, attack: 3, hit_points: 200, position: Coord::default() }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn position(&self, position: &Coord) -> Self {
        Self { id: self.id, combatant_type: self.combatant_type, attack: self.attack, hit_points: self.hit_points, position: *position }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn hit_points(&self, hp: i32) -> Self {
        Self { id: self.id, combatant_type: self.combatant_type, attack: self.attack, hit_points: hp, position: self.position }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn enemy(&self) -> CombatantType {
        match self.combatant_type {
            CombatantType::Elf => CombatantType::Goblin,
            CombatantType::Goblin => CombatantType::Elf
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Combatant {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ch = match self.combatant_type {
            CombatantType::Goblin => 'G',
            CombatantType::Elf => 'E'
        };

        write!(f, "{}#{} @{},{}", ch, self.id, self.position.x, self.position.y)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Open,
    Wall,
    Combatant(Combatant)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn combatant(&self) -> Option<Combatant> {
        if let Square::Combatant(c) = self {
            Some(*c)
        } else {
            None
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn open(&self) -> bool {
        self == &Square::Open
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Square::Wall
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Open => write!(f, "."),
            Square::Wall => write!(f, "#"),
            Square::Combatant(c) => {
                if c.combatant_type == CombatantType::Elf {
                    write!(f, "E")
                } else {
                    write!(f, "G")
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '#' => Square::Wall,
            '.' => Square::Open,
            'E' => Square::Combatant(Combatant::new(CombatantType::Elf)),
            'G' => Square::Combatant(Combatant::new(CombatantType::Goblin)),
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{self, Coord, Day};
    use crate::day15::{Day15, update_combatant, attack_order, CombatantType, turn, enemies, in_range, reachable};
    use log::debug;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_attack_order() {
        let input =
            "#######
             #.G.E.#
             #E.G.E#
             #.G.E.#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        let order = attack_order(&map);

        assert_eq!(order.len(), 7);

        assert_eq!(order[0].combatant_type, CombatantType::Goblin);
        assert_eq!(order[0].position, Coord::new(2, 1));

        assert_eq!(order[1].combatant_type, CombatantType::Elf);
        assert_eq!(order[1].position, Coord::new(4, 1));

        assert_eq!(order[2].combatant_type, CombatantType::Elf);
        assert_eq!(order[2].position, Coord::new(1, 2));

        assert_eq!(order[3].combatant_type, CombatantType::Goblin);
        assert_eq!(order[3].position, Coord::new(3, 2));

        assert_eq!(order[4].combatant_type, CombatantType::Elf);
        assert_eq!(order[4].position, Coord::new(5, 2));

        assert_eq!(order[5].combatant_type, CombatantType::Goblin);
        assert_eq!(order[5].position, Coord::new(2, 3));

        assert_eq!(order[6].combatant_type, CombatantType::Elf);
        assert_eq!(order[6].position, Coord::new(4, 3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_enemies() {
        let input =
            "#######
             #.G.E.#
             #E.G.E#
             #.G.E.#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        let order = attack_order(&map);

        let goblins = enemies(CombatantType::Goblin, &order);
        assert_eq!(goblins.len(), 3);

        assert_eq!(goblins[0].combatant_type, CombatantType::Goblin);
        assert_eq!(goblins[0].position, Coord::new(2, 1));

        assert_eq!(goblins[1].combatant_type, CombatantType::Goblin);
        assert_eq!(goblins[1].position, Coord::new(3, 2));

        assert_eq!(goblins[2].combatant_type, CombatantType::Goblin);
        assert_eq!(goblins[2].position, Coord::new(2, 3));

        let elves = enemies(CombatantType::Elf, &order);
        assert_eq!(elves.len(), 4);

        assert_eq!(elves[0].combatant_type, CombatantType::Elf);
        assert_eq!(elves[0].position, Coord::new(4, 1));

        assert_eq!(elves[1].combatant_type, CombatantType::Elf);
        assert_eq!(elves[1].position, Coord::new(1, 2));

        assert_eq!(elves[2].combatant_type, CombatantType::Elf);
        assert_eq!(elves[2].position, Coord::new(5, 2));

        assert_eq!(elves[3].combatant_type, CombatantType::Elf);
        assert_eq!(elves[3].position, Coord::new(4, 3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_in_range() {
        let input =
            "#######
             #E..G.#
             #...#.#
             #.G.#G#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        let order = attack_order(&map);
        let goblins = enemies(CombatantType::Goblin, &order);
        let mut in_range = in_range(&goblins, &map);

        in_range.sort();

        assert_eq!(in_range.len(), 6);

        assert_eq!(in_range[0], Coord::new(3, 1));
        assert_eq!(in_range[1], Coord::new(5, 1));
        assert_eq!(in_range[2], Coord::new(2, 2));
        assert_eq!(in_range[3], Coord::new(5, 2));
        assert_eq!(in_range[4], Coord::new(1, 3));
        assert_eq!(in_range[5], Coord::new(3, 3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_find_next_step() {
        //aoc::logger::init(aoc::logger::LevelFilter::Debug);
        let input =
            "#######
             #E..G.#
             #...#.#
             #.G.#G#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        assert!(reachable(Coord::new(1, 1), &[Coord::new(5, 3)], &map).is_none());

        let course = reachable(Coord::new(1, 1), &[Coord::new(3, 1)], &map);
        assert!(course.is_some());
        let course = course.unwrap();
        assert_eq!(course, (1, Coord::new(2, 1)));

        let course = reachable(Coord::new(1,1), &[Coord::new(1, 3)], &map);
        assert!(course.is_some());
        let course = course.unwrap();
        assert_eq!(course, (1, Coord::new(1, 2)));

        let course = reachable(Coord::new(1,1), &[Coord::new(2, 2)], &map);
        assert!(course.is_some());
        let course = course.unwrap();
        assert_eq!(course, (1, Coord::new(2, 1)));

        let course = reachable(Coord::new(1,1), &[Coord::new(3, 3)], &map);
        assert!(course.is_some());
        let course = course.unwrap();
        assert_eq!(course, (3, Coord::new(2, 1)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_reachable() {
        let input =
            "#######
             #E..G.#
             #...#.#
             #.G.#G#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        let order = attack_order(&map);
        let goblins = enemies(CombatantType::Goblin, &order);
        let in_range = in_range(&goblins, &map);
        let reachable = reachable(Coord::new(1, 1), &in_range, &map).unwrap();

        assert_eq!(reachable.0, 1);
        assert_eq!(reachable.1, Coord::new(2, 1));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_reachable_2() {
        //aoc::logger::init(aoc::logger::LevelFilter::Debug);
        let input =
            "#######
             #.E...#
             #.....#
             #...G.#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        let order = attack_order(&map);
        let goblins = enemies(CombatantType::Goblin, &order);
        let in_range = in_range(&goblins, &map);
        let reachable = reachable(Coord::new(2, 1), &in_range, &map).unwrap();

        assert_eq!(reachable.0, 2);
        assert_eq!(reachable.1, Coord::new(3, 1));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_movement() {
        //aoc::logger::init(aoc::logger::LevelFilter::Debug);
        let input =
            "#########
             #G..G..G#
             #.......#
             #.......#
             #G..E..G#
             #.......#
             #.......#
             #G..G..G#
             #########";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        turn(&mut map);
        debug!("After turn 1:\n{}", map);
        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 9);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(4, 3));

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 8);
        assert_eq!(goblins[0].position, Coord::new(2, 1));
        assert_eq!(goblins[1].position, Coord::new(6, 1));
        assert_eq!(goblins[2].position, Coord::new(4, 2));
        assert_eq!(goblins[3].position, Coord::new(7, 3));
        assert_eq!(goblins[4].position, Coord::new(2, 4));
        assert_eq!(goblins[5].position, Coord::new(1, 6));
        assert_eq!(goblins[6].position, Coord::new(4, 6));
        assert_eq!(goblins[7].position, Coord::new(7, 6));

        turn(&mut map);
        debug!("After turn 2:\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 9);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(4, 3));

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 8);
        assert_eq!(goblins[0].position, Coord::new(3, 1));
        assert_eq!(goblins[1].position, Coord::new(5, 1));
        assert_eq!(goblins[2].position, Coord::new(4, 2));
        assert_eq!(goblins[3].position, Coord::new(2, 3));
        assert_eq!(goblins[4].position, Coord::new(6, 3));
        assert_eq!(goblins[5].position, Coord::new(1, 5));
        assert_eq!(goblins[6].position, Coord::new(4, 5));
        assert_eq!(goblins[7].position, Coord::new(7, 5));

        turn(&mut map);
        debug!("After turn 3:\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 9);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(4, 3));

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 8);
        assert_eq!(goblins[0].position, Coord::new(3, 2));
        assert_eq!(goblins[1].position, Coord::new(4, 2));
        assert_eq!(goblins[2].position, Coord::new(5, 2));
        assert_eq!(goblins[3].position, Coord::new(3, 3));
        assert_eq!(goblins[4].position, Coord::new(5, 3));
        assert_eq!(goblins[5].position, Coord::new(1, 4));
        assert_eq!(goblins[6].position, Coord::new(4, 4));
        assert_eq!(goblins[7].position, Coord::new(7, 5));

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_hp() {
        // aoc::logger::init(aoc::logger::LevelFilter::Debug);
        let input =
            "#######
             #.G...#
             #...EG#
             #.#.#G#
             #..G#E#
             #.....#
             #######";

        let mut map = aoc::read_grid_from_string(input).unwrap();
        update_combatant(None, &mut map);

        debug!("First turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 6);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 2);
        assert_eq!(elves[0].position, Coord::new(4, 2));
        assert_eq!(elves[0].hit_points, 197);
        assert_eq!(elves[1].position, Coord::new(5, 4));
        assert_eq!(elves[1].hit_points, 197);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(3, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(5, 2));
        assert_eq!(goblins[1].hit_points, 197);
        assert_eq!(goblins[2].position, Coord::new(3, 3));
        assert_eq!(goblins[2].hit_points, 200);
        assert_eq!(goblins[3].position, Coord::new(5, 3));
        assert_eq!(goblins[3].hit_points, 197);

        debug!("Second turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 6);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 2);
        assert_eq!(elves[0].position, Coord::new(4, 2));
        assert_eq!(elves[0].hit_points, 188);
        assert_eq!(elves[1].position, Coord::new(5, 4));
        assert_eq!(elves[1].hit_points, 194);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(4, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(3, 2));
        assert_eq!(goblins[1].hit_points, 200);
        assert_eq!(goblins[2].position, Coord::new(5, 2));
        assert_eq!(goblins[2].hit_points, 194);
        assert_eq!(goblins[3].position, Coord::new(5, 3));
        assert_eq!(goblins[3].hit_points, 194);

        for _ in 3..24 {
            assert!(turn(&mut map));
        }
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 131);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(4, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(3, 2));
        assert_eq!(goblins[1].hit_points, 200);
        assert_eq!(goblins[2].position, Coord::new(5, 2));
        assert_eq!(goblins[2].hit_points, 131);
        assert_eq!(goblins[3].position, Coord::new(5, 3));
        assert_eq!(goblins[3].hit_points, 131);

        debug!("24th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 128);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(3, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(4, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(3, 3));
        assert_eq!(goblins[2].hit_points, 200);
        assert_eq!(goblins[3].position, Coord::new(5, 3));
        assert_eq!(goblins[3].hit_points, 128);

        debug!("25th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 125);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(2, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(3, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(5, 3));
        assert_eq!(goblins[2].hit_points, 125);
        assert_eq!(goblins[3].position, Coord::new(3, 4));
        assert_eq!(goblins[3].hit_points, 200);

        debug!("26th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 122);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(1, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(2, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(5, 3));
        assert_eq!(goblins[2].hit_points, 122);
        assert_eq!(goblins[3].position, Coord::new(3, 5));
        assert_eq!(goblins[3].hit_points, 200);

        debug!("27th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 119);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(1, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(2, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(5, 3));
        assert_eq!(goblins[2].hit_points, 119);
        assert_eq!(goblins[3].position, Coord::new(4, 5));
        assert_eq!(goblins[3].hit_points, 200);

        debug!("28th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 5);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 1);
        assert_eq!(elves[0].position, Coord::new(5, 4));
        assert_eq!(elves[0].hit_points, 113);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(1, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(2, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(5, 3));
        assert_eq!(goblins[2].hit_points, 116);
        assert_eq!(goblins[3].position, Coord::new(5, 5));
        assert_eq!(goblins[3].hit_points, 200);


        for _ in 29..47 {
            assert!(turn(&mut map));
        }

        debug!("47th turn");
        turn(&mut map);
        debug!("\n{}", map);

        let after_turn = map.elements.iter()
            .map(|e| e.combatant())
            .filter(|c| c.is_some())
            .map(|c| c.unwrap())
            .collect::<Vec<_>>();

        assert_eq!(after_turn.len(), 4);

        let elves = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Elf).collect::<Vec<_>>();
        assert_eq!(elves.len(), 0);

        let goblins = after_turn.iter().filter(|c| c.combatant_type == CombatantType::Goblin).collect::<Vec<_>>();
        assert_eq!(goblins.len(), 4);
        assert_eq!(goblins[0].position, Coord::new(1, 1));
        assert_eq!(goblins[0].hit_points, 200);
        assert_eq!(goblins[1].position, Coord::new(2, 2));
        assert_eq!(goblins[1].hit_points, 131);
        assert_eq!(goblins[2].position, Coord::new(5, 3));
        assert_eq!(goblins[2].hit_points, 59);
        assert_eq!(goblins[3].position, Coord::new(5, 5));
        assert_eq!(goblins[3].hit_points, 200);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input =
            "#######
             #G..#E#
             #E#E.E#
             #G.##.#
             #...#E#
             #...E.#
             #######";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(36_334));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        //aoc::logger::init(aoc::logger::LevelFilter::Debug);
        let input =
            "#######
             #E..EG#
             #.#G.E#
             #E.##E#
             #G..#.#
             #..E#.#
             #######";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(39_514));
        assert_eq!(day.part2_answer(), Some(31_284));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_3() {
        let input =
            "#######
             #E.G#.#
             #.#G..#
             #G.#.G#
             #G..#.#
             #...E.#
             #######";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(27_755));
        assert_eq!(day.part2_answer(), Some(3478));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_4() {
        let input =
            "#######
             #.E...#
             #.#..G#
             #.###.#
             #E#G#G#
             #...#G#
             #######";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(28_944));
        assert_eq!(day.part2_answer(), Some(6474));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_5() {
        let input =
            "#########
             #G......#
             #.E.#...#
             #..##..G#
             #...##..#
             #...#...#
             #.G...G.#
             #.....G.#
             #########";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(18_740));
        assert_eq!(day.part2_answer(), Some(1140));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(216_270));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_6() {
        let input =
            "#######
             #.G...#
             #...EG#
             #.#.#G#
             #..G#E#
             #.....#
             #######";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4988));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(59_339));
    }
}
