use aoc::{self, Day, Result};
use crate::elfvm::{ElfComputer, ElfInstruction, ElfOptimization};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day19 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub ip: usize,
    pub program: Vec<ElfInstruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day19 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n");
        let ip = parts.next().unwrap().split(' ').nth(1).unwrap().parse().unwrap();
        let program = parts.map(|l| l.parse().unwrap()).collect();

        Ok(Self { part1: None, part2: None, ip, program})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut computer = ElfComputer::default();
        computer.instruction_pointer_register(self.ip).program(&self.program);
        computer.optimization(1, Box::new(Optimization::default()));
        while !computer.halted() {
            computer.step()
        }

        self.part1 = Some(computer.registers[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut computer = ElfComputer::default();
        computer.instruction_pointer_register(self.ip).program(&self.program);
        computer.optimization(1, Box::new(Optimization::default()));

        computer.registers[0] = 1;
        while !computer.halted() {
            computer.step()
        }

        self.part2 = Some(computer.registers[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
struct Optimization;

////////////////////////////////////////////////////////////////////////////////////////////////////
impl ElfOptimization for Optimization {
    fn execute(&self, registers: &mut [i64; 6]) {
        for i in 1..=registers[2] {
            registers[5] = i;
            for j in 1..=(registers[2]/registers[5]) {
                registers[3] = j;
                if registers[5] * registers[3] == registers[2] {
                    registers[0] += registers[5];
                }
            }
        }

        registers[4] = 15;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day19::Day19;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1620));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(15_827_082));
    }
}
