use aoc::{self, {pseudo_list::CircularList}, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub players: usize,
    pub last_marble: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day09::Part1;
    type Part2 = crate::day09::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.trim().split(" ");

        let players = parts.next().unwrap().parse()?;
        let last_marble = parts.nth(5).unwrap().parse()?;


        Ok(Self { part1: None, part2: None, players, last_marble})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(play_game(self.players, self.last_marble));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(play_game(self.players, self.last_marble * 100));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn play_game(players: usize, last_marble: i64) -> i64 {
    let mut board = CircularList::default();
    let mut elves = Vec::new();
    elves.resize(players, 0 as i64);

    let mut current_player = 0;

    board.push_back(0);

    for marble in 1..=last_marble {

        if let Some(score) = place_marble(marble, &mut board) {
            elves[current_player] += score;
        }
        current_player = (current_player + 1) % players;
    }

    *elves.iter().max().unwrap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn place_marble(value: i64, board: &mut CircularList<i64>) -> Option<i64> {
    if value % 23 == 0 {
        (0..7).for_each(|_| board.move_left());
        let marble = board.remove();
        board.move_right();

        return Some(marble.unwrap_or(0) + value);
    }

    board.move_right();
    board.insert_right(value);
    board.move_right();

    None
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("9 players; last marble is worth 25 points", 32),
                ("10 players; last marble is worth 1618 points", 8317),
                ("13 players; last marble is worth 7999 points", 146_373),
                ("17 players; last marble is worth 1104 points", 2764),
                ("21 players; last marble is worth 6111 points", 54_718),
                ("30 players; last marble is worth 5807 points", 37_305),
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day09::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "Input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(410_375));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_314_195_047));
    }
}