use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, " ")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(sum_metadata(&self.input, 0).0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(value(&self.input, 0).0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn sum_metadata(input: &[i64], position: usize) -> (i64, usize) {
    let children = input[position];
    let metadatas = input[position + 1] as usize;

    let mut sum = 0;
    let mut current = position + 2;

    for _ in 0..children {
        let (value, current_position) = sum_metadata(input, current);
        sum += value;
        current = current_position
    }

    sum += input.iter().skip(current).take(metadatas).sum::<i64>();

    (sum, current + metadatas)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn value(input: &[i64], position: usize) -> (i64, usize) {
    let num_children = input[position];
    let num_metadatas = input[position + 1] as usize;

    let mut children = vec![];
    let mut metadatas = vec![];

    let mut current = position + 2;

    for _ in 0..num_children {
        let (value, current_position) = value(input, current);
        children.push(value);
        current = current_position
    }

    input.iter().skip(current).take(num_metadatas).for_each(|m| metadatas.push(*m as usize));

    let mut val = 0;

    if children.is_empty() {
        val = metadatas.iter().map(|&v| v as i64).sum();
    } else {
        for pos in metadatas {
            let index = pos - 1;
            if index < children.len() {
                val += children[index];
            }
        }
    }

    (val, current + num_metadatas)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::{Day08, sum_metadata, value};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example() {
        let input = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2];

        assert_eq!(sum_metadata(&input, 0), (138, 16));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(44_893));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2];

        assert_eq!(value(&input, 0), (66, 16));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(27_433));
    }
}