use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: usize,
    pub length: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day14::Part1;
    type Part2 = crate::day14::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim();
        let length = input.len();
        let input = input.trim().parse()?;

        Ok(Self { part1: None, part2: None, input, length})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut recipes = vec![3, 7];
        let mut elf1 = 0;
        let mut elf2 = 1;

        for _ in 0..self.input + 10 {
            let (first, second) = make_recipe(elf1, elf2, &recipes);
            recipes.push(first);
            if let Some(second) = second {
                recipes.push(second);
            }
            elf1 = next(elf1, &recipes);
            elf2 = next(elf2, &recipes);
        }

        self.part1 = Some(value(self.input, 10, &recipes));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut recipes = Vec::with_capacity(22_000_000);
        let mut elf1 = 0;
        let mut elf2 = 1;
        recipes.push(3);
        recipes.push(7);

        let mut answer = 0;

        for _ in 0.. {
            let (first, second) = make_recipe(elf1, elf2, &recipes);
            recipes.push(first);
            if let Some(second) = second {
                recipes.push(second);
            }
            elf1 = next(elf1, &recipes);
            elf2 = next(elf2, &recipes);

            if recipes.len() >= answer + self.length {
                if value(answer, self.length, &recipes) == self.input {
                    break;
                }
                answer += 1;
            }
        }

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn next(elf: usize, recipes: &[u8]) -> usize {
    (elf + recipes[elf] as usize + 1) % recipes.len()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn make_recipe(elf1: usize, elf2: usize, recipes: &[u8]) -> (u8, Option<u8>) {
    let sum = recipes[elf1] + recipes[elf2];
    if sum >= 10 {
        (1, Some(sum - 10))
    } else {
        (sum, None)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn value(skip: usize, count: usize, recipes: &[u8]) -> usize {
    recipes[skip..skip + count].iter().fold(0, |accum, r| accum * 10 + *r as usize)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::{Day14, make_recipe, next};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let mut recipes = vec![3, 7];
        let mut elf1 = 0;
        let mut elf2 = 1;

        let expected =
            [
                (vec![3, 7, 1, 0], 0, 1),
                (vec![3, 7, 1, 0, 1, 0], 4, 3),
                (vec![3, 7, 1, 0, 1, 0, 1], 6, 4),
                (vec![3, 7, 1, 0, 1, 0, 1, 2], 0, 6),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4], 4, 8),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5], 6, 3),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5, 1], 8, 4),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5, 1, 5], 1, 6),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5, 1, 5, 8], 9, 8),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5, 1, 5, 8, 9], 1, 13),
                (vec![3, 7, 1, 0, 1, 0, 1, 2, 4, 5, 1, 5, 8, 9, 1, 6], 9, 7)
            ];

        for (expected_recipes, expected_elf1, expected_elf2) in &expected {
            let (first, second) = make_recipe(elf1, elf2, &recipes);
            recipes.push(first);
            if let Some(second) = second {
                recipes.push(second);
            }
            elf1 = next(elf1, &recipes);
            elf2 = next(elf2, &recipes);

            assert_eq!(&recipes, expected_recipes);
            assert_eq!(elf1, *expected_elf1);
            assert_eq!(elf2, *expected_elf2);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_191_216_109));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(20_268_576));
    }
}

/*
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        assert_eq!(part1(9), 5_158_916_779);
        assert_eq!(part1(5), 124_515_891);
        assert_eq!(part1(18), 9_251_071_085);
        assert_eq!(part1(2_018), 5_941_429_882);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_3() {
        assert_eq!(part2(&[5, 1, 5, 8, 9]), 9);
        assert_eq!(part2(&[0, 1, 2, 4, 5]), 5);
        assert_eq!(part2(&[9, 2, 5, 1, 0]), 18);
        assert_eq!(part2(&[5, 9, 4, 1, 4]), 2_018);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        assert_eq!(part1(190_221), 1_191_216_109);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        assert_eq!(part2(&[1, 9, 0, 2, 2, 1]), 20_268_576);
        assert_eq!(part2(&[1, 4, 7, 0, 6, 1]), 20_283_721);
    }
}
*/