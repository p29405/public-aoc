use aoc::{self, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_list::<String>("\n")?;
    let ip = input[0].split(' ').nth(1).unwrap().parse().unwrap();
    let program = input.iter().skip(1).map(|l| l.parse().unwrap()).collect::<Vec<ElfInstruction>>();

    aoc::answer("Part 1", &part1(ip, &program))?;
    aoc::answer("Part 2", &part2(ip, &program))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
struct Optimization;

////////////////////////////////////////////////////////////////////////////////////////////////////
impl ElfOptimization for Optimization {
    fn execute(&self, registers: &mut [i64; 6]) {
        for i in 1..=registers[2] {
            registers[5] = i;
            for j in 1..=(registers[2]/registers[5]) {
                registers[3] = j;
                if registers[5] * registers[3] == registers[2] {
                    registers[0] += registers[5];
                }
            }
        }

        registers[4] = 15;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(ip: usize, program: &[ElfInstruction]) -> i64 {
    let mut computer = ElfComputer::default();
    computer.instruction_pointer_register(ip).program(program);

    let mut instructions = 0;
    while computer.ip != 28 {
        computer.step();
        instructions += 1;
    }

    println!("Before {:?}", computer.registers);
    computer.step();
    println!("After {:?}", computer.registers);
    computer.registers[0]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(ip: usize, program: &[ElfInstruction]) -> i64 {
    let mut computer = ElfComputer::default();
    computer.instruction_pointer_register(ip).program(program);

    computer.registers[0]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{self, ElfComputer, ElfInstruction};
    use crate::{part1, part2};
}