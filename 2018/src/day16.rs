use aoc::{self, Day, Result};
use log::{debug, info};
use regex::Regex;
use std::collections::{HashMap, HashSet};
use crate::elfvm::{ElfComputer, ElfInstruction};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day16 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub states: Vec<State>,
    pub program: Vec<Vec<usize>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day16 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day16::Part1;
    type Part2 = crate::day16::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut input = input.split("\n\n\n");
        let states = parse_part1(input.next().unwrap().trim())?;
        let program = aoc::read_lists_from_string(input.next().unwrap().trim(), " ")?;
        Ok(Self { part1: None, part2: None, states, program })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut vm = ElfComputer::default();

        info!("Number of checks {}", self.states.len());
        let mut accum = 0;
        for state in self.states.iter() {
            let mut count = 0;
            for instruction in 0..16 {
                vm.set_registers_day16(&state.before);
                let instruction = ElfInstruction::new_part1_loop(instruction,
                                                                 state.instruction[1],
                                                                 state.instruction[2],
                                                                 state.instruction[3]);

                debug!("Before {:?}", vm.registers);
                vm.execute(instruction);
                debug!("After {:?}", vm.registers);

                if vm.registers[..4] == state.after[..] {
                    count += 1;
                }
            }

            if count >= 3 {
                accum += 1
            }
        }

        self.part1 = Some(accum);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut vm = ElfComputer::default();

        let mut mapping: HashMap<usize, HashSet<usize>> = HashMap::new();
        for state in self.states.iter() {
            let mut possibilities: HashSet<usize> = HashSet::new();
            for instruction in 0..16 {
                vm.set_registers_day16(&state.before);
                let instruction = ElfInstruction::new_part1_loop(instruction,
                                                                 state.instruction[1],
                                                                 state.instruction[2],
                                                                 state.instruction[3]);

                debug!("Before {:?}", vm.registers);
                vm.execute(instruction);
                debug!("After {:?}", vm.registers);

                if vm.registers[..4] == state.after[..] {
                    possibilities.insert(instruction.opcode());
                }
            }

            if let Some(p) = mapping.get_mut(&state.instruction[0]) {
                // Is this needed?  Seems to run faster and still correctly with out it
                *p = p.intersection(&possibilities).copied().collect::<HashSet<_>>();
            } else {
                mapping.insert(state.instruction[0], possibilities);
            }


        }
        let mut opcodes = HashMap::new();
        while !mapping.is_empty() {
            mapping.iter().filter(|(_, v)| v.len() == 1)
                .for_each(|(k, v)| { opcodes.insert(*k, *v.iter().next().unwrap()); });

            opcodes.iter().for_each(|(k, _)| { mapping.remove(k); });

            mapping.iter_mut().for_each(|(_, v)| { opcodes.iter().for_each(|(_, f)| {v.remove(f); }) });

        }

        info!("{:#?}", opcodes);

        vm.set_registers_day16(&[0, 0, 0, 0]);

        for command in self.program.iter() {
            let instruction = ElfInstruction::new_part1_loop(*opcodes.get(&command[0]).unwrap(), command[1], command[2], command[3]);
            vm.execute(instruction);
        }

        self.part2 = Some(vm.registers[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug)]
pub struct State {
    pub before: [i64; 4],
    pub instruction: [usize; 4],
    pub after: [i64; 4]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn parse_part1(lines: &str) -> Result<Vec<State>>{
    //lines.lines().for_each(|l| info!("{}", l));
    let before = Regex::new(r"Before: \[(\d+), (\d+), (\d+), (\d+)\]")?;
    let instruction = Regex::new(r"(\d+) (\d+) (\d+) (\d+)")?;
    let after = Regex::new(r"After:  \[(\d+), (\d+), (\d+), (\d+)\]")?;

    let lines = lines.lines().map(|s| s.into()).collect::<Vec<String>>();
    let mut i = 0;

    let mut states = Vec::new();
    while i < lines.len() {
        let before_match = before.captures(&lines[i]);
        let instruction_match = instruction.captures(&lines[i + 1]);
        let after_match = after.captures(&lines[i + 2]);

        let mut state = State::default();
        if let Some(matches) = before_match {
            for i in 0..4 {
                if let Some(m) = matches.get(i + 1) {
                    state.before[i] = m.as_str().parse()?;
                }
            }
        } else {
            panic!("Failed to parse as a Before line: {}", lines[i])
        }

        if let Some(matches) = instruction_match {
            for i in 0..4 {
                if let Some(m) = matches.get(i + 1) {
                    state.instruction[i] = m.as_str().parse()?;
                }
            }
        } else {
            panic!("Failed to parse as an Instruction line: {}", lines[i + 1])
        }

        if let Some(matches) = after_match {
            for i in 0..4 {
                if let Some(m) = matches.get(i + 1) {
                    state.after[i] = m.as_str().parse()?;
                }
            }

        } else {
            panic!("Failed to parse as an After line: {}", lines[i + 2])
        }

        info!("{:?}", state);
        states.push(state);
        i += 4;
    }
    
    Ok(states)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day16::Day16;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(529));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(573));
    }
}