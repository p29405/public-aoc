use aoc::{self, Coord, Grid, GridElement, Map, Result, Direction};
use log::debug;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_grid()?;

    aoc::answer("Part 1", &part1(&input))?;
    aoc::answer("Part 2", &part2(&input))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(input: &Grid<Segment>) -> i32 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(input: &Grid<Segment>) -> i32 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Segment {
    Empty,
    HorizontalTrack,
    VerticalTrack,
    Intersection,
    ForwardSlash,
    BackSlash,
    Cart(Direction)
}
/*
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn combatant(&self) -> Option<Combatant> {
        if let Square::Combatant(c) = self {
            Some(*c)
        } else {
            None
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn open(&self) -> bool {
        self == &Square::Open
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn wall(&self) -> bool {
        self == &Square::Wall
    }
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Segment {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Segment::Empty
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Segment {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Segment::Empty => write!(f, " "),
            Segment::HorizontalTrack => write!(f, "-"),
            Segment::VerticalTrack => write!(f, "|"),
            Segment::Intersection => write!(f, "+"),
            Segment::ForwardSlash => write!(f, "/"),
            Segment::BackSlash => write!(f, "\\"),
            Segment::Cart(..) => write!(f, "#"),
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Segment {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '.' => Segment::Empty,
            '|' => Segment::VerticalTrack,
            '-' => Segment::HorizontalTrack,
            '+' => Segment::Intersection,
            '\\' => Segment::BackSlash,
            '/' => Segment::ForwardSlash,
            '^' => Segment::Cart(Direction::North),
            '>' => Segment::Cart(Direction::East),
            'v' => Segment::Cart(Direction::South),
            '<' => Segment::Cart(Direction::West),
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{self, Coord};
    use crate::{part1, part2};
    use log::debug;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let map = aoc::read_grid_from_file("input/day13.txt").unwrap();

        assert_eq!(part1(&map), 0);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let map = aoc::read_grid_from_file("input/day13.txt").unwrap();

        assert_eq!(part2(&map), 0);
    }
}
