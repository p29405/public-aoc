use aoc::{self, Result, CenteredMap, GridElement, Coord, Map};
use log::debug;
use std::str::FromStr;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let directions = aoc::read_list_by_char()?;
    let map = build_map(&directions);

    println!("{}", map);
    aoc::answer("Part 1", &part1(&map))?;
    aoc::answer("Part 2", &part2())?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(map: &CenteredMap<Element>) -> i64 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2() -> i64 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn explore(directions: &[Direction], index: usize, position: Coord, map: &mut CenteredMap<Element>, depth: i32) {
    let direction = directions[index];
    let mut current = position;
    let mut skip = 0;
    debug!("{}: {:?} at {} Depth = {}", index, direction, position, depth);
    match direction {
        Direction::Start => map.set(&current, Element::Start),
        Direction::End => return,
        Direction::BranchStart => {
            let mut starts = 0;
            for i in index+1..directions.len() {
                match directions[i] {
                    Direction::BranchStart => starts += 1,
                    Direction::BranchEnd => if starts != 0 {
                        starts -= 1;
                    } else {
                        debug!("recurse end");
                        break;
                    },
                    Direction::BranchOptionStart => if starts == 0 {
                        debug!("recurse start");
                        explore(directions, i + 1, current, map, depth + 1);
                    }
                    _ => ()
                };
            }
        },
        Direction::BranchEnd => (),
        Direction::BranchOptionStart => {
            let mut starts = 0;
            for i in index+1..directions.len() {
                skip += 1;
                match directions[i] {
                    Direction::BranchStart => starts += 1,
                    Direction::BranchEnd => if starts != 0 {
                        starts -= 1;
                    } else {
                        debug!("End of options at {}", i);
                        break;
                    },
                    Direction::BranchOptionStart => (),
                    _ => ()
                };
            }
        },
        Direction::Move(d) => {
            current = current.go_inverted(d);
            map.set(&current, match d {
                aoc::Direction::North|aoc::Direction::South => Element::HorizonalDoor,
                _ => Element::VerticalDoor
            });
            current = current.go_inverted(d);
            map.set(&current, Element::Room);
        }

    };

    explore(directions, index + 1 + skip, current, map, depth)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn build_map(directions: &[Direction]) -> CenteredMap<Element> {
    let mut map = CenteredMap::default();

    //explore_depth_check(directions, 0, Coord::new(0, 0), &mut map, 0, &depths);

    explore(directions, 0, Coord::new(0, 0), &mut map, 0);
    //map.stop_animation();


    // expand the map:
    map.upper_left = map.upper_left().go_inverted(aoc::Direction::North).go(aoc::Direction::West);
    map.lower_right = map.lower_right().go_inverted(aoc::Direction::South).go(aoc::Direction::East);

    map
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Direction {
    Start,
    End,
    BranchStart,
    BranchEnd,
    BranchOptionStart,
    Move(aoc::Direction),
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Element {
    Wall,
    Room,
    Start,
    HorizonalDoor,
    VerticalDoor
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Element::Wall
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Element::Wall => write!(f, "#"),
            Element::Room => write!(f, "."),
            Element::Start => write!(f, "X"),
            Element::HorizonalDoor => write!(f, "-"),
            Element::VerticalDoor => write!(f, "|")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '#' => Element::Wall,
            '.' => Element::Room,
            '-' => Element::HorizonalDoor,
            '|' => Element::VerticalDoor,
            'X' => Element::Start,
            _   => panic!("Unknown element: {}", c)
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Direction {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.chars().next().unwrap() {
            '^' => Ok(Direction::Start),
            '$' => Ok(Direction::End),
            '(' => Ok(Direction::BranchStart),
            ')' => Ok(Direction::BranchEnd),
            '|' => Ok(Direction::BranchOptionStart),
            c => Ok(Direction::Move(aoc::Direction::from(c)))
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{self, Grid, Map};
    use crate::{part1, part2, Direction, build_map, Element};
    use log::debug;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_regex_parsing() {
        let input = "^ENWWW(NEEE|SSE(EE|N))$";
        let directions = aoc::read_list_by_char_from_string::<Direction>(input).unwrap();

        assert_eq!(directions.len(), 23);
        assert_eq!(directions[0], Direction::Start);
        assert_eq!(directions[1], Direction::Move(aoc::Direction::East));
        assert_eq!(directions[2], Direction::Move(aoc::Direction::North));
        assert_eq!(directions[6], Direction::BranchStart);
        assert_eq!(directions[7], Direction::Move(aoc::Direction::North));
        assert_eq!(directions[11], Direction::BranchOptionStart);
        assert_eq!(directions[21], Direction::BranchEnd);
        assert_eq!(directions[22], Direction::End);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input = "^ENWWW(NEEE|SSE(EE|N))$";
        let directions = aoc::read_list_by_char_from_string::<Direction>(input).unwrap();

        let map = build_map(&directions);

        let target_map =
            "#########
             #.|.|.|.#
             #-#######
             #.|.|.|.#
             #-#####-#
             #.#.#X|.#
             #-#-#####
             #.|.|.|.#
             #########";
        let expected = aoc::read_grid_from_string::<Element>(target_map).unwrap();
        let actual: Grid<Element> = map.into();

        assert_eq!(actual, expected);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";
        let directions = aoc::read_list_by_char_from_string::<Direction>(input).unwrap();

        let map = build_map(&directions);

        let target_map =
            "###########
             #.|.#.|.#.#
             #-###-#-#-#
             #.|.|.#.#.#
             #-#####-#-#
             #.#.#X|.#.#
             #-#-#####-#
             #.#.|.|.|.#
             #-###-###-#
             #.|.|.#.|.#
             ###########";
        let expected = aoc::read_grid_from_string::<Element>(target_map).unwrap();
        let actual: Grid<Element> = map.into();

        assert_eq!(actual, expected);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_3() {
        let input = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";
        let directions = aoc::read_list_by_char_from_string::<Direction>(input).unwrap();

        let map = build_map(&directions);

        let target_map =
            "#############
             #.|.|.|.|.|.#
             #-#####-###-#
             #.#.|.#.#.#.#
             #-#-###-#-#-#
             #.#.#.|.#.|.#
             #-#-#-#####-#
             #.#.#.#X|.#.#
             #-#-#-###-#-#
             #.|.#.|.#.#.#
             ###-#-###-#-#
             #.|.#.|.|.#.#
             #############";
        let expected = aoc::read_grid_from_string::<Element>(target_map).unwrap();
        let actual: Grid<Element> = map.into();

        assert_eq!(actual, expected);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_4() {
        let input = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$";
        let directions = aoc::read_list_by_char_from_string::<Direction>(input).unwrap();

        let map = build_map(&directions);

        let target_map =
            "###############
             #.|.|.|.#.|.|.#
             #-###-###-#-#-#
             #.|.#.|.|.#.#.#
             #-#########-#-#
             #.#.|.|.|.|.#.#
             #-#-#########-#
             #.#.#.|X#.|.#.#
             ###-#-###-#-#-#
             #.|.#.#.|.#.|.#
             #-###-#####-###
             #.|.#.|.|.#.#.#
             #-#-#####-#-#-#
             #.#.|.|.|.#.|.#
             ###############";
        let expected = aoc::read_grid_from_string::<Element>(target_map).unwrap();
        let actual: Grid<Element> = map.into();

        assert_eq!(actual, expected);

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let directions = aoc::read_list_by_char_from_file::<Direction>("input/day20.txt").unwrap();
        //assert_eq!(part1(&directions), 0);
    }
}
