use aoc::{self, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_list::<PreReq>("\n")?;

    aoc::answer("Part 1", &part1(&input))?;
    aoc::answer("Part 2", &part2(&input))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(input: &[PreReq]) -> i32 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(input: &[PreReq]) -> i32 {
    0
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone)]
struct PreReq {
    first: String,
    then: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for PreReq {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let list = s.trim().split(' ').map(|s| s.trim().into()).collect::<Vec<String>>();
        let first = list[1].clone();
        let then = list[7].clone();
        Ok(PreReq { first, then })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::{part1, part2};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let _input =
            "Step C must be finished before step A can begin.
             Step C must be finished before step F can begin.
             Step A must be finished before step B can begin.
             Step A must be finished before step D can begin.
             Step B must be finished before step E can begin.
             Step D must be finished before step E can begin.
             Step F must be finished before step E can begin.";
    }
}