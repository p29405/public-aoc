use aoc::{self, Day, Result};
use std::iter::Iterator;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut twos = 0;
        let mut threes = 0;

        for id in self.input.iter() {
            let mut chars = HashMap::new();

            for c in id.chars() {
                let count = chars.entry(c).or_insert(0);
                *count += 1;
            }

            let mut has_two = false;
            let mut has_three = false;
            for v in chars.values() {
                if *v == 2 { has_two = true; }
                if *v == 3 { has_three = true; }
            }

            if has_two { twos += 1; }
            if has_three { threes += 1; }
        }

        self.part1 = Some(twos * threes);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        for i in 0..self.input.len() {
            for j in 0..self.input.len() {
                if i == j { continue; }
                let left = self.input[i].clone().into_bytes();
                let right = self.input[j].clone().into_bytes();

                if difference(&left, &right) == 1 {
                    let mut answer = vec![];
                    left.iter()
                        .enumerate()
                        .filter(|(i, &c)| c == right[*i] )
                        .for_each( |(_, &c)| answer.push(c));

                    self.part2 = Some(String::from_utf8(answer).unwrap_or_else(|_| "Failed".into()));
                    return;
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn difference(s1: &[u8], s2: &[u8]) -> i64 {
    let mut diff = 0;
    for i in 0..s1.len() {
        if s1[i] != s2[i] { diff += 1; }
    }

    diff
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(7_936));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("lnfqdscwjyteorambzuchrgpx".to_owned()));
    }
}
