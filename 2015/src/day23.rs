use aoc::{self, Day, Result};
use log::info;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u64;
type Part2 = u64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day23 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day23 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day23::Part1;
    type Part2 = crate::day23::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut computer = Computer::default();

        computer.execute(&self.input);

        self.part1 = Some(computer.registers[1]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut computer = Computer::default();
        computer.registers[0] = 1;
        computer.execute(&self.input);

        self.part2 = Some(computer.registers[1]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub enum Register {
    A,
    B
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Register {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(s: &str) -> Self {
        match s {
            "a" | "a," => Register::A,
            "b" | "b," => Register::B,
            _ => panic!("Unknown register: {}", s)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn index(&self) -> usize {
        match self {
            Register::A => 0,
            Register::B => 1
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub enum Instruction {
    Hlf(Register),
    Tpl(Register),
    Inc(Register),
    Jmp(i32),
    Jie(Register, i32),
    Jio(Register, i32)
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();
        let instruction = match parts[0] {
            "hlf" => Instruction::Hlf(Register::new(parts[1])),
            "tpl" => Instruction::Tpl(Register::new(parts[1])),
            "inc" => Instruction::Inc(Register::new(parts[1])),
            "jmp" => Instruction::Jmp(parts[1].parse().unwrap()),
            "jie" => Instruction::Jie(Register::new(parts[1]), parts[2].parse().unwrap()),
            "jio" => Instruction::Jio(Register::new(parts[1]), parts[2].parse().unwrap()),
            _ => panic!("Unknown Instruction: {}", s)
        };

        Ok(instruction)
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Default)]
pub struct Computer {
    pub registers: [u64; 2],
    pub pc: i32
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Computer {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn execute(&mut self, program: &[Instruction]) {
        while (self.pc as usize) < program.len() {
            info!("Starting {:?}, pc = {}", self.registers, self.pc);
            info!("{:?}", &program[self.pc as usize]);
            match &program[self.pc as usize] {
                Instruction::Hlf(r) => {
                    self.registers[r.index()] /= 2;
                    self.pc += 1;
                },
                Instruction::Tpl(r) => {
                    self.registers[r.index()] *= 3;
                    self.pc += 1;

                },
                Instruction::Inc(r) => {
                    self.registers[r.index()] += 1;
                    self.pc += 1;
                },
                Instruction::Jmp(offset) => self.pc += *offset,
                Instruction::Jie(r, offset) => self.pc += if self.registers[r.index()] & 1 == 0 { *offset } else { 1 },
                Instruction::Jio(r, offset) => self.pc += if self.registers[r.index()] == 1 { *offset } else { 1 },
            };
            info!("Ending {:?}, pc = {}", self.registers, self.pc);
        }

    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day23::{Day23, Computer, Instruction};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "inc a
             jio a, +2
             tpl a
             inc a";

        let program = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        let mut computer = Computer::default();

        computer.execute(&program);

        assert_eq!(computer.registers[0], 2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(170));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(247));
    }
}
