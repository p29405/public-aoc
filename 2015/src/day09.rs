use aoc::{self, Day, Result, permutations};
use std::str::FromStr;
use std::collections::HashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Distance>,
    pub cities: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day09::Part1;
    type Part2 = crate::day09::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;
        let cities = all_cities(&input);
        Ok(Self { part1: None, part2: None, input, cities})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = permutations(&self.cities).map(|path| distance(&path, &self.input)).min().unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = permutations(&self.cities).map(|path| distance(&path, &self.input)).max().unwrap();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn all_cities(distances: &[Distance]) -> Vec<String> {
    let mut cities = HashSet::new();

    for d in distances {
        let (city1, city2) = d.cities.clone();
        cities.insert(city1);
        cities.insert(city2);
    }
    cities.iter().cloned().collect()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn distance(cities: &[String], distances: &[Distance]) -> usize {
    let mut prev = cities[0].as_str();
    let mut distance = 0;

    for current in cities.iter().skip(1) {
        distance += lookup(prev, current.as_str(), distances);
        prev = current.as_str()
    }
    distance
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn lookup(city1: &str, city2: &str, distances: &[Distance]) -> usize {
    distances.iter().find(|d| d.is_distance(city1, city2)).unwrap().distance
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Distance {
    cities: (String, String),
    distance: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Distance {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(city1: &str, city2: &str, distance: usize) -> Self {
        Self { cities: (city1.into(), city2.into()), distance }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn is_distance(&self, city1: &str, city2: &str) -> bool {
        (self.cities.0 == city1 && self.cities.1 == city2) || (self.cities.0 == city2 && self.cities.1 == city1)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Distance {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        Ok(Distance::new(parts[0], parts[2], parts[4].parse().unwrap()))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "London to Dublin = 464
             London to Belfast = 518
             Dublin to Belfast = 141";

        let mut day = Day09::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(605));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(117));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "London to Dublin = 464
             London to Belfast = 518
             Dublin to Belfast = 141";

        let mut day = Day09::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(982));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(909));
    }
}
