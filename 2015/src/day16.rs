use aoc::{self, Day, Result};
use lazy_static::lazy_static;
use log::info;
use regex::Regex;
use std::collections::HashMap;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day16 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Sue>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day16 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day16::Part1;
    type Part2 = crate::day16::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        info!("Starting Sues: {}", self.input.len());
        let remaining = self.input.iter()
            .filter(|s| s.filter("children", 3))
            .filter(|s| s.filter("cats", 7))
            .filter(|s| s.filter("samoyeds", 2))
            .filter(|s| s.filter("pomeranians", 3))
            .filter(|s| s.filter("akitas", 0))
            .filter(|s| s.filter("vizslas", 0))
            .filter(|s| s.filter("goldfish", 5))
            .filter(|s| s.filter("trees", 3))
            .filter(|s| s.filter("cars", 2))
            .filter(|s| s.filter("perfumes", 1))
            .collect::<Vec<&Sue>>();

        info!("Remaing Sues: {}", remaining.len());

        self.part1 = Some(remaining.first().unwrap().id);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        info!("Starting Sues: {}", self.input.len());
        let remaining = self.input.iter()
            .filter(|s| s.filter("children", 3))
            .filter(|s| s.more("cats", 7))
            .filter(|s| s.filter("samoyeds", 2))
            .filter(|s| s.fewer("pomeranians", 3))
            .filter(|s| s.filter("akitas", 0))
            .filter(|s| s.filter("vizslas", 0))
            .filter(|s| s.fewer("goldfish", 5))
            .filter(|s| s.more("trees", 3))
            .filter(|s| s.filter("cars", 2))
            .filter(|s| s.filter("perfumes", 1))
            .collect::<Vec<&Sue>>();

        info!("Remaing Sues: {}", remaining.len());

        self.part2 = Some(remaining.first().unwrap().id);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default)]
pub struct Sue {
    id: i32,
    attributes: HashMap<String, i32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Sue {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn filter(&self, attribute: &str, amount: i32) -> bool {
        if let Some(&count) = self.attributes.get(attribute) {
            return count == amount;
        }
        true
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fewer(&self, attribute: &str, amount: i32) -> bool {
        if let Some(&count) = self.attributes.get(attribute) {
            return count < amount;
        }
        true
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn more(&self, attribute: &str, amount: i32) -> bool {
        if let Some(&count) = self.attributes.get(attribute) {
            return count > amount;
        }
        true
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Sue {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(r"Sue (?P<id>\d+): (?P<attr1>\w+): (?P<amt1>\d+), (?P<attr2>\w+): (?P<amt2>\d+), (?P<attr3>\w+): (?P<amt3>\d+)").unwrap();
        }

        let captures = REGEX.captures(s).unwrap();
        let id = captures.name("id").unwrap().as_str().parse().unwrap();
        let attr1 = captures.name("attr1").unwrap().as_str().into();
        let amt1 = captures.name("amt1").unwrap().as_str().parse().unwrap();
        let attr2 = captures.name("attr2").unwrap().as_str().into();
        let amt2 = captures.name("amt2").unwrap().as_str().parse().unwrap();
        let attr3 = captures.name("attr3").unwrap().as_str().into();
        let amt3 = captures.name("amt3").unwrap().as_str().parse().unwrap();
        let mut attributes = HashMap::new();
        attributes.insert(attr1, amt1);
        attributes.insert(attr2, amt2);
        attributes.insert(attr3, amt3);
        Ok(Sue { id, attributes })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day16::Day16;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(213));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(323));
    }
}
