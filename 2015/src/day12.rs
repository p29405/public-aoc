use aoc::{self, Day, Result};
use serde_json::{self, Value};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day12 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Value
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day12 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day12::Part1;
    type Part2 = crate::day12::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = serde_json::from_str::<Value>(&input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(sum(&self.input));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(sum_no_red(&self.input));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn sum(value: &Value) -> i64 {
    match value {
        Value::Number(n) => n.as_i64().unwrap(),
        Value::Array(a) => a.iter().fold(0, |accum, v| accum + sum(v)),
        Value::Object(o) => o.values().fold(0, |accum, v| accum + sum(v)),
        _  => 0
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn sum_no_red(value: &Value) -> i64 {
    match value {
        Value::Number(n) => n.as_i64().unwrap(),
        Value::Array(a) => a.iter().fold(0, |accum, v| accum + sum_no_red(v)),
        Value::Object(o) => {
            if o.values().any(|v| if let Value::String(s) = v { s == "red" } else { false }) {
                0
            } else {
                o.values().fold(0, |accum, v| accum + sum_no_red(v))
            }
        },
        _  => 0
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day12::{Day12, sum, sum_no_red};
    use serde_json::{self, Value};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn sum_examples() {
        let inputs: [(&str, i64); 8] =
            [
                ("[1,2,3]", 6),
                (r#"{"a":2,"b":4}"#, 6),
                ("[[[3]]]", 3),
                (r#"{"a":{"b":4},"c":-1}"#, 3),
                (r#"{"a":[-1,1]}"#, 0),
                (r#"[-1,{"a":1}]"#, 0),
                ("[]", 0),
                ("{}", 0)
            ];

        for (input, answer) in inputs.iter() {
            let json = serde_json::from_str::<Value>(input).unwrap();
            assert_eq!(sum(&json), *answer, "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(156_366));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn sum_no_red_examples() {
        let inputs: [(&str, i64); 4] =
            [
                ("[1,2,3]", 6),
                (r#"[1,{"c":"red","b":2},3]"#, 4),
                (r#"{"d":"red","e":[1,2,3,4],"f":5}"#, 0),
                (r#"[1,"red",5]"#, 6)
            ];

        for (input, answer) in inputs.iter() {
            let json = serde_json::from_str::<Value>(input).unwrap();
            assert_eq!(sum_no_red(&json), *answer, "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(96_852));
    }
}
