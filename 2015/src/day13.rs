use aoc::{self, Day, Result, permutations};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Rule>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day13::Part1;
    type Part2 = crate::day13::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let people = people(&self.input, false);

        let answer = permutations(&people).map(|p| happiness(&p, &self.input)).max().unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let people = people(&self.input, true);

        let answer = permutations(&people).map(|p| happiness(&p, &self.input)).max().unwrap();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn people(rules: &[Rule], include_me: bool) -> Vec<String> {
    let mut set = vec![];

    for r in rules {
        if !set.contains(&r.person1) {
            set.push(r.person1.clone());
        }
    }

    if include_me {
        set.push("me".into());
    }

    set
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn happiness(arrangement: &[String], rules: &[Rule]) -> i64 {
    let mut total = 0;
    for (i, person) in arrangement.iter().enumerate() {
        let left = if i == 0 {
            arrangement[arrangement.len() - 1].as_str()
        } else {
            arrangement[i - 1].as_str()
        };
        let right = if i == arrangement.len() - 1 {
            arrangement[0].as_str()
        } else {
            arrangement[i + 1].as_str()
        };

        total += lookup(person, left, rules);
        total += lookup(person, right, rules);
    }

    total
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn lookup(person1: &str, person2: &str, rules: &[Rule]) -> i64 {
    if let Some(rule) = rules.iter().find(|r| r.person1 == person1 && r.person2 == person2) {
        rule.units * if rule.add { 1 } else { -1 }
    } else {
        0
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Rule {
    person1: String,
    person2: String,
    units: i64,
    add: bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Rule {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex =
                Regex::new(r"^(?P<person1>[^ ]+) would (?P<adj>gain|lose) (?P<units>\d+) happiness units by sitting next to (?P<person2>[^.]+).$").unwrap();
        }

        let captures = REGEX.captures(s).unwrap();
        let person1 = captures.name("person1").unwrap().as_str().to_owned();
        let person2 = captures.name("person2").unwrap().as_str().to_owned();
        let units = captures.name("units").unwrap().as_str().parse().unwrap();
        let adj = captures.name("adj").unwrap().as_str();
        Ok(Self { person1, person2, units, add: adj == "gain" })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day13::Day13;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "Alice would gain 54 happiness units by sitting next to Bob.
             Alice would lose 79 happiness units by sitting next to Carol.
             Alice would lose 2 happiness units by sitting next to David.
             Bob would gain 83 happiness units by sitting next to Alice.
             Bob would lose 7 happiness units by sitting next to Carol.
             Bob would lose 63 happiness units by sitting next to David.
             Carol would lose 62 happiness units by sitting next to Alice.
             Carol would gain 60 happiness units by sitting next to Bob.
             Carol would gain 55 happiness units by sitting next to David.
             David would gain 46 happiness units by sitting next to Alice.
             David would lose 7 happiness units by sitting next to Bob.
             David would gain 41 happiness units by sitting next to Carol.";

        let mut day = Day13::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(330));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(733));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(725));
    }
}
