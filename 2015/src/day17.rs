use aoc::{self, Day, Result};
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day17 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>,
    pub liters: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day17 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day17::Part1;
    type Part2 = crate::day17::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;
        let liters = 150;
        Ok(Self { part1: None, part2: None, input, liters})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut ways = 0;

        for key in 0..(1 << self.input.len()) {
            let mut remaining = self.liters;
            for i in indexes(key, self.input.len()) {
                remaining -= self.input[i];
            }

            if remaining == 0 {
                ways += 1;
            }
        }

        self.part1 = Some(ways);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut ways = HashMap::new();

        for key in 0..(1 << self.input.len()) {
            let mut remaining = self.liters;

            let combo = indexes(key, self.input.len());
            for i in &combo {
                remaining -= self.input[*i];
            }

            if remaining == 0 {
                let entry = ways.entry(combo.len()).or_insert(0);
                *entry += 1;
            }
        }

        if let Some((_, ways)) = ways.iter().min_by_key(|(k, _)| *k) {
            self.part2 = Some(*ways);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn indexes(key: usize, length: usize) -> Vec<usize> {
    let mut result = vec![];
    for index in 0..length {
        if key & 1 << index == 1 << index {
            result.push(index);
        }
    }

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day17::{Day17, indexes};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_indexes() {
        assert_eq!(indexes(3, 2), vec![0, 1]);
        assert_eq!(indexes(2, 2), vec![1]);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "20\n15\n10\n5\n5\n";

        let mut day = Day17::setup(input).unwrap();
        day.liters = 25;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day17.txt");

        let mut day = Day17::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1304));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "20\n15\n10\n5\n5\n";

        let mut day = Day17::setup(input).unwrap();
        day.liters = 25;
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day17.txt");

        let mut day = Day17::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(18));
    }
}
