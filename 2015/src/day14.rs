use aoc::{self, Day, Result};
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Reindeer>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day14::Part1;
    type Part2 = crate::day14::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let time = 2503;
        let answer = self.input.iter().map(|r| distance(r, time)).max().unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut positions = self.input.iter()
            .map(|r| {
                let mut p = ReindeerPosition::default();
                p.reindeer = r.clone();
                p
            })
            .collect::<Vec<ReindeerPosition>>();

        let mut lead_reindeer = HashMap::new();

        for _ in 0..2503 {
            positions.iter_mut().for_each(|p| p.advance());
            let leader = leaders(&positions);
            leader.iter().for_each(|reindeer| {
                let entry = lead_reindeer.entry(reindeer.clone()).or_insert(0i64);
                *entry += 1;
            });
        }

        let answer = *lead_reindeer.values().max_by_key(|r| *r).unwrap();

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn distance(reindeer: &Reindeer, time: i64) -> i64 {
    let mut position = ReindeerPosition::default();

    position.reindeer = reindeer.clone();

    (0..time).for_each(|_| position.advance());

    position.distance
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn leaders(positions: &[ReindeerPosition]) -> Vec<String> {
    let max = positions.iter().max_by_key(|p| p.distance).unwrap();
    positions.iter()
        .filter(|p| p.distance == max.distance)
        .map(|p| p.reindeer.name.clone())
        .collect()

}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct ReindeerPosition {
    pub reindeer: Reindeer,
    pub resting: bool,
    pub time: i64,
    pub distance: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl ReindeerPosition {
    pub fn advance(&mut self) {

        if self.resting &&  self.time == self.reindeer.rest {
            self.resting = false;
            self.time = 0;
        } else if !self.resting && self.time == self.reindeer.time {
            self.resting = true;
            self.time = 0;
        }

        if !self.resting {
            self.distance += self.reindeer.speed;
        }
        self.time += 1;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Reindeer {
    pub name: String,
    pub speed: i64,
    pub time: i64,
    pub rest: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Reindeer {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').map(|s| s.into()).collect::<Vec<String>>();

        Ok(Reindeer { name: parts[0].to_owned(), speed: parts[3].parse::<i64>().unwrap(), time: parts[6].parse::<i64>().unwrap(), rest: parts[13].parse::<i64>().unwrap() })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::{Day14, Reindeer};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
             Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.";

        let reindeer = aoc::read_list_from_string::<Reindeer>(input, "\n").unwrap();

        assert_eq!(reindeer[0].name, "Comet");
        assert_eq!(reindeer[0].speed, 14);
        assert_eq!(reindeer[0].time, 10);
        assert_eq!(reindeer[0].rest, 127);

        assert_eq!(reindeer[1].name, "Dancer");
        assert_eq!(reindeer[1].speed, 16);
        assert_eq!(reindeer[1].time, 11);
        assert_eq!(reindeer[1].rest, 162);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        /*
        let input = "";

        let mut day = Day13::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), None);

         */
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2655));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1059));
    }
}
    /*

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input =
            "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
             Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.";

        let reindeer = aoc::read_list_from_string::<Reindeer>(input, "\n").unwrap();

        assert_eq!(distance(&reindeer[0], 1), 14);
        assert_eq!(distance(&reindeer[1], 1), 16);


        assert_eq!(distance(&reindeer[0], 10), 140);
        assert_eq!(distance(&reindeer[1], 10), 160);

        assert_eq!(distance(&reindeer[0], 11), 140);
        assert_eq!(distance(&reindeer[1], 11), 176);

        assert_eq!(distance(&reindeer[0], 12), 140);
        assert_eq!(distance(&reindeer[1], 12), 176);


        assert_eq!(distance(&reindeer[0], 1_000), 1_120);
        assert_eq!(distance(&reindeer[1], 1_000), 1_056);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input =
            "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
             Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.";

        let reindeer = aoc::read_list_from_string::<Reindeer>(input, "\n").unwrap();

        let mut positions = reindeer.iter()
            .map(|r| {
                let mut p = ReindeerPosition::default();
                p.reindeer = r.clone();
                p
            })
            .collect::<Vec<ReindeerPosition>>();

        let mut lead_reindeer = HashMap::new();

        for _ in 0..1000 {
            positions.iter_mut().for_each(|p| p.advance());
            let leader = leaders(&positions);
            leader.iter().for_each(|reindeer| {
                let entry = lead_reindeer.entry(reindeer.clone()).or_insert(0);
                *entry += 1;
            });
        }

        let comet = lead_reindeer.get("Comet").unwrap();
        let dancer = lead_reindeer.get("Dancer").unwrap();

        assert_eq!(*comet, 312);
        assert_eq!(*dancer, 689);
    }
*/
