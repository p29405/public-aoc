use aoc::{self, Day, Result, Coord, GridElement, Grid, Map};
use std::{fmt, str::FromStr};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day06::Part1;
    type Part2 = crate::day06::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut display = Grid::new(1_000, 1_000);

        self.input.iter().for_each(|&i| match i {
            Instruction::Toggle(start, end) =>
                Coord::range_inclusive(start, end).for_each(|position| {
                    let flip = match display.get(&position) {
                        Light::On => Light::Off,
                        Light::Off => Light::On
                    };
                    display.set(&position, flip);
                }),
            Instruction::TurnOn(start, end) =>
                Coord::range_inclusive(start, end).for_each(|position| display.set(&position, Light::On)),
            Instruction::TurnOff(start, end) =>
                Coord::range_inclusive(start, end).for_each(|position| display.set(&position, Light::Off)),
        });

        let answer = display.elements.iter().filter(|&l| l == &Light::On).count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut display = Grid::new(1_000, 1_000);

        self.input.iter().for_each(|&i| {
            let start: Coord;
            let end: Coord;
            let adjust: i64;
            match i {
                Instruction::Toggle(s, e) => {
                    start = s;
                    end = e;
                    adjust = 2;
                },
                Instruction::TurnOn(s, e) => {
                    start = s;
                    end = e;
                    adjust = 1;
                },
                Instruction::TurnOff(s, e) => {
                    start = s;
                    end = e;
                    adjust = -1;
                }
            };

            Coord::range_inclusive(start, end).for_each(|position| {
                let Brightness::Level(current) = display.get(&position);
                let new= if current == 0 && adjust < 0 {
                    0
                } else {
                    current + adjust
                };

                display.set(&position, Brightness::Level(new));
            })
        });

        let answer = display.elements.iter().fold(0, |accum, &Brightness::Level(b)| accum + b);
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub enum Instruction {
    TurnOn(Coord, Coord),
    TurnOff(Coord, Coord),
    Toggle(Coord, Coord)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        let instruction = match parts[0] {
            "toggle" => Instruction::Toggle(Coord::from_pair(parts[1]), Coord::from_pair(parts[3])),
            "turn" => match parts[1] {
                "off" => Instruction::TurnOff(Coord::from_pair(parts[2]), Coord::from_pair(parts[4])),
                "on" => Instruction::TurnOn(Coord::from_pair(parts[2]), Coord::from_pair(parts[4])),
                _ => panic!("Failed to parse Instruction: {}", s)
            },
            _ => panic!("Failed to parse Instruction: {}", s)
        };

        Ok(instruction)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Brightness {
    Level(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Brightness {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Brightness::Level(0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Brightness {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Brightness::Level(_) => write!(f, ".")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Brightness {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Light {
    Off,
    On
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Light {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Light::Off
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Light {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Light::Off => write!(f, "."),
            Light::On => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Light {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::Day06;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(377_891));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(14_110_788));
    }
}
