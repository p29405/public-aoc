use aoc::{self, Day, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day15 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Ingredient>,
    pub names: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day15 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day15::Part1;
    type Part2 = crate::day15::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<Ingredient>(input, "\n")?;
        let names = input.iter().map(|i| i.name.clone()).collect::<Vec<_>>();
        Ok(Self { part1: None, part2: None, input, names})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = combos(self.input.len())
            .take_while(|c| c[self.names.len() - 1] != 100)
            .map(|c| {
                let mut recipe = HashMap::new();
                for (name, amount) in self.names.iter().zip(c) {
                    recipe.insert(name.clone(), amount);
                }
                part1_score(&recipe, &self.input)
            })
            .max().unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = combos(self.input.len())
            .take_while(|c| c[self.names.len() - 1] != 100)
            .map(|c| {
                let mut recipe = HashMap::new();
                for (name, amount) in self.names.iter().zip(c) {
                    recipe.insert(name.clone(), amount);
                }
                if calories(&recipe, &self.input) == 500 {
                    part1_score(&recipe, &self.input)
                } else {
                    i64::MIN
                }
            })
            .max().unwrap();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1_score(amounts: &HashMap<String, i64>, ingredients: &[Ingredient]) -> i64 {
    let mut capacity = 0;
    let mut durability = 0;
    let mut flavor = 0;
    let mut texture = 0;
    for (name, amount) in amounts {
        let ingredient = lookup(name, ingredients);
        capacity += ingredient.capacity * amount;
        durability += ingredient.durability * amount;
        flavor += ingredient.flavor * amount;
        texture += ingredient.texture * amount;

    }
    if capacity < 0 || durability < 0 || flavor < 0 || texture < 0 {
        0
    } else {
        capacity * durability * flavor * texture
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn calories(amounts: &HashMap<String, i64>, ingredients: &[Ingredient]) -> i64 {
    let mut calories = 0;
    for (name, amount) in amounts {
        let ingredient = lookup(name, ingredients);
        calories += ingredient.calories * amount;

    }
    calories
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn lookup<'a>(name: &str, ingredients: &'a [Ingredient]) -> &'a Ingredient {
    ingredients.iter().find(|i| i.name == name).unwrap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Ingredient {
    name: String,
    capacity: i64,
    durability: i64,
    flavor: i64,
    texture: i64,
    calories: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Ingredient {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(r"^(?P<name>[^ ]+): capacity (?P<capacity>[-]?\d+), durability (?P<durability>[-]?\d+), flavor (?P<flavor>[-]?\d+), texture (?P<texture>[-]?\d+), calories (?P<calories>[-]?\d+)$").unwrap();
        }

        let captures = REGEX.captures(s).unwrap();
        let name = captures.name("name").unwrap().as_str().to_owned();
        let capacity = captures.name("capacity").unwrap().as_str().parse().unwrap();
        let durability = captures.name("durability").unwrap().as_str().parse().unwrap();
        let flavor = captures.name("flavor").unwrap().as_str().parse().unwrap();
        let texture = captures.name("texture").unwrap().as_str().parse().unwrap();
        let calories = captures.name("calories").unwrap().as_str().parse().unwrap();
        Ok(Self { name, capacity, durability, flavor, texture, calories })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn combos(size: usize) -> Combos {
    Combos { counter: 0, size }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Combos {
    counter: i64,
    size: usize
}

//////////////////////////////////////////////////////////////////////////////
impl Iterator for Combos
{
    type Item = Vec<i64>;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        let mut next = vec![];
        next.resize(self.size, 0);

        loop {
            let mut current = self.counter;
            let mut sum = 0;
            for c in next.iter_mut() {
                let val = current % 101;
                current /= 101;
                sum += val;
                *c = val;
            }
            self.counter += 1;

            if sum == 100 {
                break;
            }
        }
        Some(next)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day15::{Day15, combos};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_combos() {
        assert_eq!(combos(2).next(), Some(vec![100, 0]));
        assert_eq!(combos(2).skip(1).next(), Some(vec![99, 1]));
        assert_eq!(combos(2).skip(2).next(), Some(vec![98, 2]));

        assert_eq!(combos(4).next(), Some(vec![100, 0, 0, 0]));
        assert_eq!(combos(4).skip(100).next(), Some(vec![0, 100, 0, 0]));
        assert_eq!(combos(4).skip(200).next(), Some(vec![0, 99, 1, 0]));

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
             Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3";

        let mut day = Day15::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(62_842_880));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(21_367_368));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_766_400));
    }
}
