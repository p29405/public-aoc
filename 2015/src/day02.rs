use aoc::{self, Day, Result};
use std::cmp::min;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Vec<i64>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_lists_from_string(input, "x")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().map(|v| area(v[0], v[1], v[2])).sum();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {

        let answer = self.input.iter().map(|v| ribbon_length(v[0], v[1], v[2])).sum();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn area(width: i64, height: i64, length: i64) -> i64 {
    let side1 = length * width;
    let side2 = width * height;
    let side3 = height * length;
    let min_side = min(side1, min(side2, side3));
    2 * side1 + 2 * side2 + 2 * side3 + min_side
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn ribbon_length(width: i64, height: i64, length: i64) -> i64 {
    let perimeter1 = length + width;
    let perimeter2 = width + height;
    let perimeter3 = height + length;
    let min_perimeter = min(perimeter1, min(perimeter2, perimeter3));
    min_perimeter * 2 + width * height * length
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::{Day02, area, ribbon_length};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        assert_eq!(area(2, 3, 4), 58);
        assert_eq!(area(1, 1, 10), 43);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_598_415));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        assert_eq!(ribbon_length(2, 3, 4), 34);
        assert_eq!(ribbon_length(1, 1, 10), 14);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_812_909));
    }
}
