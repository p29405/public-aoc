use aoc::{self, Day, Result};
use std::collections::BTreeMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day20 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day20 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day20::Part1;
    type Part2 = crate::day20::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim().parse()?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = (1..(self.input / 10)).find(|i| presents(*i) >= self.input).unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(house_part2(self.input));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn house_part2(target: i64) -> i64 {
    let mut delivered: BTreeMap<i64, i64> = BTreeMap::new();
    let mut elf = 1;
    let mut end = target;
    loop {
        let mut house = elf;

        if elf > end {
            return *delivered.iter().find(|(_, &v)| v >= target).unwrap().0
        }
        for _ in 0..50 {
            let entry = delivered.entry(house).or_insert(0);
            *entry += elf * 11;
            if *entry >= target { end = house; break; }
            house += elf;
        }

        elf += 1;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn presents(house: i64) -> i64 {
    let end: i64 = (house as f64).sqrt() as i64;
    let mut accum = 0;
    for i in 1..=end {
        if house % i == 0 {
            let j = house / i;
            accum += i + if i != j { j } else { 0 };
        }
    }

    accum * 10
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day20::{Day20, presents};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        assert_eq!(presents(1), 10);
        assert_eq!(presents(2), 30);
        assert_eq!(presents(3), 40);
        assert_eq!(presents(4), 70);
        assert_eq!(presents(5), 60);
        assert_eq!(presents(6), 120);
        assert_eq!(presents(7), 80);
        assert_eq!(presents(8), 150);
        assert_eq!(presents(9), 130);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(776_160));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(786_240));
    }
}
