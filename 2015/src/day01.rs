use aoc::{self, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day01 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Direction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day01 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day01::Part1;
    type Part2 = crate::day01::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.input.iter().map(|d| d.value()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .enumerate()
            .map(|(e, d)| (e, d.value()))
            .fold((0, 0), |(accum, step), (e, v)| (accum + v, if accum + v < 0 && step == 0 { e + 1 } else { step })).1;
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
pub enum Direction {
    Up,
    Down
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Direction {
    pub fn value(&self) -> i64 {
        match self {
            Direction::Up => 1,
            Direction::Down => -1
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Direction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.chars().next().unwrap() {
            '(' => Ok(Direction::Up),
            ')' => Ok(Direction::Down),
            e => Err(aoc::Error::new(&format!("Unknown direction {}", e)))
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day01::Day01;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs: [(&str, i64); 9] =
            [
                ("(())", 0),
                ("()()", 0),
                ("(((" , 3),
                ("(()(()(" , 3),
                ("))(((((" , 3),
                ("())" , -1),
                ("))(" , -1),
                (")))" , -3),
                (")())())" , -3)
        ];

        for (input, answer) in inputs.iter() {
            let mut day = Day01::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input \"{}\"", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(280));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs: [(&str, usize); 2] =
            [
                (")", 1),
                ("()())", 5)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day01::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer), "Input \"{}\"", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1797));
    }
}
