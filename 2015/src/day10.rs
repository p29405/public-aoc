use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut result = self.input.clone();
        for _ in 0..40 {
            result = step(&result);
        }

        self.part1 = Some(result.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut result = self.input.clone();
        for _ in 0..50 {
            result = step(&result);
        }

        self.part2 = Some(result.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn step(input: &[i64]) -> Vec<i64> {
    let mut count = 0;
    let mut result = Vec::new();
    for i in 0..input.len() {
        if i == 0 {
            count = 1;
        } else if input[i] == input[i - 1] {
            count += 1;
        } else {
            result.push(count);
            result.push(input[i - 1]);
            count = 1;
        }
    }
    result.push(count);
    result.push(*input.last().unwrap());

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day10::{Day10, step};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        assert_eq!(step(&[1]), vec![1, 1]);
        assert_eq!(step(&[1, 1]), vec![2, 1]);
        assert_eq!(step(&[2, 1]), vec![1, 2, 1, 1]);
        assert_eq!(step(&[1, 2, 1, 1]), vec![1, 1, 1, 2, 2, 1]);
        assert_eq!(step(&[1, 1, 1, 2, 2, 1]), vec![3, 1, 2, 2, 1, 1]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(492_982));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6_989_950));
    }
}
