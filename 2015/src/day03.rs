use aoc::{self, Day, Result, Direction, Coord, GridElement, CenteredMap, Map};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Direction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day03::Part1;
    type Part2 = crate::day03::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut position = Coord::default();
        let mut map = CenteredMap::default();
        map.set(&position, House::Visited);

        self.input.iter().for_each(|&d| { position = position.go(d); map.set(&position, House::Visited); });

        self.part1 = Some(map.elements.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut santa = Coord::default();
        let mut robo_santa = Coord::default();
        let mut map = CenteredMap::default();
        map.set(&santa, House::Visited);

        for d in self.input.chunks_exact(2) {
            santa = santa.go(d[0]);
            robo_santa = robo_santa.go(d[1]);
            map.set(&santa, House::Visited);
            map.set(&robo_santa, House::Visited);
        }

        self.part2 = Some(map.elements.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum House {
    NotVisited,
    Visited
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for House {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        House::NotVisited
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for House {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            House::NotVisited => write!(f, "."),
            House::Visited => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for House {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day03::Day03;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs: [(&str, usize); 3] =
            [
                (">", 2),
                ("^>v<", 4),
                ("^v^v^v^v^v", 2),
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input: \"{}\"", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2565));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs: [(&str, usize); 3] =
            [
                ("^v", 3),
                ("^>v<", 3),
                ("^v^v^v^v^v", 11),
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer), "Input: \"{}\"", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2639));
    }
}
