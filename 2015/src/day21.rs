use aoc::{self, Day, Result};
use std::cmp::max;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day21 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub boss: Player
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day21 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day21::Part1;
    type Part2 = crate::day21::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(_input: &str) -> Result<Self> {

        Ok(Self { part1: None, part2: None, boss : Player { hp: 104, damage: 8, armor: 1, cost: 0 }})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = (0..1470)
            .map(|k| {
                let mut player = Player { hp: 100, damage: 0, armor: 0, cost: 0};
                if player.equip(k) {
                    let mut foe = self.boss.clone();

                    while foe.hp > 0 && player.hp > 0 {
                        let damage = max(1, player.damage - foe.armor);
                        foe.hp -= damage;

                        if foe.hp > 0 {
                            let damage = max(1, foe.damage - player.armor);
                            player.hp -= damage;
                        }
                    }

                    if player.hp > 0 {
                        return player.cost;
                    }
                }
                i64::MAX
            })
            .min().unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = (0..1470)
            .map(|k| {
                let mut player = Player { hp: 100, damage: 0, armor: 0, cost: 0};
                if player.equip(k) {
                    let mut foe = self.boss.clone();

                    while foe.hp > 0 && player.hp > 0 {
                        let damage = max(1, player.damage - foe.armor);
                        foe.hp -= damage;

                        if foe.hp > 0 {
                            let damage = max(1, foe.damage - player.armor);
                            player.hp -= damage;
                        }
                    }

                    if foe.hp > 0 {
                        return player.cost;
                    }
                }
                i64::MIN
            })
            .max().unwrap();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Player {
    hp: i64,
    damage: i64,
    armor: i64,
    cost: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Player {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn equip(&mut self, key: usize) -> bool {
        let mut selector = key;
        let weapons = [(8, 4), (10, 5), (25, 6), (40, 7), (74, 8)];
        let armors = [(0, 0), (13, 1), (31, 2), (53, 3), (75, 4), (102, 5)];
        let rings = [(0, 0, 0), (25, 1, 0), (50, 2, 0), (100, 3, 0), (20, 0, 1), (40, 0, 2), (80, 0, 3)];

        let weapon = selector % 5;
        selector /= 5;
        let armor = selector % 6;
        selector /= 6;
        let ring1 = selector % 7;
        selector /= 7;
        let ring2 = selector % 7;

        self.damage += weapons[weapon].1 + rings[ring1].1 + rings[ring2].1;
        self.armor += armors[armor].1  + rings[ring1].2 + rings[ring2].2;
        self.cost += weapons[weapon].0 + armors[armor].0 + rings[ring1].0 + rings[ring2].0;

        ring1 == 0 || ring1 != ring2
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day21::Day21;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(78));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(148));
    }
}
