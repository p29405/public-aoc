use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().map(|s| counts(s)).fold(0, |accum, (code, data)| accum + (code - data));
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter().map(|s| counts(&encode(s)).0 - counts(s).0).sum();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn counts(input: &str) -> (usize, usize) {
    (input.len(), data_count(input))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn data_count(input: &str) -> usize {
    let mut count = 0;
    let mut escape = false;
    let mut i = 1;
    let chars = input.chars().collect::<Vec<_>>();

    loop {
        if i == input.len() - 1 { break; }

        if !escape && chars[i] != '\\' {
            count += 1;
        } else if !escape && chars[i] == '\\' {
            escape = true;
        } else if escape && chars[i] != 'x' {
            count += 1;
            escape = false;
        } else {
            i += 2;
            count += 1;
            escape = false;
        }

        i += 1;
    }

    count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn encode(input: &str) -> String {
    let mut encoded = String::new();
    encoded.push('"');

    for c in input.chars() {
        if c == '\\' {
            encoded.push('\\');
            encoded.push('\\');
        } else if c == '"' {
            encoded.push('\\');
            encoded.push('"');
        } else {
            encoded.push(c);
        }
    }
    encoded.push('"');

    encoded
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::{Day08, counts, encode};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        assert_eq!(counts(r#""""#), (2, 0));
        assert_eq!(counts(r#""abc""#), (5, 3));
        assert_eq!(counts(r#""aaa\"aaa""#), (10, 7));
        assert_eq!(counts(r#""\x27""#), (6, 1));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn encoding() {
        assert_eq!(encode(r#""""#), r#""\"\"""#);
        assert_eq!(encode(r#""abc""#), r#""\"abc\"""#);
        assert_eq!(encode(r#""aaa\"aaa""#), r#""\"aaa\\\"aaa\"""#);
        assert_eq!(encode(r#""\x27""#), r#""\"\\x27\"""#);

        assert_eq!(counts(&encode(r#""""#)).0, 6);
        assert_eq!(counts(&encode(r#""abc""#)).0, 9);
        assert_eq!(counts(&encode(r#""aaa\"aaa""#)).0, 16);
        assert_eq!(counts(&encode(r#""\x27""#)).0, 11);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1371));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2117));
    }
}
