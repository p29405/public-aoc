use aoc::{self, Day, Result};
use std::str::FromStr;
use std::collections::HashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day19 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Replacements>,
    pub medicine: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day19 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day19::Part1;
    type Part2 = crate::day19::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<String>(input, "\n\n")?;

        let medicine = input[1].to_string();
        let input = aoc::read_list_from_string(&input[0], "\n")?;
        Ok(Self { part1: None, part2: None, input, medicine})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(replace(&self.input, &self.medicine).iter().filter(|&m| m != &self.medicine).count());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}
/*
////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(replacements: &[Replacements], medicine: &str) -> i32 {
    let mut possibilities = HashSet::new();
    let mut count = 0;

    possibilities.insert("e".to_owned());

    loop {
        let mut next = HashSet::new();
        for p in possibilities {
            for n in replace(replacements, &p) {
                if n.len() <= medicine.len() {
                    next.insert(n);
                }
            }
        }
        count += 1;
        possibilities = next;
        if possibilities.contains(medicine) {
            break;
        }
    }

    return count;
}
*/
////////////////////////////////////////////////////////////////////////////////////////////////////
fn replace(replacements: &[Replacements], starting: &str) -> HashSet<String> {
    let mut possibilities = HashSet::new();
    let chars = starting.chars().collect::<Vec<_>>();

    for i in 0..chars.len() {
        for replacement in replacements {
            let length = replacement.start.len();
            if i + length > chars.len() { continue; }
            let current = to_string(&chars[i..i+length]);
            if replacement.start == current {
                let mut next = to_string(&chars[0..i]) + &replacement.result;
                if i + length < chars.len() {
                    next += &to_string(&chars[i+length..chars.len()]);
                }
                possibilities.insert(next);
            }
        }
    }

    possibilities
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn reverse(replacements: &[Replacements], starting: &str) -> HashSet<String> {
    let mut possibilities = HashSet::new();
    let chars = starting.chars().collect::<Vec<_>>();

    for i in 0..chars.len() {
        for replacement in replacements {
            let length = replacement.result.len();
            if i + length > chars.len() { continue; }
            let current = to_string(&chars[i..i+length]);
            if replacement.result == current {
                let mut next = to_string(&chars[0..i]) + &replacement.start;
                if i + length < chars.len() {
                    next += &to_string(&chars[i+length..chars.len()]);
                }
                possibilities.insert(next);
            }
        }
    }

    possibilities
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn reduce(replacements: &[Replacements], medicine: &str) -> usize {
    if medicine == "e" {
        return 0;
    }
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn to_string(chars: &[char]) ->String {
    chars.iter().fold("".to_owned(), |string, &c| string + &c.to_string())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Replacements {
    start: String,
    result: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Replacements {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(" => ").collect::<Vec<_>>();

        Ok(Replacements { start: parts[0].to_owned(), result: parts[1].to_owned() })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day19::Day19;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "H => HO
             H => OH
             O => HH

             HOH";

        let mut day = Day19::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4));

        day.medicine = "HOHOHO".to_owned();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(7));

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(576));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), None);
    }
}
/*
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc;
    use crate::{part1, part2, Replacements};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input =
            "H => HO
             H => OH
             O => HH";

        let replacements = aoc::read_list_from_string::<Replacements>(&input, "\n").unwrap();

        assert_eq!(part1(&replacements, "HOH"), 4);
        assert_eq!(part1(&replacements, "HOHOHO"), 7);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part1() {
        let input = aoc::read_list_from_file::<String>("input/day19.txt", "\n\n").unwrap();
        let replacements = aoc::read_list_from_string(&input[0], "\n").unwrap();

        assert_eq!(part1(&replacements, &input[1]), 576);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input =
            "e => H
             e => O
             H => HO
             H => OH
             O => HH";

        let replacements = aoc::read_list_from_string::<Replacements>(&input, "\n").unwrap();

        assert_eq!(part2(&replacements, "HOH"), 3);
        assert_eq!(part2(&replacements, "HOHOHO"), 6);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_part2() {
        let input = aoc::read_list_from_file::<String>("input/day19.txt", "\n\n").unwrap();
        //let replacements = aoc::read_list_from_string(&input[0], "\n").unwrap();

        //assert_eq!(part2(&replacements, &input[1]), 0);
    }

}

 */
