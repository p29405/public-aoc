use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day05 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day05 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day05::Part1;
    type Part2 = crate::day05::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().map(|s| check_part1(s)).filter(|&s| s == Status::Nice ).count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter().map(|s| check_part2(s)).filter(|&s| s == Status::Nice ).count();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn is_vowel(c: char) -> bool {
    matches!(c, 'a'|'e'|'i'|'o'|'u')
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn is_banned(c1: char, c2: char) -> bool {
    (c1 == 'a' && c2 == 'b') || (c1 == 'c' && c2 == 'd') || (c1 == 'p' && c2 == 'q') || (c1 == 'x' && c2 == 'y')
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn check_part1(string: &str) -> Status {
    let mut prev_char: Option<char> = None;
    let mut vowels = 0;
    let mut double_letter = false;
    let mut banned = false;

    string.chars()
          .for_each(|c| {
              if is_vowel(c) {
                  vowels += 1;
              }

              if let Some(prev) = prev_char {
                  if prev == c { double_letter = true; }
                  if is_banned(prev, c) { banned = true; }
              }

              prev_char = Some(c);
          });

    if vowels > 2 && double_letter && !banned { Status::Nice } else { Status::Naughty }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn check_part2(string: &str) -> Status {
    let chars = string.chars().collect::<Vec<char>>();
    let mut pair = false;
    let mut repeat = false;

    'outer: for i in 0..chars.len() - 3 {
        for j in i + 2..chars.len() - 1 {
            if chars[i] == chars[j] && chars[i + 1] == chars[j + 1] {
                pair = true;
                break 'outer;
            }
        }
    }

    for i in 0..chars.len() - 2 {
        if chars[i] == chars[i + 2] {
            repeat = true;
            break;
        }
    }

    if repeat && pair { Status::Nice } else { Status::Naughty }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Status {
    Naughty,
    Nice
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day05::{Day05, Status, check_part1, check_part2};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("ugknbfddgicrmopn", Status::Nice),
                ("jchzalrnumimnmhp", Status::Naughty),
                ("haegwjzuvuyypxyu", Status::Naughty),
                ("dvszwmarrgswjxmb", Status::Naughty)


            ];
        for (input, status) in inputs.iter() {
            assert_eq!(check_part1(input), *status, "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(258));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("qjhvhtzxzqqjkmpb", Status::Nice),
                ("xxyxx", Status::Nice),
                ("uurcxstgmygtbstg", Status::Naughty),
                ("ieodomkazucvgmuy", Status::Naughty)


            ];
        for (input, status) in inputs.iter() {
            assert_eq!(check_part2(input), *status, "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(53));
    }
}
