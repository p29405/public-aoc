use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day11 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day11 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day11::Part1;
    type Part2 = crate::day11::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim().to_owned();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = passwords(&self.input).find(|p| valid(p)).unwrap();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = passwords(&self.part1.as_ref().unwrap()).find(|p| valid(p)).unwrap();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn valid(password: &str) -> bool {
    let characters = password.chars().collect::<Vec<_>>();

    let mut prev = None;
    let mut prev_prev = None;

    let mut straight = false;
    let mut doubles = 0;
    let mut invalid = false;
    let mut check_double = true;

    for c in characters {
        if ['i', 'l', 'o'].contains(&c) {
            invalid = true;
            break;
        }

        if let Some(b) = prev {
            if b == c && check_double{
                doubles += 1;
            } else if let Some(a) = prev_prev {
                if increment(a) == b && increment(b) == c {
                    straight = true;
                }
            }
            check_double = b != c;
        }

        prev_prev = prev.take();
        prev = Some(c);

    }

    straight && !invalid && doubles > 1
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn increment(c: char) -> char {
    (c as u8 + 1) as char
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn passwords(password: &str) -> Passwords {
    Passwords { current: password.chars().collect() }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Passwords {
    current: Vec<char>,
}

//////////////////////////////////////////////////////////////////////////////
impl Iterator for Passwords
{
    type Item = String;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        for i in (0..self.current.len()).rev() {
            if self.current[i] == 'z' {
                self.current[i] = 'a';
            } else {
                self.current[i] = increment(self.current[i]);
                break;
            }
        }

        Some(self.current.iter().fold("".into(), |accum, c| accum + &*c.to_string()))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day11::{Day11, passwords, valid};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_valid() {
        assert!(!valid("hijklmmn"));
        assert!(!valid("abbceffg"));
        assert!(!valid("abbcegjk"));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_password() {
        assert_eq!(passwords("abcd").next(), Some("abce".to_owned()));
        assert_eq!(passwords("xyz").next(), Some("xza".to_owned()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("abcdefgh", "abcdffaa"),
                ("ghijklmn", "ghjaabcc")
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day11::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer.to_string()));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("cqjxxyzz".to_owned()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.part1 = Some("cqjxxyzz".to_owned());
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("cqkaabcc".to_owned()));
    }
}
