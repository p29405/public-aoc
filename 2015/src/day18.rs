use aoc::{self, Day, Result, Grid, Coord, Map, Pixel};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day18 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Pixel>,
    pub steps: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day18 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day18::Part1;
    type Part2 = crate::day18::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input, steps: 100 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut grid = self.input.clone();
        for _ in 0..self.steps {
            let mut step = Grid::new(grid.columns, grid.rows);

            for p in Coord::range(grid.upper_left(), grid.lower_right()) {
                let neighbors_on = grid.count_neighbors(p.x, p.y, |&l| l == Pixel::On);
                let next = match grid.get(&p) {
                    Pixel::On => if neighbors_on == 2 || neighbors_on == 3 {
                        Pixel::On
                    } else {
                        Pixel::Off
                    },
                    Pixel::Off => if neighbors_on == 3 {
                        Pixel::On
                    } else {
                        Pixel::Off
                    }
                };

                step.set(&p, next);
            }
            grid = step;
        }

        self.part1 = Some(grid.elements.iter().fold(0, |accum, &l| accum + (l == Pixel::On) as usize));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut grid = self.input.clone();
        grid.set_xy(0, 0, Pixel::On);
        grid.set_xy(0, grid.rows - 1, Pixel::On);
        grid.set_xy(grid.columns - 1, 0, Pixel::On);
        grid.set_xy(grid.columns - 1, grid.rows - 1, Pixel::On);

        for _ in 0..self.steps {
            let mut step = Grid::new(grid.columns, grid.rows);

            for p in Coord::range(grid.upper_left(), grid.lower_right()) {
                let neighbors_on = grid.count_neighbors(p.x, p.y, |&l| l == Pixel::On);
                let next = match grid.get(&p) {
                    Pixel::On => if neighbors_on == 2 || neighbors_on == 3 {
                        Pixel::On
                    } else {
                        Pixel::Off
                    },
                    Pixel::Off => if neighbors_on == 3 {
                        Pixel::On
                    } else {
                        Pixel::Off
                    }
                };

                step.set(&p, next);
            }
            grid = step;
            grid.set_xy(0, 0, Pixel::On);
            grid.set_xy(0, grid.rows - 1, Pixel::On);
            grid.set_xy(grid.columns - 1, 0, Pixel::On);
            grid.set_xy(grid.columns - 1, grid.rows - 1, Pixel::On);

        }

        self.part2 = Some(grid.elements.iter().fold(0, |accum, &l| accum + (l == Pixel::On) as usize));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day};
    use crate::day18::Day18;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            ".#.#.#
             ...##.
             #....#
             ..#...
             #.#..#
             ####..";

        let mut day = Day18::setup(&input).unwrap();
        day.steps = 4;
        day.run_part1();

        assert_eq!(day.part1_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day18.txt");

        let mut day = Day18::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(821));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day18.txt");

        let mut day = Day18::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(886));
    }
}
