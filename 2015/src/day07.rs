use aoc::{self, Day, Result};
use std::collections::HashMap;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u16;
type Part2 = u16;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day07 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Gate>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day07 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day07::Part1;
    type Part2 = crate::day07::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut wires = HashMap::default();

        self.part1 = Some(evaluate("a", &self.input, &mut wires));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        if let Some(part1) = self.part1 {
            let mut wires = HashMap::default();
            wires.insert("b".to_string(), part1);

            self.part2 = Some(evaluate("a", &self.input, &mut wires));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn get_value(target: &str, input: &[Gate], wires: &mut HashMap<String, u16>) -> u16 {
    if let Ok(value) = target.parse::<u16>() {
        value
    } else {
        evaluate(target, input, wires)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn evaluate(target: &str, input: &[Gate], wires: &mut HashMap<String, u16>) -> u16 {
    if wires.get(target).is_none() {
        if let Some(gate) = input.iter().find(|g| g.destination() == target) {
            match gate {
                Gate::Value(op1, _) => {
                    let value = get_value(op1, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = value;
                },
                Gate::And(op1, op2, _) => {
                    let op1_value = get_value(op1, input, wires);
                    let op2_value = get_value(op2, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = op1_value & op2_value;
                },
                Gate::Or(op1, op2, _) => {
                    let op1_value = get_value(op1, input, wires);
                    let op2_value = get_value(op2, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = op1_value | op2_value;
                },
                Gate::LShift(op1, op2, _) => {
                    let op1_value = get_value(op1, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = op1_value << *op2;
                },
                Gate::RShift(op1, op2, _) => {
                    let op1_value = get_value(op1, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = op1_value >> *op2;
                },
                Gate::Not(op1, _) => {
                    let op1_value = get_value(op1, input, wires);
                    let entry = wires.entry(target.into()).or_insert(0);
                    *entry = !op1_value;
                }
            }
        }
    }

    *wires.get(target).unwrap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Gate {
    Value(String, String),
    And(String, String, String),
    Or(String, String, String),
    LShift(String, u16, String),
    RShift(String, u16, String),
    Not(String, String)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Gate {
    fn destination(&self) -> &str {
        match self {
            Gate::Value(_, dest) => dest,
            Gate::And(_, _, dest) => dest,
            Gate::Or(_, _, dest) => dest,
            Gate::LShift(_, _, dest) => dest,
            Gate::RShift(_, _, dest) => dest,
            Gate::Not(_, dest) => dest
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Gate {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').map(|s| s.into()).collect::<Vec<String>>();

        match parts.len() {
            3 => Ok(Gate::Value(parts[0].to_owned(), parts[2].to_owned())),
            4 => match parts[0].as_str() {
                    "NOT" => Ok(Gate::Not(parts[1].to_owned(), parts[3].to_owned())),
                    _ => Err(aoc::Error::new(&format!("Failed to parse Gate: {}", s)))
            },
            5 => match parts[1].as_str() {
                "AND" => Ok(Gate::And(parts[0].to_owned(), parts[2].to_owned(), parts[4].to_owned())),
                "OR" => Ok(Gate::Or(parts[0].to_owned(), parts[2].to_owned(), parts[4].to_owned())),
                "LSHIFT" => Ok(Gate::LShift(parts[0].to_owned(), parts[2].parse().unwrap(), parts[4].to_owned())),
                "RSHIFT" => Ok(Gate::RShift(parts[0].to_owned(), parts[2].parse().unwrap(), parts[4].to_owned())),
                _ => Err(aoc::Error::new(&format!("Failed to parse Gate: {}", s)))
            },
            _ => Err(aoc::Error::new(&format!("Failed to parse Gate: {}", s)))

        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day07::{Day07, Gate, evaluate};
    use std::collections::HashMap;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "123 -> x
            456 -> y
            x AND y -> d
            x OR y -> e
            x LSHIFT 2 -> f
            y RSHIFT 2 -> g
            NOT x -> h
            NOT y -> i";

        let input = aoc::read_list_from_string::<Gate>(input, "\n").unwrap();
        assert_eq!(input.len(), 8);
        assert_eq!(input[0], Gate::Value("123".into(), "x".into()));
        assert_eq!(input[1], Gate::Value("456".into(), "y".into()));
        assert_eq!(input[2], Gate::And("x".into(), "y".into(), "d".into()));
        assert_eq!(input[3], Gate::Or("x".into(), "y".into(), "e".into()));
        assert_eq!(input[4], Gate::LShift("x".into(), 2, "f".into()));
        assert_eq!(input[5], Gate::RShift("y".into(), 2, "g".into()));
        assert_eq!(input[6], Gate::Not("x".into(), "h".into()));
        assert_eq!(input[7], Gate::Not("y".into(), "i".into()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn evaulate() {
        let input =
            "123 -> x
             456 -> y
             x AND y -> d
             x OR y -> e
             x LSHIFT 2 -> f
             y RSHIFT 2 -> g
             NOT x -> h
             NOT y -> i";

        let input = aoc::read_list_from_string::<Gate>(input, "\n").unwrap();
        let mut wires = HashMap::default();

        assert_eq!(evaluate("x", &input, &mut wires), 123);
        assert_eq!(evaluate("y", &input, &mut wires), 456);
        assert_eq!(evaluate("d", &input, &mut wires), 72);
        assert_eq!(evaluate("e", &input, &mut wires), 507);
        assert_eq!(evaluate("f", &input, &mut wires), 492);
        assert_eq!(evaluate("g", &input, &mut wires), 114);
        assert_eq!(evaluate("h", &input, &mut wires), 65412);
        assert_eq!(evaluate("i", &input, &mut wires), 65079);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(46_065));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.part1 = Some(46_065);
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(14_134));
    }
}
