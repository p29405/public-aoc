use aoc::{self, Coord, Day, Grid, GridElement, Map, Result};
use std::fmt;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day05 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Line>,
    pub map: Grid<Square>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day05 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day05::Part1;
    type Part2 = crate::day05::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input, map: Grid::new(1000, 1000) })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.input.iter().filter(|l| {
                let (dx, dy) = l.step_size();

                dx == 0 || dy == 0
            }).for_each(|l| {
                l.points().for_each(|c| {
                    let current = self.map.get(&c);
                    self.map.set(&c, Square(current.0 + 1));
                })
            });

        let answer = self.map.iter().fold(0, |accum, (_, s)| accum + if s.0 > 1 { 1 } else { 0 });
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.input.iter().filter(|l| {
                let (dx, dy) = l.step_size();

                dx != 0 && dy != 0
            }).for_each(|l| {
                l.points().for_each(|c| {
                    let current = self.map.get(&c);
                    self.map.set(&c, Square(current.0 + 1));
                })
            });

        let answer = self.map.iter().fold(0, |accum, (_, s)| accum + if s.0 > 1 { 1 } else { 0 });
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Square(u32);

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    fn default() -> Self {
        Square(0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0 == 0 {
            write!(f, ".")
        } else {
            write!(f, "{}", self.0)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub struct Line {
    start: Coord,
    end: Coord
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Line {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step_size(&self) -> (i64, i64) {
        let dx = if self.start.x == self.end.x {
            0
        } else if self.start.x < self.end.x {
            1
        } else {
            -1
        };

        let dy = if self.start.y == self.end.y {
            0
        } else if self.start.y < self.end.y {
            1
        } else {
            -1
        };

        (dx, dy)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn points(&self) -> impl Iterator<Item = Coord> {
        
        std::iter::successors(Some((self.start, self.end, self.step_size(), self.start)), 
            |&(start, end, (dx, dy), current)| {
            if current != end {
                Some((start, end, (dx, dy), Coord::new(current.x + dx, current.y + dy)))
            } else {
                None
            }
        }).map(|(_, _, _, current)| current)
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Line {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(" -> ");
        let start = parts.next().unwrap().parse().unwrap();
        let end = parts.next().unwrap().parse().unwrap();

        Ok(Self { start, end })

    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Coord, Day};
    use crate::day05::{Day05, Line};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn points() {
        let line = Line { start: Coord::new(1, 1), end: Coord::new(4, 1)};

        let mut points = line.points();

        assert_eq!(points.next(), Some(Coord::new(1, 1)));
        assert_eq!(points.next(), Some(Coord::new(2, 1)));
        assert_eq!(points.next(), Some(Coord::new(3, 1)));
        assert_eq!(points.next(), Some(Coord::new(4, 1)));
        assert_eq!(points.next(), None);

        let line = Line { start: Coord::new(4, 4), end: Coord::new(1, 1)};

        points = line.points();

        assert_eq!(points.next(), Some(Coord::new(4, 4)));
        assert_eq!(points.next(), Some(Coord::new(3, 3)));
        assert_eq!(points.next(), Some(Coord::new(2, 2)));
        assert_eq!(points.next(), Some(Coord::new(1, 1)));
        assert_eq!(points.next(), None);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "0,9 -> 5,9
             8,0 -> 0,8
             9,4 -> 3,4
             2,2 -> 2,1
             7,0 -> 7,4
             6,4 -> 2,0
             0,9 -> 2,9
             3,4 -> 1,4
             0,0 -> 8,8
             5,5 -> 8,2";

        let mut day = Day05::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = 
            "0,9 -> 5,9
             8,0 -> 0,8
             9,4 -> 3,4
             2,2 -> 2,1
             7,0 -> 7,4
             6,4 -> 2,0
             0,9 -> 2,9
             3,4 -> 1,4
             0,0 -> 8,8
             5,5 -> 8,2";

        let mut day = Day05::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(12));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(8_622));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day05.txt");

        let mut day = Day05::setup(&input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(22_037));
    }
}