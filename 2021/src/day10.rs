use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<String>(input, "\n")?;
        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.input.iter().map(|l| syntax_error_score(l)).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut scores = self.input.iter().map(|l| auto_complete_score(l)).filter(|&s| s != 0).collect::<Vec<_>>();
        scores.sort_unstable();

        self.part2 = Some(scores[scores.len() / 2]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn value(c: char) -> usize {
    match c {
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4,
        ')' => 3,
        ']' => 57,
        '}' => 1_197,
        '>' => 25_137,
        _ => unreachable!("Shouldn't Exist {}", c)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid(c: char, stack: &mut Vec<char>) -> bool {
    match c {
        ')' => Some('(') == stack.pop(),
        ']' => Some('[') == stack.pop(),
        '}' => Some('{') == stack.pop(),
        '>' => Some('<') == stack.pop(),
        _ => {
            stack.push(c);
            true
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn syntax_error_score(line: &str) -> usize {
    let mut stack = Vec::new();
    stack.reserve(line.len() / 2);

    for c in line.chars() {
        if !valid(c, &mut stack) {
            return value(c);
        }
    }

    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn auto_complete_score(line: &str) -> usize {
    let mut stack = Vec::new();
    stack.reserve(line.len() / 2);

    for c in line.chars() {
        if !valid(c, &mut stack) {
            return 0;
        }
    }

    stack.iter().rev().fold(0, |accum, &c| accum * 5 + value(c))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day10::Day10;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "[({(<(())[]>[[{[]{<()<>>
             [(()[<>])]({[<{<<[]>>(
             {([(<{}[<>[]}>{[]{[(<()>
             (((({<>}<{<{<>}{[]{[]{}
             [[<[([]))<([[{}[[()]]]
             [{[{({}]{}}([{[{{{}}([]
             {<[[]]>}<{[{[{[]{()[[[]
             [<(<(<(<{}))><([]([]()
             <{([([[(<>()){}]>(<<{{
             <{([{{}}[<[[[<>{}]]]>[]]";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(26_397));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "[({(<(())[]>[[{[]{<()<>>
             [(()[<>])]({[<{<<[]>>(
             {([(<{}[<>[]}>{[]{[(<()>
             (((({<>}<{<{<>}{[]{[]{}
             [[<[([]))<([[{}[[()]]]
             [{[{({}]{}}([{[{{{}}([]
             {<[[]]>}<{[{[{[]{()[[[]
             [<(<(<(<{}))><([]([]()
             <{([([[(<>()){}]>(<<{{
             <{([{{}}[<[[[<>{}]]]>[]]";

        let mut day = Day10::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(288_957));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(318_081));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4_361_305_341));
    }
}