use aoc::{self, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Line>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.input.iter().map(|l| l.count_uniques()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(self.input.iter().map(|l| l.decode()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Line {
    pub input: [u8; 10],
    pub output: [u8; 4]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Line {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn count_uniques(&self) -> usize {
        self.output.iter().filter(|l| {
            let bits = l.count_ones();
            bits == 2 || bits == 3 || bits == 4 || bits == 7
        }).count()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn decode(&self) -> usize {
        let mut nums = [0; 10];

        for &l in self.input.iter() {
            match l.count_ones() {
                2 => nums[1] = l,
                3 => nums[7] = l,
                4 => nums[4] = l,
                5=> {
                    if l & nums[1] == nums[1] {
                        nums[3] = l;
                    } else if (l & nums[4]).count_ones() == 3 {
                        nums[5] = l;
                    } else {
                        nums[2] = l;
                    }
                },
                6 => {
                    if l & nums[1] != nums[1] {
                        nums[6] = l;
                    } else if l & nums[4] == nums[4] {
                        nums[9] = l;
                    } else {
                        nums[0] = l;
                    }
                },
                7 => nums[8] = l,
                _ => unreachable!("Invalid number of bits {}", l.count_ones())
            }
        }

        self.output.iter().fold(0, |acc, &l| acc * 10 + value_bit(l, &nums))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn value_bit(s: u8, numbers: &[u8; 10]) -> usize {
    numbers.iter().enumerate().find(|(_, &n)| s == n).unwrap().0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn convert(s1: &str) -> u8 {
    s1.chars().fold(0, |acc, c| 
        acc | match c {
            'a'=> 0b0000_0001,
            'b'=> 0b0000_0010,
            'c'=> 0b0000_0100,
            'd'=> 0b0000_1000,
            'e'=> 0b0001_0000,
            'f'=> 0b0010_0000,
            'g'=> 0b0100_0000,
            _ => 0
        })
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Line {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(" | ");
        let mut input = [0; 10];
        let mut output = [0; 4];

        for (i, s) in parts.next().unwrap().split(" ").enumerate() {
            input[i] = convert(s);
        }
        for (i, s) in parts.next().unwrap().split(" ").enumerate() {
            output[i] = convert(s);
        }

        input.sort_unstable_by_key(|i| i.count_ones());
        Ok(Self { input, output })

    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::Day08;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
             edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
             fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
             fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
             aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
             fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
             dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
             bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
             egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
             gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

        let mut day = Day08::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(26));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = 
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
             edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
             fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
             fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
             aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
             fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
             dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
             bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
             egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
             gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

        let mut day = Day08::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(61_229));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(548));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_074_888));
    }
}