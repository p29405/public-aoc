use aoc::{self, Day, Result};
use std::collections::HashMap;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub template: Vec<char>,
    pub rules: Vec<Rule>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day14::Part1;
    type Part2 = crate::day14::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");

        let template = parts.next().unwrap().chars().collect();
        let rules = aoc::read_list_from_string(parts.next().unwrap(), "\n")?;


        Ok(Self { part1: None, part2: None, template, rules})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut memo = HashMap::new();
        let mut elements = HashMap::new();

        for pair in self.template.as_slice().windows(2) {
            let current = descent(pair[0], pair[1], &self.rules, &mut memo, 10);
            elements = combine(elements, current);
            let entry = elements.entry(pair[0]).or_insert(0);
            *entry += 1;
        }

        let entry = elements.entry(*self.template.last().unwrap()).or_insert(0);
        *entry += 1;
        
        let max = elements.iter().max_by_key(|&(_, count)| *count).unwrap().1;
        let min = elements.iter().min_by_key(|&(_, count)| *count).unwrap().1;
        self.part1 = Some(*max - *min);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut memo = HashMap::new();
        let mut elements = HashMap::new();

        for pair in self.template.as_slice().windows(2) {
            let current = descent(pair[0], pair[1], &self.rules, &mut memo, 40);
            elements = combine(elements, current);
            let entry = elements.entry(pair[0]).or_insert(0);
            *entry += 1;
        }

        let entry = elements.entry(*self.template.last().unwrap()).or_insert(0);
        *entry += 1;
        
        let max = elements.iter().max_by_key(|&(_, count)| *count).unwrap().1;
        let min = elements.iter().min_by_key(|&(_, count)| *count).unwrap().1;
        self.part2 = Some(*max - *min);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn descent(first: char, second: char, rules: &[Rule], 
             memo: &mut HashMap<Key, HashMap<char, usize>>, depth: usize) -> HashMap<char, usize> {
    if depth == 0 {
        return HashMap::new();
    }

    if let Some(elements) = memo.get(&Key { first, second, depth }) {
        return elements.clone();
    }

    let mut elements = HashMap::new();

    if let Some(rule) = rules.iter().find(|r| r.pair == (first, second)) {
        let entry = elements.entry(rule.insert).or_insert(0);
        *entry += 1;

        let first_half = descent(first, rule.insert, rules, memo, depth - 1);
        let second_half = descent(rule.insert, second, rules, memo, depth - 1);
        let combined = combine(first_half, second_half);
        elements = combine(elements, combined);
    } else {
        panic!("Rule not found for {}{}", first, second);
    }

    memo.insert(Key { first, second, depth }, elements.clone());
    elements
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn combine(e1: HashMap<char, usize>, e2: HashMap<char, usize>) -> HashMap<char, usize> {
    let mut result = e1.clone();

    for (c, size) in e2 {
        let entry = result.entry(c).or_insert(0);
        *entry += size;
    }

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Key {
    pub first: char,
    pub second: char,
    pub depth: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Rule {
    pub pair: (char, char),
    pub insert: char
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Rule {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(" -> ");
        let mut pair_chars = parts.next().unwrap().chars();
        let pair = (pair_chars.next().unwrap(), pair_chars.next().unwrap());
        let insert = parts.next().unwrap().chars().next().unwrap();
        Ok(Self { pair, insert })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::Day14;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "NNCB

             CH -> B
             HH -> N
             CB -> H
             NH -> C
             HB -> C
             HC -> B
             HN -> C
             NN -> C
             BH -> H
             NC -> B
             NB -> B
             BN -> B
             BB -> N
             BC -> B
             CC -> N
             CN -> C";

        let mut day = Day14::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_588));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = 
            "NNCB

             CH -> B
             HH -> N
             CB -> H
             NH -> C
             HB -> C
             HC -> B
             HN -> C
             NN -> C
             BH -> H
             NC -> B
             NB -> B
             BN -> B
             BB -> N
             BC -> B
             CC -> N
             CN -> C";

        let mut day = Day14::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2_188_189_693_529));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2_891));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4_607_749_009_683));
    }
}