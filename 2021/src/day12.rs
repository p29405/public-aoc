use aoc::{self, Day, Result};
use std::collections::HashMap;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day12 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: HashMap<String, Vec<String>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day12 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day12::Part1;
    type Part2 = crate::day12::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let segments: Vec::<Segment> = aoc::read_list_from_string(input, "\n")?;
        let mut input = HashMap::new();

        for s in segments {
            let entry = input.entry(s.start.clone()).or_insert(Vec::new());
            (*entry).push(s.end.clone());
            let reverse = input.entry(s.end).or_insert(Vec::new());
            (*reverse).push(s.start);
        }

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut visited = HashMap::new();
        let answer = traverse("start", "end", &self.input, &mut visited, 1);
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> {
        self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut visited = HashMap::new();
        let answer = traverse("start", "end", &self.input, &mut visited, 2);
        self.part2 =  Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn traverse(start: &str, end: &str, map: &HashMap<String, Vec<String>>, visited: &mut HashMap<String, usize>, limit: usize) -> usize {
    let mut paths = 0;
    if start.chars().next().unwrap().is_ascii_lowercase() {
        let entry = visited.entry(start.to_owned()).or_insert(0);
        *entry += 1;
    }

    if let Some(destination) = map.get(start) {
        for d in destination.iter().filter(|&d| d != "start") {
            if d == end {
                paths += 1;
            } else {
                if visited.contains_key(d) {
                    if visited.values().any(|&d| d >= limit) {
                        continue;
                    }
                }
                paths += traverse(d, end, map, &mut visited.clone(), limit);
            }
        }
    }

    paths
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Segment {
    pub start: String,
    pub end: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Segment {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split("-");
        let start = parts.next().unwrap().to_owned();
        let end = parts.next().unwrap().to_owned();
        Ok(Self { start, end })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day12::Day12;

const CAVES1: &str =
    "start-A
     start-b
     A-c
     A-b
     b-d
     A-end
     b-end";

const CAVES2: &str =
    "dc-end
     HN-start
     start-kj
     dc-start
     dc-HN
     LN-dc
     HN-end
     kj-sa
     kj-HN
     kj-dc";

const CAVES3: &str =
    "fs-end
     he-DX
     fs-he
     start-DX
     pj-DX
     end-zg
     zg-sl
     zg-pj
     pj-he
     RW-he
     fs-DX
     pj-RW
     zg-RW
     start-pj
     he-WI
     zg-he
     pj-fs
     start-RW";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = [(CAVES1, 10), (CAVES2, 19), (CAVES3, 226)];

        for (cave, expected) in input {
            let mut day = Day12::setup(cave).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(expected));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = [(CAVES1, 36), (CAVES2, 103), (CAVES3, 3_509)];

        for (cave, expected) in input {
            let mut day = Day12::setup(cave).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(expected));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(3_485));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(85_062));
    }
}