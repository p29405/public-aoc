use aoc::{self, Coord, Day, Grid, GridElement, Map, Result};
use std::{fmt};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day15 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Risk>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day15 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let path = find_paths(&self.input);
        self.part1 = path.elements.last().map(|p| p.cost());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let grid = Grid::combine(&expand(&self.input), 5, 5);
        let path = find_paths(&grid);
        self.part2 = path.elements.last().map(|p| p.cost());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn expand(start: &Grid<Risk>) -> Vec<Grid<Risk>> {
    let mut expanded = vec![];
    expanded.push(start.clone());

    for y in 0..5usize {
        let x_start = if y == 0 { 1 } else { 0 };
        for x in x_start..5 {
            let mut next = if y == 0 {
                expanded[x - 1].clone()
            } else {
                expanded[(y - 1) * 5 + x].clone()
            };

            for e in next.elements.iter_mut() {
                let mut level = e.level + 1;
                if level == 10 {
                    level = 1;
                }
                *e = Risk { level };
            }

            expanded.push(next);
        }
    }

    expanded
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Risk {
    level: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Risk {
    fn default() -> Self {
        Risk { level: 0 }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Risk {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.level)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Risk {
    fn to_element(c: char) -> Self {
        let level = c.to_string().parse().unwrap();
        Risk { level }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn cost(map: &Grid<Risk>) -> Grid<Path> {
    let mut paths = Grid::new(map.columns, map.rows);

    paths.set_xy(0, 0, Path::Cost(0));

    for (c, risk) in map.iter().skip(1) {
        let p = paths.adjacent(c.x, c.y).filter(|&p| p != Path::Unreachable).min_by_key(|p| p.cost()).unwrap();
        paths.set(&c, Path::Cost(p.cost() + risk.level));
    }

    paths
}


////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn find_paths(map: &Grid<Risk>) -> Grid<Path> {
    let mut paths = Grid::new(map.columns, map.rows);
    let mut frontier = vec![];
 
    frontier.push(Coord::new(0, 0));
    paths.set_xy(0, 0, Path::Cost(0));

    loop {
        let mut next = vec![];

        for p in &frontier {
            for n in p.adjacent() {
                if n.x < 0 || n.y < 0 || n.x >= map.columns || n.y >= map.rows { continue; }
                if let Path::Unreachable = paths.get(&n) {
                    paths.set(&n, Path::Cost(paths.get(p).cost() + map.get(&n).level));
                    next.push(n);
                } else if paths.get(&n).cost() > paths.get(p).cost() + map.get(&n).level {
                    paths.set(&n, Path::Cost(paths.get(p).cost() + map.get(&n).level));
                    next.push(n);
                }
            }
        }

        frontier = next;

        if frontier.is_empty() {
            break;
        }
    }

    paths
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Path {
    Cost(usize),
    Unreachable
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn cost(&self) -> usize {
        if let Path::Cost(c) = self {
            *c
        } else {
            panic!("");
        }
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Path::Unreachable
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Path::Cost(c) => write!(f, "{:3}", c),
            Path::Unreachable => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Path {}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day15::Day15;

    const TEST_INPUT: &str =
        "1163751742
         1381373672
         2136511328
         3694931569
         7463417111
         1319128137
         1359912421
         3125421639
         1293138521
         2311944581";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {

        let mut day = Day15::setup(TEST_INPUT).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(40));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(619));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let mut day = Day15::setup(TEST_INPUT).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(315));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), None);
    }
}