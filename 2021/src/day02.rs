use aoc::{self, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Command>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().fold((0, 0), |(horizontal, verticle), c| {
            match c.direction {
                Direction::Forward => return (horizontal + c.value, verticle),
                Direction::Up => return (horizontal, verticle - c.value),
                Direction::Down => return (horizontal, verticle + c.value)
            }
        });
        self.part1 = Some(answer.0 * answer.1);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {

        let answer = self.input.iter().fold((0, 0, 0), |(horizontal, verticle, aim), c| {
            match c.direction {
                Direction::Forward => return (horizontal + c.value, verticle + c.value * aim, aim),
                Direction::Up => return (horizontal, verticle, aim  - c.value),
                Direction::Down => return (horizontal, verticle, aim + c.value)
            }
        });
        self.part2 = Some(answer.0 * answer.1);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub enum Direction {
    Forward,
    Up,
    Down,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Direction {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "forward" => Ok(Direction::Forward),
            "up" => Ok(Direction::Up),
            "down" => Ok(Direction::Down),
            _ => Err(aoc::Error::new(&format!("Unable to match {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub struct Command {
    direction: Direction,
    value: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Command {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        Ok(Self { direction: parts[0].parse()?, value: parts[1].parse().unwrap() })
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "forward 5
             down 5
             forward 8
             up 3
             down 8
             forward 2";

        let mut day = Day02::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(150));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "forward 5
             down 5
             forward 8
             up 3
             down 8
             forward 2";

        let mut day = Day02::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(900));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_636_725));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_872_757_425));
    }
}