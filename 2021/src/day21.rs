use aoc::{self, Day, Result};
use std::cmp::{min, max};
use std::collections::HashMap;
use std::mem::swap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day21 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub player1: usize,
    pub player2: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day21 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day21::Part1;
    type Part2 = crate::day21::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(_input: &str) -> Result<Self> {
        Ok(Self { part1: None, part2: None, player1: 2, player2: 1})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut dice = DeterministicDice::new();
        let mut score = (0, 0);
        let mut position = (self.player1 - 1, self.player2 - 1);

        'game: loop {
            for p in [Player::One, Player::Two] {
                let (score, position) = match p {
                    Player::One => (&mut score.0, &mut position.0),
                    Player::Two => (&mut score.1, &mut position.1)
                };

                let next = move_player(*position, dice.roll());

                *score += next + 1;
                *position = next;

                if *score >= 1000 {
                    break 'game;
                }
            }
        }

        let answer = dice.rolls() * min(score.0, score.1);
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // fn run_part2(&mut self) {
    //     let mut winner = (0, 0);
    //     let mut universes = vec![Universe::new((self.player1, self.player2))];

    //     while !universes.is_empty() {
    //         let (p1, wins) = Universe::player_move(&universes, Player::One);
    //         winner.0 += wins;

    //         let (p2, wins)  = Universe::player_move(&p1, Player::Two);
    //         universes = p2;
    //         winner.1 += wins;
    //     }

        
    //     self.part2 = Some(max(winner.0, winner.1));
    // }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut winner = (0, 0);
        let mut universes = HashMap::new();
        let start = Universe::new((self.player1, self.player2));

        universes.insert(start.pack(), start.number);

        while !universes.is_empty() {
            let wins = Universe::round(&mut universes, Player::One);
            winner.0 += wins;

            let wins  = Universe::round(&mut universes, Player::Two);
            winner.1 += wins;
        }
        
        self.part2 = Some(max(winner.0, winner.1));
    }
 
    // fn run_part2(&mut self) {
    //     let mut winner = (0, 0);
    //     let mut universes = [0; 44100];
    //     let start = Universe::new((self.player1, self.player2));

    //     universes[start.pack()] = start.number;

    //     while !universes.is_empty() {
    //         let wins = Universe::advance(&mut universes, Player::One);
    //         winner.0 += wins;

    //         let wins  = Universe::advance(&mut universes, Player::Two);
    //         winner.1 += wins;
    //     }
        
    //     self.part2 = Some(max(winner.0, winner.1));
    // }
    //////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct DeterministicDice {
    pub state: usize,
    pub rolls: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl DeterministicDice {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new() -> Self {
        Self { state: 1, rolls: 0 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn roll(&mut self) -> usize {
        self.rolls += 3;

        match self.state {
            98 => {
                self.state = 1;
                98 + 99 + 100
            },
            99 => {
                self.state = 2;
                99 + 100 + 1
            },
            100 => {
                self.state = 3;
                100 + 1 + 2
            },
            _ => {
                let n = self.state + 1;
                self.state += 3;
                3 * n
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn rolls(&self) -> usize {
        self.rolls
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn dirac_dice() -> [(usize, usize); 7] {
    [ 
        (3, 1),
        (4, 3),
        (5, 6),
        (6, 7),
        (7, 6),
        (8, 3),
        (9, 1)
    ]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn move_player(position: usize, steps: usize) -> usize {
    (position + steps) % 10
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug)]
pub enum Player {
    One,
    Two
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug)]
pub struct Universe {
    pub position: (usize, usize),
    pub score: (usize, usize),
    pub number: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Universe {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(start: (usize, usize)) -> Universe {
        Universe { position: (start.0 - 1, start.1 - 1), score: (0, 0), number: 1 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn player_move(universe: &[Universe], p: Player) -> (Vec<Universe>, usize) {
        let mut next = vec![];
        let mut wins = 0;

        for u in universe {
            for (roll, number) in dirac_dice() {
                let mut new = u.clone();

                new.number *= number;

                let (score, position) = match p {
                    Player::One => (&mut new.score.0, &mut new.position.0),
                    Player::Two => (&mut new.score.1, &mut new.position.1),
                };

                let updated = move_player(*position, roll);

                *score += updated + 1;
                *position = updated;

                if *score >= 21 {
                    wins += new.number;
                } else {
                    next.push(new);
                }
            }
        }
        (next, wins)    
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn round(universes: &mut HashMap<usize, usize>, p: Player) -> usize {
        let mut wins = 0;
        let keys = universes.iter().map(|(&k, &v)| (k, v)).collect::<Vec<_>>();
        universes.clear();
        for (i, scale) in keys {
            for (roll, number) in dirac_dice() {
                let mut new = Universe::unpack(i, scale * number);

                let (score, position) = match p {
                    Player::One => (&mut new.score.0, &mut new.position.0),
                    Player::Two => (&mut new.score.1, &mut new.position.1),
                };

                let next = move_player(*position, roll);

                *score += next + 1;
                *position = next;

                if *score >= 21 {
                    wins += new.number;
                } else {
                    let entry = universes.entry(new.pack()).or_insert(0);
                    *entry += new.number;
                }
            }
        }
        wins
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn advance(universes: &mut [usize; 44100], p: Player) -> usize {
        let mut wins = 0;
        let mut new_universes = [0; 44100];
        for index in 0..44100 {
            if universes[index] == 0 { continue; }

            for (roll, number) in dirac_dice() {
                let mut new = Universe::unpack(index, universes[index] * number);

                let (score, position) = match p {
                    Player::One => (&mut new.score.0, &mut new.position.0),
                    Player::Two => (&mut new.score.1, &mut new.position.1),
                };

                let next = move_player(*position, roll);

                *score += next + 1;
                *position = next;

                if *score >= 21 {
                    wins += new.number;
                } else {
                    new_universes[new.pack()] += new.number;
                }
            }
        }
        swap(universes, &mut new_universes);
        wins
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn pack(&self) -> usize {
        ((self.score.0 * 10 + self.position.0) << 8) | (self.score.1 * 10 + self.position.1)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn unpack(packed: usize, number: usize) -> Universe {
        let position = (((packed >> 8) & 0xFF) % 10, (packed & 0xFF) % 10);
        let score = (((packed >> 8) & 0xFF) / 10, (packed & 0xFF) / 10);

        Self { position, score, number }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// Dirac
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day21::{Day21, Universe};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn pack() {
        let mut u = Universe::new((0, 0));

        for i in 0..10 {
            for j in 0..10 {
                for k in 0..21 {
                    for l in 0..21 {
                        u.score = (k, l);
                        u.position = (i, j);

                        let unpacked = Universe::unpack(u.pack(), 1);
                        println!("Packed {:?}\nUnPacked {:?}", u, unpacked);
                        assert_eq!(unpacked.position, (i, j));
                        assert_eq!(unpacked.score, (k, l));
                
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let mut day = Day21::setup("").unwrap();
        day.player1 = 4;
        day.player2 = 8;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(739_785));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let mut day = Day21::setup("").unwrap();
        day.player1 = 4;
        day.player2 = 8;
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(444_356_092_776_315));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(797_160));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(27_464_148_626_406));
    }
}