use aoc::{self, Day, Result, Coord, Grid, Pixel, Map, to_string, AocFontType};
use std::str::FromStr;
use std::cmp::max;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub grid: Grid<Pixel>,
    pub folds: Vec<Fold>,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day13::Part1;
    type Part2 = crate::day13::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");

        let coords : Vec<Coord> = aoc::read_list_from_string(parts.next().unwrap(), "\n")?;
        let folds = aoc::read_list_from_string(parts.next().unwrap(), "\n")?;

        let mut max_x = 0;
        let mut max_y = 0;

        for c in coords.iter() {
            max_x = max(c.x, max_x);
            max_y = max(c.y, max_y);
        }

        let mut grid: Grid<Pixel> = Grid::new(max_x + 1, max_y + 1);

        for c in coords.iter() {
            grid.set(&c, Pixel::On);
        }

        Ok(Self { part1: None, part2: None, grid, folds})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let next = fold_map(&self.grid, &self.folds[0]);
        let answer = next.elements.iter().filter(|e| e.on()).count();

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let result = self.folds.iter().fold(self.grid.clone(), |grid, f| fold_map(&grid, f));

        self.part2 = to_string(&result, Pixel::On, AocFontType::Aoc2016);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn fold_map(map: &Grid<Pixel>, fold: &Fold) -> Grid<Pixel> {
    let mut next: Grid<Pixel>;
    if fold.axis == Axis::Horizontal {
        next = Grid::new(map.columns, fold.line);

        for y in 0..fold.line {
            for x in 0..map.columns {
                let pixel = if map.get_xy(x, fold.line + y + 1).on() || map.get_xy(x, fold.line - y - 1).on() {
                    Pixel::On
                } else {
                    Pixel::Off
                };
                next.set_xy(x, fold.line - y - 1, pixel);
            }
        }
    } else {
        next = Grid::new(fold.line, map.rows);

        for y in 0..map.rows {
            for x in 0..fold.line {
                let pixel = if map.get_xy(fold.line + x + 1, y).on() || map.get_xy(fold.line - x - 1, y).on() {
                    Pixel::On
                } else {
                    Pixel::Off
                };

                next.set_xy(fold.line - x - 1, y, pixel);
            }
        }
    }

    next
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Axis {
    Horizontal,
    Vertical
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Axis {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        if s == "y" {
            Ok(Axis::Horizontal)
        } else {
            Ok(Axis::Vertical)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Fold {
    axis: Axis,
    line: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Fold {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let words = s.split(" ");
        let position = words.skip(2).next().unwrap();
        let mut parts = position.split("=");
        let axis = parts.next().unwrap().parse().unwrap();
        let line = parts.next().unwrap().parse().unwrap();
        Ok(Self { axis, line })
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day13::Day13;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "6,10
             0,14
             9,10
             0,3
             10,4
             4,11
             6,0
             6,12
             4,1
             0,13
             10,12
             3,4
             3,0
             8,4
             1,10
             2,14
             8,10
             9,0

             fold along y=7
             fold along x=5";

        let mut day = Day13::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(17));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(745));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("ABKJFBGC".to_owned()));
    }
}