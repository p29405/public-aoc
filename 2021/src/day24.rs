use std::collections::VecDeque;
use aoc::{self, Day, Result};
use std::ops::IndexMut;
use std::str::FromStr;
use crate::day24::Instruction::{Add, Div, Eql, Input, Mod, Mul};
use crate::day24::Value::{Immediate, Reg};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day24 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day24 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = Part1;
    type Part2 = Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        // let upper = 99_999_999_999_999;
        // let lower = 11_111_111_111_111;
        let mut model = [9; 14];
        loop {
            let mut alu = Alu { registers: [0; 4], input: model.into() };

            for instruction in self.input.iter() {
                alu.execute(instruction);
            }

            println!("{:?} => {}", model, alu.registers[Register::Z.offset()]);

            if alu.registers[Register::Z.offset()] == 0 {
                break;
            }

            next(&mut model);
        }

        self.part1 = Some(model.iter().fold(0, |accum, m| accum * 10 + m));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn next(model: &mut [i64; 14]) {
    for i in (0..14).rev() {
        model[i] -= 1;

        if model[i] == 0 {
            model[i] = 9
        } else {
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Copy, Clone)]
pub enum Register {
    W,
    X,
    Y,
    Z
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Register {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn offset(&self) -> usize {
        match self {
            Register::W => 0,
            Register::X => 1,
            Register::Y => 2,
            Register::Z => 3
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Register {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "w" => Ok(Register::W),
            "x" => Ok(Register::X),
            "y" => Ok(Register::Y),
            "z" => Ok(Register::Z),
            _ => Err(aoc::Error::new(&format!("Failed to parse Register: {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Copy, Clone)]
pub enum Value {
    Reg(Register),
    Immediate(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Value {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        if let Ok(r) = s.parse::<Register>() {
            Ok(Reg(r))
        } else {
            Ok(Immediate(s.parse().unwrap()))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Copy, Clone)]
pub enum Instruction {
    Input(Register),
    Add(Register, Value),
    Mul(Register, Value),
    Div(Register, Value),
    Mod(Register, Value),
    Eql(Register, Value)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');

        let op = parts.next().unwrap();

        match op {
            "inp" => Ok(Input(parts.next().unwrap().parse().unwrap())),
            "add" => Ok(Add(parts.next().unwrap().parse().unwrap(), parts.next().unwrap().parse().unwrap())),
            "mul" => Ok(Mul(parts.next().unwrap().parse().unwrap(), parts.next().unwrap().parse().unwrap())),
            "div" => Ok(Div(parts.next().unwrap().parse().unwrap(), parts.next().unwrap().parse().unwrap())),
            "mod" => Ok(Mod(parts.next().unwrap().parse().unwrap(), parts.next().unwrap().parse().unwrap())),
            "eql" => Ok(Eql(parts.next().unwrap().parse().unwrap(), parts.next().unwrap().parse().unwrap())),
            _ => Err(aoc::Error::new(&format!("Failed to parse Instruction: {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Alu {
    pub registers: [i64; 4],
    pub input: VecDeque<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Alu {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn register(&mut self, r: &Register) -> &mut i64 {
        self.registers.index_mut(r.offset())
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fetch(&self, v: &Value) -> i64 {
        match *v {
            Immediate(i) => i,
            Reg(r) => self.registers[r.offset()]
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: &Instruction) {
        match instruction {
            Input(r) => *self.register(r) = self.input.pop_front().unwrap(),
            Add(r, v) => *self.register(r) = *self.register(r) + self.fetch(v),
            Mul(r, v) => *self.register(r) = *self.register(r) * self.fetch(v),
            Div(r, v) => {
                let value = self.fetch(v);

                if value != 0 {
                    *self.register(r) = *self.register(r) / value;
                } else {
                    panic!("Unable to divide by zero");
                }
            },
            Mod(r, v) => {
                let value = self.fetch(v);
                if value > 0 {
                    *self.register(r) = *self.register(r) % value;
                } else {
                    panic!("Invalid mod divisor {}", value);
                }
            },
            Eql(r, v) => *self.register(r) = if *self.register(r) == self.fetch(v) { 1 } else { 0 },
        };
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day24::{Alu, Day24, Register};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn negation() {
        let input =
            "inp x
             mul x -1";

        let instructions = aoc::read_list_from_string(input, "\n").unwrap();
        let mut alu = Alu { registers: [0; 4], input: vec![5].into() };
        for instruction in instructions.iter() {
            alu.execute(instruction);
        }

        assert_eq!(alu.registers[Register::X.offset()], -5);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn three_times_larger() {
        let input =
            "inp z
             inp x
             mul z 3
             eql z x";

        let instructions = aoc::read_list_from_string(input, "\n").unwrap();

        let tests =
            [
                ((5, 15), 1),
                ((5, 14), 0),
                ((5, 16), 0),
                ((-1, -3), 1),
                ((0, 0), 1),

            ];

        for ((i1, i2), result) in tests {
            let mut alu = Alu { registers: [0; 4], input: vec![i1, i2].into() };
            for instruction in instructions.iter() {
                alu.execute(instruction);
            }
            assert_eq!(alu.registers[Register::Z.offset()], result);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn to_binary() {
        let input =
            "inp w
             add z w
             mod z 2
             div w 2
             add y w
             mod y 2
             div w 2
             add x w
             mod x 2
             div w 2
             mod w 2";

        let instructions = aoc::read_list_from_string(input, "\n").unwrap();

        let tests =
            [
                (0, [0, 0, 0, 0]),
                (1, [0, 0, 0, 1]),
                (2, [0, 0, 1, 0]),
                (3, [0, 0, 1, 1]),
                (4, [0, 1, 0, 0]),
                (5, [0, 1, 0, 1]),
                (6, [0, 1, 1, 0]),
                (7, [0, 1, 1, 1]),
                (8, [1, 0, 0, 0]),
                (9, [1, 0, 0, 1]),
                (10, [1, 0, 1, 0]),
                (11, [1, 0, 1, 1]),
                (12, [1, 1, 0, 0]),
                (13, [1, 1, 0, 1]),
                (14, [1, 1, 1, 0]),
                (15, [1, 1, 1, 1]),

            ];

        for (input, registers) in tests {
            let mut alu = Alu { registers: [0; 4], input: vec![input].into() };
            for instruction in instructions.iter() {
                alu.execute(instruction);
            }
            assert_eq!(alu.registers, registers);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), None);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), None);
    }
}