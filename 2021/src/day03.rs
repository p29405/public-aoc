use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = u32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<u32>,
    pub size: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day03::Part1;
    type Part2 = crate::day03::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.split('\n').map(|l| u32::from_str_radix(l.trim(), 2).unwrap()).collect();

        Ok(Self { part1: None, part2: None, input, size: 12 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut accum = Vec::new();
        accum.resize(self.size, 0);
        
        self.input.iter().for_each(|measurement| {
            (0..self.size).for_each(|i| {
                let shift = self.size - i - 1;
                accum[i] += if measurement >> shift & 1 == 1 { 1 } else { -1 };
            })
        });

        let gamma = accum.iter().fold(0, |accum, &d| accum << 1 | if d >= 0 { 1 } else { 0 });
        let mask = (1 << self.size) - 1;
        let epsilon = !gamma & mask;
        self.part1 = Some(gamma * epsilon);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut oxygen = self.input.clone();
        let mut co2 = self.input.clone();

        let mut oxygen_generator_rating = None;
        let mut co2_scrubber_rating = None;

        for i in 0..self.size {
            let shift = self.size - i - 1;
            if !oxygen.is_empty() {
                let oxygen_bit_count = oxygen.iter().fold(0, |count, &v| count + if ((v >> shift) & 1) == 1 { 1 } else { -1 });
                let oxygen_filter_bit = if oxygen_bit_count >= 0 { 1 } else { 0 };

                oxygen = oxygen.iter().filter(|&o| ((*o >> shift) & 1) == oxygen_filter_bit).cloned().collect();

                if oxygen.len() == 1 {
                    oxygen_generator_rating = Some(oxygen[0]);
                    oxygen = vec!();
                }
            }

            if !co2.is_empty() {
                let co2_bit_count = co2.iter().fold(0, |count, v| count + if ((v >> shift) & 1) == 1 { 1 } else { -1 });
                let co2_filter_bit = if co2_bit_count < 0 { 1 } else { 0 };

                co2 = co2.iter().filter(|&c| ((*c >> shift) & 1) == co2_filter_bit).cloned().collect();

                if co2.len() == 1 {
                    co2_scrubber_rating = Some(co2[0]);
                    co2 = vec!();
                }
            }

        }

        self.part2 = Some(oxygen_generator_rating.unwrap() * co2_scrubber_rating.unwrap());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day03::Day03;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "00100
             11110
             10110
             10111
             10101
             01111
             00111
             11100
             10000
             11001
             00010
             01010";

        let mut day = Day03::setup(input).unwrap();
        day.size = 5;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(198));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "00100
             11110
             10110
             10111
             10101
             01111
             00111
             11100
             10000
             11001
             00010
             01010";

        let mut day = Day03::setup(input).unwrap();
        day.size = 5;
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(230));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_071_734));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6_124_992));
    }
}