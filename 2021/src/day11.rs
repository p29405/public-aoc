use aoc::{self, Day, Grid, GridElement, Map, Result};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day11 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Octopus>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day11 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day11::Part1;
    type Part2 = crate::day11::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut map = self.input.clone();
        let mut flashes = 0;
        for _ in 0..100 {
            for o in map.elements.iter_mut() {
                o.energy += 1;
                o.flashed = false;
            }

            while map.elements.iter().any(|o| o.energy > 9 && !o.flashed) {
                for c in map.coord_iter() {
                    let o = map.get(&c);
                    if o.energy > 9 && !o.flashed {
                        flashes += 1;
                        map.set(&c, Octopus { energy: 0, flashed: true });
                        for n in c.neighbors() {
                            if n.x >= 0 && n.x < map.columns && n.y >= 0 && n.y < map.rows {
                                let neighbor = map.get(&n);
                                if !neighbor.flashed {
                                    map.set(&n, Octopus { energy: neighbor.energy + 1, flashed: neighbor.flashed});
                                }
                            }
                        }
                    }
                }
            }

        }
        self.part1 = Some(flashes);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut steps = 1;
        loop {
            for o in self.input.elements.iter_mut() {
                o.energy += 1;
                o.flashed = false;
            }

            while self.input.elements.iter().any(|o| o.energy > 9 && !o.flashed) {
                for c in self.input.coord_iter() {
                    let o = self.input.get(&c);
                    if o.energy > 9 && !o.flashed {
                        self.input.set(&c, Octopus { energy: 0, flashed: true });
                        for n in c.neighbors() {
                            if n.x >= 0 && n.x < self.input.columns && n.y >= 0 && n.y < self.input.rows {
                                let neighbor = self.input.get(&n);
                                if !neighbor.flashed {
                                    self.input.set(&n, Octopus { energy: neighbor.energy + 1, flashed: neighbor.flashed});
                                }
                            }
                        }
                    }
                }
            }

            if self.input.elements.iter().all(|o| o.flashed) {
                break;
            }
            steps += 1;

        }
        self.part2 = Some(steps);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Octopus {
    energy: usize,
    flashed: bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Octopus {
    fn default() -> Self {
        Octopus { energy: 0, flashed: false}
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Octopus {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.energy)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Octopus {
    fn to_element(c: char) -> Self {
        let energy = c.to_string().parse().unwrap();
        Octopus { energy, flashed: false }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day11::Day11;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "5483143223
             2745854711
             5264556173
             6141336146
             6357385478
             4167524645
             2176841721
             6882881134
             4846848554
             5283751526";

        let mut day = Day11::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_656));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = 
            "5483143223
             2745854711
             5264556173
             6141336146
             6357385478
             4167524645
             2176841721
             6882881134
             4846848554
             5283751526";

        let mut day = Day11::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(195));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_686));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(360));
    }
}