use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<u8>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day06::Part1;
    type Part2 = crate::day06::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(spawn(&self.input, 80));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(spawn(&self.input, 256));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn spawn(input: &[u8], limit: usize) -> usize {
    let mut fish = [0; 9];
    for &i in input {
        fish[i as usize] += 1;
    }

    for _ in 0..limit {
        let spawning = fish[0];

        for i in 1..9 {
            fish[i - 1] = fish[i];
        }

        fish[6] += spawning;
        fish[8] = spawning;
    }

    fish.iter().sum()
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::{Day06, spawn};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn spawn_fn() {
        let input = "3,4,3,1,2";
        let day = Day06::setup(input).unwrap();

        assert_eq!(spawn(&day.input, 1), 5, "Failed day 1");
        assert_eq!(spawn(&day.input, 2), 6, "Failed day 2");
        assert_eq!(spawn(&day.input, 3), 7, "Failed day 3");
        assert_eq!(spawn(&day.input, 4), 9, "Failed day 4");
        assert_eq!(spawn(&day.input, 5), 10, "Failed day 5");
        assert_eq!(spawn(&day.input, 6), 10, "Failed day 6");
        assert_eq!(spawn(&day.input, 7), 10, "Failed day 7");
        assert_eq!(spawn(&day.input, 8), 10, "Failed day 8");
        assert_eq!(spawn(&day.input, 9), 11, "Failed day 9");
        assert_eq!(spawn(&day.input, 10), 12, "Failed day 10");
        assert_eq!(spawn(&day.input, 11), 15, "Failed day 11");
        assert_eq!(spawn(&day.input, 12), 17, "Failed day 12");
        assert_eq!(spawn(&day.input, 13), 19, "Failed day 13");
        assert_eq!(spawn(&day.input, 14), 20, "Failed day 14");
        assert_eq!(spawn(&day.input, 15), 20, "Failed day 15");
        assert_eq!(spawn(&day.input, 16), 21, "Failed day 16");
        assert_eq!(spawn(&day.input, 17), 22, "Failed day 17");
        assert_eq!(spawn(&day.input, 18), 26, "Failed day 18");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "3,4,3,1,2";

        let mut day = Day06::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5_934));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "3,4,3,1,2";

        let mut day = Day06::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(26_984_457_539));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(386_536));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_732_821_262_171));
    }
}