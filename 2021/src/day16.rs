use aoc::{self, BitField, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = u64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day16 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub bits: Bits
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day16 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day16::Part1;
    type Part2 = crate::day16::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let bits = input.parse()?;

        Ok(Self { part1: None, part2: None, bits})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(walk_packets(&self.bits, 0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(self.bits.evaluate(0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn walk_packets(bits: &Bits, index: usize) -> usize {
    if bits.packet_type(index) == PacketType::Literal {
        return bits.packet_version(index);
    }

    let mut accum = 0;
    match bits.subpacket_length(index).unwrap() {
        LengthType::Size(size, start) => {
            let mut read = 0;
            while read < size {
                accum += walk_packets(bits, index + start + read);
                read += bits.packet_length(index + start + read);
            }
        },
        LengthType::SubPackets(number, start) => {
            let mut count = 0;
            let mut offset = start;

            while count < number {
                accum += walk_packets(bits, index + offset);
                offset += bits.packet_length(index + offset);
                count += 1;            
            }
        }
    };

    bits.packet_version(index) + accum
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PacketType {
    Sum,
    Product,
    Min,
    Max,
    Literal,
    Greater,
    Less,
    Equal
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum LengthType {
    Size(usize, usize),
    SubPackets(usize, usize)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone)]
pub struct Bits {
    bit_field: BitField
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Bits {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn packet_version(&self, index: usize) -> usize {
        self.bit_field.retrieve_usize(index..index + 3).unwrap()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn packet_id(&self, index: usize) -> u16 {
        self.bit_field.retrieve_u16(index + 3..index + 6).unwrap()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn packet_type(&self, index: usize) -> PacketType {
        match self.packet_id(index) {
            0 => PacketType::Sum,
            1 => PacketType::Product,
            2 => PacketType::Min,
            3 => PacketType::Max,
            4 => PacketType::Literal,
            5 => PacketType::Greater,
            6 => PacketType::Less,
            7 => PacketType::Equal,
            _ => unreachable!("Invalid Packet type id {}", self.packet_id(index))
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn literal_value(&self, index: usize) -> Option<u64> {
        if self.packet_type(index) == PacketType::Literal {
            let mut start = index + 6;
            let mut result = 0;
            let mut finished = false;

            while !finished {
                finished = !self.bit_field.retrieve_bool(start).unwrap();
                result = result << 4 | self.bit_field.retrieve_u64(start + 1..start + 5).unwrap();
                start += 5;
            }

            Some(result)
        } else {
            None
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn literal_length(&self, index: usize) -> Option<usize> {
        if self.packet_type(index) == PacketType::Literal {
            let mut start = index + 6;
            let mut finished = false;

            while !finished {
                finished = !self.bit_field.retrieve_bool(start).unwrap();
                start += 5;
            }

            Some(start - (index + 6))
        } else {
            None
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn subpacket_length(&self, index: usize) -> Option<LengthType> {
        if self.packet_type(index) != PacketType::Literal {
            let start = index + 6;
            if self.bit_field.retrieve_bool(start).unwrap() {
                let packets = self.bit_field.retrieve_usize(start + 1..start + 12).unwrap();
                Some(LengthType::SubPackets(packets, start + 12 - index))
            } else {
                let size =  self.bit_field.retrieve_usize(start + 1..start + 16).unwrap();
                Some(LengthType::Size(size, start + 16 - index))
            }
        } else {
            None
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn packet_length(&self, index: usize) -> usize {
        if self.packet_type(index) == PacketType::Literal {
            6 + self.literal_length(index).unwrap()
        } else {
            match self.subpacket_length(index).unwrap() {
                LengthType::Size(len, start) => start + len,
                LengthType::SubPackets(number, start) => {
                    let mut len = start + index;
                    let mut count = 0;
                    while count < number {
                        len += self.packet_length(len);
                        count += 1;
                    }
                    // ugh
                    len - index
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn evaluate(&self, index: usize) -> u64 {
        if self.packet_type(index) == PacketType::Literal {
            return self.literal_value(index).unwrap();
        }
    
        let operation = self.packet_type(index);
        let mut values = vec![];
        match self.subpacket_length(index).unwrap() {
            LengthType::Size(size, start) => {
                let mut read = 0;
                while read < size {
                    values.push(self.evaluate(index + start + read));
                    read += self.packet_length(index + start + read);
                }
            },
            LengthType::SubPackets(number, start) => {
                let mut count = 0;
                let mut offset = start;
    
                while count < number {
                    values.push(self.evaluate(index + offset));
                    offset += self.packet_length(index + offset);
                    count += 1;            
                }
            }
        };

        match operation {
            PacketType::Sum => values.iter().sum(),
            PacketType::Product => values.iter().product(),
            PacketType::Min => *values.iter().min().unwrap(),
            PacketType::Max => *values.iter().max().unwrap(),
            PacketType::Greater => if values[0] > values[1] { 1 } else { 0 },
            PacketType::Less => if values[0] < values[1] { 1 } else { 0 },
            PacketType::Equal => if values[0] == values[1] { 1 } else { 0 },
            _ => unreachable!("Literanl packets should not reach here")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Bits {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Self { bit_field: BitField::from_hex_string(s)})
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day16::{Bits, Day16, LengthType, PacketType};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn bits_literal() {
        let input = "D2FE28";

        let bits: Bits = input.parse().unwrap();

        assert_eq!(bits.packet_version(0), 6, "Packet Version mismatch");
        assert_eq!(bits.packet_id(0), 4, "Packet ID mismatch");
        assert_eq!(bits.packet_type(0), PacketType::Literal, "Packet Type mismatch");
        assert_eq!(bits.literal_value(0), Some(2021), "Literal mismatch");
        assert_eq!(bits.literal_length(0), Some(15), "Literal length mismatch");
        assert_eq!(bits.packet_length(0), 21, "Packet length mismatch");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn bits_operator_1() {
        let input = "38006F45291200";

        let bits: Bits = input.parse().unwrap();

        println!("{:?}", bits);
        assert_eq!(bits.packet_version(0), 1, "Packet Version mismatch");
        assert_eq!(bits.packet_id(0), 6, "Packet ID mismatch");
        //assert_eq!(bits.packet_type(0), PacketType::Operator, "Packet Type mismatch");
        assert_eq!(bits.literal_value(0), None, "Literal mismatch");
        assert_eq!(bits.literal_length(0), None, "Literal length mismatch");
        assert_eq!(bits.subpacket_length(0), Some(LengthType::Size(27, 22)), "Subpacket length mismatch");
        assert_eq!(bits.packet_type(22), PacketType::Literal, "SubPacket Type mismatch");
        assert_eq!(bits.literal_value(22), Some(10), "SubPacketLiteral mismatch");
        assert_eq!(bits.packet_length(22), 11, "SubPacket length mismatch");
        assert_eq!(bits.packet_type(33), PacketType::Literal, "SubPacket Type mismatch");
        assert_eq!(bits.literal_value(33), Some(20), "SubPacketLiteral mismatch");
        assert_eq!(bits.packet_length(33), 16, "SubPacket length mismatch");
        assert_eq!(bits.packet_length(0), 49, "Packet length mismatch");
    }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #[test]
        fn bits_operator_2() {
            let input = "EE00D40C823060";
    
            let bits: Bits = input.parse().unwrap();
    
            println!("{:?}", bits);
            assert_eq!(bits.packet_version(0), 7, "Packet Version mismatch");
            assert_eq!(bits.packet_id(0), 3, "Packet ID mismatch");
            //assert_eq!(bits.packet_type(0), PacketType::Operator, "Packet Type mismatch");
            assert_eq!(bits.literal_value(0), None, "Literal mismatch");
            assert_eq!(bits.literal_length(0), None, "Literal length mismatch");
            assert_eq!(bits.subpacket_length(0), Some(LengthType::SubPackets(3, 18)), "Subpacket length mismatch");
            assert_eq!(bits.packet_length(0), 51, "Packet length mismatch");
        }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs = [
            ("8A004A801A8002F478", 16),
            ("620080001611562C8802118E34", 12),
            ("C0015000016115A2E0802F182340", 23),
            ("A0016C880162017C3686B18A3D4780", 31)
        ];

        for (input, expected) in inputs {
            let mut day = Day16::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(expected), "Input: {}", input);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs = [
            ("C200B40A82", 3),
            ("04005AC33890", 54),
            ("880086C3E88112", 7),
            ("CE00C43D881120", 9),
            ("D8005AC2A8F0", 1),
            ("F600BC2D8F", 0),
            ("9C005AC2F8F0", 0),
            ("9C0141080250320F1802104A08", 1)
        ];

        for (input, expected) in inputs {
            let mut day = Day16::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(expected), "Input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part1();
        println!("{:?}", day.bits.bit_field);
        assert_eq!(day.part1_answer(), Some(889));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(739_303_923_668));
    }
}