use aoc::{self, Day, Result, GridElement, Grid, Coord, Map, Path};
use std::fmt;
////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Floor>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day09::Part1;
    type Part2 = crate::day09::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut risk = 0;
        for (c, f) in self.input.iter() {
            if self.input.adjacent(c.x, c.y).all(|n| f.0 < n.0) {
                risk += f.0 + 1;
            }
        }
        self.part1 = Some(risk);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut low_points = Vec::new();

        for (c, f) in self.input.iter() {
            if self.input.adjacent(c.x, c.y).all(|n| f.0 < n.0) {
                low_points.push(c);
            }
        }

        let answer = find_basins(&low_points, &[Coord::new(-1, -1)], self.input.columns, self.input.rows, |c| self.input.get(c).0 != 9);

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Floor(usize);

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Floor {
    fn default() -> Self {
        Floor(9)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Floor {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Floor {
    fn to_element(c: char) -> Self {
        Floor(c.to_string().parse().unwrap())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn find_basins<P>(starts: &[Coord], ends: &[Coord], columns: i64, rows: i64, mut predicate: P) -> usize
    where P: FnMut(&Coord) -> bool {
    let mut paths = Grid::new(columns, rows);
    let mut frontier = vec![];
    let mut basins = vec![];

    for start in starts {
        let mut depth = 0;
        let mut basin = 1;

        if paths.get(start) != Path::Unreachable {
            continue;
        }
        frontier.push(*start);
        paths.set(start, Path::Distance(depth));

        loop {
            let mut next = vec![];
            depth += 1;

            for p in &frontier {
                for n in p.adjacent() {
                    if paths.get(&n) == Path::Unreachable && !next.contains(&n) && (predicate)(&n) {
                        paths.set(&n, Path::Distance(depth));
                        next.push(n);
                        basin += 1;
                    }
                }
            }

            frontier = next;

            if frontier.is_empty() || ends.iter().all(|e| paths.get(e) != Path::Unreachable) {
                basins.push(basin);
                break;
            }
        }
    }

    basins.sort();

    basins.iter().rev().take(3).product()
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "2199943210
             3987894921
             9856789892
             8767896789
             9899965678";

        let mut day = Day09::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(15));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "2199943210
             3987894921
             9856789892
             8767896789
             9899965678";

        let mut day = Day09::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_134));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(417));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_148_965));
    }
}