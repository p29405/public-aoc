use aoc::{self, Day, Grid, GridElement, Map, Result};
use std::fmt;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = u32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day04 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub boards: Vec<Board>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day04 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day04::Part1;
    type Part2 = crate::day04::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");

        let numbers = aoc::read_list_from_string(parts.next().unwrap().trim(), ",")?;

        let mut boards: Vec<Board> = parts.map(|p| p.trim()).filter(|p| !p.is_empty()).map(|b| b.parse().unwrap()).collect();

        for &number in numbers.iter() {
            boards.iter_mut().for_each(|b| b.mark(number));
        }
        Ok(Self { part1: None, part2: None, boards })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = self.boards.iter().min_by_key(|b| b.marks).map(|b| b.score());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = self.boards.iter().max_by_key(|b| b.marks).map(|b| b.score());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Unmarked(u32),
    Marked(u32)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    fn default() -> Self {
        Square::Unmarked(0)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Unmarked(x) => write!(f, "{:2}  ", x),
            Square::Marked(x) => write!(f, "{:2}X ", x)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Board {
    grid: Grid<Square>,
    marks: u32,
    bingo: bool,
    last: u32
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Board {
    ///////////////////////////////////////////////////////////////////////////////////////////////
    fn mark(&mut self, number: u32) {
        if self.bingo { return; }
        self.marks += 1;
        let element = self.grid.iter().find(|(_, s)| if let Square::Unmarked(i) = s { *i == number } else { false });

        if let Some((c, _)) = element {
            self.grid.set(&c, Square::Marked(number));
        }

        self.bingo = self.has_bingo();
        self.last = number;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    fn has_bingo(&self) -> bool {
        let row_bingo = (0..5).any(|y| (0..5).all(|x| if let Square::Marked(_) = self.grid.get_xy(x, y) { true} else { false }));
        let col_bingo = (0..5).any(|x| (0..5).all(|y| if let Square::Marked(_) = self.grid.get_xy(x, y) { true} else { false }));
        row_bingo || col_bingo
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    fn score(&self) -> u32 {
        self.last * self.grid.iter()
            .fold(0, |accum, (_, s)| if let Square::Unmarked(i) = s { accum + i } else { accum })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Board {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut grid = Grid::new(5, 5);

        s.lines().filter(|l| !l.is_empty()).enumerate().for_each(|(y, line)| {
            line.trim().split(" ").enumerate().for_each(|(x, v)| {
                grid.set_xy(x as i64, y as i64, Square::Unmarked(v.parse().unwrap()));
            });          
        });

        Ok(Self { grid, marks: 0, bingo: false, last: 0 })
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day04::Day04;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

             22 13 17 11 0
             8 2 23 4 24
             21 9 14 16 7
             6 10 3 18 5
             1 12 20 15 19

             3 15 0 2 22
             9 18 13 17 5
             19 8 7 25 23
             20 11 10 24 4
             14 21 16 12 6

             14 21 17 24 4
             10 16 15 9 19
             18 8 23 26 20
             22 11 13 6 5
             2 0 12 3 7";

        let mut day = Day04::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4_512));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = 
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

             22 13 17 11 0
             8 2 23 4 24
             21 9 14 16 7
             6 10 3 18 5
             1 12 20 15 19

             3 15 0 2 22
             9 18 13 17 5
             19 8 7 25 23
             20 11 10 24 4
             14 21 16 12 6

             14 21 17 24 4
             10 16 15 9 19
             18 8 23 26 20
             22 11 13 6 5
             2 0 12 3 7";

        let mut day = Day04::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_924));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(39_984));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(8_468));
    }
}