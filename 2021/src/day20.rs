use aoc::{self, Coord, Day, Grid, Pixel, Map, Result, Velocity};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day20 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Pixel>,
    pub image: Grid<Pixel>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day20 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day20::Part1;
    type Part2 = crate::day20::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");
        let input = aoc::read_list_by_char_from_string(parts.next().unwrap())?;
        let image = aoc::read_grid_from_string(parts.next().unwrap())?;

        Ok(Self { part1: None, part2: None, input, image})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut result = self.image.clone();
        for i in 0..2 {
            if self.input[0].on() {
                if i % 2 == 0 {
                    result.default_override = Some(Pixel::Off);
                } else {
                    result.default_override = Some(Pixel::On);
                }
            }
            result = enhance(&result, &self.input);
        }

        let answer = result.elements.iter().fold(0, |accum,  p| accum + if p.on() { 1 } else { 0 });
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut result = self.image.clone();
        for i in 0..50 {
            if self.input[0].on() {
                if i % 2 == 0 {
                    result.default_override = Some(Pixel::Off);
                } else {
                    result.default_override = Some(Pixel::On);
                }
            }
            result = enhance(&result, &self.input);
        }

        let answer = result.elements.iter().fold(0, |accum,  p| accum + if p.on() { 1 } else { 0 });
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn enhance(image: &Grid<Pixel>, data: &[Pixel]) -> Grid<Pixel> {
    let mut next = Grid::new(image.columns + 2, image.rows + 2);
    let offset = Velocity::new(1, 1);
    let start = image.upper_left().prev(&offset);
    let end = image.lower_right().next(&offset);

    for c in Coord::range(start, end) {
        let index = index(&c, image);
        next.set(&c.next(&offset), data[index]);
    }
    
    next
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn index(c: &Coord, map: &Grid<Pixel>) -> usize {
    Coord::range_inclusive(Coord::new(c.x - 1, c.y - 1), Coord::new(c.x + 1, c.y + 1))
        .map(|c| if map.get(&c) == Pixel::On { 1 } else { 0 })
        .fold(0, |accum, v| accum << 1 | v)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day20::Day20;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = 
            "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

             #..#.
             #....
             ##..#
             ..#..
             ..###";

        let mut day = Day20::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(35));
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_351));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5_663));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(19_638));
    }
}