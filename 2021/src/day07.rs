use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day07 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day07 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day07::Part1;
    type Part2 = crate::day07::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut input = aoc::read_list_from_string(input, ",")?;
        input.sort_unstable();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let median = median(&self.input);

        self.part1 = Some(self.input.iter().map(|p| (p - median).abs()).sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mean = mean(&self.input);

        self.part2 = (mean..=mean+1).map(|c| self.input.iter().map(|p| sum_up_to((c - p).abs())).sum()).min();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn median(n: &[i64]) -> i64 {
    n[n.len() as usize / 2]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn mean(n: &[i64]) -> i64 {
    let sum: i64 = n.iter().sum();
    sum / (n.len() as i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn sum_up_to(n: i64) -> i64 {
    n * (n + 1) / 2
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day07::Day07;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "16,1,2,0,4,2,7,1,2,14";

        let mut day = Day07::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(37));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "16,1,2,0,4,2,7,1,2,14";

        let mut day = Day07::setup(input).unwrap();
        day.run_part2();
        // mean doesn't work for this example
        assert_eq!(day.part2_answer(), Some(168));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(348_664));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(100_220_525));
    }
}