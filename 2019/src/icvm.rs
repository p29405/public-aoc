use log::{debug, trace};
use std::collections::VecDeque;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub type Word = i64;
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, PartialEq)]
enum Operand {
    Position(Word),
    Immediate(Word),
    Relative(Word)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Operand {
    fn new(mode: Word, value: Word) -> Self {
        match mode {
            0 => Operand::Position(value),
            1 => Operand::Immediate(value),
            2 => Operand::Relative(value),
            _ => panic!("Unknown addressing mode: {}", mode)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, PartialEq)]
enum Instruction {
    Add { op1: Operand, op2: Operand, dst: Operand },
    Mul { op1: Operand, op2: Operand, dst: Operand },
    Inp { dst: Operand },
    Out { op1: Operand },
    Jnz { op1: Operand, op2: Operand },
    Jez { op1: Operand, op2: Operand },
    Lt  { op1: Operand, op2: Operand, dst: Operand },
    Eq  { op1: Operand, op2: Operand, dst: Operand },
    Urb { op1: Operand },
    Hlt
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Instruction {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn new(pc: usize, program: &[Word]) -> Self {
        let mut opcode = program[pc];

        let a_mode = get_mode(&mut opcode, 10_000, 20_000);
        let b_mode = get_mode(&mut opcode, 1_000, 2_000);
        let c_mode = get_mode(&mut opcode, 100, 200);

        match opcode {
            1 => Instruction::Add {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2]),
                dst: Operand::new(a_mode, program[pc + 3])
            },
            2 => Instruction::Mul {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2]),
                dst: Operand::new(a_mode, program[pc + 3])
            },
            3 => Instruction::Inp { dst: Operand::new(c_mode, program[pc + 1]) },
            4 => Instruction::Out { op1: Operand::new(c_mode, program[pc + 1]) },
            5 => Instruction::Jnz {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2])
            },
            6 => Instruction::Jez {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2])
            },
            7 => Instruction::Lt {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2]),
                dst: Operand::new(a_mode, program[pc + 3])
            },
            8 => Instruction::Eq {
                op1: Operand::new(c_mode, program[pc + 1]),
                op2: Operand::new(b_mode, program[pc + 2]),
                dst: Operand::new(a_mode, program[pc + 3])
            },
            9 => Instruction::Urb { op1: Operand::new(c_mode, program[pc + 1]) },
            99 => Instruction::Hlt,
            _ => unreachable!("Unknown OP Code {}", opcode)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn store(&self) -> Option<&Operand> {
        match self {
            Instruction::Add { dst: result, .. } => Some(result),
            Instruction::Mul { dst: result, .. } => Some(result),
            Instruction::Inp { dst: result } => Some(result),
            Instruction::Lt { dst: result, .. } => Some(result),
            Instruction::Eq { dst: result, .. } => Some(result),
            _ => None
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn size(&self) -> usize {
        match self {
            Instruction::Inp { .. } => 2,
            Instruction::Out { .. } => 2,
            Instruction::Jnz { .. } => 3,
            Instruction::Jez { .. } => 3,
            Instruction::Urb { .. } => 2,
            Instruction::Hlt => 0,
            _ => 4
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn get_mode(opcode: &mut Word, imm: Word, rel: Word) -> Word {
    if *opcode > rel {
        *opcode -= rel;
        2
    } else if *opcode > imm {
        *opcode -= imm;
        1
    } else {
        0
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, PartialEq)]
enum Result {
    Value(Word),
    Output(Word),
    Jump(usize),
    RelativeBase(Word),
    Halt,
    None
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, PartialEq)]
pub enum IoMode {
    Output(Word),
    InputRequest,
    Halt
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Default)]
pub struct IntCodeVM {
    pub input: VecDeque<Word>,
    pub output: Vec<Word>,
    program: Vec<Word>,
    relative_base: Word,
    pc: usize,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl IntCodeVM {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn reset(&mut self) ->&mut Self {
        self.input = VecDeque::new();
        self.output = vec!();
        self.program = vec!();
        self.relative_base = 0;
        self.pc = 0;
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn input(&mut self, input: &[Word]) -> &mut Self {
        self.input = input.to_vec().into();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn ascii_input(&mut self, input: &str) -> &mut Self {
        self.input = input.as_bytes().iter().map(|b| *b as Word).collect::<VecDeque<Word>>();
        self
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[Word]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn poke(&mut self, address: usize, value: Word) -> &mut Self {
        self.program[address] = value;
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn peek(&self, address: usize) -> Word {
        self.program[address]
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fetch(&self) -> Instruction {
        Instruction::new(self.pc, &self.program)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn lookup(&mut self, operand: &Operand) -> usize {
        let address = match operand {
            Operand::Position(addr) => *addr,
            Operand::Relative(offset) => self.relative_base + *offset,
            _ => unreachable!()
        };

        if address < 0 {
            panic!("Invalid address: {}", address);
        }

        if address as usize >= self.program.len() {
            debug!("Resizing program to {} words", address + 100);
            self.program.resize((address + 100) as usize, 0);
        }

        address as usize
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn decode(&mut self, operand: &Operand) -> Word {
        match operand {
            Operand::Immediate(value) => *value,
            op => {
                let address = self.lookup(op);
                let value = self.program[address];
                trace!("Decoded operand {:?} = {}", operand, value);
                value
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: &Instruction) -> Result {
        match instruction {
            Instruction::Add { op1, op2, .. } => Result::Value(self.decode(op1) + self.decode(op2)),
            Instruction::Mul { op1, op2, .. } => Result::Value(self.decode(op1) * self.decode(op2)),
            Instruction::Inp { .. } => Result::Value(self.input.pop_front().unwrap()),
            Instruction::Out { op1 } => Result::Output(self.decode(op1)),
            Instruction::Jnz { op1, op2 } => if self.decode(op1) != 0 {
                Result::Jump(self.decode(op2) as usize)
            } else {
                Result::None
            },
            Instruction::Jez { op1, op2 } => if self.decode(op1) == 0 {
                Result::Jump(self.decode(op2) as usize)
            } else {
                Result::None
            },
            Instruction::Lt  { op1, op2, .. } => if self.decode(op1) < self.decode(op2) {
                Result::Value(1)
            } else {
                Result::Value(0)
            },
            Instruction::Eq  { op1, op2, .. } => if self.decode(op1) == self.decode(op2) {
                Result::Value(1)
            } else {
                Result::Value(0)
            },
            Instruction::Urb { op1 } => Result::RelativeBase(self.relative_base + self.decode(op1)),
            Instruction::Hlt => Result::Halt
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn step(&mut self) -> Result {
        let instruction = self.fetch();

        let result = self.execute(&instruction);
        trace!("{:?} => {:?}, relative base = {}, pc = {}", instruction, result, self.relative_base, self.pc);
        self.pc += instruction.size();

        match result {
            Result::Value(value) => {
                if let Some(store) = instruction.store() {
                    let address = self.lookup(store);
                    self.program[address as usize] = value;
                }
            },
            Result::RelativeBase(base) => self.relative_base = base,
            Result::Jump(pc) => self.pc = pc,
            Result::Output(output) => self.output.push(output),
            _ => ()
        };

        result
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn run(&mut self) {
        loop {
            if let Result::Halt = self.step() {
                return
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn run_to_output(&mut self) -> Option<Word>{
        match self.run_until_io() {
            IoMode::Output(output) => Some(output),
            IoMode::Halt => None,
            _ => unreachable!()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn run_until_io(&mut self) -> IoMode{
        loop {
            if let Instruction::Inp { .. } = self.fetch() {
                if self.input.is_empty() {
                    return IoMode::InputRequest;
                }
            }

            match self.step() {
                Result::Halt => return IoMode::Halt,
                Result::Output(output) => return IoMode::Output(output),
                _ => ()
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn run_to_ascii_output(&mut self) -> Option<String>{
        loop {
            match self.run_until_io() {
                IoMode::Output(output) => {
                    if output == 10 {
                        let bytes = self.output.iter().map(|n| *n as u8).collect::<Vec<u8>>();
                        self.output.clear();
                        return Some(std::str::from_utf8(&bytes).unwrap().into());
                    }
                },
                IoMode::Halt => return None,
                _ => unreachable!()
            };
        }
    }
}

#[cfg(test)]
mod day02 {
    use crate::IntCodeVM;

    #[test]
    fn test_example_1() {
        let program = [1, 0, 0, 0, 99];
        let mut vm = IntCodeVM::default();

        vm.program(&program).run();
        assert_eq!(vm.program, [2, 0, 0, 0, 99]);
    }

    #[test]
    fn test_example_2() {
        let program = [2, 3, 0, 3, 99];
        let mut vm = IntCodeVM::default();

        vm.program(&program).run();
        assert_eq!(vm.program, [2, 3, 0, 6, 99]);
    }

    #[test]
    fn test_example_3() {
        let program = [2, 4, 4, 5, 99, 0];
        let mut vm = IntCodeVM::default();

        vm.program(&program).run();
        assert_eq!(vm.program, [2, 4, 4, 5, 99, 9801]);
    }

    #[test]
    fn test_example_4() {
        let program = [1, 1, 1, 4, 99, 5, 6, 0, 99];
        let mut vm = IntCodeVM::default();

        vm.program(&program).run();
        assert_eq!(vm.program, [30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }
}

#[cfg(test)]
mod day05 {
    use crate::IntCodeVM;

    #[test]
    fn test_equals_8_position_mode() {
        let program = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[8]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 0);
    }

    #[test]
    fn test_less_than_8_position_mode() {
        let program = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[8]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 0);
    }

    #[test]
    fn test_equals_8_immediate_mode() {
        let program = [3, 3, 1108, -1, 8, 3, 4, 3, 99];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[8]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 0);
    }

    #[test]
    fn test_less_than_8_immediate_mode() {
        let program = [3, 3, 1107, -1, 8, 3, 4, 3, 99];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[8]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 0);
    }

    #[test]
    fn test_jump_position_mode() {
        let program = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[0]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 1);
    }

    #[test]
    fn test_jump_immediate_mode() {
        let program = [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 1);

        vm.reset().input(&[0]).program(&program).run();
        assert_eq!(vm.output[0], 0);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 1);
    }

    #[test]
    fn test_larger_example() {
        let program = [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
            1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
            999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99];
        let mut vm = IntCodeVM::default();

        vm.input(&[5]).program(&program).run();
        assert_eq!(vm.output[0], 999);

        vm.reset().input(&[8]).program(&program).run();
        assert_eq!(vm.output[0], 1_000);

        vm.reset().input(&[10]).program(&program).run();
        assert_eq!(vm.output[0], 1_001);
    }
}


#[cfg(test)]
mod day09 {
    use crate::IntCodeVM;

    #[test]
    fn test_example_1() {
        let input = [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99];

        let mut vm = IntCodeVM::default();
        vm.program(&input).run();

        assert_eq!(vm.output, input);
    }

    #[test]
    fn test_example_2() {
        let input = [1102, 34915192, 34915192, 7, 4, 7, 99, 0];

        let mut vm = IntCodeVM::default();
        vm.program(&input).run();

        assert_eq!(vm.output, [1219070632396864]);
    }

    #[test]
    fn test_example_3() {
        let input = [104, 1125899906842624, 99];

        let mut vm = IntCodeVM::default();
        vm.program(&input).run();

        assert_eq!(vm.output, [1125899906842624]);
    }
}
