use aoc::{self, Day, Grid, GridElement, Result, Map, Coord};
use icvm::{IntCodeVM, Word};
use log::info;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day17 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>,
    pub grid: Option<Grid<Square>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day17 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day17::Part1;
    type Part2 = crate::day17::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input, grid: None})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut ascii = IntCodeVM::default();
        ascii.program(&self.input);
        let mut map = String::new();
        while let Some(output) = ascii.run_to_ascii_output() {
            if &output != "\n" {
                map += &output;
            }
        }

        self.grid = aoc::read_grid_from_string(&map).ok();

        if let Some(grid) = self.grid.as_ref() {
            info!("\n{}", grid);
            self.part1 = Some(alignment_parameters(grid));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        /*
            ............................CCCCCCC............
            ............................C.....C............
            ............................C.....C............
            ............................C.....C............
            ..........B.....AAAAAAAAAAAAC.....C............
            ..........B.....A.................C............
            ..........B...AAAAA...............C............
            ..........B...A.A.A...............C............
            ..........B.CCACACACA.............C............
            ..........B.C.A.A.A.A.............C............
            ....BBBBBBB.C.ABBBBBBBB...........C............
            ....B.......C...A.A.A.B...........C............
            CCCCBCCCCCCCC...AAAAA.B...........CCCCC........
            C...B.............A...B...............C........
            C...B.^AAAAAAAAAAAA...B...............C........
            C...B.................B...............C........
            C...B.................BBBBBBB.....AAAAAAAAA....
            C...B.......................A.....A...C...A....
            CCCCC.......................A.....A...C...A....
            ............................A.....A...C...A....
            ............................A.....AAAAA...A....
            ............................A.............A....
            ............................A.............A....
            ............................A.............A....
            ............................A...AAAAA.....A....
            ............................A...B...A.....A....
            ............................A...B...A.....A....
            ............................A...B...A.....A....
            ............................AAAABAAAA.....CCCCC
            ................................B.............C
            ................................B.............C
            ................................B.............C
            ..........................BBBBBBB.............C
            ..........................B...................C
            ..........................B.......CCCCCCCCCCCCC
            ..........................B.......C............
            ..........................B.......C............
            ..........................B.......C............
            ..........................BCCCCCCCC............
        */

        // Full Path:
        // R,12,L,8,L,4,L,4,L,8,R,6,L,6,R,12,L,8,L,4,L,4,L,8,R,6,L,6,L,8,L,4,R,12,L,6,L,4,R,12,L,8,L,4,L,4,L,8,L,4,R,12,L,6,L,4,R,12,L,8,L,4,L,4,L,8,L,4,R,12,L,6,L,4,L,8,R,6,L,6 <- C,B

        // A,B,A,B,C,A,C,A,C,B
        // A => R,12,L,8,L,4,L,4
        // B => L,8,R,6,L,6
        // C => L,8,L,4,R,12,L,6,L,4

        let movement = "A,B,A,B,C,A,C,A,C,B\nR,12,L,8,L,4,L,4\nL,8,R,6,L,6\nL,8,L,4,R,12,L,6,L,4\nn\n";
        // let movement = "A,B,A,B,C,A,C,A,C,B\nR,12,L,8,L,4,L,4\nL,8,R,6,L,6\nL,8,L,4,R,12,L,6,L,4\ny\n";
        let mut vacuum = IntCodeVM::default();
        vacuum.program(&self.input).poke(0, 2).ascii_input(movement);

        /*
        while let Some(string) = vacuum.run_to_ascii_output() {
            print!("{}", string);
        }
        */

        vacuum.run();

        self.part2 = vacuum.output.last().map(|o| *o);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn alignment_parameters(grid: &Grid<Square>) -> i64 {
    let mut sum = 0;
    for c in Coord::range(grid.upper_left(), grid.lower_right()) {
        if grid.get(&c) == Square::Scaffold {
            if  grid.adjacent(c.x, c.y).all(|s| s == Square::Scaffold) {
                sum += c.x * c.y;
            }
        }
    }
    sum
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Open,
    Scaffold,
    Robot
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    fn default() -> Self {
        Square::Open
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Open => write!(f, "."),
            Square::Scaffold => write!(f, "#"),
            Square::Robot => write!(f, "^")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    fn to_element(c: char) -> Self {
        match c {
            '#' => Square::Scaffold,
            '.' => Square::Open,
            '^' => Square::Robot,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day17::Day17;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parts() {
        let input = aoc::get_input_from_file("input/day17.txt");

        let mut day = Day17::setup(&input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(2788));
        assert_eq!(day.part2_answer(), Some(761_085));
    }
}
