use aoc::{self, AocFontType, Day, Result, Coord, Direction, CenteredMap, GridElement, Map, to_string};
use icvm::{IntCodeVM, Word};
use log::debug;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day11 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day11 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day11::Part1;
    type Part2 = crate::day11::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut brain = IntCodeVM::default();
        brain.program(&self.input);

        let mut panels: CenteredMap<Panel> = CenteredMap::default();
        let mut position = Coord::default();
        let mut direction = Direction::North;

        panels.invert();

        loop {
            let current = panels.get(&position);

            brain.input(&[if current == Panel::Black { 0 } else { 1 }]);
            match brain.run_to_output() {
                Some(color) => {
                    match color {
                        0 => panels.set(&position, Panel::Black),
                        _ => panels.set(&position, Panel::White)
                    }
                },
                None => break
            }
            if let Some(new_direciton) = brain.run_to_output() {
                direction = turn(direction, new_direciton);
                position = position.go(direction);
            } else {
                panic!("What happened");
            }
        }

        debug!("{}", panels);

        self.part1 = Some(panels.elements.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut brain = IntCodeVM::default();
        brain.program(&self.input);

        let mut panels: CenteredMap<Panel> = CenteredMap::default();
        let mut position = Coord::default();
        let mut direction = Direction::North;

        panels.invert();

        panels.set(&position, Panel::White);

        loop {
            let current = panels.get(&position);

            brain.input(&[if current == Panel::Black { 0 } else { 1 }]);
            match brain.run_to_output() {
                Some(color) => {
                    match color {
                        0 => panels.set(&position, Panel::Black),
                        _ => panels.set(&position, Panel::White)
                    }
                },
                None => break
            }
            if let Some(new_direction) = brain.run_to_output() {
                direction = turn(direction, new_direction);
                position = position.go(direction);
            } else {
                panic!("What happened");
            }
        }
        debug!("{}", panels);
        let answer = to_string(&panels.into(), Panel::White, AocFontType::Aoc2016).unwrap_or_else(|| "Not Found!!".to_string());
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn turn(current: Direction, input: Word) -> Direction {
    if input == 0 {
        current.turn_left()
    } else {
        current.turn_right()
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Panel {
    Black,
    White
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Panel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Panel::Black
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Panel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Panel::Black => write!(f, " "),
            Panel::White => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Panel {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day11::Day11;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2088));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("URCAFLCP".to_owned()));
    }
}
