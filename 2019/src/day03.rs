use aoc::{self, Coord, Day, Direction, Result};
use fnv::FnvHashSet;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = u64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub wire1: FnvHashSet<Coord>,
    pub wire2: FnvHashSet<Coord>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day03::Part1;
    type Part2 = crate::day03::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_lists_from_string::<WirePath>(input, ",")?;
        let wire1 = path(&input[0]);
        let wire2 = path(&input[1]);

        Ok(Self { part1: None, part2: None, wire1, wire2})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let center = Coord::default();
        let crosses = self.wire1.intersection(&self.wire2).collect::<Vec<&Coord>>();
        let mut distances: Vec<i64> = crosses.iter().map(|&c| center.distance(c)).collect();
        distances.sort();

        self.part1 = Some(distances[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut crosses = Vec::new();

        for c in self.wire1.iter() {
            if let Some(t) = self.wire2.get(c) {
                crosses.push(c.d + t.d)
            }
        }

        crosses.sort();

        self.part2 = Some(crosses[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
struct WirePath {
    pub direction: Direction,
    pub distance: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for WirePath {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut chars = s.chars();
        if let Some(direction) = chars.next() {
            return Ok( WirePath {
                    direction: Direction::from(direction),
                    distance: chars.as_str().parse().unwrap()
            });
        }
        Err(aoc::Error::new(&format!("Failed to parse WirePath: {}", s)))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn segment(coord: &mut Coord, path: &WirePath) -> Vec<Coord> {
    let mut coords = Vec::with_capacity(path.distance as usize);

    for _ in 0..path.distance {
        *coord = coord.go(path.direction);
        coords.push(*coord);
    }

    coords
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn path(wire_path: &[WirePath]) -> FnvHashSet<Coord> {

    let mut start = Coord::default();
    let mut coords = FnvHashSet::default();

    for p in wire_path {
        for c in segment(&mut start, p) {
            coords.insert(c);
        }
    }

    coords
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day03::Day03;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 159),
                ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(627));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 610),
                ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410)
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day03::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");

        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(13_190));
    }
}
