use aoc::{self, Day, Result};
use std::collections::HashMap;
use std::ops::Range;

////////////////////////////////////////////////////////////////////////////////////////////////////
// I really don't like this solution, what if I convert to a string/chars

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day04 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub range: Range<i32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day04 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day04::Part1;
    type Part2 = crate::day04::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "-")?;
        let range = input[0]..input[1];

        Ok(Self { part1: None, part2: None, range })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.range.clone().fold(0, |accum, i| accum + valid(i) as i32));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(self.range.clone().fold(0, |accum, i| accum + valid_part2(i) as i32));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid(password: i32) -> bool {
    let mut x = password;
    let mut denom = 100_000;
    let mut double = false;


    let mut prev = x / denom;
    x %= denom;
    denom /= 10;

    while denom > 0 {
        let current = x / denom;

        if current == prev {
            double = true;
        }

        if current < prev {
            return false;
        }

        prev = current;
        x %= denom;
        denom /= 10;
    }

    double
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid_part2(password: i32) -> bool {
    let mut x = password;
    let mut denom = 100_000;

    let mut prev = x / denom;
    x %= denom;
    denom /= 10;

    let mut runs = HashMap::new();

    while denom > 0 {
        let current = x / denom;

        if current == prev {
            let accum = runs.entry(current).or_insert(1);
            *accum += 1;
        }

        if current < prev {
            return false;
        }

        prev = current;
        x %= denom;
        denom /= 10;
    }

    if !runs.is_empty() {
        for (_, v) in runs {
            if v == 2 {
                return true;
            }
        }
    }

    false
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day04::Day04;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1653));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day04.txt");

        let mut day = Day04::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1133));
    }
}
