use aoc::{self, Coord3D, Day, Result, Velocity3D};
use lazy_static::lazy_static;
use log::info;
use regex::Regex;
use std::fmt::{self, Display};
use std::str::FromStr;
use std::cmp::Ordering;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day12 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub steps: usize,
    pub input: Vec<Moon>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day12 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day12::Part1;
    type Part2 = crate::day12::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, steps: 1000, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut moons = self.input.clone();

        for _ in 0..self.steps {
            moons = step(&moons);
        }

        let answer = moons.iter().fold(0, |accum, m| accum + m.energy());
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let x = find_steps_axis(self.input.iter().map(|m| m.pos.x).collect());
        let y = find_steps_axis(self.input.iter().map(|m| m.pos.y).collect());
        let z = find_steps_axis(self.input.iter().map(|m| m.pos.z).collect());
        self.part2 = Some(lcm3(x, y, z));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Default, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Moon {
    pub pos: Coord3D,
    pub vel: Velocity3D
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Moon {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(x: i64, y: i64, z: i64) -> Self {
        Moon { pos: Coord3D::new(x, y, z), vel: Velocity3D::default() }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new_moon(pos: Coord3D, vel: Velocity3D) -> Self {
        Moon { pos, vel }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn energy(&self) -> i64 {
        (self.pos.x.abs() + self.pos.y.abs() + self.pos.z.abs() ) *
            (self.vel.dx.abs() + self.vel.dy.abs() + self.vel.dz.abs() )
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Moon {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "pos={}, vel={}", self.pos, self.vel)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Moon {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^<x=(-?\d+), y=(-?\d+), z=(-?\d+)>$").unwrap();
        }
        if let Some(moon) = RE.captures(s) {
            let x = moon.get(1).unwrap().as_str().parse().unwrap();
            let y = moon.get(2).unwrap().as_str().parse().unwrap();
            let z = moon.get(3).unwrap().as_str().parse().unwrap();

            Ok(Self::new(x, y, z))
        } else {
            Err(aoc::Error::new(&format!("Unable to match {}", s)))
        }
    }
}

fn compare_axis(a: i64, b: i64) -> i64 {
    match a.cmp(&b) {
        Ordering::Less => 1,
        Ordering::Equal => 0,
        Ordering::Greater => -1,
    }
}

fn velocity_diff(positions: &[i64]) -> Vec<i64> {
    let mut result = vec![0; positions.len()];
    for (idx1, pos1) in positions.iter().enumerate() {
        for (idx2, pos2) in positions.iter().enumerate().skip(idx1 + 1) {
            result[idx1] += compare_axis(*pos1, *pos2);
            result[idx2] += compare_axis(*pos2, *pos1);
        }
    }
    result
}

fn find_steps_axis(mut positions: Vec<i64>) -> usize {
    let mut velocities = vec![0; positions.len()];
    let velocities_end = velocities.clone();

    let mut steps = 0;
    loop {
        let velocity_change = velocity_diff(&positions);
        for (v, change) in velocities.iter_mut().zip(velocity_change) {
            *v += change;
        }
        for (p, v) in positions.iter_mut().zip(velocities.iter()) {
            *p += v;
        }

        steps += 1;

        if velocities == velocities_end {
            break;
        }
    }
    steps * 2
}

fn gcd(x: usize, y: usize) -> usize {
    let mut x = x;
    let mut y = y;
    while y != 0 {
        let t = y;
        y = x % y;
        x = t;
    }
    x
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd(a, b)
}

fn lcm3(a: usize, b: usize, c: usize) -> usize {
    lcm(a, lcm(b, c))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn step(moons: &[Moon]) -> Vec<Moon> {
    let velocities = moons.iter().map(|m| calc_velocity(m, moons)).collect::<Vec<Velocity3D>>();
    moons.iter().zip(velocities).map(|(m, v)| Moon::new_moon(m.pos.next(&v), v)).collect::<Vec<Moon>>()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn calc_velocity(moon: &Moon, moons: &[Moon]) -> Velocity3D {
    let (mut dx, mut dy, mut dz) = (0, 0, 0);

    moons.iter()
        .filter(|&m| m != moon)
        .for_each(|m| {
            dx += if moon.pos.x < m.pos.x { 1 } else if moon.pos.x > m.pos.x { -1 } else { 0 };
            dy += if moon.pos.y < m.pos.y { 1 } else if moon.pos.y > m.pos.y { -1 } else { 0 };
            dz += if moon.pos.z < m.pos.z { 1 } else if moon.pos.z > m.pos.z { -1 } else { 0 };
        });

    let v = Velocity3D::new(moon.vel.dx + dx, moon.vel.dy + dy, moon.vel.dz + dz);
    info!("pos={}, vel={}", moon.pos, v);
    v
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day12::Day12;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>", 10, 179),
                ("<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>", 100, 1940)
            ];

        for &(input, steps, answer) in inputs.iter() {
            let mut day = Day12::setup(&input).unwrap();
            day.steps = steps;
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "Input:\n{}\n", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(9876));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>", 2772),
                ("<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>", 4686774924)
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day12::setup(&input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(answer), "Input:\n{}\n", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(307_043_147_758_488));
    }
}
