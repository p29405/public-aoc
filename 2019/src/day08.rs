use aoc::{self, AocFontType, Day, Result, GridElement, Grid, Map, Coord, to_string};
use log::debug;
use std::fmt;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub layers: Vec<Grid<Pixel>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let pixels = aoc::read_list_by_char_from_string(input)?;
        let layers = parse_grids(25, 6, &pixels);
        Ok(Self { part1: None, part2: None, layers})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let (_black, white, trans) = self.layers.iter()
            .map(|g| g.elements.iter()
                .fold((0, 0, 0), |(black, white, trans), p| match p {
                    Pixel::Black => (black + 1, white, trans),
                    Pixel::White => (black, white + 1, trans),
                    Pixel::Transparent => (black, white, trans + 1)
                }))
            .min_by(|(x, _, _), (y, _, _)| x.cmp(y)).unwrap();

        self.part1 = Some(white * trans);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut result = Grid::new(25, 6);

        for coord in Coord::range(result.upper_left(), result.lower_right()) {
            for layer in self.layers.iter() {
                if layer.get(&coord) != Pixel::Transparent {
                    result.set(&coord, layer.get(&coord));
                    break;
                }
            }
        }

        debug!("{}", result);
        let answer = to_string(&result, Pixel::White, AocFontType::Aoc2016).unwrap_or_else(|| "Not Found!".into());
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn parse_grids(width: i64, height: i64, pixels: &[Pixel]) -> Vec<Grid<Pixel>> {
    let mut layers = vec![];

    let num_layers = pixels.len() / (width * height) as usize;
    let mut pixel_iterator = pixels.iter();

    for _ in 0..num_layers {
        let mut grid = Grid::new(width, height);
        Coord::range(grid.upper_left(), grid.lower_right())
            .for_each(|coord| grid.set(&coord, *pixel_iterator.next().unwrap()));
        layers.push(grid);
    }

    layers
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Pixel {
    Black,
    White,
    Transparent
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Pixel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Pixel::Transparent
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Pixel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Pixel::Black => write!(f, " "),
            Pixel::White => write!(f, "#"),
            Pixel::Transparent => write!(f, " ")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Pixel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '0' => Pixel::Black,
            '1' => Pixel::White,
            '2' => Pixel::Transparent,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Pixel {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Pixel::to_element(s.chars().next().unwrap()))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::Day08;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1596));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("LBRCE".to_owned()));
    }
}
