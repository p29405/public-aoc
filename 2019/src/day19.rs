use aoc::{self, Day, Grid, GridElement, Result, Coord, Map};
use icvm::{IntCodeVM, Word};
use log::{debug, info};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day19 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day19 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day19::Part1;
    type Part2 = crate::day19::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let size = 50;
        let mut grid : Grid<Square> = Grid::new(size, size);
        let mut drone = IntCodeVM::default();

        Coord::range(Coord::new(0, 0), Coord::new(size, size))
            .for_each(|position| {
                drone.reset().program(&self.input).input(&[position.x, position.y]).run();

                grid.set(&position, if drone.output[0] == 1 { Square::Pull } else { Square::NoPull });

                debug!("({}) -> {:?}", position, drone.output);
            });

        info!("\n{}", grid);
        self.part1 = Some(grid.elements.iter().fold(0, |accum, &e| accum + if e == Square::Pull { 1 } else { 0 }));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut drone = IntCodeVM::default();
        let mut upper_right: Coord;
        let mut y = 100;
        let mut start_x = 0;

        'outer: loop {
            let mut x = start_x;
            'inner: loop {
                drone.reset().program(&self.input).input(&[x, y]).run();

                if drone.output[0] == 1 {
                    debug!("Possible Lower Left: ({}, {})", x, y);
                    start_x = x;

                    // Check upper right
                    upper_right = Coord::new(x + 99, y - 99);
                    drone.reset().program(&self.input).input(&[upper_right.x, upper_right.y]).run();

                    if drone.output[0] == 1 {
                        break 'outer;
                    } else {
                        break 'inner;
                    }
                }
                x += 1;
            }
            y += 1;
        }

        self.part2 = Some(start_x * 10_000 + upper_right.y);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    NoPull,
    Pull,
    Unknown
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Square::Unknown
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::NoPull => write!(f, "."),
            Square::Pull => write!(f, "#"),
            Square::Unknown => write!(f, " ")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '#' => Square::Pull,
            '.' => Square::NoPull,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day19::Day19;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(231));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(9_210_745));
    }
}
