use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day16 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day16 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day16::Part1;
    type Part2 = crate::day16::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(phase(&self.input, 100).iter().take(8).fold(0, |accum, i| accum * 10 + *i));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let offset = self.input[0..7].iter().fold(0, |accum, i| accum * 10 + *i);
        let signal = self.input.iter().cycle().take(self.input.len() * 10_000).skip(offset as usize).copied().collect::<Vec<i32>>();
        self.part2 = Some(phase_2(&signal, 100).iter().take(8).fold(0, |accum, i| accum * 10 + *i));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn phase(input: &[i32], phases: i32) -> Vec<i32> {
    let mut result = input.to_vec();

    for _ in 0..phases {
        for i in 0..result.len() {
            result[i] = result.iter()
                .zip(pattern(i as i32 + 1).skip(1))
                .filter(|&(_, b)| b != 0)
                .fold(0, |accum, (a, b)| accum + b * *a).abs() % 10;
        }
    }

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn phase_2(input: &[i32], phases: i32) -> Vec<i32> {
    let mut result = input.to_vec();

    for _ in 0..phases {
        let mut accum = 0;
        for i in (0..result.len()).rev() {
            accum += result[i];
            result[i] = accum % 10;
        }
    }

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn pattern(row: i32) -> impl Iterator<Item=i32> {
    std::iter::successors(Some((0, row, 0)), |&(i, row, _)| {
        let value = i + 1;
        let next = match (value / row) % 4 {
            0 | 2 => 0,
            1 => 1,
            3 => -1,
            _ => unreachable!()
        };
        Some((value, row, next))
    }).map(|(_, _,current)| current)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day16::Day16;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("80871224585914546619083218645595", 24_176_176),
                ("19617804207202209144916044189917", 73_745_418),
                ("69317163492948606335995924319873", 52_432_133)
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day16::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "Input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(28_430_146));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs =
            [
                ("03036732577212944063491565474664", 84_462_026),
                ("02935109699940807407585447034323", 78_725_270),
                ("03081770884921959731165446850517", 53_553_731)
            ];

        for &(input, answer) in inputs.iter() {
            let mut day = Day16::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(answer), "Input: {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(12_064_286));
    }
}