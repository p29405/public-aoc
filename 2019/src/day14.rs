use aoc::{self, Day, Result};
use log::debug;
use std::collections::{HashMap};
use std::str::FromStr;
use std::hash::Hash;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Reaction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day14::Part1;
    type Part2 = crate::day14::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(reduce(&self.input, "FUEL"));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn element_needs(reactions: &[Reaction], chemical: &str, quantity: i32, needs: &mut HashMap<String, i32>) -> Option<(String, i32)> {
    let current = find(reactions, chemical);

    if current.inputs[0].kind == "ORE" {
        return Some((current.result.kind, quantity));
    } else {
        // the multipiler isn't correct...
        let multiplier = quantity / current.result.amount + if quantity % current.result.amount != 0 { 1 } else { 0 };
        for input in current.inputs {
            debug!("Looking for {} {}", multiplier, input.kind);
            if let Some((element, value)) = element_needs(reactions, &input.kind, input.amount * multiplier, needs) {
                debug!("Found {} {}", value * multiplier, element);
                let entry = needs.entry(element).or_insert(0);
                *entry += value;

                debug!("Current {:?}", needs);
            }
        }
    }
    None
}
////////////////////////////////////////////////////////////////////////////////////////////////////
fn reduce(reactions: &[Reaction], chemical: &str) -> i32 {
    let mut needed = HashMap::new();
    debug!("");
    element_needs(reactions, chemical, 1, &mut needed);
    debug!("Needed {:?}", needed);

    let mut ore_needed = 0;

    for (element, amount) in needed {
        let formula = find(reactions, &element);

        if formula.inputs.len() != 1 {
            panic!("{:?}", formula);
        }
        let ore = &formula.inputs[0];
        let needed = formula.result;
        debug!("{} {} -> {} {}", ore.amount, ore.kind, needed.amount, needed.kind);

        let units = (amount / needed.amount) + if amount % needed.amount != 0 { 1 } else { 0 };

        ore_needed += units * ore.amount;
    }

    ore_needed
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn find(reactions: &[Reaction], chemical: &str) -> Reaction {
    for r in reactions {
        if r.result.kind == chemical {
            return r.clone();
        }
    }

    panic!("{} not found!", chemical);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct Element {
    kind: String,
    amount: i32
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Element {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.trim().split(' ').map(|s| s.trim().into()).collect::<Vec<String>>();
        let kind = parts[1].to_owned();
        let amount = parts[0].parse().unwrap();
        Ok(Element { kind, amount })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone)]
pub struct Reaction {
    result: Element,
    inputs: Vec<Element>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Reaction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let reaction_list = s.trim().split("=>").map(|s| s.trim().into()).collect::<Vec<String>>();
        let result = reaction_list[1].parse().unwrap();
        let inputs = reaction_list[0].split(',').map(|s| s.trim().parse().unwrap()).collect::<Vec<Element>>();
        Ok(Reaction { result, inputs })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::Day14;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const REACTIONS_1: &str =
        "10 ORE => 10 A
         1 ORE => 1 B
         7 A, 1 B => 1 C
         7 A, 1 C => 1 D
         7 A, 1 D => 1 E
         7 A, 1 E => 1 FUEL";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const REACTIONS_2: &str =
        "9 ORE => 2 A
         8 ORE => 3 B
         7 ORE => 5 C
         3 A, 4 B => 1 AB
         5 B, 7 C => 1 BC
         4 C, 1 A => 1 CA
         2 AB, 3 BC, 4 CA => 1 FUEL";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const REACTIONS_3: &str =
        "157 ORE => 5 NZVS
         165 ORE => 6 DCFZ
         44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
         12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
         179 ORE => 7 PSHF
         177 ORE => 5 HKGWZ
         7 DCFZ, 7 PSHF => 2 XJWVT
         165 ORE => 2 GPVTF
         3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const REACTIONS_4: &str =
        "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
         17 NVRVD, 3 JNWZP => 8 VPVL
         53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
         22 VJHF, 37 MNCFX => 5 FWMGM
         139 ORE => 4 NVRVD
         144 ORE => 7 JNWZP
         5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
         5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
         145 ORE => 6 MNCFX
         1 NVRVD => 8 CXFTF
         1 VJHF, 6 MNCFX => 4 RFSQX
         176 ORE => 6 VJHF";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    const REACTIONS_5: &str =
        "171 ORE => 8 CNZTR
         7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
         114 ORE => 4 BHXH
         14 VRPVC => 6 BMBT
         6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
         6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
         15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
         13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
         5 BMBT => 4 WPTQ
         189 ORE => 9 KTJDG
         1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
         12 VRPVC, 27 CNZTR => 2 XDBXC
         15 KTJDG, 12 BHXH => 5 XCVML
         3 BHXH, 2 VRPVC => 7 MZWV
         121 ORE => 7 VRPVC
         7 XCVML => 6 RJRHP
         5 BHXH, 4 VRPVC => 5 LTCX";

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        aoc::logger::init(aoc::logger::LevelFilter::Debug).unwrap();
        let inputs =
            [
                //(REACTIONS_1, 31),
                //(REACTIONS_2, 165),
                (REACTIONS_3, 13_312),
                //(REACTIONS_4, 180_697),
                //(REACTIONS_5, 2_210_736)
            ];

        for (i, &(input, answer)) in inputs.iter().enumerate() {
            let mut day = Day14::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(answer), "\nTest Case {} Input:\n         {}\n", i, input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
//        assert_eq!(day.part1_answer(), None);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), None);
    }
}
