use aoc::{self, Day, Modulus, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i128;
type Part2 = i128;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day22 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<ShuffleType>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day22 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day22::Part1;
    type Part2 = crate::day22::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let deck = 10_007;
        let modulus = Modulus::new(deck);
        let (m, b) = reduce(&self.input, &modulus);

        self.part1 = Some(modulus.add(modulus.mul(2019, m,), b));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let deck = 119_315_717_514_047i128;
        let num_shuffles = 101_741_582_076_661i128;
        let modulus = Modulus::new(deck);

        let target = 2020;

        let (m, b) = reduce(&self.input, &modulus);

        let x = modulus.mul(b, modulus.pow(m - 1, deck - 2,));
        let answer = modulus.add(
            modulus.mul(
                modulus.add(x, target),
                modulus.pow(modulus.pow(m, deck - 2), num_shuffles)),
            -x);
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn reduce(shuffles: &[ShuffleType], modulus: &Modulus) -> (i128, i128) {
    let transforms = shuffles.iter().map(|s| s.transform(modulus.modulus)).collect::<Vec<(i128, i128)>>();
    let mut m = transforms[0].0;
    let mut b = transforms[0].1;

    for transform in transforms.iter().skip(1) {
        b = modulus.add(modulus.mul(b, transform.0), transform.1);
        m = modulus.mul(m, transform.0);

    }

    (m, b)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy)]
pub enum ShuffleType {
    DealIntoNewStack,
    Cut(i128),
    DealWithIncrement(i128)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl ShuffleType {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn transform(&self, size: i128) -> (i128, i128) {
        match self {
            ShuffleType::DealIntoNewStack => (-1, size - 1),
            ShuffleType::Cut(c) => (1, size - c.rem_euclid(size)),
            ShuffleType::DealWithIncrement(i) => (*i, 0)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for ShuffleType {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        let shuffle_type = match parts[0] {
            "cut" => ShuffleType::Cut(parts[1].parse().unwrap()),
            "deal" => match parts[1] {
                "with" => ShuffleType::DealWithIncrement(parts[3].parse().unwrap()),
                "into" => ShuffleType::DealIntoNewStack,
                _ => panic!("Unknown shuffle {}", s)
            },
            _ => panic!("Unknown shuffle {}", s)
        };

        Ok(shuffle_type)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day22::Day22;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2519));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(58_966_729_050_483));
    }
}
