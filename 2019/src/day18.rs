use aoc::{self, Result, GridElement, Grid, Map, Coord};
use log::debug;
use std::fmt;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
fn main() -> Result<()> {
    aoc::initialize();
    let input = aoc::read_grid()?;
    aoc::setup_finished();

    aoc::answer("Part 1", &part1(&input))?;
    aoc::answer("Part 2", &part2(&input))?;

    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part1(map: &Grid<Square>) -> i32 {
    debug!("{}", map);
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn part2(_map:  &Grid<Square>) -> i32 {
    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn path_length(map: &Grid<Square>) -> u32 {
    let _start = Coord::range(map.upper_left(), map.lower_right())
        .find(|c| map.get(c) == Square::Start)
        .unwrap();

    let _keys = Coord::range(map.upper_left(), map.lower_right())
        .filter(|c| map.get(c).key())
        .map(|c| (c, map.get(&c)))
        .collect::<HashMap<Coord, Square>>();

    let _doors = Coord::range(map.upper_left(), map.lower_right())
        .filter(|c| map.get(c).door())
        .map(|c| (c, map.get(&c)))
        .collect::<HashMap<Coord, Square>>();



    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default)]
struct Path {
    path: Vec<Coord>,
    keys: Vec<char>,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Square {
    Wall,
    Empty,
    Door(char),
    Key(char),
    Start
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn key(&self) -> bool {
        match self {
            Square::Key(_) => true,
            _ => false
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn door(&self) -> bool {
        match self {
            Square::Door(_) => true,
            _ => false
        }
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Square::Wall
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Wall => write!(f, "#"),
            Square::Empty => write!(f, "."),
            Square::Door(d) => write!(f, "{}", d),
            Square::Key(k) => write!(f, "{}", k),
            Square::Start => write!(f, "@")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '#' => Square::Wall,
            '.' => Square::Empty,
            '@' => Square::Start,
            key if c >= 'a' && c <= 'z' => Square::Key(key),
            door if c >= 'A' && c <= 'Z' => Square::Door(door),
            _ => panic!("Unknown element {}", c)
        }
    }
}

#[cfg(test)]
mod test {
    use aoc;
    use crate::{part1, part2, Square};

    #[test]
    fn test_example_1() {
        let input =
            "#########
             #b.A.@.a#
             #########";
        let map = aoc::read_grid_from_string::<Square>(input).unwrap();

        //6
    }

    #[test]
    fn test_example_2() {
        let input =
            "########################
             #f.D.E.e.C.b.A.@.a.B.c.#
             ######################.#
             #d.....................#
             ########################";
        let map = aoc::read_grid_from_string::<Square>(input).unwrap();

        //86
    }
    #[test]
    fn test_example_3() {
        let input =
            "########################
             #...............b.C.D.f#
             #.######################
             #.....@.a.B.c.d.A.e.F.g#
             ########################";
        let map = aoc::read_grid_from_string::<Square>(input).unwrap();

        //132
    }

    #[test]
    fn test_example_4() {
        let input =
            "#################
             #i.G..c...e..H.p#
             ########.########
             #j.A..b...f..D.o#
             ########@########
             #k.E..a...g..B.n#
             ########.########
             #l.F..d...h..C.m#
             #################";
        let map = aoc::read_grid_from_string::<Square>(input).unwrap();

        //136
    }

    #[test]
    fn test_example_5() {
        let input =
            "########################
             #@..............ac.GI.b#
             ###d#e#f################
             ###A#B#C################
             ###g#h#i################
             ########################";
        let map = aoc::read_grid_from_string::<Square>(input).unwrap();

        //81
    }
}