use aoc::{self, Coord, Day, Direction, Result, GridElement, CenteredMap, Map};
use icvm::{IntCodeVM, Word};
use log::info;
use std::collections::VecDeque;
use std::cmp::min;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day15 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub map: CenteredMap<Square>,
    pub animate: bool,
    pub origin: Option<Coord>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day15 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day15::Part1;
    type Part2 = crate::day15::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let program = aoc::read_list_from_string(input, ",")?;
        let animate = false;
        let map = build_map(&program, animate);

        Ok(Self { part1: None, part2: None, map, animate, origin: None})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let (origin, oxygen_system) = self.map.elements.iter().find(|&(_, &r)| matches!(r, Square::OxygenSystem { min_path: _ })).unwrap();

        info!("{}", self.map);
        if let Square::OxygenSystem { min_path} = oxygen_system {
            self.origin = Some(*origin);
            self.part1 = Some(*min_path);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let oxygen_system = self.origin.unwrap();

        let mut minutes = 0;
        let mut frontier = vec![oxygen_system];
        let mut left = self.map.elements.iter()
            .filter(|&(_, &r)| r == Square::Empty)
            .map(|(c, _)| *c)
            .collect::<Vec<_>>();

        while !left.is_empty() {
            minutes += 1;
            let new = frontier.iter()
                .map(|&c| c.adjacent()
                    .filter(|c| self.map.get(c) == Square::Empty)
                    .collect::<Vec<_>>())
                .flatten()
                .collect::<Vec<_>>();

            for c in new.iter() {
                self.map.set(c, Square::Oxygen);
            }
            frontier = new;
            left = left.iter().filter(|&c| !frontier.contains(c)).copied().collect();
        }

        if self.animate { self.map.stop_animation(); }

        self.part2 = Some(minutes);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn build_map(program: &[Word], animate: bool) -> CenteredMap<Square> {
    let mut map = CenteredMap::default();
    let mut robot = IntCodeVM::default();
    let mut min_path = i64::max_value();
    robot.program(program);
    if animate { map.animate(Coord::new(-25, -25)); }
    map.set(&Coord::default(), Square::Empty);


    let mut unvisited: VecDeque<(Direction, Coord, i64, IntCodeVM)>;
    unvisited = [
        (Direction::North, Coord::default(), 0, robot.clone()),
        (Direction::East, Coord::default(), 0, robot.clone()),
        (Direction::South, Coord::default(), 0, robot.clone()),
        (Direction::West, Coord::default(), 0, robot),
    ].to_vec().into();

    while !unvisited.is_empty() {
        let (direction, origin, steps, mut robot) = unvisited.pop_front().unwrap();

        robot.input(&[to_input(direction)]);
        match robot.run_to_output() {
            Some(0) => {
                map.set(&origin.go(direction), Square::Wall);
            },
            Some(1) => {
                let current = origin.go(direction);
                map.set(&current, Square::Empty);

                for &next in &[Direction::North, Direction::East, Direction::South, Direction::West] {
                    if map.get(&current.go(next)) == Square::Unknown {
                        unvisited.push_back((next, current, steps + 1, robot.clone()));
                    }
                }
            },
            Some(2) => {
                min_path = min(min_path, steps + 1);
                let current = origin.go(direction);
                map.set(&current, Square::OxygenSystem { min_path });
                for &next in &[Direction::North, Direction::East, Direction::South, Direction::West] {
                    if map.get(&current.go(next)) == Square::Unknown {
                        unvisited.push_back((next, current, steps + 1, robot.clone()));
                    }
                }
            },
            Some(e) =>  panic!("Received unknown output {}", e),
            None => break
        }
    }
    map
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn to_input(direction: Direction) -> i64 {
    match direction {
        Direction::North => 1,
        Direction::East => 4,
        Direction::South => 2,
        Direction::West => 3
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Square {
    Unknown,
    Wall,
    Empty,
    OxygenSystem { min_path: i64 },
    Oxygen

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    fn default() -> Self {
        Square::Unknown
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Unknown => write!(f, " "),
            Square::Wall => write!(f, "#"),
            Square::Empty => write!(f, "."),
            Square::OxygenSystem { .. }=> write!(f, "o"),
            Square::Oxygen => write!(f, "O")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day, Coord};
    use crate::day15::Day15;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(300));
        assert_eq!(day.origin, Some(Coord::new(18, -18)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day15.txt");

        let mut day = Day15::setup(&input).unwrap();
        day.origin = Some(Coord::new(18, -18));
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(312));
    }
}
