use aoc::{self, Day, Result, Coord, GridElement, CenteredMap, Map};
use icvm::{IntCodeVM, Word};
use log::info;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>,
    pub animate: bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day13::Part1;
    type Part2 = crate::day13::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input, animate: false })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut game = IntCodeVM::default();
        let mut board = CenteredMap::default();

        game.program(&self.input);

        while let Some((x, y, t)) = collect_tile(&mut game) {
            board.set(&Coord::new(x, y), Tile::new(t));
        }

        info!("\n{}", board.invert());
        let answer = board.elements.values().fold(0, |accum, &v| accum + if v == Tile::Block { 1 } else { 0 });
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut game = IntCodeVM::default();
        let mut board = CenteredMap::default();
        let (mut paddle, mut ball) = (0, 0);
        let mut score = 0;

        if self.animate { board.animate(Coord::new(-5, -5)); }
        game.program(&self.input).poke(0, 2);

        while let Some((x, y, t)) = collect_tile(&mut game) {
            if x == -1 && y == 0 {
                score = t;
                if self.animate { print!("\x1B[4;6HScore: {}", score); }
            } else {
                if t == 3 { paddle = x; }
                if t == 4 { ball = x; }
                board.set(&Coord::new(x, y), Tile::new(t));
            }
            game.input(&[next_direction(ball, paddle)]);
        }

        if self.animate { board.stop_animation(); }

        self.part2 = Some(score);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn next_direction(ball: i64, paddle: i64) -> Word {
    let compare = ball - paddle;
    if compare == 0 { 0 } else { compare / compare.abs() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn collect_tile(game: &mut IntCodeVM) -> Option<(Word, Word, Word)> {
    let mut tile = vec![];
    for _ in 0..3 {
        match game.run_to_output() {
            Some(o) => tile.push(o),
            None => return None,
        };
    }

    Some((tile[0], tile[1], tile[2]))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Tile {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Tile {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn new(value: Word) -> Self {
        match value {
            0 => Tile::Empty,
            1 => Tile::Wall,
            2 => Tile::Block,
            3 => Tile::Paddle,
            4 => Tile::Ball,
            _ => panic!("Unknown value {}", value)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Tile {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Tile::Empty
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Tile {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Tile::Empty => write!(f, " "),
            Tile::Wall => write!(f, "|"),
            Tile::Block => write!(f, "#"),
            Tile::Paddle => write!(f, "_"),
            Tile::Ball => write!(f, "o"),
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Tile {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day13::Day13;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(412));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(20_940));
    }
}
