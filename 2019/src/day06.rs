use aoc::{self, Day, Result};
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u32;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub orbits: HashMap<String, String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day06::Part1;
    type Part2 = crate::day06::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_lists_from_string(input, ")")?;
        let orbits = parse(&input);

        Ok(Self { part1: None, part2: None, orbits})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut store = HashMap::new();

        let answer = self.orbits.keys().fold(0, |accum, outer| accum + orbit_depth(outer, &self.orbits, &mut store));
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let my_path = path("YOU", &self.orbits);
        let santa_path = path("SAN", &self.orbits);

        let mut distance = 0;
        for planet in my_path {
            if let Some(count) = santa_path.iter().position(|p| p == &planet) {
                distance += count;
                break;
            }
            distance += 1;
        }

        self.part2 = Some(distance);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn orbit_depth(positon: &str, orbits: &HashMap<String, String>, store: &mut HashMap<String, u32>) -> u32 {
    if let Some(inner) = orbits.get(positon) {
        if let Some(value) = store.get(inner) {
            return *value;
        }
        let value = orbit_depth(inner, orbits, store) + 1;
        store.insert(inner.to_string(), value);
        return value;
    }

    0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn path(start: &str, orbits: &HashMap<String, String>) -> Vec<String> {
    let mut path = Vec::new();
    let mut current = start;
    while let Some(inner) = orbits.get(current) {
        path.push(inner.to_string());
        current = inner;
    }

    path
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn parse(input: &[Vec<String>]) -> HashMap<String, String> {
    let mut orbits = HashMap::with_capacity(input.len());
    for orbit in input {
        orbits.insert(orbit[1].to_string(), orbit[0].to_string());
    }

    orbits
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::Day06;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L";

        let mut day = Day06::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(42));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(621_125));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN";

        let mut day = Day06::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(550));
    }
}
