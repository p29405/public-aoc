use aoc::{self, Day, Result};
use icvm::{IntCodeVM, Word, IoMode};
use log::{debug, info};
use std::collections::{HashMap, VecDeque};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day23 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day23 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day23::Part1;
    type Part2 = crate::day23::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut network = HashMap::new();
        for address in 0..50 {
            network.insert(address, NetworkNode::new(address as i64, &self.input));
        }

        loop {
            for address in 0..network.len() {
                let node = network.get_mut(&address).unwrap();
                node.execute();
                if let Some(packet) = node.output() {
                    debug!("Node {} -> {}: ({}, {})", address, packet.0, packet.1, packet.2);
                    if packet.0 == 255 {
                        self.part1 = Some(packet.2);
                        return;
                    } else {
                        network.get_mut(&(packet.0 as usize)).unwrap().input((packet.1, packet.2));
                    }
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut network = HashMap::new();
        let mut idle: bool;
        let mut last_sent_y = None;
        let mut nat: Option<(Word, Word)> = None;
        for address in 0..50 {
            network.insert(address, NetworkNode::new(address as i64, &self.input));
        }
        loop {
            idle = true;
            for address in 0..network.len() {
                let node = network.get_mut(&address).unwrap();
                node.execute();
                if let Some(packet) = node.output() {
                    idle = false;
                    info!("Node {} -> {}: ({}, {})", address, packet.0, packet.1, packet.2);
                    if packet.0 == 255 {
                        nat = Some((packet.1, packet.2));
                    } else {
                        network.get_mut(&(packet.0 as usize)).unwrap().input((packet.1, packet.2));
                    }
                }
            }

            if idle {
                if let Some(nat) = nat {
                    info!("NAT -> Node 0: Packet ({}, {})", nat.0, nat.1);
                    network.get_mut(&0).unwrap().input(nat);

                    if let Some(last_sent_y) = last_sent_y {
                        if last_sent_y == nat.1 {
                            self.part2 = Some(nat.1);
                            return;
                        }
                    }
                    last_sent_y = Some(nat.1);
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
struct NetworkNode {
    pub address: Word,
    computer: IntCodeVM,
    input: VecDeque<(Word, Word)>,
    output: VecDeque<(Word, Word, Word)>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl NetworkNode {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(address: Word, program: &[Word]) -> Self {
        let mut computer = IntCodeVM::default();
        computer.input(&[address]).program(program);
        NetworkNode { address, computer, input: VecDeque::new(), output: VecDeque::new() }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn input(&mut self, packet: (Word, Word)) {
        self.input.push_back(packet);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn output(&mut self) -> Option<(Word, Word, Word)>{
        self.output.pop_front()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn execute(&mut self) {
        let mut packet = Vec::with_capacity(3);
        loop {
            match self.computer.run_until_io() {
                IoMode::Output(output) => {
                    packet.push(output);
                    if packet.len() == 3 {
                        debug!("Node {} generating {} -> ({}, {})", self.address, packet[0], packet[1], packet[2]);
                        self.output.push_back((packet[0], packet[1], packet[2]));
                        packet.clear();
                    }
                },
                IoMode::InputRequest => {
                    if self.input.is_empty() {
                        self.computer.input.push_back(-1);
                        break;
                    } else {
                        for (x, y) in &self.input {
                            debug!("Node {} receiving ({}, {})", self.address, x, y);
                            self.computer.input.push_back(*x);
                            self.computer.input.push_back(*y);
                        }

                        self.input.clear();
                    }
                },
                IoMode::Halt => {
                    info!("Node {} Halting", self.address);
                    break;
                }
            };
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day23::Day23;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(17_286));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(11_249));
    }
}