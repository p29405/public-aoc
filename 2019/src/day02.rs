use aoc::{self, Day, Result};
use icvm::{IntCodeVM, Word};
use log::debug;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = Word;
type Part2 = Word;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut vm = IntCodeVM::default();
        vm.program(&self.input).poke(1, 12).poke(2, 2).run();

        self.part1 = Some(vm.peek(0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut vm = IntCodeVM::default();
        let target = 19_690_720;

        for noun in 0..100 {
            for verb in 0..100 {
                vm.reset().program(&self.input).poke(1, noun).poke(2, verb).run();

                if vm.peek(0) == target {
                    self.part2 = Some(100 * noun + verb);
                    return;
                } else {
                    debug!("Noun: {}, Verb: {}, Value = {}", noun, verb, vm.peek(0));
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4_090_701));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6_421));
    }
}