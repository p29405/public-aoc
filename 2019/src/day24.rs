use aoc::{self, Day, Grid, GridElement, Result, Map};
use std::fmt;
use std::collections::HashMap;
use std::cmp::{max, min};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = u64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day24 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Tile>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day24 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day24::Part1;
    type Part2 = crate::day24::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut map = HashMap::new();

        let mut grid = self.input.clone();

        loop {
            let score = biodiversity(&grid);
            let entry = map.entry(score).or_insert(0);
            *entry += 1;

            if *entry > 1 {
                self.part1 = Some(score);
                return;
            }
            grid = next_minute(&grid);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut grid = GridPart2::new(&self.input);
        for _ in 0..200 {
            grid.next_minute();
        }

        self.part2 = Some(grid.count());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Tile {
    Bug,
    Empty,
    Middle
}

impl Default for Tile {
    fn default() -> Self {
        Tile::Empty
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Tile {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Tile::Empty => write!(f, "."),
            Tile::Bug => write!(f, "#"),
            Tile::Middle => write!(f, "?")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Tile {
    fn to_element(c: char) -> Self {
        match c {
            '#' => Tile::Bug,
            '.' => Tile::Empty,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn next_minute(grid: &Grid<Tile>) -> Grid<Tile> {
    let mut next = Grid::new(grid.columns, grid.rows);

    for y in 0..grid.rows {
        for x in 0..grid.columns {
            let count: i32 = grid.adjacent(x, y).map(|s| if s == Tile::Empty { 0 } else { 1 }).sum();

            let mut is_bug = grid.get_xy(x, y) == Tile::Bug && count == 1;
            is_bug |= grid.get_xy(x, y) == Tile::Empty && (count == 1 || count == 2);

            if is_bug {
                next.set_xy(x, y, Tile::Bug);
            }
        }
    }

    next
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn biodiversity(grid: &Grid<Tile>) -> u64 {
    let mut points = 1;
    let mut score = 0;


    for y in 0..grid.rows {
        for x in 0..grid.columns {
            if grid.get_xy(x, y) == Tile::Bug {
                score += points;
            }
            points <<= 1;
        }
    }

    score
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct GridPart2 {
    pub grids: HashMap<i32, Grid<Tile>>,
    pub min: i32,
    pub max: i32
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridPart2 {
    pub fn new(grid: &Grid<Tile>) -> Self {
        let mut grids = HashMap::new();
        let mut initial = grid.clone();

        initial.set_xy(2, 2, Tile::Middle);

        grids.insert(0, initial);

        Self { grids, min: 0, max: 0 }
    }

    pub fn new_level(&mut self, level: i32) {
        let mut new  = Grid::default();
        new.columns = 5;
        new.rows = 5;
        new.elements.resize(25, Tile::Empty);
        new.set_xy(2, 2, Tile::Middle);

        self.grids.insert(level, new);
        self.max = max(self.max, level);
        self.min = min(self.min, level);
    }

    pub fn outer_has_bugs(&self, level: i32) -> bool {
        let grid = self.grids.get(&level).unwrap();

        let mut found = false;

        for y in 0..5 {
            found |= grid.get_xy(0, y) == Tile::Bug;
            found |= grid.get_xy(grid.rows - 1, y) == Tile::Bug;
        }

        for x in 0..5 {
            found |= grid.get_xy(x, 0) == Tile::Bug;
            found |= grid.get_xy(x, grid.columns - 1) == Tile::Bug;
        }

        found
    }

    pub fn inner_has_bugs(&self, level: i32) -> bool {
        let grid = self.grids.get(&level).unwrap();

        grid.get_xy(1, 2) == Tile::Bug ||
            grid.get_xy(2, 1) == Tile::Bug ||
            grid.get_xy(2, 3) == Tile::Bug ||
            grid.get_xy(3, 2) == Tile::Bug
}

    pub fn next_minute(&mut self) {
        if self.inner_has_bugs(self.min) {
            self.new_level(self.min - 1);
        }

        if self.outer_has_bugs(self.max) {
            self.new_level(self.max + 1);
        }

        let mut new_grids =  HashMap::new();

        for l in self.min..=self.max {
            let mut new_grid = Grid::default();
            new_grid.columns = 5;
            new_grid.rows = 5;
            new_grid.elements.resize(25, Tile::Empty);
            for y in 0..5 {
                for x in 0..5 {
                    if x == 2 && y == 2 {
                        new_grid.set_xy(x, y, Tile::Middle);
                        continue;
                    }

                    let bugs = self.adjacent(l, x, y);
                    let grid = self.grids.get(&l).unwrap();

                    if grid.get_xy(x, y) == Tile::Bug && bugs == 1 {
                        new_grid.set_xy(x, y, Tile::Bug);
                    }
                    if grid.get_xy(x, y) == Tile::Empty && (bugs == 1 || bugs == 2) {
                        new_grid.set_xy(x, y, Tile::Bug);
                    }

                }
            }
            new_grids.insert(l, new_grid);
        }
        self.grids = new_grids;
    }

    fn adjacent(&self, level: i32, x: i64, y: i64) -> u32 {
        let grid = self.grids.get(&level).unwrap();

        let mut bugs = grid.adjacent(x, y).map(|t| if t == Tile::Bug { 1 } else { 0} ).sum();

        if let Some(inner) = self.grids.get(&(level - 1)) {
            if x == 2 && y == 1 {
                for inner_x in 0..inner.columns {
                    bugs += if inner.get_xy(inner_x, 0) == Tile::Bug { 1 } else { 0 };
                }
            } else if x == 1 && y == 2 {
                for inner_y in 0..inner.rows {
                    bugs += if inner.get_xy(0, inner_y) == Tile::Bug { 1 } else { 0 };
                }
            } else if x == 3 && y == 2 {
                for inner_y in 0..inner.rows {
                    bugs += if inner.get_xy(inner.columns - 1, inner_y) == Tile::Bug { 1 } else { 0 };
                }
            } else if x == 2 && y == 3 {
                for inner_x in 0..inner.columns {
                    bugs += if inner.get_xy(inner_x, inner.rows - 1) == Tile::Bug { 1 } else { 0 };
                }
            }
        }

        if let Some(outer) = self.grids.get(&(level + 1)) {
            if x == 0 {
                bugs += if outer.get_xy(1, 2) == Tile::Bug { 1 } else { 0 };
            }
            if y == 0 {
                bugs += if outer.get_xy(2, 1) == Tile::Bug { 1 } else { 0 };
            }
            if x == grid.columns - 1 {
                bugs += if outer.get_xy(3, 2) == Tile::Bug { 1 } else { 0 };
            }
            if y == grid.rows - 1 {
                bugs += if outer.get_xy(2, 3) == Tile::Bug { 1 } else { 0 };
            }
        }

        bugs

    }

    pub fn count(&self) -> i64 {
        let mut sum: i64 = 0;
        for g in self.grids.values() {
            sum += g.elements.iter().map(|&t| if t == Tile::Bug { 1 } else { 0 }).sum::<i64>();
        }

        sum
    }


}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day24::Day24;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "....#
             #..#.
             #..##
             ..#..
             #....";

        let mut day = Day24::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2_129_920));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(3_186_366));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2031));
    }
}