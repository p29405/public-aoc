use aoc::{self, Day, Result};
use icvm::{IntCodeVM, Word};
use log::debug;
use std::io;
use std::collections::VecDeque;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day25 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day25 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day25::Part1;
    type Part2 = crate::day25::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut droid = IntCodeVM::default();

        droid.reset().program(&self.input);

        run_until_commands_exhausted(&mut droid, get_to_security_checkpoint().iter().map(|&s| s.to_owned()).collect::<VecDeque<String>>());

        for selector in 0..1u8 << 7 {
            let mut commands = take_drop(selector);
            droid.ascii_input(&commands.pop_front().unwrap());
            if let Some(code) = run_until_commands_exhausted(&mut droid, commands) {
                self.part1 = Some(code);
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn run_until_commands_exhausted(droid: &mut IntCodeVM, mut commands: VecDeque<String>) -> Option<String>{
    while let Some(string) = droid.run_to_ascii_output() {
        debug!("{}", string);

        if &string == "Command?\n" {
            let input: String;
            if commands.is_empty() {
                break;
            } else {
                input = commands.pop_front().unwrap();
            }
            droid.ascii_input(&input);
        }

        if string.starts_with("\"Oh, hello! You should be able to get in by typing") {
            return Some(string.split(" ").nth(11).unwrap().to_owned());
        }
    }

    None
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn _part1_first(program: &[Word]) -> Word {
    let mut previous_commands = get_to_security_checkpoint().iter().map(|&s| s.to_owned()).collect::<VecDeque<String>>();
    let mut droid = IntCodeVM::default();

    loop {
        let mut current_commands = vec![];
        droid.reset().program(program);
        while let Some(string) = droid.run_to_ascii_output() {
            debug!("{}", string);

            if &string == "Command?\n" {
                let mut input = String::new();
                if previous_commands.is_empty() {
                    io::stdin().read_line(&mut input).unwrap();
                } else {
                    input = previous_commands.pop_front().unwrap();
                }
                current_commands.push(input.to_owned());
                droid.ascii_input(&input);
            }
        }

        previous_commands = current_commands.iter().map(|s| s.to_owned()).collect();
        previous_commands.pop_back();
    }
}

fn take_drop(select: u8) -> VecDeque<String> {
    let mut commands = VecDeque::new();
    let items = items();

    for i in 0..items.len() as u8 {
        if select & 1 << i == 1 << i {
            commands.push_back(format!("take {}", items[i as usize]));
        } else {
            commands.push_back(format!("drop {}", items[i as usize]));
        }
    }

    commands.push_back("east\n".to_owned());

    commands
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn items() -> [&'static str; 7] {
    [
        "shell\n",
        "klein bottle\n",
        "tambourine\n",
        "weather machine\n",
        "antenna\n",
        "spool of cat6\n",
        "mug\n"
    ]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn get_to_security_checkpoint() -> [&'static str; 44] {
    [
        "north\n",
        "take weather machine\n",
        "north\n",
        "take klein bottle\n",
        "east\n",
        "take spool of cat6\n",
        "east\n",
        "north\n",
        "east\n",
        "north\n",
        "north\n",
        "take tambourine\n",
        "south\n",
        "south\n",
        "south\n",
        "take shell\n",
        "east\n",
        "south\n",
        "north\n",
        "west\n",
        "north\n",
        "west\n",
        "south\n",
        "south\n",
        "take mug\n",
        "north\n",
        "west\n",
        "south\n",
        "south\n",
        "north\n",
        "south\n",
        "north\n",
        "north\n",
        "west\n",
        "south\n",
        "south\n",
        "east\n",
        "take antenna\n",
        "west\n",
        "north\n",
        "north\n",
        "east\n",
        "south\n",
        "south\n",
    ]
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day25::Day25;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day25.txt");

        let mut day = Day25::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("805307408".to_owned()));
    }
}