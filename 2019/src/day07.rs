use aoc::{self, Day, Result, permutations};
use icvm::{IntCodeVM, Word};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = Word;
type Part2 = Word;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day07 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Word>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day07 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day07::Part1;
    type Part2 = crate::day07::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = permutations(&[0, 1, 2, 3, 4])
            .map(|phase| calc_thrust(&phase, &self.input))
            .max();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = permutations(&[5, 6, 7, 8, 9])
            .map(|phase| calc_thrust_2(&phase, &self.input))
            .max();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn calc_thrust(input: &[Word], program: &[Word]) -> Word {
    input.iter()
        .map(|&input| {
            let mut amp = IntCodeVM::default();
            amp.program(program).input(&[input]);
            amp
        })
        .fold(0, |output, mut amp| {
            amp.input.push_back(output);
            amp.run_to_output().unwrap_or(-1)
        })
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn calc_thrust_2(input: &[Word], program: &[Word]) -> Word {
    let num_amps = input.len();

    let mut amps: Vec<IntCodeVM> = input.iter()
        .map(|&i| {
            let mut amp = IntCodeVM::default();
            amp.program(program).input(&[i]);
            amp
        }).collect();

    let mut output = 0;

    for index in 0.. {
        let amp = index % num_amps;
        amps[amp].input.push_back(output);
        match amps[amp].run_to_output() {
            Some(val) => output = val,
            None => break
        }
    }

    output
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day07::{Day07, calc_thrust, calc_thrust_2};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_1() {
        let input =
            [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0];

        assert_eq!(calc_thrust(&[4, 3, 2, 1, 0], &input), 43_210);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_2() {
        let input =
            [3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0];

        assert_eq!(calc_thrust(&[0, 1, 2, 3, 4], &input), 54_321);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_3() {
        let input =
            [3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0];

        assert_eq!(calc_thrust(&[1, 0, 4, 3, 2], &input), 65_210);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(38_500));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_4() {
        let input =
            [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
                27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5];

        assert_eq!(calc_thrust_2(&[9, 8, 7, 6, 5], &input), 139_629_729);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example_5() {
        let input =
            [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
                -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
                53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10];

        assert_eq!(calc_thrust_2(&[9, 7, 8, 5, 6], &input), 18_216);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day07.txt");

        let mut day = Day07::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(33_660_560));
    }

}