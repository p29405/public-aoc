use aoc::{self, Coord, Day, Grid, GridElement, Result, Slope, Map};
use log::info;
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Square>,
    pub base: Option<Coord>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input, base: None })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let (base, answer) = best(&self.input);
        self.base = Some(base);
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let base = self.base.unwrap();

        let solution = firing_solution(&base, &self.input);
        let goal = solution[199].1;

        self.part2 = Some(goal.x * 100 + goal.y);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn best(map: &Grid<Square>) -> (Coord, usize) {
    map.iter()
        .filter(|(_, s)| *s == Square::Asteroid)
        .map(|(current, _)| (current, asteroids_visible(&current, map)))
        .max_by_key(|(_, most_visible)| *most_visible)
        .unwrap()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn asteroids_visible(coord: &Coord, map: &Grid<Square>) -> usize {
    let mut working = map.clone();

    for target in Coord::range(map.upper_left(), map.lower_right()) {
        if coord != &target && working.get(&target).asteroid() {
            let slope = Slope::new(coord, &target);
            info!("{} -> {} has {:?} angle {}", coord, target, slope, slope.angle() + std::f64::consts::FRAC_PI_2);
            let mut check = slope.next(*coord);
            let mut blocked = false;
            loop {
                match working.get(&check) {
                    Square::Unknown => break,
                    Square::Asteroid => {
                        if blocked {
                            working.set(&check, Square::Blocked);
                        } else {
                            blocked = true;
                        }
                    },
                    _ => ()
                };
                check = slope.next(check);
            }
        }
    }

    info!("\n{}", working);

    working.elements.iter().fold(0, |sum, &e| sum + if e == Square::Asteroid { 1 } else { 0 }) - 1
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn firing_solution(coord: &Coord, map: &Grid<Square>) -> Vec<(f64, Coord)> {
    let mut working = map.clone();

    for target in Coord::range(map.upper_left(), map.lower_right()) {
        if coord != &target && working.get(&target) == Square::Asteroid {
            let slope = Slope::new(coord, &target);
            let mut check = slope.next(*coord);
            let mut depth = 0;
            loop {
                match working.get(&check) {
                    Square::Unknown => break,
                    Square::Asteroid => {
                        let mut angle = slope.angle();
                        if angle < -std::f64::consts::FRAC_PI_2 {
                            angle += std::f64::consts::PI * 2f64;
                        }
                        angle += std::f64::consts::PI * 2f64 * depth as f64;
                        info!("{} -> {} has slope {:?} atan2 of {} angle {}", coord, check, slope, slope.angle(), angle);

                        working.set(&check, Square::Target{ coord: check, angle});
                        depth += 1;

                    },
                    _ => ()
                };
                check = slope.next(check);
            }
        }
    }

    let mut solution = working.elements.iter()
        .filter(|e| e.target())
        .map(|e|
            if let Square::Target { coord, angle } = e {
                (*angle, *coord)
            } else {
                unreachable!();
            })
        .collect::<Vec<(f64, Coord)>>();


    // Yuck!
    solution.sort_by_key(|(f, _)| (f * 1_000_000f64) as i64);
    solution

}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Square {
    Unknown,
    Asteroid,
    Empty,
    Blocked,
    Target { coord: Coord, angle: f64 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn target(&self) -> bool {
        matches!(self, Square::Target { .. })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn asteroid(&self) -> bool {
        matches!(self, Square::Asteroid)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Square::Unknown
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Unknown => write!(f, " "),
            Square::Empty => write!(f, "."),
            Square::Asteroid => write!(f, "#"),
            Square::Blocked => write!(f, "X"),
            Square::Target { .. } => write!(f, "T")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    fn to_element(c: char) -> Self {
        match c {
            '#' => Square::Asteroid,
            '.' => Square::Empty,
            _   => panic!("Unknown element: {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{self, Coord, Day};
    use crate::day10::{Day10, Square, asteroids_visible};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_visible() {
        let input =
            ".#..#
             .....
             #####
             ....#
             ...##";

        let grid = aoc::read_grid_from_string::<Square>(input).unwrap();

        assert_eq!(asteroids_visible(&Coord::new(1, 0), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(4, 0), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(0, 2), &grid), 6);
        assert_eq!(asteroids_visible(&Coord::new(1, 2), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(2, 2), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(3, 2), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(4, 2), &grid), 5);
        assert_eq!(asteroids_visible(&Coord::new(4, 3), &grid), 7);
        assert_eq!(asteroids_visible(&Coord::new(3, 4), &grid), 8);
        assert_eq!(asteroids_visible(&Coord::new(4, 4), &grid), 7);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "......#.#.
                           #..#.#....
                           ..#######.
                           .#.#.###..
                           .#..#.....
                           ..#....#.#
                           #..#....#.
                           .##.#..###
                           ##...#..#.
                           .#....####";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(33));
        assert_eq!(day.base, Some(Coord::new(5, 8)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "#.#...#.#.
                           .###....#.
                           .#....#...
                           ##.#.#.#.#
                           ....#.#.#.
                           .##..###.#
                           ..#...##..
                           ..##....##
                           ......#...
                           .####.###.";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(35));
        assert_eq!(day.base, Some(Coord::new(1, 2)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_3() {
        let input =
            ".#..##.###...#######
             ##.############..##.
             .#.######.########.#
             .###.#######.####.#.
             #####.##.#.##.###.##
             ..#####..#.#########
             ####################
             #.####....###.#.#.##
             ##.#################
             #####.##.###..####..
             ..######..##.#######
             ####.##.####...##..#
             .#####..#.######.###
             ##...#.##########...
             #.##########.#######
             .####.#.###.###.#.##
             ....##.##.###..#####
             .#.#.###########.###
             #.#.#.#####.####.###
             ###.##.####.##.#..##";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(210));
        assert_eq!(day.base, Some(Coord::new(11, 13)));
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(802));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(314));
        assert_eq!(day.base, Some(Coord::new(27, 19)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.base = Some(Coord::new(27, 19));
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1513));
    }
}