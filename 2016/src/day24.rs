use aoc::{self, Day, Result, GridElement, Coord, Grid, Map, Path, find_paths, permutations};
use std::fmt;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day24 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub paths: HashMap<Coord, Grid<Path>>,
    pub start: Coord,
    pub destinations: Vec<Coord>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day24 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day24::Part1;
    type Part2 = crate::day24::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;
        let points = points_of_interest(&input);
        let paths = build_paths(&points, &input);
        let start = *points.iter()
                         .find(|p| input.get(p).is_point(0))
                         .unwrap();
        let destinations = points.iter()
                               .filter(|&p| !input.get(p).is_point(0))
                               .copied()
                               .collect::<Vec<_>>();
        Ok(Self { part1: None, part2: None, paths, start, destinations })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = permutations(&self.destinations)
            .map(|path| path_distance(self.start, &path, &self.paths))
            .min();

        self.part1 = answer;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = permutations(&self.destinations)
            .map(|path| path_distance_2(self.start, &path, &self.paths))
            .min();

        self.part2 = answer;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fn points_of_interest(map: &Grid<Element>) -> Vec<Coord> {
    map.iter().filter(|(_, e)| e.interesting()).map(|(p, _)| p).collect()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn build_paths(points: &[Coord], map: &Grid<Element>) -> HashMap<Coord, Grid<Path>> {
    let mut paths = HashMap::new();

    for point in points {
        let start = *point;
        let ends = points.iter().filter(|&p| p != &start).copied().collect::<Vec<_>>();
        let path = find_paths(&[start], &ends, map.columns, map.rows, |_, p| map.get(p) != Element::Wall);
        paths.insert(*point, path);
    }
    paths
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn path_distance(start: Coord, path: &[Coord], paths: &HashMap<Coord, Grid<Path>>) -> i32 {
    let mut distance = 0;
    let mut prev = start;

    for current in path {
        let entry = paths.get(&prev).unwrap();
        distance += entry.get(current).distance() as i32;
        prev = *current;
    }

    distance
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn path_distance_2(start: Coord, path: &[Coord], paths: &HashMap<Coord, Grid<Path>>) -> i32 {
    let mut distance = 0;
    let mut prev = start;

    for current in path {
        let entry = paths.get(&prev).unwrap();
        distance += entry.get(current).distance() as i32;
        prev = *current;
    }

    let entry = paths.get(&prev).unwrap();
    distance += entry.get(&start).distance() as i32;

    distance
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Element {
    Open,
    Wall,
    Point(i32)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn interesting(&self) -> bool {
        matches!(self, Element::Point(_))
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn is_point(&self, id: i32) -> bool {
        if let Element::Point(p) = self {
            p == &id
        } else {
            false
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Element::Wall
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Element::Open => write!(f, "."),
            Element::Wall => write!(f, "#"),
            Element::Point(d) => write!(f, "{}", d)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Element {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(c: char) -> Self {
        match c {
            '.' => Element::Open,
            '#' => Element::Wall,
            d => Element::Point(d as i32  - '0' as i32),
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day24::Day24;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "###########
             #0.1.....2#
             #.#######.#
             #4.......3#
             ###########";

        let mut day = Day24::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(14));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(500));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(748));
    }
}
