use aoc::{self, Day, Result};
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<String>,
    pub length: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day06::Part1;
    type Part2 = crate::day06::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input, length: 8})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = counts(&self.input, self.length).iter().
            fold(String::new(), |message, map| {
                format!("{}{}", message, map.iter().max_by_key(|(_, count)| *count).unwrap().0)
            });
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = counts(&self.input, self.length).iter().
            fold(String::new(), |message, map| {
                format!("{}{}", message, map.iter().min_by_key(|(_, count)| *count).unwrap().0)
            });
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn counts(input: &[String], len: usize) -> Vec<HashMap<char, i64>> {
    let mut counts = Vec::with_capacity(len);

    for _ in 0..len {
        counts.push(HashMap::new());
    }

    input.iter().for_each(|m| {
        m.chars().enumerate().for_each(|(i, c)| {
            let entry = counts[i].entry(c).or_insert(0);
            *entry += 1;
        });
    });

    counts
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::Day06;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "eedadn
             drvtee
             eandsr
             raavrd
             atevrs
             tsrnev
             sdttsa
             rasrtv
             nssdts
             ntnada
             svetve
             tesnvt
             vntsnd
             vrdear
             dvrsen
             enarar";

        let mut day = Day06::setup(input).unwrap();
        day.length = 6;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("easter".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("kqsdmzft".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "eedadn
             drvtee
             eandsr
             raavrd
             atevrs
             tsrnev
             sdttsa
             rasrtv
             nssdts
             ntnada
             svetve
             tesnvt
             vntsnd
             vrdear
             dvrsen
             enarar";

        let mut day = Day06::setup(input).unwrap();
        day.length = 6;
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("advent".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");

        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("tpooccyo".to_string()));
    }
}
