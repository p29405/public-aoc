use aoc::{self, Day, Coord, Direction, Result};
use std::collections::HashSet;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day01 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Step>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day01 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day01::Part1;
    type Part2 = crate::day01::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut direction = Direction::North;
        let mut position = Coord::default();

        for step in self.input.iter() {
            match step {
                Step::Left(x) => {
                    direction = direction.turn_left();
                    (0..*x).for_each(|_| position = position.go(direction));
                },
                Step::Right(x) => {
                    direction = direction.turn_right();
                    (0..*x).for_each(|_| position = position.go(direction));
                }
            };
        }

        self.part1 = Some(position.x.abs() + position.y.abs());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut direction = Direction::North;
        let mut position = Coord::default();
        let mut positions: HashSet<Coord> = HashSet::default();

        positions.insert(position);

        'outer: for step in self.input.iter() {
            let distance;
            match step {
                Step::Left(x) => {
                    direction = direction.turn_left();
                    distance = *x;
                },
                Step::Right(x) => {
                    direction = direction.turn_right();
                    distance= *x;
                }
            };

            for _ in 0..distance {
                position = position.go(direction);

                if positions.get(&position).is_some() {
                    break 'outer;
                } else {
                    positions.insert(position);
                }
            }
        }

        self.part2 = Some(position.x.abs() + position.y.abs());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Step {
    Left(i64),
    Right(i64)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Step {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut chars = s.chars();
        let turn = chars.next().unwrap();

        let distance = chars.as_str().parse::<i64>().unwrap();
        match turn {
            'L' => Ok(Step::Left(distance)),
            'R' => Ok(Step::Right(distance)),
            e => Err(aoc::Error::new(&format!("Unknown direction {}", e)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day01::{Day01, Step};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        assert_eq!("L5".parse::<Step>().unwrap(), Step::Left(5));
        assert_eq!("R2".parse::<Step>().unwrap(), Step::Right(2));
        assert_eq!("L17".parse::<Step>().unwrap(), Step::Left(17));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs: [(&str, i64); 3] =
            [
                ("R2, L3", 5),
                ("R2, R2, R2", 2),
                ("R5, L5, R5, R3", 12)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day01::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(253));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "R8, R4, R4, R8";

        let mut day = Day01::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day01.txt");

        let mut day = Day01::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(126));
    }
}
