use aoc::{self, Day, Result};
use lib_2016::bunny::{Instruction, BunnyVM, Register, BunnyOptimization};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day23 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day23 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day23::Part1;
    type Part2 = crate::day23::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut bunny = BunnyVM::default();
        bunny.program(&self.input).poke(Register::A, 7);
        bunny.optimization(4, Box::new(Optimization::default()));
        while bunny.step() {}

        self.part1 = Some(bunny.peek(Register::A));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut bunny = BunnyVM::default();
        bunny.program(&self.input).poke(Register::A, 12);
        bunny.optimization(4, Box::new(Optimization::default()));

        while bunny.step() {}

        self.part2 = Some(bunny.peek(Register::A));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
struct Optimization;

////////////////////////////////////////////////////////////////////////////////////////////////////
impl BunnyOptimization for Optimization {
    fn execute(&self, registers: &mut [i64; 4]) -> usize {
        registers[0] = registers[1] * registers[3];
        registers[2] = 0;
        registers[3] = 0;
        10
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day23::Day23;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(11_760));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(479_008_320));
    }
}
