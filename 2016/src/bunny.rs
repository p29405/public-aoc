use log::debug;
use std::str::FromStr;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Register { A, B, C, D }

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Register {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "a" => Ok(Register::A),
            "b" => Ok(Register::B),
            "c" => Ok(Register::C),
            "d" => Ok(Register::D),
            _ => Err(aoc::Error::new(&format!("Unknown register {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Value {
    Immediate { value: i64 },
    Register { register: Register }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Value {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.parse::<i64>() {
            Ok(value) => Ok(Value::Immediate { value }),
            _ => Ok(Value::Register { register: s.parse().unwrap() })
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Instruction {
    Cpy { op1: Value, op2: Value },
    Inc { op1: Value },
    Dec { op1: Value },
    Jnz { op1: Value, op2: Value },
    Tgl { op1: Value },
    Out { op1: Value }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts = s.split(' ').collect::<Vec<&str>>();

        match parts[0] {
            "cpy" => Ok(Instruction::Cpy {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "inc" => Ok(Instruction::Inc {
                op1: parts[1].parse().unwrap()
            }),
            "dec" => Ok(Instruction::Dec {
                op1: parts[1].parse().unwrap()
            }),
            "jnz" => Ok(Instruction::Jnz {
                op1: parts[1].parse().unwrap(),
                op2: parts[2].parse().unwrap()
            }),
            "tgl" => Ok(Instruction::Tgl {
                op1: parts[1].parse().unwrap()
            }),
            "out" => Ok(Instruction::Out {
                op1: parts[1].parse().unwrap()
            }),
            _ => Err(aoc::Error::new(&format!("Unknown opcode {}", parts[0])))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub trait BunnyOptimization {
    fn execute(&self, registers: &mut [i64; 4]) -> usize;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
pub struct BunnyVM {
    registers: [i64; 4],
    ip: usize,
    program: Vec<Instruction>,
    optimizations: HashMap<usize, Box<dyn BunnyOptimization>>,
    output: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl BunnyVM {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[Instruction]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn peek(&mut self, register: Register) -> i64 {
        *self.register(register)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn poke(&mut self, register: Register, value: i64) {
        *self.register(register) = value
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn optimization(&mut self, instruction: usize, optimization: Box<dyn BunnyOptimization>) -> &mut Self {
        self.optimizations.insert(instruction, optimization);
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn output(&self) -> &[i64] {
        &self.output
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(&mut self) -> bool {
        debug!("ip: {} {:?} {:?}", self.ip, self.registers, self.program[self.ip]);

        if let Some(optimization) = self.optimizations.get(&self.ip) {
            self.ip = optimization.execute(&mut self.registers);
        } else if let Some(jump) = self.execute(self.program[self.ip]) {
            self.ip = self.ip.overflowing_add(jump).0;
        } else {
            self.ip += 1;
        }

        self.ip < self.program.len()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn register(&mut self, register: Register) -> &mut i64 {
        match register {
            Register::A => &mut self.registers[0],
            Register::B => &mut self.registers[1],
            Register::C => &mut self.registers[2],
            Register::D => &mut self.registers[3],
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fetch(&mut self, value: Value) -> i64 {
        match value {
            Value::Immediate { value} => value,
            Value::Register { register } => *self.register(register)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: Instruction) -> Option<usize> {
        match instruction {
            Instruction::Cpy { op1, op2 } => {
                if let Value::Register { register } = op2 {
                    *self.register(register) = self.fetch(op1);
                }
                None
            },
            Instruction::Inc { op1 } => {
                if let Value::Register { register } = op1 {
                    *self.register(register) += 1
                }
                None
            },
            Instruction::Dec { op1 } => {
                if let Value::Register { register } = op1 {
                    *self.register(register) -= 1
                }
                None
            },
            Instruction::Jnz { op1, op2 } => {
                if self.fetch(op1) != 0 {
                    Some(self.fetch(op2) as usize)
                } else {
                    None
                }
            },
            Instruction::Tgl { op1 } => {
                let offset = self.fetch(op1) as usize;
                if self.ip + offset < self.program.len() {
                    let toggled = match self.program[self.ip + offset] {
                        Instruction::Inc { op1 } => Instruction::Dec { op1 },
                        Instruction::Jnz { op1, op2 } => Instruction::Cpy { op1, op2 },
                        Instruction::Cpy { op1, op2 } => Instruction::Jnz { op1, op2 },
                        Instruction::Dec { op1 } |
                        Instruction::Tgl { op1 } |
                        Instruction::Out { op1 } => Instruction::Inc { op1 }
                    };

                    self.program[self.ip + offset] = toggled;
                }

                None
            },
            Instruction::Out { op1 } => {
                let value = self.fetch(op1);
                self.output.push(value);
                None
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod assembunny_day12 {
    use crate::{bunny::{BunnyVM, Instruction, Value, Register}};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "cpy 41 a
             inc a
             inc b
             dec c
             jnz d 2
             dec a";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions.len(), 6);
        assert_eq!(instructions[0], Instruction::Cpy { op1: Value::Immediate { value: 41 }, op2: Value::Register { register: Register::A } });
        assert_eq!(instructions[1], Instruction::Inc { op1: Value::Register { register: Register::A } });
        assert_eq!(instructions[2], Instruction::Inc { op1: Value::Register { register: Register::B } });
        assert_eq!(instructions[3], Instruction::Dec { op1: Value::Register { register: Register::C } });
        assert_eq!(instructions[4], Instruction::Jnz { op1: Value::Register { register: Register::D }, op2: Value::Immediate { value: 2 } });
        assert_eq!(instructions[5], Instruction::Dec { op1: Value::Register { register: Register::A } });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn day12_example() {
        let input =
            "cpy 41 a
             inc a
             inc a
             dec a
             jnz a 2
             dec a";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        let mut bunny = BunnyVM::default();

        bunny.program(&instructions);
        assert_eq!(bunny.peek(Register::A), 0);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 41);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 42);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 43);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 42);

        assert!(!bunny.step());
        assert_eq!(bunny.peek(Register::A), 42);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod assembunny_day23 {
    use crate::{bunny::{BunnyVM, Instruction, Value, Register}};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "cpy 2 a
             tgl a
             tgl a
             tgl a
             cpy 1 a
             dec a
             dec a";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions.len(), 7);
        assert_eq!(instructions[0], Instruction::Cpy { op1: Value::Immediate { value: 2 }, op2: Value::Register { register: Register::A } });
        assert_eq!(instructions[1], Instruction::Tgl { op1: Value::Register { register: Register::A } });
        assert_eq!(instructions[2], Instruction::Tgl { op1: Value::Register { register: Register::A } });
        assert_eq!(instructions[3], Instruction::Tgl { op1: Value::Register { register: Register::A } });
        assert_eq!(instructions[4], Instruction::Cpy { op1: Value::Immediate { value: 1 }, op2: Value::Register { register: Register::A } });
        assert_eq!(instructions[5], Instruction::Dec { op1: Value::Register { register: Register::A } });
        assert_eq!(instructions[6], Instruction::Dec { op1: Value::Register { register: Register::A } });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn day23_example() {
        let input =
            "cpy 2 a
             tgl a
             tgl a
             tgl a
             cpy 1 a
             dec a
             dec a";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        let mut bunny = BunnyVM::default();

        bunny.program(&instructions);
        assert_eq!(bunny.peek(Register::A), 0);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 2);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 2);
        assert_eq!(bunny.program[3], Instruction::Inc { op1: Value::Register { register: Register::A } });

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 2);
        assert_eq!(bunny.program[4], Instruction::Jnz { op1: Value::Immediate { value: 1 }, op2: Value::Register { register: Register::A } });

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 3);

        assert!(!bunny.step());
        assert_eq!(bunny.peek(Register::A), 3);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod assembunny_day25 {
    use crate::{bunny::{BunnyVM, Instruction, Value, Register}};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn sanity() {
        let input =
            "cpy 7 a
             out 5
             out a";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions.len(), 3);
        assert_eq!(instructions[0], Instruction::Cpy { op1: Value::Immediate { value: 7 }, op2: Value::Register { register: Register::A } });
        assert_eq!(instructions[1], Instruction::Out { op1: Value::Immediate { value: 5 } });
        assert_eq!(instructions[2], Instruction::Out { op1: Value::Register { register: Register::A } });

        let mut bunny = BunnyVM::default();

        bunny.program(&instructions);
        assert_eq!(bunny.peek(Register::A), 0);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 7);

        assert!(bunny.step());
        assert_eq!(bunny.peek(Register::A), 7);
        assert_eq!(bunny.output(), &[5]);

        assert!(!bunny.step());
        assert_eq!(bunny.peek(Register::A), 7);
        assert_eq!(bunny.output(), &[5, 7]);
    }
}
