use aoc::{self, Day, Result, Coord, GridElement, Grid};
use lazy_static::lazy_static;
use regex::Regex;
use std::fmt;
use std::str::FromStr;
use std::collections::HashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day22 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Node>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day22 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day22::Part1;
    type Part2 = crate::day22::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<String>(input, "\n")?;
        let nodes = input.iter()
                         .skip(2)
                         .map(|i| i.parse().unwrap())
                         .collect::<Vec<Node>>();

        let input = create_grid(nodes);

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut pairs = HashSet::new();

        for i in 0..self.input.elements.len() {
            for j in 0..self.input.elements.len() {
                if i == j { continue; }

                if self.input.elements[i].used != 0 && self.input.elements[i].used <= self.input.elements[j].avail {
                    pairs.insert((i, j));
                }
            }
        }

        self.part1 = Some(pairs.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn create_grid(mut nodes: Vec<Node>) -> Grid<Node> {
    nodes.sort_by_key(|n| n.location);
    let last = nodes.last().unwrap().location;
    let mut grid = Grid::new(last.x + 1, last.y + 1);
    grid.elements = nodes;
    grid.elements[last.x as usize].goal = true;


    grid
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Node {
    location: Coord,
    size: usize,
    used: usize,
    avail: usize,
    used_percent: usize,
    goal: bool
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Node {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Self { location: Coord::new(-1, -1), size: 0, used: 0, avail: 0, used_percent: 0, goal: false}
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Node {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(r"/dev/grid/node-x(?P<x>\d+)-y(?P<y>\d+) +(?P<size>\d+)T +(?P<used>\d+)T +(?P<avail>\d+)T +(?P<percent>\d+)").unwrap();
        }

        let captures = REGEX.captures(s).unwrap();
        let x = captures.name("x").unwrap().as_str().parse().unwrap();
        let y = captures.name("y").unwrap().as_str().parse().unwrap();
        let size = captures.name("size").unwrap().as_str().parse().unwrap();
        let used = captures.name("used").unwrap().as_str().parse().unwrap();
        let avail = captures.name("avail").unwrap().as_str().parse().unwrap();
        let used_percent = captures.name("percent").unwrap().as_str().parse().unwrap();
        Ok(Self { location: Coord::new(x, y), size, used, avail, used_percent, goal: false })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Node {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.used == 0 {
            write!(f, " _ ")
        } else if self.goal {
            write!(f, " G ")
        } else if self.used > 92 {
            write!(f, " # ")
        } else {
            write!(f, " . ")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Node {}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day, Coord};
    use crate::day22::{Day22, create_grid, Node};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_parse() {
        let input = "/dev/grid/node-x0-y10    89T   66T    23T   74%";
        let node: Node = input.parse().unwrap();

        assert_eq!(node, Node { location: Coord::new(0, 10), size: 89, used: 66, avail: 23, used_percent: 74, goal: false});
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_example() {
        let input =
            [
                "/dev/grid/node-x0-y0   10T    8T     2T   80%",
                "/dev/grid/node-x0-y1   11T    6T     5T   54%",
                "/dev/grid/node-x0-y2   32T   28T     4T   87%",
                "/dev/grid/node-x1-y0    9T    7T     2T   77%",
                "/dev/grid/node-x1-y1    8T    0T     8T    0%",
                "/dev/grid/node-x1-y2   11T    7T     4T   63%",
                "/dev/grid/node-x2-y0   10T    6T     4T   60%",
                "/dev/grid/node-x2-y1    9T    8T     1T   88%",
                "/dev/grid/node-x2-y2    9T    6T     3T   66%"
            ];

        let nodes = input.iter()
                         .map(|i| i.parse().unwrap())
                         .collect::<Vec<Node>>();

        let grid = create_grid(nodes);

        println!("{}", grid);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(981));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), None);
    }
}
