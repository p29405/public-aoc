use aoc::{self, Day, Result };
use lib_2016::bunny::{ Instruction, BunnyVM, Register };


////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day25 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day25 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day25::Part1;
    type Part2 = crate::day25::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut answer = None;
        'outer: for i in 0.. {
            let mut bunny = BunnyVM::default();
            bunny.program(&self.input).poke(Register::A, i);
            while bunny.step() && bunny.output().len() < 128 {
                for (index, value) in bunny.output().iter().enumerate() {
                    if *value != index as i64 % 2 {
                        continue 'outer;
                    }
                }
            }
            if bunny.output().len() == 128 {
                answer = Some(i);
                break 'outer;
            }
            println!("{}: {:?}", i, bunny.output());
        }
        self.part1 = answer;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day25::Day25;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day25.txt");

        let mut day = Day25::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(189));
    }
}
