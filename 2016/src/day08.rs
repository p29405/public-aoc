use aoc::{self, Day, Result, Grid, AocFontType, to_string};
use log::debug;
use lib_2016::tiny_code_display::{ Instruction, Pixel, draw };

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>,
    pub screen: Grid<Pixel>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;
        let screen = draw(&input, 50, 6);

        Ok(Self { part1: None, part2: None, input, screen})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(self.screen.elements.iter().filter(|&p| p == &Pixel::On).count());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        debug!("\n{}", self.screen);
        self.part2 = to_string(&self.screen, Pixel::On, AocFontType::Aoc2016);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day08::Day08;
    use lib_2016::tiny_code_display::Instruction;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "rect 3x2
             rotate column x=1 by 1
             rotate row y=0 by 4
             rotate column x=1 by 1";

        let instructions = aoc::read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions[0], Instruction::DrawRect { width: 3, height: 2});
        assert_eq!(instructions[1], Instruction::RotateColumn { column: 1, by: 1});
        assert_eq!(instructions[2], Instruction::RotateRow { row: 0, by: 4});
        assert_eq!(instructions[3], Instruction::RotateColumn { column: 1, by: 1});

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(115));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("EFEYKFRFIJ".to_string()));
    }
}
