use aoc::{self, Grid, Coord, Map};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use aoc::pseudo_list::CircularList;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub type Pixel = aoc::Pixel;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn draw(input: &[Instruction], screen_width: i64, screen_height: i64) -> Grid<Pixel> {
    let mut screen = Grid::new(screen_width, screen_height);

    for instruction in input {

        match instruction {
            Instruction::DrawRect { width, height } =>
                for c in Coord::range(Coord::new(0, 0), Coord::new(*width, *height)) {
                    screen.set(&c, Pixel::On)
                }
            Instruction::RotateColumn { column, by } => {
                let start = Coord::new(*column, 0);
                let end = Coord::new(*column, screen_height);
                shift(&mut screen, start, end, *by);
            },
            Instruction::RotateRow { row, by } => {
                let start = Coord::new(0, *row);
                let end = Coord::new(screen_width, *row);
                shift(&mut screen, start, end, *by);
            }
        }
    }

    screen
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn shift(screen: &mut Grid<Pixel>, start: Coord, end: Coord, by: i64) {
    let mut current = CircularList::default();

    for coord in Coord::range(start, end) {
        current.push_back(screen.get(&coord));
    }

    for _ in 0..by {
        current.move_left();
    }

    for coord in Coord::range(start, end) {
        screen.set(&coord, current.pop_front().unwrap());
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Instruction {
    DrawRect { width: i64, height: i64 },
    RotateRow { row: i64, by: i64 },
    RotateColumn { column: i64, by: i64 },
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref RECT_RE: Regex = Regex::new(r"^rect (?P<width>[\d]+)x(?P<height>[\d]+)$").unwrap();
            static ref ROTATE_ROW_RE: Regex = Regex::new(r"^rotate row y=(?P<row>[\d]+) by (?P<by>[\d]+)$").unwrap();
            static ref ROTATE_COLUMN_RE: Regex = Regex::new(r"^rotate column x=(?P<column>[\d]+) by (?P<by>[\d]+)$").unwrap();
        }

        if let Some(captures) =  RECT_RE.captures(s) {
            let width = captures["width"].parse().unwrap();
            let height = captures["height"].parse().unwrap();

            return Ok(Self::DrawRect { width, height });
        }

        if let Some(captures) =  ROTATE_ROW_RE.captures(s) {
            let row = captures["row"].parse().unwrap();
            let by = captures["by"].parse().unwrap();

            return Ok(Self::RotateRow { row, by });
        }

        if let Some(captures) =  ROTATE_COLUMN_RE.captures(s) {
            let column = captures["column"].parse().unwrap();
            let by = captures["by"].parse().unwrap();

            return Ok(Self::RotateColumn { column, by });
        }

        Err(aoc::Error::new(&format!("Failed to parse Instruction: {}", s)))
    }
}