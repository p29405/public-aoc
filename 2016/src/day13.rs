use aoc::{self, Day, Result, Coord, bfs};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: i64,
    pub target: Coord
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day13::Part1;
    type Part2 = crate::day13::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = input.trim().parse()?;

        Ok(Self { part1: None, part2: None, input, target: Coord::new(31, 39)})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut search =  bfs(Coord::new(1, 1),
                              |p| p.x >= 0 && p.y >= 0 && open(*p, self.input));
        for i in 0.. {
            let visited = search.get_next();
            if visited.contains(&self.target) {
                self.part1 = Some(i);
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut search = bfs(Coord::new(1, 1),
            |p| p.x >= 0 && p.y >= 0 && open(*p, self.input));

        let mut visited = 0;
        for _ in 0..=50 {
            visited = search.get_next().len();
        };
        self.part2 = Some(visited);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn open(position: Coord, magic: i64) -> bool {
    let x = position.x;
    let y = position.y;

    let value = x * (x + 3 + (2 * y)) + y * ( 1 + y) + magic;

    value.count_ones() % 2 == 0
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::{Day, GridElement, Grid, Coord, Map};
    use crate::day13::{Day13, open};
    use std::fmt;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub enum Element {
        Open,
        Wall
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl Default for Element {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        fn default() -> Self {
            Element::Wall
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl fmt::Display for Element {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Element::Open => write!(f, "."),
                Element::Wall => write!(f, "#")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl GridElement for Element {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        fn to_element(c: char) -> Self {
            match c {
                '.' => Element::Open,
                '#' => Element::Wall,
                _ => panic!("Unknown element {}", c)
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    fn build_map(columns: i64, row: i64, magic: i64) -> Grid<Element> {
        let mut map = Grid::new(columns, row);

        for p in Coord::range(map.upper_left(), map.lower_right()) {
            map.set(&p, if open(p, magic) { Element::Open } else { Element::Wall });
        }

        map
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn map() {
        let expected_str =
            ".#.####.##
             ..#..#...#
             #....##...
             ###.#.###.
             .##..#..#.
             ..##....#.
             #...##.###";
        let expected = aoc::read_grid_from_string(expected_str).unwrap();
        let actual = build_map(expected.columns, expected.rows, 10);

        assert_eq!(actual, expected);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "10";

        let mut day = Day13::setup(input).unwrap();
        day.target = Coord::new(7, 4);
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(11));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(90));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(135));
    }
}
