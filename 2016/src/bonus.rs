use aoc::{ self, Day, Result, AocFontType, to_string };
use lib_2016::{ bunny::{ self, BunnyVM }, tiny_code_display::{ Instruction, Pixel, draw }};
use log::info;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct BonusDay {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<bunny::Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for BonusDay {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::bonus::Part1;
    type Part2 = crate::bonus::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut bunny = BunnyVM::default();
        bunny.program(&self.input);

        while bunny.step() {}

        let bytes = bunny.output().iter().map(|n| *n as u8).collect::<Vec<u8>>();
        let output = std::str::from_utf8(&bytes).unwrap();

        let commands = aoc::read_list_from_string::<Instruction>(output, "\n").unwrap();
        let screen = draw(&commands, 50, 6);
        info!("\n{}", screen);
        self.part1 = to_string(&screen, Pixel::On, AocFontType::Aoc2016);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::bonus::BonusDay;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day26.txt");

        let mut day = BonusDay::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("AoC 2017".to_string()));
    }
}
