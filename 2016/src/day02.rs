use aoc::{self, Day, Coord, Direction, Grid, GridElement, Result, Map};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = String;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Vec<Direction>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<String>(input, "\n")?.iter()
            .map(|list| aoc::read_list_by_char_from_string(list.trim()).unwrap())
            .collect::<Vec<_>>();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut current = Coord::new(1, 1);
        let keypad = keypad();
        let answer = self.input.iter()
            .map(|d| apply_directions(&mut current, d, &keypad))
            .fold("".into(), |accum, c| format!("{}{}", accum, c));
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1.clone() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut current = Coord::new(0, 2);
        let keypad = keypad_part2();
        let answer = self.input.iter()
            .map(|d| apply_directions(&mut current, d, &keypad))
            .fold("".into(), |accum, c| format!("{}{}", accum, c));
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn apply_directions(current: &mut Coord, directions: &[Direction], keypad: &Grid<Key>) -> char {
    for d in directions {
        let next= current.go_inverted(*d);

        if let Key::Digit(_) = keypad.get(&next) {
            *current = next;
        }
    }

    keypad.get(&current).value()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn keypad() -> Grid<Key> {
    let mut keypad = Grid::new(3, 3);

    keypad.elements = vec![Key::Digit('1'), Key::Digit('2'), Key::Digit('3'),
                           Key::Digit('4'), Key::Digit('5'), Key::Digit('6'),
                           Key::Digit('7'), Key::Digit('8'), Key::Digit('9')];

    keypad
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn keypad_part2() -> Grid<Key> {
    let mut keypad = Grid::new(5, 5);

    keypad.elements = vec![Key::Invalid,    Key::Invalid,    Key::Digit('1'), Key::Invalid,    Key::Invalid,
                           Key::Invalid,    Key::Digit('2'), Key::Digit('3'), Key::Digit('4'), Key::Invalid,
                           Key::Digit('5'), Key::Digit('6'), Key::Digit('7'), Key::Digit('8'), Key::Digit('9'),
                           Key::Invalid,    Key::Digit('A'), Key::Digit('B'), Key::Digit('C'), Key::Invalid,
                           Key::Invalid,    Key::Invalid,    Key::Digit('D'), Key::Invalid,    Key::Invalid,];

    keypad
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Key {
    Invalid,
    Digit(char)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Key {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn value(&self) -> char {
        match self {
            Key::Digit(d) => *d,
            _ => panic!("We should never get here")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Key {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Key::Invalid
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Key {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Unneeded")
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Key {}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "ULL
             RRDDD
             LURDL
             UUUUD";

        let mut day = Day02::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("1985".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some("65556".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "ULL
             RRDDD
             LURDL
             UUUUD";

        let mut day = Day02::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("5DB3".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");

        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("CB779".to_string()));
    }
}
