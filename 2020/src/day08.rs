use aoc::{self, Day, Result};

use crate::game_console::{Console, Instruction};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day08 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day08 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day08::Part1;
    type Part2 = crate::day08::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut console = Console::default();
        console.program(&self.input);

        console.halts();

        self.part1 = Some(console.accumulator);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        for (i, instruction) in self.input.iter().enumerate().rev() {
            let replacement = match instruction {
                Instruction::Jmp { op1, ..} => Instruction::Nop { op1: *op1 },
                Instruction::Nop { op1, ..} => Instruction::Jmp { op1: *op1 },
                Instruction::Acc { .. } => continue
            };

            let mut modified = self.input.clone();

            modified[i] = replacement;

            let mut console = Console::default();
            console.program(&modified);

            if console.halts() {
                self.part2 = Some(console.accumulator);
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;

    use crate::day08::Day08;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "nop +0
             acc +1
             jmp +4
             acc +3
             jmp -3
             acc -99
             acc +1
             jmp -4
             acc +6";

        let mut day = Day08::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1941));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "nop +0
             acc +1
             jmp +4
             acc +3
             jmp -3
             acc -99
             acc +1
             jmp -4
             acc +6";

        let mut day = Day08::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(8));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day08.txt");

        let mut day = Day08::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2096));
    }
}
