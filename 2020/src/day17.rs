use aoc::{self, Day, Result, Coord3D, GridElement, Grid, Coord4D};
use std::fmt;
use fnv::FnvHashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day17 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<State>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day17 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day17::Part1;
    type Part2 = crate::day17::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut space = FnvHashMap::default();
        let mut working_space = FnvHashMap::default();
        let x_offset = self.input.columns / 2;
        let y_offset = self.input.rows / 2;

        for (c, s) in self.input.iter() {
            let position = Coord3D::new(c.x - x_offset, c.y - y_offset, 0);
            space.insert(position, s);
        }
        let mut start = Coord3D::new(-x_offset, -y_offset, 0);
        let mut end = Coord3D::new(self.input.columns- x_offset - self.input.columns % 2, self.input.rows - y_offset - self.input.rows % 2, 0);

        let mut answer = 0;
        for _ in 0..6 {
            start.x -= 1;
            start.y -= 1;
            start.z -= 1;
            end.x += 1;
            end.y += 1;
            end.z += 1;

            answer = cycle(&space, &mut working_space, (start, end));
            std::mem::swap(&mut space, &mut working_space);
        }

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut space = FnvHashMap::default();
        let mut working_space = FnvHashMap::default();
        let x_offset = self.input.columns / 2;
        let y_offset = self.input.rows / 2;

        for (c, s) in self.input.iter() {
            let position = Coord4D::new(c.x - x_offset, c.y - y_offset, 0, 0);
            space.insert(position, s);
        }
        let mut start = Coord4D::new(-x_offset, -y_offset, 0, 0);
        let mut end = Coord4D::new(self.input.columns- x_offset - self.input.columns % 2, self.input.rows - y_offset - self.input.rows % 2, 0, 0);

        let mut answer = 0;
        for _ in 0..6 {
            start.x -= 1;
            start.y -= 1;
            start.z -= 1;
            start.w -= 1;
            end.x += 1;
            end.y += 1;
            end.z += 1;
            end.w += 1;

            answer = cycle_4d(&space, &mut working_space, (start, end));
            std::mem::swap(&mut space, &mut working_space);
        }

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////
pub fn cycle(space: &FnvHashMap<Coord3D, State>, next: &mut FnvHashMap<Coord3D, State>, range: (Coord3D, Coord3D)) -> usize {
    let mut count = 0;

    for p in Coord3D::range_inclusive(range.0, range.1) {
        let active = neighbors_active(p, space);
        match space.get(&p) {
            Some(State::Active) => {
                if active == 2 || active == 3 {
                    next.insert(p, State::Active);
                    count += 1;
                } else {
                    next.insert(p, State::Inactive);
                }
            },
            _ => {
                if active == 3 {
                    next.insert(p, State::Active);
                    count += 1;
                } else {
                    next.insert(p, State::Inactive);
                }
            }
        }
    }

    count
}

////////////////////////////////////////////////////////////////////////////////////////////////
pub fn cycle_4d(space: &FnvHashMap<Coord4D, State>, next: &mut FnvHashMap<Coord4D, State>, range: (Coord4D, Coord4D)) -> usize {
    let mut count = 0;
    for p in Coord4D::range_inclusive(range.0, range.1) {
        let active = neighbors_active_4d(p, space);
        match space.get(&p) {
            Some(State::Active) => {
                if active == 2 || active == 3 {
                    next.insert(p, State::Active);
                    count += 1;
                } else {
                    next.insert(p, State::Inactive);
                }
            },
            _ => {
                if active == 3 {
                    next.insert(p, State::Active);
                    count += 1;
                } else {
                    next.insert(p, State::Inactive);
                }
            }
        }
    }
    count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn neighbors_active(p: Coord3D, space: &FnvHashMap<Coord3D, State>) -> usize {
    let deltas =
        [
            (-1, -1, -1), (-1, -1, 0), (-1, -1, 1), (-1 , 0, -1), (-1, 0, 0), (-1, 0, 1), (-1 , 1, -1), (-1, 1, 0), (-1, 1, 1),
            ( 0, -1, -1), ( 0, -1, 0), ( 0, -1, 1), ( 0 , 0, -1), ( 0, 0, 1),             ( 0 , 1, -1), ( 0, 1, 0), ( 0, 1, 1),
            ( 1, -1, -1), ( 1, -1, 0), ( 1, -1, 1), ( 1 , 0, -1), ( 1, 0, 0), ( 1, 0, 1), ( 1 , 1, -1), ( 1, 1, 0), ( 1, 1, 1)
        ];
    deltas.iter().fold(0, |accum, (dx, dy, dz)| if space.get(&Coord3D::new(p.x + dx, p.y + dy, p.z + dz)).map_or(false, |&n| n == State::Active) { accum + 1 } else { accum })
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn neighbors_active_4d(p: Coord4D, space: &FnvHashMap<Coord4D, State>) -> usize {

    Coord4D::range_inclusive(Coord4D::new(p.x - 1, p.y -1, p.z - 1, p.w - 1), Coord4D::new(p.x + 1, p.y + 1, p.z + 1, p.w + 1))
        .map(|neighbor| if space.get(&neighbor).map_or(false, |&n| n == State::Active) && p != neighbor { 1 } else { 0 })
        .sum()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum State {
    Inactive,
    Active
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for State {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        State::Inactive
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for State {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            State::Inactive => write!(f, "."),
            State::Active => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for State {
    fn to_element(c: char) -> Self {
        match c {
            '.' => Self::Inactive,
            '#' => Self::Active,
            _ => panic!("Unknown")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day17::Day17;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            ".#.
             ..#
             ###";

        let mut day = Day17::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(112));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day17.txt");

        let mut day = Day17::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(346));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            ".#.
             ..#
             ###";

        let mut day = Day17::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(848));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day17.txt");

        let mut day = Day17::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1632));
    }
}
