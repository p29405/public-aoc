use aoc::{self, Day, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use fnv::FnvHashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day07 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Bag>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day07 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day07::Part1;
    type Part2 = crate::day07::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = outer_bags(&self.input, "shiny gold").len();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = needed_bags(&self.input, "shiny gold", 1) - 1;
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn outer_bags(bags: &[Bag], target: &str) -> FnvHashSet<String> {
    let next = bags.iter()
        .filter(|b| {
            b.contains.iter().any(|(_, bag)| bag == target)
        })
        .map(|b| b.color.to_owned())
        .collect::<FnvHashSet<_>>();

    let outer = next.iter()
        .fold(FnvHashSet::default(),
              |accum, b| accum.union(&outer_bags(bags, b)).cloned().collect::<FnvHashSet<_>>());

    outer.union(&next).cloned().collect()
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn needed_bags(bags: &[Bag], target: &str, multiplier: usize) -> usize {
    let bag = bags.iter().find(|b| b.color == target).unwrap();

    bag.contains.iter()
        .fold(multiplier,
              |accum, (count, color)| accum + needed_bags(bags, color, multiplier * count))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Bag {
    color: String,
    contains: Vec<(usize, String)>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Bag {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref CONTAINED_BAGS: Regex = Regex::new(r"(\d+) ([^ ]+ [^ ]+) bag").unwrap();
        }

        let mut parts = s.split(" bags contain ");

        let mut contains = vec![];

        let color = parts.next().unwrap().to_owned();
        let contained_bags = parts.next().unwrap();

        for capture in CONTAINED_BAGS.captures_iter(contained_bags) {
            let count = capture.get(1).unwrap().as_str().parse().unwrap();
            let color = capture.get(2).unwrap().as_str().to_owned();
            contains.push((count, color));
        }

        Ok(Self { color, contains })
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day07::Day07;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "light red bags contain 1 bright white bag, 2 muted yellow bags.
             dark orange bags contain 3 bright white bags, 4 muted yellow bags.
             bright white bags contain 1 shiny gold bag.
             muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
             shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
             dark olive bags contain 3 faded blue bags, 4 dotted black bags.
             vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
             faded blue bags contain no other bags.
             dotted black bags contain no other bags.";

        let mut day = Day07::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(4));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day07.txt");
        let mut day = Day07::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(126));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "light red bags contain 1 bright white bag, 2 muted yellow bags.
             dark orange bags contain 3 bright white bags, 4 muted yellow bags.
             bright white bags contain 1 shiny gold bag.
             muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
             shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
             dark olive bags contain 3 faded blue bags, 4 dotted black bags.
             vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
             faded blue bags contain no other bags.
             dotted black bags contain no other bags.";

        let mut day = Day07::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(32));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_3() {
        let input =
            "shiny gold bags contain 2 dark red bags.
             dark red bags contain 2 dark orange bags.
             dark orange bags contain 2 dark yellow bags.
             dark yellow bags contain 2 dark green bags.
             dark green bags contain 2 dark blue bags.
             dark blue bags contain 2 dark violet bags.
             dark violet bags contain no other bags.";

        let mut day = Day07::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(126));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day07.txt");
        let mut day = Day07::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(220_149));
    }
}
