use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i32;
type Part2 = i32;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day01 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i32>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day01 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day01::Part1;
    type Part2 = crate::day01::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut input = aoc::read_list_from_string(input, "\n")?;
        input.sort_unstable();

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = find_sum(&self.input, 2020, None);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let input = &self.input;
        for (i, x) in input.iter().enumerate() {
            if let Some(total) = find_sum(input, 2020 - x, Some(i)) {
                self.part2 = Some(total * x);
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn find_sum(input: &[i32], target: i32, exclude: Option<usize>) -> Option<i32> {
    let mut i = 0;
    let mut j = input.len() - 1;

    while i < j {
        if let Some(exclude) = exclude {
            if i == exclude {
                i += 1;
            } else if j == exclude {
                j -= 1;
            }
        }
        let sum = input[i] + input[j];

        if sum == target {
            return Some(input[i] * input[j]);
        } else if sum > target {
            j -= 1;
        } else {
            i += 1
        }
    }
    None
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day01::Day01;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "1721
             979
             366
             299
             675
             1456";

        let mut day = Day01::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(514_579));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day01.txt");
        let mut day = Day01::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(751_776));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "1721
             979
             366
             299
             675
             1456";

        let mut day = Day01::setup(input).unwrap();
        day.run_part2();

        assert_eq!(day.part2_answer(), Some(241_861_950));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day01.txt");
        let mut day = Day01::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(42_275_090));
    }
}
