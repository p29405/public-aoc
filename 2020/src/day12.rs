use aoc::{self, Day, Result, Coord, Direction};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day12 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day12 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day12::Part1;
    type Part2 = crate::day12::Part2;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut position = Coord::default();
        let mut heading = Direction::East;

        for instruction in self.input.iter() {
            let update = instruction.apply(position, heading);
            position = update.0;
            heading = update.1;
        }

        let answer = position.x.abs() + position.y.abs();

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut ship = Coord::default();
        let mut waypoint = Coord::new(10, 1);

        for instruction in self.input.iter() {
            let update = instruction.apply_p2(ship, waypoint);
            ship = update.0;
            waypoint = update.1;

            //println!("Ship {:?}, Waypoint {}", ship, waypoint);
        }

        let answer = ship.x.abs() + ship.y.abs();


        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub enum Action {
    North,
    East,
    South,
    West,
    Left,
    Right,
    Forward
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug)]
pub struct Instruction {
    action: Action,
    input: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Instruction {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn apply(&self, start: Coord, heading: Direction) -> (Coord, Direction) {
        let mut next = start;
        let mut new_heading = heading;
        let movement = match self.action {
            Action::North => {
                Some(Direction::North)
            },
            Action::East => {
                Some(Direction::East)
            },
            Action::South => {
                Some(Direction::South)
            },
            Action::West => {
                Some(Direction::West)
            },
            Action::Left => {
                match self.input {
                    90 => {
                        new_heading = new_heading.turn_left();
                    },
                    180 => {
                        new_heading = new_heading.turn_left();
                        new_heading = new_heading.turn_left();
                    },
                    270 => {
                        new_heading = new_heading.turn_left();
                        new_heading = new_heading.turn_left();
                        new_heading = new_heading.turn_left();
                    },
                    _ => {
                        panic!("Unhandled input {:?}", self);
                    }
                };
                None
            },
            Action::Right => {
                match self.input {
                    90 => {
                        new_heading = new_heading.turn_right();
                    },
                    180 => {
                        new_heading = new_heading.turn_right();
                        new_heading = new_heading.turn_right();
                    },
                    270 => {
                        new_heading = new_heading.turn_right();
                        new_heading = new_heading.turn_right();
                        new_heading = new_heading.turn_right();
                    },
                    _ => {
                        panic!("Unhandled input {:?}", self)
                    }
                };
                None
            },
            Action::Forward => {
                Some(heading)
            }
        };

        if let Some(movement) = movement {
            for _ in 0..self.input {
                next = next.go(movement);
            }
        }

        (next, new_heading)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn apply_p2(&self, ship: Coord, waypoint: Coord) -> (Coord, Coord) {
        let mut new_ship = ship;
        let mut new_waypoint = waypoint;
        match self.action {
            Action::North => {
                new_waypoint = Coord::new(waypoint.x, waypoint.y + self.input);
            },
            Action::East => {
                new_waypoint = Coord::new(waypoint.x + self.input, waypoint.y);
            },
            Action::South => {
                new_waypoint = Coord::new(waypoint.x, waypoint.y - self.input);
            },
            Action::West => {
                new_waypoint = Coord::new(waypoint.x - self.input, waypoint.y);
            },
            Action::Left => {
                match self.input {
                    90 => {
                        new_waypoint = Coord::new(-waypoint.y, waypoint.x)
                    },
                    180 => {
                        new_waypoint = Coord::new(-waypoint.x, -waypoint.y)
                    },
                    270 => {
                        new_waypoint = Coord::new(waypoint.y, -waypoint.x)
                    },
                    _ => {
                        panic!("Unhandled input {:?}", self);
                    }
                };
            },
            Action::Right => {
                match self.input {
                    90 => {
                        new_waypoint = Coord::new(waypoint.y, -waypoint.x)
                    },
                    180 => {
                        new_waypoint = Coord::new(-waypoint.x, -waypoint.y)
                    },
                    270 => {
                        new_waypoint = Coord::new(-waypoint.y, waypoint.x)
                    },
                    _ => {
                        panic!("Unhandled input {:?}", self)
                    }
                };
            },
            Action::Forward => {
                for _ in 0..self.input {
                    new_ship.x += waypoint.x;
                    new_ship.y += waypoint.y;
                }
            }
        };

        (new_ship, new_waypoint)
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut chars = s.chars();
        let action = match chars.next().unwrap() {
            'N' => Action::North,
            'E' => Action::East,
            'S' => Action::South,
            'W' => Action::West,
            'L' => Action::Left,
            'R' => Action::Right,
            'F' => Action::Forward,
            e => {
                return Err(aoc::Error::new(&format!("Unknown Action: {}", e)));
            }
        };
        let input = chars.as_str().parse().unwrap();

        Ok(Self { action, input })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day12::Day12;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "F10\nN3\nF7\nR90\nF11";

        let mut day = Day12::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(25));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "F10\nN3\nF7\nR90\nF11";

        let mut day = Day12::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(286));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(796));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day12.txt");

        let mut day = Day12::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(39_446));
    }
}
