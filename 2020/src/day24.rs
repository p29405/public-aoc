use aoc::{self, Day, Result, Coord};
use std::collections::HashSet;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day24 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Vec<HexDirection>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day24 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day24::Part1;
    type Part2 = crate::day24::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut list = vec![];
        for line in input.lines() {
            let line = line.trim();
            let mut directions = vec![];
            let mut chars = line.chars();
            'inner: loop {

                let start = if let Some(c) = chars.next() {
                    c
                } else {
                    break 'inner;
                };

                match start {
                    'w' => directions.push(HexDirection::West),
                    'e' => directions.push(HexDirection::East),
                    's' => match chars.next().unwrap() {
                        'w' => directions.push(HexDirection::Southwest),
                        'e' => directions.push(HexDirection::Southeast),
                        _ => panic!("Unknown char {}", start)
                    },
                    'n' => match chars.next().unwrap() {
                        'w' => directions.push(HexDirection::Northwest),
                        'e' => directions.push(HexDirection::Northeast),
                        _ => panic!("Unknown char {}", start)
                    },
                    _ => panic!("Unknown char {}", start)
                }
            }
            list.push(directions);
        }

        Ok(Self { part1: None, part2: None, input: list})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut black_tiles = HashSet::new();

        for directions in self.input.iter() {
            let tile = directions.iter().fold(Coord::default(), |t, d| next(t, *d));

            if black_tiles.contains(&tile) {
                black_tiles.remove(&tile);
            } else {
                black_tiles.insert(tile);
            }
        }

        self.part1 = Some(black_tiles.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut black_tiles = HashSet::new();
        let mut white_tiles = HashSet::new();

        for directions in self.input.iter() {
            let tile = directions.iter().fold(Coord::default(), |t, d| next(t, *d));

            if black_tiles.contains(&tile) {
                black_tiles.remove(&tile);
            } else {
                black_tiles.insert(tile);
            }
        }

        let mut next_black = vec![];
        let mut next_white = vec![];

        for _ in 0..100 {
            for tile in black_tiles.iter() {
                for n in neighbors(*tile) {
                    if !black_tiles.contains(&n) && !white_tiles.contains(&n){
                        white_tiles.insert(n);
                    }
                }
            }

            next_black.clear();
            next_white.clear();
            for tile in black_tiles.iter() {
                let count = neighbors(*tile).fold(0, |accum, c| accum + black_tiles.contains(&c) as i32);

                if count == 0 || count > 2 {
                    next_white.push(*tile);
                } else {
                    next_black.push(*tile);
                }

            }
            for tile in white_tiles.iter() {
                let count = neighbors(*tile).fold(0, |accum, c| accum + black_tiles.contains(&c) as i32);

                if count == 2 {
                    next_black.push(*tile);
                } else {
                    next_white.push(*tile);
                }
            }
            black_tiles = next_black.iter().copied().collect();
            white_tiles = next_white.iter().copied().collect();
        }

        self.part2 = Some(black_tiles.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum HexDirection {
    East,
    Southeast,
    Southwest,
    West,
    Northwest,
    Northeast
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn next(coord: Coord, direction: HexDirection) -> Coord {
    match direction {
        HexDirection::East => { Coord::new(coord.x + 2, coord.y) },
        HexDirection::Southeast => { Coord::new(coord.x + 1, coord.y - 1) },
        HexDirection::Southwest => { Coord::new(coord.x - 1, coord.y - 1) },
        HexDirection::West => { Coord::new(coord.x - 2, coord.y) },
        HexDirection::Northwest => { Coord::new(coord.x - 1, coord.y + 1) },
        HexDirection::Northeast => { Coord::new(coord.x + 1, coord.y + 1) },
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn neighbors(coord: Coord) -> impl Iterator<Item=Coord> {
    [HexDirection::East, HexDirection::Northeast, HexDirection::Northwest,
     HexDirection::West, HexDirection::Southeast, HexDirection::Southwest].iter()
        .map(move |d| next(coord, *d))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day24::Day24;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "sesenwnenenewseeswwswswwnenewsewsw
             neeenesenwnwwswnenewnwwsewnenwseswesw
             seswneswswsenwwnwse
             nwnwneseeswswnenewneswwnewseswneseene
             swweswneswnenwsewnwneneseenw
             eesenwseswswnenwswnwnwsewwnwsene
             sewnenenenesenwsewnenwwwse
             wenwwweseeeweswwwnwwe
             wsweesenenewnwwnwsenewsenwwsesesenwne
             neeswseenwwswnwswswnw
             nenwswwsewswnenenewsenwsenwnesesenew
             enewnwewneswsewnwswenweswnenwsenwsw
             sweneswneswneneenwnewenewwneswswnese
             swwesenesewenwneswnwwneseswwne
             enesenwswwswneneswsenwnewswseenwsese
             wnwnesenesenenwwnenwsewesewsesesew
             nenewswnwewswnenesenwnesewesw
             eneswnwswnwsenenwnwnwwseeswneewsenese
             neswnwewnwnwseenwseesewsenwsweewe
             wseweeenwnesenwwwswnew";

        let mut day = Day24::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(10));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(230));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "sesenwnenenewseeswwswswwnenewsewsw
             neeenesenwnwwswnenewnwwsewnenwseswesw
             seswneswswsenwwnwse
             nwnwneseeswswnenewneswwnewseswneseene
             swweswneswnenwsewnwneneseenw
             eesenwseswswnenwswnwnwsewwnwsene
             sewnenenenesenwsewnenwwwse
             wenwwweseeeweswwwnwwe
             wsweesenenewnwwnwsenewsenwwsesesenwne
             neeswseenwwswnwswswnw
             nenwswwsewswnenenewsenwsenwnesesenew
             enewnwewneswsewnwswenweswnenwsenwsw
             sweneswneswneneenwnewenewwneswswnese
             swwesenesewenwneswnwwneseswwne
             enesenwswwswneneswsenwnewswseenwsese
             wnwnesenesenenwwnenwsewesewsesesew
             nenewswnwewswnenesenwnesewesw
             eneswnwswnwsenenwnwnwwseeswneewsenese
             neswnwewnwnwseenwseesewsenwsweewe
             wseweeenwnesenwwwswnew";

        let mut day = Day24::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2208));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day24.txt");

        let mut day = Day24::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3565));
    }
}
