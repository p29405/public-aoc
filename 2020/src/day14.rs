use aoc::{self, Day, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use fnv::FnvHashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day14 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Instruction>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day14 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day14::Part1;
    type Part2 = crate::day14::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut memory = FnvHashMap::default();
        run(&self.input, &mut memory);

        self.part1 = Some(memory.values().sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut memory = FnvHashMap::default();
        run_p2(&self.input, &mut memory);

        self.part2 = Some(memory.values().sum());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn run(instructions: &[Instruction], memory: &mut FnvHashMap<usize, i64>) {
    let mut current_mask = None;

    for instruction in instructions {
        match instruction {
            Instruction::Mask { mask } => {
                current_mask = Some(mask);
            },
            Instruction::Set { address, value }=> {
                let result = if let Some(mask) = current_mask { value & !mask.clear | mask.set } else { *value };
                memory.insert(*address, result);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn run_p2(instructions: &[Instruction], memory: &mut FnvHashMap<usize, i64>) {
    let mut current_mask = None;

    for instruction in instructions {
        match instruction {
            Instruction::Mask { mask } => {
                current_mask = Some(mask);
            },
            Instruction::Set { address, value }=> {
                if let Some(mask) = current_mask {
                    floating(*address, mask).for_each(|address| { memory.insert(address, *value); });
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn floating(address: usize, mask: &Mask) -> Floating {
    let initial = (address | mask.set as usize) & !mask.floating;
    Floating { address: initial, counter: 0, end: 1 << mask.floating.count_ones() as usize, mask: mask.floating }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Floating {
    address: usize,
    counter: usize,
    end: usize,
    mask: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Iterator for Floating {
    type Item = usize;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        let mut value_mask = self.counter;
        let mut next = self.address;
        if value_mask < self.end {
            for i in 0..36 {
                let bit = 1usize << i;
                if self.mask & bit == bit {
                    if value_mask & 1 == 1 { next |=  bit }
                    value_mask >>= 1;
                }
                if value_mask == 0 {
                    break;
                }
            }
            self.counter += 1;
            Some(next)
        } else {
            None
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Mask {
    pub set: i64,
    pub clear: i64,
    pub floating: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub enum Instruction {
    Mask { mask: Mask },
    Set { address: usize, value: i64 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref MASK_REGEX: Regex = Regex::new(r"^mask = (.+)$").unwrap();
            static ref MEM_REGEX: Regex = Regex::new(r"^mem\[(\d+)\] = (\d+)").unwrap();
        }

        if let Some(mem_match) = MEM_REGEX.captures(s) {
            let address = mem_match.get(1).unwrap().as_str().parse().unwrap();
            let value = mem_match.get(2).unwrap().as_str().parse().unwrap();

            Ok(Self::Set{ address, value })
        } else if let Some(mask_match) = MASK_REGEX.captures(s) {
            let mask = mask_match.get(1).unwrap().as_str();

            let set = mask.chars().fold(0, |set, c| set << 1 | (c == '1') as i64);
            let clear = mask.chars().fold(0, |clear, c| clear << 1 | (c == '0') as i64);
            let floating = mask.chars().fold(0, |floating, c| floating << 1 | (c == 'X') as usize);

            Ok(Self::Mask { mask: Mask { set, clear, floating }})

        } else {
            Err(aoc::Error::new(&format!("Unable to match {}", s)))
        }

    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day14::Day14;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
             mem[8] = 11
             mem[7] = 101
             mem[8] = 0";

        let mut day = Day14::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(165));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "mask = 000000000000000000000000000000X1001X
             mem[42] = 100
             mask = 00000000000000000000000000000000X0XX
             mem[26] = 1";

        let mut day = Day14::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(208));
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(9_967_721_333_886));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day14.txt");

        let mut day = Day14::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(4_355_897_790_573));
    }
}
