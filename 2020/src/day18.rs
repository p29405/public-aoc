use aoc::{self, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day18 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Expression>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day18 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day18::Part1;
    type Part2 = crate::day18::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter()
            .map(|e| {
                let mut index = 0;
                evaluate(e, &mut index).unwrap()
            })
            .sum();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {

        let answer = self.input.iter()
            .map(|e| {
                let mut index = 0;
                evaluate_p2(e, &mut index).unwrap()
            })
            .sum();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn evaluate(expr: &Expression, current: &mut usize) -> Option<i64> {
    let mut result = None;
    let mut operation = None;

    while *current < expr.0.len() {
        match expr.0[*current] {
            Element::Value { v } => result = apply(&result, &operation, v),
            Element::Multiplication => operation = Some(Element::Multiplication),
            Element::Addition => operation = Some(Element::Addition),
            Element::GroupStart => {
                *current += 1;
                result = apply(&result, &operation, evaluate(expr, current).unwrap());
            }
            Element::GroupEnd => break
        }
        *current += 1;
    }

    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn apply(left: &Option<i64>, op: &Option<Element>, right: i64) -> Option<i64> {
    let mut result = Some(right);
    if let Some(left) = left {
        if let Some(op) = op {
            result = match op {
                Element::Addition => Some(left + right),
                Element::Multiplication => Some(left * right),
                _ => panic!("Invalid operation {:?}", op)
            }
        }
    }
    result
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn evaluate_p2(expr: &Expression, current: &mut usize) -> Option<i64> {
    let mut result = None;
    let mut operation = None;
    let mut pending = None;

    while *current < expr.0.len() {
        match expr.0[*current] {
            Element::Value { v } => {
                let (r, p)  = apply_p2(&result, &operation, &pending, v);
                result = r;
                pending = p;
            },
            Element::Multiplication => operation = Some(Element::Multiplication),
            Element::Addition => operation = Some(Element::Addition),
            Element::GroupStart => {
                *current += 1;
                let (r, p) = apply_p2(&result, &operation, &pending, evaluate_p2(expr, current).unwrap());
                result = r;
                pending = p;
            }
            Element::GroupEnd => break
        }
        *current += 1;
    }

    apply(&result, &Some(Element::Multiplication), pending.unwrap_or(1))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn apply_p2(left: &Option<i64>, op: &Option<Element>, pending: &Option<i64>, right: i64) -> (Option<i64>, Option<i64>) {
    let mut result = Some(right);
    let mut new_pending = *pending;
    if let Some(Element::Multiplication) = op {
        new_pending = apply(left, op, pending.unwrap_or(1));
    } else {
        result = apply(left, op, right)
    }

    (result, new_pending)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub enum Element {
    Value { v: i64 },
    Addition,
    Multiplication,
    GroupStart,
    GroupEnd
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Expression(Vec<Element>);

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Expression {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut elements = vec![];

        for c in s.chars() {
            match c {
                '*' => elements.push(Element::Multiplication),
                '+' => elements.push(Element::Addition),
                '(' => elements.push(Element::GroupStart),
                ')' => elements.push(Element::GroupEnd),
                c => if let Some(v) = c.to_digit(10) {
                    elements.push(Element::Value { v: v as i64 });
                } else {
                    continue;
                }
            }
        }

        Ok(Self(elements))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day18::Day18;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs: [(&str, i64); 6] =
            [
                ("1 + 2 * 3 + 4 * 5 + 6", 71),
                ("1 + (2 * 3) + (4 * (5 + 6))", 51),
                ("2 * 3 + (4 * 5)", 26),
                ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437),
                ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12_240),
                ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13_632),
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day18::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input = {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day18.txt");

        let mut day = Day18::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(800_602_729_153));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs: [(&str, i64); 8] =
            [
                ("2 * 3 + 4", 14),
                ("(1 + 1) * (2 + 1) + 4", 14),
                ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23_340),
                ("1 + 2 * 3 + 4 * 5 + 6", 231),
                ("1 + (2 * 3) + (4 * (5 + 6))", 51),
                ("2 * 3 + (4 * 5)", 46),
                ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445),
                ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669_060),
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day18::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer), "Input = {}", input);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day18.txt");

        let mut day = Day18::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(92_173_009_047_076));
    }
}
