use aoc::{self, Day, Result};
use std::str::FromStr;
use std::collections::{HashMap, HashSet};
use std::ops::RangeInclusive;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day16 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub notes: Vec<Note>,
    pub my_ticket: Ticket,
    pub other_tickets: Vec<Ticket>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day16 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day16::Part1;
    type Part2 = crate::day16::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");
        let notes = aoc::read_list_from_string(parts.next().unwrap(), "\n").unwrap();
        let my_ticket = parts.next().unwrap().lines().nth(1).unwrap().parse().unwrap();
        let other_tickets = parts.next().unwrap().lines().skip(1).map(|s| s.parse().unwrap()).collect();

        Ok(Self { part1: None, part2: None, notes, my_ticket, other_tickets})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.other_tickets.iter()
            .fold(0, |accum, ticket| {
                accum + ticket.details.iter().fold(0, |accum, field| {
                    if !self.notes.iter().any(|n| n.ranges.0.contains(field) || n.ranges.1.contains(field)) {
                        accum + field
                    } else {
                        accum
                    }
                })
            });
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let valid = self.other_tickets.iter()
            .filter(|ticket| {
                ticket.details.iter().all(|field| {
                    self.notes.iter().any(|n| n.ranges.0.contains(field) || n.ranges.1.contains(field))
                })
            })
            .collect::<Vec<_>>();

        let mut possibilites = vec![];
        for v in valid[0].details.iter() {
            let possible = self.notes.iter()
                .filter(|n| n.ranges.0.contains(v) || n.ranges.1.contains(v))
                .map(|n| n.name.to_string()).collect::<HashSet<String>>();
            possibilites.push(possible);
        }

        for ticket in valid.iter().skip(1) {
            for (i, v) in ticket.details.iter().enumerate() {
                self.notes.iter()
                    .filter(|n| !n.ranges.0.contains(v) && !n.ranges.1.contains(v))
                    .for_each(|n| { possibilites[i].remove(&n.name); });
            }
        }

        let mut known = HashMap::new();
        loop {
            let single = possibilites.iter().enumerate()
                .find(|(_, p)| p.len() == 1)
                .map(|(i, p)| (i, p.iter().next().unwrap().to_string())).unwrap();
            known.insert(single.1.clone(), single.0);

            if known.len() == 20 { break; }
            possibilites.iter_mut().for_each(|p| { p.remove(&single.1); })

        }

        let mut answer = 1;
        for (field, i) in known.iter() {
            if field.starts_with("departure") {
                answer *= self.my_ticket.details[*i];
            }
        }

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Note {
    name: String,
    ranges: (RangeInclusive<i64>, RangeInclusive<i64>)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Note {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(':');

        let name = parts.next().unwrap().to_string();

        let mut ranges_parts = parts.next().unwrap().trim().split(" or ");

        let mut range_parts = ranges_parts.next().unwrap().split('-');
        let range_start = range_parts.next().unwrap().parse::<i64>().unwrap();
        let range_end = range_parts.next().unwrap().parse::<i64>().unwrap();
        let range1 = range_start..=range_end;

        let mut range_parts = ranges_parts.next().unwrap().split('-');
        let range_start = range_parts.next().unwrap().parse::<i64>().unwrap();
        let range_end = range_parts.next().unwrap().parse::<i64>().unwrap();
        let range2 = range_start..=range_end;

        Ok(Self { name, ranges: (range1, range2) })
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Ticket {
    details: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Ticket {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {

        Ok(Self { details: aoc::read_list_from_string(s, ",").unwrap() })
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day16::Day16;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "class: 1-3 or 5-7
             row: 6-11 or 33-44
             seat: 13-40 or 45-50

             your ticket:
             7,1,14

             nearby tickets:
             7,3,47
             40,4,50
             55,2,20
             38,6,12";

        let mut day = Day16::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(71));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(29_878));
    }

    /*
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "class: 0-1 or 4-19
             row: 0-5 or 8-19
             seat: 0-13 or 16-19

             your ticket:
             11,12,13

             nearby tickets:
             3,9,18
             15,1,5
             5,14,9";

        let mut day = Day16::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(71));
    }*/
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day16.txt");

        let mut day = Day16::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(855_438_643_439));
    }
}
