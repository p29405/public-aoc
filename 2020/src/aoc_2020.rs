use structopt::StructOpt;

use aoc::{self, logger, Result, Aoc, day_match};

///////////////////////////////////////////////////////////////////////////////
mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod game_console;

///////////////////////////////////////////////////////////////////////////////
#[derive(StructOpt, Debug)]
#[structopt(name = "aoc")]
struct Options {
    #[structopt(flatten)]
    logging: logger::Options,
    #[structopt(possible_values = &Aoc::variants(), case_insensitive = true)]
    day: Aoc
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn input_file(day: i32) -> String {
    format!("2020/input/day{:02}.txt", day)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn main() -> Result<()> {
    let opts = Options::from_args();
    logger::initialize(opts.logging);

    day_match!(opts, Aoc::Day01, day01, Day01, 1);
    day_match!(opts, Aoc::Day02, day02, Day02, 2);
    day_match!(opts, Aoc::Day03, day03, Day03, 3);
    day_match!(opts, Aoc::Day04, day04, Day04, 4);
    day_match!(opts, Aoc::Day05, day05, Day05, 5);
    day_match!(opts, Aoc::Day06, day06, Day06, 6);
    day_match!(opts, Aoc::Day07, day07, Day07, 7);
    day_match!(opts, Aoc::Day08, day08, Day08, 8);
    day_match!(opts, Aoc::Day09, day09, Day09, 9);
    day_match!(opts, Aoc::Day10, day10, Day10, 10);
    day_match!(opts, Aoc::Day11, day11, Day11, 11);
    day_match!(opts, Aoc::Day12, day12, Day12, 12);
    day_match!(opts, Aoc::Day13, day13, Day13, 13);
    day_match!(opts, Aoc::Day14, day14, Day14, 14);
    day_match!(opts, Aoc::Day15, day15, Day15, 15);
    day_match!(opts, Aoc::Day16, day16, Day16, 16);
    day_match!(opts, Aoc::Day17, day17, Day17, 17);
    day_match!(opts, Aoc::Day18, day18, Day18, 18);
    day_match!(opts, Aoc::Day19, day19, Day19, 19);
    day_match!(opts, Aoc::Day20, day20, Day20, 20);
    day_match!(opts, Aoc::Day21, day21, Day21, 21);
    day_match!(opts, Aoc::Day22, day22, Day22, 22);
    day_match!(opts, Aoc::Day23, day23, Day23, 23);
    day_match!(opts, Aoc::Day24, day24, Day24, 24);
    day_match!(opts, Aoc::Day25, day25, Day25, 25);

    Ok(())
}


