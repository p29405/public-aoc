use aoc::{self, Day, Result};
use std::collections::HashMap;
use std::str::FromStr;


////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day19 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub rules: Rules,
    pub input: Vec<Vec<char>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day19 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day19::Part1;
    type Part2 = crate::day19::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.split("\n\n");

        let rules = parts.next().unwrap().parse()?;
        let input = parts.next().unwrap().lines().map(|l| l.trim().chars().collect::<Vec<_>>()).collect();
        Ok(Self { part1: None, part2: None, rules, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter()
            .filter(|s| {
                let result = matches(&s, 0, &self.rules, 0);
                result.0 && result.1 == s.len()
            })
            .count();

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.rules.map.insert(8, Rule::Option { left: vec![42], right: vec![42, 8]});
        self.rules.map.insert(11, Rule::Option { left: vec![42, 31], right: vec![42, 11, 31]});

        let answer = self.input.iter()
            .filter(|s| {
                let result = matches_p2(&s, 0, &self.rules, 0);
                result.iter().any(|(valid, len)| *valid && *len == s.len())
            })
            .count();

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn matches(string: &[char], index: usize, rules: &Rules, rule: usize) -> (bool, usize) {
    let rule = rules.map.get(&rule).unwrap();
    let mut current = index;

    match rule {
        Rule::Char(c) => {
            return if string[current] != *c {
                (false, current)
            } else {
                (true, current + 1)
            }
        },
        Rule::Sequence(sequence) => {
            let result = check_sequence(string, current, rules, sequence);
            if result.0 {
                current = result.1;
            } else {
                return (false, current);
            }
        },
        Rule::Option {left, right} => {
            let result = check_sequence(string, current, rules, left);

            if result.0 {
                current = result.1;
            } else {
                let result = check_sequence(string, current, rules, right);
                if result.0 {
                    current = result.1;
                } else {
                    return (false, current);
                }
            }
        }
    };

    (true, current)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn check_sequence(string: &[char], index: usize, rules: &Rules, sequence: &[usize]) -> (bool, usize) {
    if index >= string.len() - 1 {
        return (false, index);
    }
    let mut current = index;
    for r in sequence.iter() {
        let result = matches(string, current, rules, *r);

        if result.0 {
            current = result.1;
        } else {
            return (false, current);
        }
    }

    (true, current)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn matches_p2(string: &[char], index: usize, rules: &Rules, rule: usize) -> Vec<(bool, usize)> {
    let rule = rules.map.get(&rule).unwrap();
    if index >= string.len() {
        return vec![];
    }
    match rule {
        Rule::Char(c) => if string[index] != *c { vec![] } else { vec![(true, index + 1)] },
        Rule::Sequence(sequence) => check_sequence_p2(string, index, rules, sequence),
        Rule::Option {left, right} => {
            let mut result = check_sequence_p2(string, index, rules, left);
            result.append(&mut check_sequence_p2(string, index, rules, right));

            result
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn check_sequence_p2(string: &[char], index: usize, rules: &Rules, sequence: &[usize]) -> Vec<(bool, usize)> {
    if index >= string.len() - 1 {
        return vec![];
    }

    let mut options = vec![(true, index)];
    for r in sequence.iter() {
        let starts = options.clone();
        options.clear();
        for (_, i) in starts {
            let results = matches_p2(string, i, rules, *r);
            options.extend(results.iter().filter(|(v, i)| *v && *i <= string.len()));
        }
    }

    options
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub enum Rule {
    Char(char),
    Sequence(Vec<usize>),
    Option { left: Vec<usize>, right: Vec<usize> }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Rules {
    map: HashMap<usize, Rule>
}


////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Rules {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut map = HashMap::new();
        for l in s.lines() {
            let mut parts = l.trim().split(':');
            let id= parts.next().unwrap().parse().unwrap();
            let rule = parts.next().unwrap().trim();
            if rule.contains('"') {
                let c = rule.chars().nth(1).unwrap();
                map.insert(id, Rule::Char(c));
            } else if rule.contains('|') {
                let mut parts = rule.trim().split('|');
                let left = parts.next().unwrap().trim().split(' ').map(|c| c.trim().parse().unwrap()).collect();
                let right = parts.next().unwrap().trim().split(' ').map(|c| c.parse().unwrap()).collect();
                map.insert(id, Rule::Option { left, right });
            } else {
                let sequence = rule.split(' ').map(|c| c.parse().unwrap()).collect();
                map.insert(id, Rule::Sequence(sequence));
            }
        }

        Ok(Self { map })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day19::Day19;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "0: 4 1 5
             1: 2 3 | 3 2
             2: 4 4 | 5 5
             3: 4 5 | 5 4
             4: \"a\"
             5: \"b\"

             ababbb
             bababa
             abbbab
             aaabbb
             aaaabbb";

        let mut day = Day19::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(102));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "42: 9 14 | 10 1
             9: 14 27 | 1 26
             10: 23 14 | 28 1
             1: \"a\"
             11: 42 31
             5: 1 14 | 15 1
             19: 14 1 | 14 14
             12: 24 14 | 19 1
             16: 15 1 | 14 14
             31: 14 17 | 1 13
             6: 14 14 | 1 14
             2: 1 24 | 14 4
             0: 8 11
             13: 14 3 | 1 12
             15: 1 | 14
             17: 14 2 | 1 7
             23: 25 1 | 22 14
             28: 16 1
             4: 1 1
             20: 14 14 | 1 15
             3: 5 14 | 16 1
             27: 1 6 | 14 18
             14: \"b\"
             21: 14 1 | 1 14
             25: 1 1 | 1 14
             22: 14 14
             8: 42
             26: 14 22 | 1 20
             18: 15 15
             7: 14 5 | 1 21
             24: 14 1

             abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
             bbabbbbaabaabba
             babbbbaabbbbbabbbbbbaabaaabaaa
             aaabbbbbbaaaabaababaabababbabaaabbababababaaa
             bbbbbbbaaaabbbbaaabbabaaa
             bbbababbbbaaaaaaaabbababaaababaabab
             ababaaaaaabaaab
             ababaaaaabbbaba
             baabbaaaabbaaaababbaababb
             abbbbabbbbaaaababbbbbbaaaababb
             aaaaabbaabaaaaababaa
             aaaabbaaaabbaaa
             aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
             babaaabbbaaabaababbaabababaaab
             aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";

        let mut day = Day19::setup(input).unwrap();
        day.run_part1();
        day.run_part2();
        assert_eq!(day.part1_answer(), Some(3));
        assert_eq!(day.part2_answer(), Some(12));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day19.txt");

        let mut day = Day19::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(318));
    }
}
