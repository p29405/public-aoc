use aoc::{self, Day, Result, GridElement, Map, Grid, Coord};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day03 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Square>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day03 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day03::Part1;
    type Part2 = crate::day03::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(count_trees(&self.input, (3, 1)));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)].iter()
            .map(|s| count_trees(&self.input, *s))
            .product();

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn count_trees(grid: &Grid<Square>, slope: (i64, i64)) -> i64 {
    let mut current = Coord::new(0, 0);
    let mut trees = 0;

    while current.y < grid.rows {
        if grid.get(&current) == Square::Tree {
            trees += 1;
        }

        current.x = (current.x + slope.0) % grid.columns;
        current.y += slope.1;
    }

    trees
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Square {
    Empty,
    Tree
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Square::Empty
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Square {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Square::Empty => write!(f, "."),
            Square::Tree => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Square {
    fn to_element(c: char) -> Self {
        match c {
            '.' => Self::Empty,
            '#' => Self::Tree,
            _ => panic!("Unknown")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day03::Day03;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "..##.......
             #...#...#..
             .#....#..#.
             ..#.#...#.#
             .#...##..#.
             ..#.##.....
             .#.#.#....#
             .#........#
             #.##...#...
             #...##....#
             .#..#...#.#";

        let mut day = Day03::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(7));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day03.txt");
        let mut day = Day03::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(240));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "..##.......
             #...#...#..
             .#....#..#.
             ..#.#...#.#
             .#...##..#.
             ..#.##.....
             .#.#.#....#
             .#........#
             #.##...#...
             #...##....#
             .#..#...#.#";

        let mut day = Day03::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(336));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day03.txt");
        let mut day = Day03::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2_832_009_600));
    }
}
