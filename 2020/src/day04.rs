use aoc::{self, Day, Result};
use fnv::FnvHashMap;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day04 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Id>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day04 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day04::Part1;
    type Part2 = crate::day04::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter()
            .filter(|id| valid(id, true))
            .count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .filter(|id| valid(id, false))
            .count();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid(id: &Id, part1: bool) -> bool {
    if id.details.len() < 7 { return false; }
    if id.details.len() == 7 &&  id.details.get("cid").is_some() { return false; }
    if part1 { return true; }

    if !valid_year(id, "byr", 1920, 2002) { return false; }
    if !valid_year(id, "iyr", 2010, 2020) { return false; }
    if !valid_year(id, "eyr", 2020, 2030) { return false; }

    if let Some(ecl) = id.details.get("ecl") {
        match ecl.as_str() {
            "amb"|"blu"|"brn"|"gry"|"grn"|"hzl"|"oth" => (),
            _ => return false
        }
    } else {
        return false;
    }

    if let Some(hcl) = id.details.get("hcl") {
        let mut chars = hcl.chars();
        if chars.next().unwrap() != '#' {
            return false;
        }

        let rest = chars.as_str();
        if usize::from_str_radix(rest, 16).is_err() {
            return false;
        }
    } else {
        return false;
    }

    if let Some(pid) = id.details.get("pid") {
        if pid.len() != 9 || pid.parse::<usize>().is_err() {
            return false;
        }
    } else {
        return false;
    }

    if let Some(hgt) = id.details.get("hgt") {
        let mut chars = hgt.chars();
        let mut height = 0;
        let mut last = chars.next();
        loop {
            if let Some(c) = last {
                if c.is_ascii_digit() {
                    height = height * 10 + c.to_digit(10).unwrap();
                } else {
                    break;
                }
            } else {
                return false;
            }

            last = chars.next();
        }
        if hgt.ends_with("in") {
            if !(59..=76).contains(&height) {
                return false;
            }
        } else if hgt.ends_with("cm") {
            if !(150..=193).contains(&height) {
                return false;
            }
        } else {
            return false;
        }

    } else {
        return false;
    }
    true
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid_year(id: &Id, field: &str, min: u32, max: u32) -> bool {
    id.details.get(field).map_or_else(|| false, |s| valid_number(s, min, max))
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn valid_number(s: &str, min: u32, max: u32) -> bool {
    s.parse::<u32>().map_or_else(|_| false, |i| i >= min && i <= max)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Id {
    details: FnvHashMap<String, String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Id {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut details = FnvHashMap::default();

        for l in s.lines() {
            l.trim().split(' ').for_each(|part| {
                let mut parts = part.trim().split(':');
                details.insert(parts.next().unwrap().to_owned(), parts.next().unwrap().to_owned());
            })
        }

        Ok(Self { details })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day04::Day04;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
             byr:1937 iyr:2017 cid:147 hgt:183cm

             iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
             hcl:#cfa07d byr:1929

             hcl:#ae17e1 iyr:2013
             eyr:2024
             ecl:brn pid:760753108 byr:1931
             hgt:179cm

             hcl:#cfa07d eyr:2025 pid:166559648
             iyr:2011 ecl:brn hgt:59in";

        let mut day = Day04::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2));

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day04.txt");
        let mut day = Day04::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(202));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day04.txt");
        let mut day = Day04::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(137));
    }
}
