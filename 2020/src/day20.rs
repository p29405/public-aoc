use aoc::{self, Day, Grid, Map, Pixel, Result, Direction, Coord};
use std::str::FromStr;
use std::collections::{HashSet, HashMap};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day20 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Tile>,
    pub neighbors: HashMap<usize, HashSet<usize>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day20 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day20::Part1;
    type Part2 = crate::day20::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n\n")?;

        let mut neighbors = HashMap::new();

        for t in input.iter() {
            let possible = matches(t, &input);
            neighbors.insert(t.id, possible);
        }

        Ok(Self { part1: None, part2: None, input, neighbors})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.neighbors.iter().filter(|(_, p)| p.len() == 2).map(|(id, _)| id).product();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        // define the layout....
        let (first_corner, _) = self.neighbors.iter().find(|(_, p)| p.len() == 2).unwrap();
        let dim = (self.input.len() as f64).sqrt() as usize;
        let mut layout = vec![];
        let (mut x, mut y) = (0, 0);

        layout.resize(dim * dim, 0);
        layout[0] = *first_corner;

        while y * dim + x < self.input.len() {
            let current = layout[y * dim + x];
            let possible = self.neighbors.get(&current).unwrap();
            if y == 0 && possible.len() > 3 { panic!("Crap"); }
            let filtered = possible.iter().filter(|id| !layout.contains(id)).collect::<Vec<_>>();

            if x == 0 && y == 0 {
                layout[1] = *filtered[0];
                layout[dim] = *filtered[1];
            } else if y != dim - 1 {
                if x != 0 {
                    let companion = layout[dim * (y + 1) + x - 1];
                    let shared = self.neighbors.get(&companion).unwrap().iter().find(|id| !layout.contains(id) && filtered.contains(id)).unwrap();
                    layout[dim * (y + 1) + x] = *shared;
                } else {
                    let lower = filtered.iter().find(|id| !layout.contains(id) && filtered.contains(id)).unwrap();
                    layout[dim * (y + 1) + x] = **lower;
                }

                if x != dim - 1  && y == 0 {
                    let right = filtered.iter().find(|&id| !layout.contains(id)).unwrap();
                    layout[dim * y + x + 1] = **right;
                }
            } else {
                break;
            }

            y += if x == dim - 1 { 1 } else { 0 };
            x = (x + 1) % dim;
        }

        // figure out orientation
        x = 0;
        y = 0;

        while y * dim + x < self.input.len() {
            let current = layout[y * dim + x];
            let right = if x != dim - 1 { Some(layout[y * dim + x + 1]) } else { None };
            let left = if x != 0 { Some(layout[y * dim + x - 1]) } else { None };
            let down = if y != dim - 1 { Some(layout[(y + 1)* dim + x]) } else { None };
            let up = if y != 0 { Some(layout[(y - 1)* dim + x]) } else { None };

            let tile = self.input.iter().find(|t| t.id == current).unwrap();
            let mut found = false;

            let mut scratch;

            for i in 0..8 {
                scratch = tile.image.clone();

                if i >= 4 {
                    scratch = scratch.flip_horizontal();
                }

                for _ in 0..(i % 4) {
                    scratch = scratch.rotate_right();
                }

                if let Some(right) = right {
                    let east = self.input.iter().find(|t| t.id == right).unwrap();
                    if !east.borders.contains(&Tile::border(&scratch, Direction::East)) {
                        continue;
                    }
                }
                if let Some(down) = down {
                    let south = self.input.iter().find(|t| t.id == down).unwrap();
                    if !south.borders.contains(&Tile::border(&scratch, Direction::South)) {
                        continue;
                    }
                }
                if let Some(left) = left {
                    let west = self.input.iter().find(|t| t.id == left).unwrap();
                    if Tile::border(&west.image, Direction::East) != Tile::border(&scratch, Direction::West) {
                        continue;
                    }
                }
                if let Some(up) = up {
                    let north = self.input.iter().find(|t| t.id == up).unwrap();
                    if Tile::border(&north.image, Direction::South) != Tile::border(&scratch, Direction::North) {
                        continue;
                    }
                }

                found = true;
                let mut tile = self.input.iter_mut().find(|t| t.id == current).unwrap();
                tile.image = scratch.clone();
                break;
            }

            if !found {
                panic!("Orientation not found!!!");
            }

            y += if x == dim - 1 { 1 } else { 0 };
            x = (x + 1) % dim;
        }

        // TODO: The above sucks, I need to work through what actually needs to happen rather than just trying random things.

        // strip border and stitch
        let stripped_images = layout.iter().map(|id| self.input.iter().find(|t| *id == t.id).unwrap()).map(|t| strip_border(&t.image)).collect::<Vec<Grid<Pixel>>>();

        let full = Grid::combine(&stripped_images, dim as i64, dim as i64);

        let monster_locations = search(&full);

        let monster =
            "..................#.".to_owned() + "\n" +
            "#....##....##....###" + "\n" +
            ".#..#..#..#..#..#...";

        let monster_count = monster.chars().fold(0, |accum, c| accum + (c == '#') as usize);
        let element_count = full.elements.iter().fold(0, |accum, p| accum + (p == &Pixel::On) as usize);
        self.part2 = Some(element_count - monster_count * monster_locations.len());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn search(image: &Grid<Pixel>) -> Vec<Coord> {
    // search of monsters

    // 20x3
    // 0x00_0002
    // 0x08_6187
    // 0x04_9248
    let mut scratch = image.clone();
    let mut locations = vec![];

    for i in 0..8 {
        if i == 5 {
            scratch = scratch.flip_horizontal();
        }

        if i > 0 {
            scratch = scratch.rotate_right();
        }

        for y in 0..scratch.rows-2 {
            for x in 0..scratch.columns-19 {
                let value = monster_value(&scratch, Coord::new(x, y));

                if value[0] & 0x00_0002 == 0x00_0002 && value[1] & 0x08_6187 == 0x08_6187 && value[2] & 0x04_9248 == 0x04_9248 {
                    // // println!("Found at {}, {}", x, y);
                    locations.push(Coord::new(x, y));
                }
            }
        }

        if !locations.is_empty() {
            break;
        }
    }

    locations
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn monster_value(full: &Grid<Pixel>, start: Coord) -> Vec<usize> {
    let mut value = vec![];

    for y in 0..3 {
        let v = Coord::range_inclusive(start, Coord::new(start.x + 20, start.y + y)).map(|p| (full.get(&p) == Pixel::On) as usize).fold(0, |accum, v| (accum << 1) | v);
        value.push(v);
    }

    value
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn strip_border(image: &Grid<Pixel>) -> Grid<Pixel> {
    let mut grid = Grid::new(image.columns - 2, image.rows - 2);

    for p in Coord::range(Coord::new(1, 1), Coord::new(image.columns - 1, image.rows - 1)) {
        grid.set_xy(p.x - 1, p.y - 1, image.get(&p));
    }

    grid
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn matches(tile: &Tile, tiles: &[Tile]) -> HashSet<usize> {
    let mut neighbors = HashSet::new();

    'outer: for t in tiles.iter() {
        if tile.id == t.id { continue; }
        for v in t.borders.iter() {
            if tile.borders.contains(v) {
                neighbors.insert(t.id);
                continue 'outer;
            }
        }
    }

    neighbors
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn reverse(original: usize, bits: usize) -> usize {
    let mut temp = original;
    let mut reversed = 0;

    for _ in 0..bits {
        reversed <<= 1;
        reversed |= temp & 1;
        temp >>= 1;
    }

    reversed
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Tile {
    pub image: Grid<Pixel>,
    pub id: usize,
    pub borders: Vec<usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Tile {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn border(image: &Grid<Pixel>, side: Direction) -> usize {
        match side {
            Direction::North => (0..image.columns).map(|x| (image.get_xy(x, 0) == Pixel::On) as usize).fold(0, |accum, v| (accum << 1) | v),
            Direction::East => (0..image.rows).map(|y| (image.get_xy(image.columns - 1, y) == Pixel::On) as usize).fold(0, |accum, v| (accum << 1) | v),
            Direction::South => (0..image.columns).map(|x| (image.get_xy(x, image.rows - 1) == Pixel::On) as usize).fold(0, |accum, v| (accum << 1) | v),
            Direction::West => (0..image.rows).map(|y| (image.get_xy(0, y) == Pixel::On) as usize).fold(0, |accum, v| (accum << 1) | v),
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Tile {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split('\n');
        let id = parts.next().unwrap().replace(':', "").split(' ').nth(1).unwrap().parse().unwrap();
        let s: String = parts.collect::<Vec<_>>().join("\n");
        let image: Grid<Pixel> = s.parse().unwrap();
        let mut borders = [Direction::North, Direction::East, Direction::South, Direction::West].iter().map(|d| Tile::border(&image, *d)).collect::<Vec<_>>();

        for i in 0..4 {
            borders.push(reverse(borders[i], image.columns as usize));
        }

        Ok(Self { image, id, borders })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day20::Day20;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        aoc::logger::init(aoc::logger::LevelFilter::Debug).unwrap();
        let input = aoc::get_input_from_file("input/day20_example.txt");

        let mut day = Day20::setup(&input).unwrap();
        assert_eq!(day.input.len(), 9);
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(20_899_048_083_289));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(108_603_771_107_737));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = aoc::get_input_from_file("input/day20_example.txt");

        let mut day = Day20::setup(&input).unwrap();
        assert_eq!(day.input.len(), 9);
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(273));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day20.txt");

        let mut day = Day20::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2129));
    }
}
