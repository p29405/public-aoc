use log::debug;
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Instruction {
    Nop { op1: i64 },
    Acc { op1: i64 },
    Jmp { op1: i64 },
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Instruction {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');
        match parts.next().unwrap() {
            "nop" => Ok(Instruction::Nop {
                op1: parts.next().unwrap().parse().unwrap()
            }),
            "acc" => Ok(Instruction::Acc {
                op1: parts.next().unwrap().parse().unwrap()
            }),
            "jmp" => Ok(Instruction::Jmp {
                op1: parts.next().unwrap().parse().unwrap()
            }),
            _ => Err(aoc::Error::new(&format!("Unknown instruction {}", s)))
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default)]
pub struct Console {
    pub accumulator: i64,
    ip: usize,
    program: Vec<Instruction>,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Console {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn program(&mut self, program: &[Instruction]) -> &mut Self {
        self.program = program.to_vec();
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[allow(dead_code)]
    pub fn reset(&mut self) -> &mut Self {
        self.accumulator = 0;
        self.ip = 0;
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn halts(&mut self) -> bool {
        let mut executed = [false; 650];
        loop {
            if executed[self.ip] {
                break;
            } else {
                executed[self.ip] = true;
            }

            if !self.step() {
                return true;
            }
        }

        false
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn step(&mut self) -> bool {
        debug!("Before: ip: {}, acc: {}, {:?}", self.ip, self.accumulator, self.program[self.ip]);

        if let Some(jump) = self.execute(self.program[self.ip]) {
            self.ip = (self.ip as i64 + jump) as usize;
        } else {
            self.ip += 1;
        }
        debug!("Next: ip: {}, acc: {}", self.ip, self.accumulator);
        debug!("===========================");
        self.ip < self.program.len()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn execute(&mut self, instruction: Instruction) -> Option<i64> {
        match instruction {
            Instruction::Nop { .. } => {
                None
            },
            Instruction::Acc { op1, ..} => {
                self.accumulator += op1;
                None
            },
            Instruction::Jmp { op1, .. } => {
                Some(op1)
            },
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod console_day08 {
    use aoc::read_list_from_string;
    use crate::game_console::Instruction;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "nop +0
             acc +1
             jmp +4
             acc +3
             jmp -3
             acc -99
             acc +1
             jmp -4
             acc +6";

        let instructions = read_list_from_string::<Instruction>(input, "\n").unwrap();

        assert_eq!(instructions.len(), 9);
        assert_eq!(instructions[0], Instruction::Nop { op1: 0, });
        assert_eq!(instructions[1], Instruction::Acc { op1: 1 });
        assert_eq!(instructions[2], Instruction::Jmp { op1: 4 });
        assert_eq!(instructions[3], Instruction::Acc { op1: 3 });
        assert_eq!(instructions[4], Instruction::Jmp { op1: -3 });
        assert_eq!(instructions[5], Instruction::Acc { op1: -99 });
        assert_eq!(instructions[6], Instruction::Acc { op1: 1 });
        assert_eq!(instructions[7], Instruction::Jmp { op1: -4 });
        assert_eq!(instructions[8], Instruction::Acc { op1: 6 });
    }
}
