use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day09 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>,
    preamble: usize,
    part1_position: Option<usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day09 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day09::Part1;
    type Part2 = crate::day09::Part2;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input, preamble: 25, part1_position: None})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut xmas = Xmas::new(self.preamble, &self.input);

        for (i, &n) in self.input.iter().enumerate().skip(self.preamble) {
            if !xmas.insert(n) {
                self.part1_position = Some(i);
                self.part1 = Some(n);
                return;
            }
        }
        self.part1 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut cumulative_sums = vec![];
        let mut accum = 0;
        let index = self.part1_position.unwrap();
        for v in self.input.iter().take(index) {
            accum += v;
            cumulative_sums.push(accum);
        }

        for j in 0..index {
            for k in 3..(index - j) {
                let sum: i64 = cumulative_sums[j + k] - cumulative_sums[j];
                if sum == self.part1.unwrap() {
                    let range = self.input.iter().skip(j).take(k).copied().collect::<Vec<_>>();
                    let (min, max) = aoc::min_max(&range);
                    self.part2 = Some(min + max);
                    return;
                }
            }
        }
        self.part2 = None;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Xmas {
    preamble: usize,
    numbers: [i64; 25],
    sums: [i64; 625],
    insert: usize,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Xmas {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn new(preamble: usize, initial: &[i64]) -> Self {
        let mut sums = [-1; 625];
        let mut numbers = [0; 25];
        for (i, n) in initial[0..preamble].iter().enumerate() {
            numbers[i] = *n;
        }

        for i in 0..preamble {
            for j in i..preamble {
                sums[i * preamble + j] = initial[i] + initial[j];
            }
        }

        Self { preamble, numbers, sums, insert: 0 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn insert(&mut self, next: i64) -> bool {
        if self.sums.contains(&next) {
            self.numbers[self.insert] = next;
            for i in 0..self.preamble {
                if i == self.insert { continue; }

                self.sums[self.insert * self.preamble  + i] = if i == self.insert { -1 } else { next + self.numbers[i] };

            }
            self.insert = (self.insert + 1) % self.preamble;
            true
        } else {
            false
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day09::Day09;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576";

        let mut day = Day09::setup(input).unwrap();
        day.preamble = 5;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(127));
        assert_eq!(day.part1_position, Some(14));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576";

        let mut day = Day09::setup(input).unwrap();
        day.preamble = 5;
        day.part1 = Some(127);
        day.part1_position = Some(14);
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(62));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1_721_308_972));
        assert_eq!(day.part1_position, Some(664));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day09.txt");

        let mut day = Day09::setup(&input).unwrap();
        day.part1 = Some(1_721_308_972);
        day.part1_position = Some(664);
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(209_694_133));
    }
}
