use aoc::{self, Day, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use std::collections::{HashMap, HashSet};


////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = String;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day21 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Recipe>,
    pub allergy_map: HashMap<String, HashSet<String>>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day21 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day21::Part1;
    type Part2 = crate::day21::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string::<Recipe>(input, "\n")?;

        let mut allergy_map = HashMap::new();

        for recipe in input.iter() {
            for allergy in recipe.allergens.iter() {
                let ingredients = recipe.ingredients.iter().cloned().collect::<HashSet<_>>();
                let entry = allergy_map.entry(allergy.clone()).or_insert_with(|| ingredients.clone());
                *entry = entry.intersection(&ingredients).cloned().collect();
            }
        }

        Ok(Self { part1: None, part2: None, input, allergy_map})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut all_ingredients = HashMap::new();

        for recipe in self.input.iter() {
            for i in recipe.ingredients.iter() {
                let entry = all_ingredients.entry(i).or_insert(0);
                *entry += 1;
            }
        }

        let mut possible = vec![];
        for (_, ingredient) in self.allergy_map.iter() {
            ingredient.iter().for_each(|i| { possible.push(i.clone()); });
        }
        let answer = all_ingredients.iter()
            .map(|(ingredient, count)| if possible.contains(ingredient) { 0 } else { *count })
            .sum();

        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut found = vec![];

        loop {
            let (allergy, ingredients) = self.allergy_map.iter().find(|(_, i)| i.len() == 1).unwrap();
            let allergy = allergy.clone();
            let ingredient = ingredients.iter().next().unwrap().clone();
            found.push((allergy.clone(), ingredient.clone()));

            self.allergy_map.remove(&allergy);

            for (_, ingredients) in self.allergy_map.iter_mut() {
                ingredients.remove(&ingredient);
            }

            if self.allergy_map.is_empty() { break; }
        }

        found.sort_by_key(|(k, _)| k.clone());

        let answer = found.iter().map(|(_, ingredient)| ingredient).cloned().collect::<Vec<String>>();

        self.part2 = Some(answer.join(","));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2.clone() }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Recipe {
    pub ingredients: Vec<String>,
    pub allergens: Vec<String>,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Recipe {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        lazy_static! {
            static ref ALLERGENS: Regex = Regex::new(r"([a-z]+)[,\)]").unwrap();
        }

        let mut parts = s.split(" (contains ");

        let ingredients = parts.next().unwrap().split(' ').map(|i| i.to_owned()).collect();

        let allergens = ALLERGENS.captures_iter(parts.next().unwrap()).map(|c| c.get(1).unwrap().as_str().to_owned()).collect();

        Ok(Self { ingredients, allergens })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day21::Day21;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
             trh fvjkl sbzzf mxmxvkd (contains dairy)
             sqjhc fvjkl (contains soy)
             sqjhc mxmxvkd sbzzf (contains fish)";

        let mut day = Day21::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(5));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2078));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
             trh fvjkl sbzzf mxmxvkd (contains dairy)
             sqjhc fvjkl (contains soy)
             sqjhc mxmxvkd sbzzf (contains fish)";

        let mut day = Day21::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("mxmxvkd,sqjhc,fvjkl".to_string()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day21.txt");

        let mut day = Day21::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some("lmcqt,kcddk,npxrdnd,cfb,ldkt,fqpt,jtfmtpd,tsch".to_string()));
    }
}
