use aoc::{self, Day, Result, Coord, Grid, GridElement, Map};
use std::fmt;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day11 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Grid<Layout>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day11 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day11::Part1;
    type Part2 = crate::day11::Part2;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_grid_from_string(input)?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut prev = -1;
        let mut current = 0;
        let mut current_grid = self.input.clone();
        let mut working_grid = self.input.clone();

        while current != prev {
            prev = current;
            current = round(&current_grid, &mut working_grid);
            std::mem::swap(&mut current_grid, &mut working_grid);
        }

        self.part1 = Some(current);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut prev = -1;
        let mut current = 0;
        let mut current_grid = self.input.clone();
        let mut working_grid = self.input.clone();

        while current != prev {
            prev = current;
            current = round_2(&current_grid, &mut working_grid);
            std::mem::swap(&mut current_grid, &mut working_grid);
        }

        self.part2 = Some(current);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn round(current: &Grid<Layout>, next: &mut Grid<Layout>) -> i64 {
    let mut count = 0;
    for Coord { x, y, ..} in current.coord_iter() {
        match current.get_xy(x, y) {
            Layout::Floor => (),
            Layout::Empty => {
                if current.count_neighbors(x, y, |&l| l == Layout::Occupied) > 0 {
                    next.set_xy(x, y, Layout::Empty);
                } else {
                    next.set_xy(x, y, Layout::Occupied);
                    count += 1;
                }
            },
            Layout::Occupied => {
                if current.count_neighbors(x, y,|&l| l == Layout::Occupied) >= 4 {
                    next.set_xy(x, y, Layout::Empty);
                } else {
                    next.set_xy(x, y, Layout::Occupied);
                    count += 1;
                }
            }
        }
    }

    count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn round_2(current: &Grid<Layout>, next: &mut Grid<Layout>) -> i64 {
    let mut count = 0;
    let slopes = [(0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1)];

    for Coord { x, y, ..} in current.coord_iter() {
        match current.get_xy(x, y) {
            Layout::Floor => (),
            Layout::Empty => {
                if slopes.iter().map(|s| first_seat(&current, x, y, s)).any(|l| l == Layout::Occupied) {
                    next.set_xy(x, y, Layout::Empty);
                } else {
                    next.set_xy(x, y, Layout::Occupied);
                    count += 1;
                }
            },
            Layout::Occupied => {
                if slopes.iter().filter(|s| first_seat(&current, x, y, s) == Layout::Occupied).count() >= 5 {
                    next.set_xy(x, y, Layout::Empty);
                } else {
                    next.set_xy(x, y, Layout::Occupied);
                    count += 1;
                }
            }
        }
    }

    count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn first_seat(grid: &Grid<Layout>, start_x: i64, start_y: i64, slope: &(i64, i64)) -> Layout {
    let mut x = start_x;
    let mut y = start_y;
    let mut seat = Layout::Floor;

    while seat == Layout::Floor {
        x += slope.0;
        y += slope.1;

        if !(0..grid.columns).contains(&x) { break; }
        if !(0..grid.rows).contains(&y) { break; }

        seat = grid.get_xy(x, y);
    }

    seat
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Layout {
    Floor,
    Empty,
    Occupied
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Layout {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Layout::Floor
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Layout {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Layout::Floor => write!(f, "."),
            Layout::Empty => write!(f, "L"),
            Layout::Occupied => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Layout {
    fn to_element(c: char) -> Self {
        match c {
            '.' => Self::Floor,
            'L' => Self::Empty,
            '#' => Self::Occupied,
            _ => panic!("Unknown")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day11::Day11;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "L.LL.LL.LL
             LLLLLLL.LL
             L.L.L..L..
             LLLL.LL.LL
             L.LL.LL.LL
             L.LLLLL.LL
             ..L.L.....
             LLLLLLLLLL
             L.LLLLLL.L
             L.LLLLL.LL";

        let mut day = Day11::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(37));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "L.LL.LL.LL
             LLLLLLL.LL
             L.L.L..L..
             LLLL.LL.LL
             L.LL.LL.LL
             L.LLLLL.LL
             ..L.L.....
             LLLLLLLLLL
             L.LLLLLL.L
             L.LLLLL.LL";

        let mut day = Day11::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(26));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2321));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day11.txt");

        let mut day = Day11::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2102));
    }
}
