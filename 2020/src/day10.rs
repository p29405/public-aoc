use aoc::{self, Day, Result};
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day10 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day10 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day10::Part1;
    type Part2 = crate::day10::Part2;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut input = aoc::read_list_from_string(input, "\n")?;
        input.push(0);
        input.sort_unstable();
        input.push(input.last().unwrap() + 3);

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let (ones, threes) = self.input.iter()
            .zip(self.input.iter().skip(1))
            .fold((0, 0), |(ones, threes), (prev, curr)| {
                let diff = curr - prev;
                (ones + if diff == 1 { 1 } else { 0 }, threes + if diff == 3 { 1 } else { 0 })
            });

        self.part1 = Some(ones * threes);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut store = HashMap::new();
        store.insert(self.input.len() - 1, 1);

        self.part2 = Some(counts(&self.input, 0, &mut store));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn counts(input: &[i64], i: usize, store: &mut HashMap<usize, usize>) -> usize {
    if let Some(count) = store.get(&i) { return *count; }

    let value = input[i];
    let count = (1..4)
        .filter(|&j| i + j < input.len())
        .filter(|&j| input[i + j] <= value + 3)
        .map(|j| counts(input, i + j, store))
        .sum();
    store.insert(i,  count);

    count
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day10::Day10;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(35));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3";

        let mut day = Day10::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(220));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2312));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_3() {
        let input = "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4";

        let mut day = Day10::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(8));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_4() {
        let input = "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3";

        let mut day = Day10::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(19_208));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day10.txt");

        let mut day = Day10::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(12_089_663_946_752));
    }
}
