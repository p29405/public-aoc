use aoc::{self, Day, Result};
use std::collections::VecDeque;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day22 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub player1: Vec<usize>,
    pub player2: Vec<usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day22 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day22::Part1;
    type Part2 = crate::day22::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut parts = input.trim().split("\n\n");

        let player1 = aoc::read_list_from_string(parts.next().unwrap(), "\n")?;
        let player2 = aoc::read_list_from_string(parts.next().unwrap(), "\n")?;

        Ok(Self { part1: None, part2: None, player1, player2 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(combat(&self.player1, &self.player2));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(recursive_combat(&self.player1, &self.player2).1);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn combat(player1: &[usize], player2: &[usize]) -> usize {
    let mut player1: VecDeque<usize> = player1.to_vec().into();
    let mut player2: VecDeque<usize> = player2.to_vec().into();
    while !player1.is_empty() && !player2.is_empty() {
        let p1 = player1.pop_front().unwrap();
        let p2 = player2.pop_front().unwrap();

        if p1 > p2 {
            player1.push_back(p1);
            player1.push_back(p2);
        } else {
            player2.push_back(p2);
            player2.push_back(p1);
        }
    }

    if player1.is_empty() {
        score(&player2)
    } else {
        score(&player1)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn recursive_combat(player1: &[usize], player2: &[usize]) -> (usize, usize) {
    let mut previous_hands = vec![];
    let mut player1: VecDeque<usize> = player1.to_vec().into();
    let mut player2: VecDeque<usize> = player2.to_vec().into();

    while !player1.is_empty() && !player2.is_empty() {
        // Check for repeated hand sequence:
        let score1 = score(&player1);
        let score2 = score(&player2);

        if previous_hands.contains(&(score1, score2)) {
            return (1, score1);
        } else {
            previous_hands.push((score1, score2));
        }

        let (card1, card2) = (player1.pop_front().unwrap(), player2.pop_front().unwrap());

        let player1_wins = if player1.len() >= card1 && player2.len() >= card2 {
            let new_p1 = player1.iter().take(card1).copied().collect::<Vec<_>>();
            let new_p2 = player2.iter().take(card2).copied().collect::<Vec<_>>();
            let result = recursive_combat(&new_p1, &new_p2);

            result.0 == 1
        } else {
            card1 > card2
        };

        if player1_wins {
            player1.extend(&[card1, card2]);
        } else {
            player2.extend(&[card2, card1]);
        }
    }

    if player1.is_empty() {
        (2, score(&player2))
    } else {
        (1, score(&player1))
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn score(hand: &VecDeque<usize>) -> usize {
    hand.iter().rev().enumerate().fold(0, |accum, (i, c)| accum + (i + 1) * c)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day22::Day22;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "9\n2\n6\n3\n1\n\n5\n8\n4\n7\n10";

        let mut day = Day22::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(306));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(32629));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input = "9\n2\n6\n3\n1\n\n5\n8\n4\n7\n10";

        let mut day = Day22::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(291));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_3() {
        let input = "43\n19\n\n2\n29\n14";

        let mut day = Day22::setup(input).unwrap();
        day.run_part2();
        // just checking that it terminates...
        //assert_eq!(day.part2_answer(), Some(291));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day22.txt");

        let mut day = Day22::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(32519));
    }
}
