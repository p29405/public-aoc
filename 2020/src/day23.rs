use aoc::{self, Day, Result};
use std::ops::RangeInclusive;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day23 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<usize>,
    pub moves: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day23 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day23::Part1;
    type Part2 = crate::day23::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_by_char_from_string(input)?;

        Ok(Self { part1: None, part2: None, input, moves: 100 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let mut game = Game::new(&self.input);
        let mut current = self.input[0];

        for _ in 0..self.moves {
            current = game.round(current);
        }

        current = game.list[1];

        let mut accum = 0;

        while current != 1 {
            accum = accum * 10 + current;
            current = game.list[current];
        }

        self.part1 = Some(accum);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let mut game = Game::new(&self.input);
        game.extend(10..=1_000_000);
        let mut current = self.input[0];

        for _ in 0..10_000_000 {
            current = game.round(current);
        }

        let c1 = game.list[1];
        let c2 = game.list[c1];

        self.part2 = Some(c1 * c2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Default, Debug)]
pub struct Game {
    pub list: Vec<usize>,
    pub max: usize,
    pub first: usize,
    pub last: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Game {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(list: &[usize]) -> Self {
        let mut game = Self::default();
        game.first = list[0];
        game.list.resize(10, 0);
        for (current, next) in list.iter().zip(list.iter().skip(1)) {
            game.list[*current] = *next;
        }
        game.last = list[list.len() - 1];
        game.list[game.last] = game.first;
        game.max = 9;

        game
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn extend(&mut self, range: RangeInclusive<usize>) {
        self.list.reserve(range.end() - range.start() + 1);
        self.max = *range.end();
        for v in range {
            self.append(v);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn pick_up(&mut self, current: usize) -> (usize, usize, usize) {
        let p1 = self.list[current];
        let p2 = self.list[p1];
        let p3 = self.list[p2];
        (p1, p2, p3)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn insert(&mut self, current: usize, nodes: (usize, usize, usize)) {
        let (start, _, end) = nodes;
        self.list[end] = self.list[current];
        self.list[current] = start;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn append(&mut self, value: usize) {
        let node = self.first;

        self.list[self.last] = self.list.len();
        self.list.push(node);
        self.last = value;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn round(&mut self, current: usize) -> usize {
        let picked_up = self.pick_up(current);
        let next = self.list[picked_up.2];
        self.list[current] = next;

        let mut dest = current;
        loop {
            dest = if dest == 1 { self.max } else { dest - 1 };

            if !(picked_up.0 == dest || picked_up.1 == dest || picked_up.2 == dest) {
                break;
            }
        }

        self.insert(dest, picked_up);

        next
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day23::Day23;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "389125467";

        let mut day = Day23::setup(input).unwrap();
        day.moves = 10;
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(92658374));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(54896723));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day23.txt");

        let mut day = Day23::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(146304752384));
    }
}
