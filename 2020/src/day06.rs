use aoc::{self, Day, Result};
use std::str::FromStr;
use fnv::FnvHashMap;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day06 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Answers>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day06 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day06::Part1;
    type Part2 = crate::day06::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter().map(|a| a.yes.len()).sum();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .map(|a|
                {
                    a.yes.iter()
                        .filter(|(_, count)| *count == &a.group)
                        .count()
                })
            .sum();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Answers {
    yes: FnvHashMap<char, usize>,
    group: usize
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Answers {
    type Err = aoc::Error;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut yes = FnvHashMap::default();
        let mut group = 0;
        for l in s.lines() {
            l.trim().chars().for_each(|c| {
                let entry = yes.entry(c).or_insert(0);
                *entry += 1;
            });

            group += 1;
        }
        Ok(Self { yes, group })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day06::Day06;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb";
        let mut day = Day06::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(11));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day06.txt");
        let mut day = Day06::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(6_530));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let input =
            "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb";
        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day06.txt");
        let mut day = Day06::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_323));
    }
}
