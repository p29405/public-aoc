use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day15 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<usize>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day15 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day15::Part1;
    type Part2 = crate::day15::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, ",")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        self.part1 = Some(find_nth(&self.input, 2020));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        self.part2 = Some(find_nth(&self.input, 30_000_000));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn find_nth(input: &[usize], target: usize) -> usize {
    let mut history = vec![0; target];

    for (i, &num) in input.iter().enumerate().take(input.len() - 1) {
        history[num] = i + 1;
    }

    (input.len()..target)
        .fold(*input.last().unwrap(), |spoken, i| {
            let prev = history[spoken];
            history[spoken] = i;
            if prev == 0 { 0 } else { i - prev }
        })
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day15::{Day15};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let inputs =
            [
                ("0,3,6", 436),
                ("1,3,2", 1),
                ("2,1,3", 10),
                ("1,2,3", 27),
                ("2,3,1", 78),
                ("3,2,1", 438),
                ("3,1,2", 1836)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day15::setup(input).unwrap();
            day.run_part1();
            assert_eq!(day.part1_answer(), Some(*answer), "Input {}", input);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day15.txt");
        let mut day = Day15::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(1428));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2a() {
        let input = "0,3,6";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(175_594), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2b() {
        let input = "1,3,2";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(2578), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2c() {
        let input = "2,1,3";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_544_142), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2d() {
        let input = "1,2,3";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(261_214), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2e() {
        let input = "2,3,1";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(6_895_259), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2f() {
        let input = "3,2,1";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(18), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2g() {
        let input = "3,1,2";

        let mut day = Day15::setup(input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(362), "Input {}", input);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day15.txt");
        let mut day = Day15::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(3_718_541));
    }
}
