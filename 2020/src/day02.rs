use aoc::{self, Day, Result};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = usize;
type Part2 = usize;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Day02 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub input: Vec<Policy>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day02 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day02::Part1;
    type Part2 = crate::day02::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let input = aoc::read_list_from_string(input, "\n")?;

        Ok(Self { part1: None, part2: None, input})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let answer = self.input.iter()
            .filter(|p| {
                let count = p.password.chars().filter(|c| {
                    *c == p.letter
                }).count();

                count <= p.highest && count >= p.lowest
            })
            .count();
        self.part1 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let answer = self.input.iter()
            .filter(|p| {
                let mut chars = p.password.chars();
                let first = chars.nth(p.lowest - 1).unwrap();
                let second = chars.nth(p.highest - p.lowest - 1).unwrap();

                (first == p.letter) ^ (second == p.letter)
            })
            .count();
        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug)]
pub struct Policy {
    lowest: usize,
    highest: usize,
    letter: char,
    password: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Policy {
    type Err = aoc::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(' ');

        let mut min_max = parts.next().unwrap().split('-');
        let lowest = min_max.next().unwrap().parse().unwrap();
        let highest =min_max.next().unwrap().parse().unwrap();
        let letter = parts.next().unwrap().chars().next().unwrap();

        Ok(Policy { lowest, highest, letter, password: parts.next().unwrap().to_owned() })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day02::Day02;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input =
            "1-3 a: abcde
             1-3 b: cdefg
             2-9 c: ccccccccc";
        let mut day = Day02::setup(input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2));

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day02.txt");
        let mut day = Day02::setup(&input).unwrap();
        day.run_part1();
        assert_eq!(day.part1_answer(), Some(396));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day02.txt");
        let mut day = Day02::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(428));
    }
}
