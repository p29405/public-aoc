use aoc::{self, Day, Result};

////////////////////////////////////////////////////////////////////////////////////////////////////
type Part1 = i64;
type Part2 = i64;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Day13 {
    pub part1: Option<Part1>,
    pub part2: Option<Part2>,
    pub earliest: i64,
    pub schedule: Vec<i64>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Day for Day13 {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1 = crate::day13::Part1;
    type Part2 = crate::day13::Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(input: &str) -> Result<Self> {
        let mut lines = input.lines();

        let earliest = lines.next().unwrap().parse()?;
        let buses = aoc::read_list_from_string::<String>(&lines.next().unwrap(), ",")?;
        let schedule = buses.iter().map(|b| if b == "x" { -1 } else { b.parse().unwrap()} ).collect();
        Ok(Self { part1: None, part2: None, earliest, schedule})
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) {
        let (seconds, bus) = self.schedule.iter()
            .filter(|&bus| *bus != -1)
            .map(|b| (b - (self.earliest % b), b))
            .min_by_key(|(t, _)| *t)
            .unwrap();
        self.part1 = Some(seconds * bus);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { self.part1 }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) {
        let (_, answer) = self.schedule.iter()
                .enumerate()
                .filter(|(_, &bus)| bus != -1)
                .fold((1, 0), |(m, mut time), (offset, bus)| {
                    while (time + offset as i64) % bus != 0 {
                        time += m;
                    }
                    (m * bus, time)
                });

        self.part2 = Some(answer);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { self.part2 }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test {
    use aoc::Day;
    use crate::day13::Day13;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_1() {
        let input = "939\n7,13,x,x,59,x,31,19";
        let mut day = Day13::setup(input).unwrap();

        day.run_part1();
        assert_eq!(day.part1_answer(), Some(295));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part1() {
        let input = aoc::get_input_from_file("input/day13.txt");
        let mut day = Day13::setup(&input).unwrap();

        day.run_part1();
        assert_eq!(day.part1_answer(), Some(2215));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn example_2() {
        let inputs: [(&str, i64); 6] =
            [
                ("939\n7,13,x,x,59,x,31,19", 1_068_781),
                ("0\n17,x,13,19", 3417),
                ("0\n67,7,59,61", 754_018),
                ("0\n67,x,7,59,61", 779_210),
                ("0\n67,7,x,59,61", 1_261_476),
                ("0\n1789,37,47,1889", 1_202_161_486)
            ];

        for (input, answer) in inputs.iter() {
            let mut day = Day13::setup(input).unwrap();
            day.run_part2();
            assert_eq!(day.part2_answer(), Some(*answer));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn part2() {
        let input = aoc::get_input_from_file("input/day13.txt");

        let mut day = Day13::setup(&input).unwrap();
        day.run_part2();
        assert_eq!(day.part2_answer(), Some(1_058_443_396_696_792));
    }
}
