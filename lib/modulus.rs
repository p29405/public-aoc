
////////////////////////////////////////////////////////////////////////////////////////////////////
pub struct Modulus {
    pub modulus: i128
}

impl Modulus {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(modulus: i128) -> Self {
        Self { modulus }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn add(&self, a: i128, b: i128) -> i128 {
        (a + b).rem_euclid(self.modulus)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn mul(&self, a: i128, b: i128) -> i128 {
        (a * b).rem_euclid(self.modulus)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn pow(&self, a: i128, b: i128) -> i128 {
        let mut result = 1;
        let mut value = b;
        let mut multiplier = a;
        loop {
            if value == 0 {
                return result;
            }
            if value & 1 == 1 {
                result = self.mul(result, multiplier);
            }
            multiplier = self.mul(multiplier, multiplier);
            value >>= 1;
        }
    }

}
