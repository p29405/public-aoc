
////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn permutations<T: Clone>(elements: &[T]) -> Permutations<T> {
    let mut counters = Vec::new();
    counters.resize(elements.len(), 0);
    Permutations { elements: elements.to_vec(), counters, index: 0, next: Some(elements.to_vec()) }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Permutations<T> {
    elements: Vec<T>,
    counters: Vec<usize>,
    index: usize,
    next: Option<Vec<T>>
}

//////////////////////////////////////////////////////////////////////////////
impl<T: Clone> Iterator for Permutations<T>
{
    type Item = Vec<T>;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.next.take();

        while self.index < self.elements.len() {
            if self.counters[self.index] < self.index {
                if self.index % 2 == 0 {
                    self.elements.swap(0, self.index);
                } else {
                    self.elements.swap(self.counters[self.index], self.index);
                }
                self.counters[self.index] += 1;
                self.index = 0;
                self.next = Some(self.elements.clone());
                break;
            } else {
                self.counters[self.index] = 0;
                self.index += 1;
            }
        }

        next
    }
}
/*
////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn combinations<T: Clone>(elements: &[T], size: usize) -> Combinations<T> {
    if size > elements.len() {
        panic!("Combination size {} is bigger than number of elements {}", size, elements.len());
    }

    let mut counters = Vec::new();
    counters.resize(size, 0);
    for i in 0..size {
        counters[i] = i;
    }

    Combinations { elements: elements.to_vec(), counters, index: size - 1, next: Some(elements[0..size].to_vec()) }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Combinations<T> {
    elements: Vec<T>,
    counters: Vec<usize>,
    index: usize,
    next: Option<Vec<T>>
}

//////////////////////////////////////////////////////////////////////////////
impl<T: Clone> Iterator for Combinations<T>
{
    type Item = Vec<T>;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.next.take();

        // look at https://www.cs.utexas.edu/users/djimenez/utsa/cs3343/lecture25.html
        while self.counters[0] != self.elements.len() - self.counters.len() {
            if self.index == self.counters.len()
        }


        while self.index < self.elements.len() {
            if self.counters[self.index] < self.index && {
                if self.index % 2 == 0 {
                    self.elements.swap(0, self.index);
                } else {
                    self.elements.swap(self.counters[self.index], self.index);
                }
                self.counters[self.index] += 1;
                self.index = 0;
                self.next = Some(self.elements.clone());
                break;
            } else {
                self.counters[self.index] = 0;
                self.index += 1;
            }
        }

        next
    }
}
*/

//////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod combos_test {
    use crate::{permutations/*, combinations*/};

    //////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_permutations() {
        let perms = permutations(&[0, 1]).collect::<Vec<_>>();

        assert_eq!(perms.len(), 2);
        assert_eq!(perms[0], vec![0, 1]);
        assert_eq!(perms[1], vec![1, 0]);

        let perms = permutations(&[0, 1, 2]).collect::<Vec<_>>();

        assert_eq!(perms.len(), 6);
        assert_eq!(perms[0], vec![0, 1, 2]);
        assert_eq!(perms[1], vec![1, 0, 2]);
        assert_eq!(perms[2], vec![2, 0, 1]);
        assert_eq!(perms[3], vec![0, 2, 1]);
        assert_eq!(perms[4], vec![1, 2, 0]);
        assert_eq!(perms[5], vec![2, 1, 0]);
    }
/*
    //////////////////////////////////////////////////////////////////////////
    #[test]
    fn combinations() {
        let perms = combinations(&[0, 1], 1).collect::<Vec<_>>();

        assert_eq!(perms.len(), 2);
        assert_eq!(perms[0], vec![0]);
        assert_eq!(perms[1], vec![1]);

        let perms = combinations(&[0, 1], 2).collect::<Vec<_>>();

        assert_eq!(perms.len(), 1);
        assert_eq!(perms[0], vec![0, 1]);

        let perms = combinations(&[0, 1, 2, 3, 4], 2).collect::<Vec<_>>();

        assert_eq!(perms.len(), 10);
        assert_eq!(perms[0], vec![0, 1]);
        assert_eq!(perms[1], vec![0, 2]);
        assert_eq!(perms[2], vec![0, 3]);
        assert_eq!(perms[3], vec![0, 4]);
        assert_eq!(perms[4], vec![1, 2]);
        assert_eq!(perms[5], vec![1, 3]);
        assert_eq!(perms[6], vec![1, 4]);
        assert_eq!(perms[7], vec![2, 3]);
        assert_eq!(perms[8], vec![2, 4]);
        assert_eq!(perms[9], vec![3, 4]);

        let perms = combinations(&[0, 1, 2, 3, 4], 3).collect::<Vec<_>>();

        assert_eq!(perms.len(), 10);
        assert_eq!(perms[0], vec![0, 1, 2]);
        assert_eq!(perms[1], vec![0, 2]);
        assert_eq!(perms[2], vec![0, 3]);
        assert_eq!(perms[3], vec![0, 4]);
        assert_eq!(perms[4], vec![1, 2]);
        assert_eq!(perms[5], vec![1, 3]);
        assert_eq!(perms[6], vec![1, 4]);
        assert_eq!(perms[7], vec![2, 3]);
        assert_eq!(perms[8], vec![2, 4]);
        assert_eq!(perms[9], vec![3, 4]);

    }
*/
}