use log::{Record, Metadata, SetLoggerError};
use structopt::{clap::arg_enum, StructOpt};

pub use log::LevelFilter;
use std::io::{self, Write};

///////////////////////////////////////////////////////////////////////////////
struct Logger;

arg_enum! {
    #[derive(Debug)]
    pub enum LogLevel {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Silent
    }
}

///////////////////////////////////////////////////////////////////////////////
#[derive(StructOpt, Debug)]
#[structopt(name = "logger")]
pub struct Options {
    #[structopt(short = "l", long = "log-level", possible_values = &LogLevel::variants(), case_insensitive = true, default_value = "Error")]
    /// Log level
    log_level: LogLevel,
}

///////////////////////////////////////////////////////////////////////////////
impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= log::max_level()
    }

    #[allow(clippy::explicit_write)]
    fn log(&self, record: &Record) {
        let file = record.file().unwrap_or("unknown");

        if self.enabled(record.metadata()) {
            writeln!(io::stdout(),"[{}:{}] {:?} - {}",
                      file,
                      record.line().unwrap_or(0),
                      record.level(),
                      record.args()).unwrap();
        }
    }

    fn flush(&self) {}
}

///////////////////////////////////////////////////////////////////////////////
pub fn init(level: LevelFilter) -> Result<(), SetLoggerError> {
    log::set_max_level(level);
    log::set_logger(&Logger)
}

///////////////////////////////////////////////////////////////////////////////
pub fn initialize(opts: Options) {
    let level = match opts.log_level {
        LogLevel::Trace  => LevelFilter::Trace,
        LogLevel::Debug  => LevelFilter::Debug,
        LogLevel::Info   => LevelFilter::Info,
        LogLevel::Warn   => LevelFilter::Warn,
        LogLevel::Error  => LevelFilter::Error,
        LogLevel::Silent => LevelFilter::Off,
    };

    if let Err(e) = init(level) {
        eprintln!("Logging failed to initialize {}", e)
    }
}