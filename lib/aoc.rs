use std::fmt::{self, Debug, Display};
use std::fs::File;
use std::io::{self, Read};
use std::str::FromStr;
use std::sync::Mutex;
use std::time::Instant;

use lazy_static::lazy_static;
use structopt::StructOpt;

pub use combos::*;
pub use coord::*;
pub use day::*;
pub use mapping::*;
pub use modulus::*;
pub use ocr::*;
pub use search::*;

////////////////////////////////////////////////////////////////////////////////////////////////////
mod combos;
mod coord;
mod day;
mod mapping;
mod modulus;
mod ocr;
mod search;

pub mod logger;
pub mod pseudo_list;

////////////////////////////////////////////////////////////////////////////////////////////////////
pub type Result<T> = ::std::result::Result<T, Box<dyn ::std::error::Error>>;

///////////////////////////////////////////////////////////////////////////////
#[derive(StructOpt, Debug)]
#[structopt(name = "aoc")]
struct Options {
    #[structopt(flatten)]
    logging: logger::Options,
    #[structopt(name = "file")]
    file: Option<String>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
lazy_static! {
    static ref FILE_NAME: Mutex<Option<String>> = Mutex::new(None);
    static ref STOP_WATCH: Mutex<Option<Instant>> = Mutex::new(None);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn initialize() {
    let opts = Options::from_args();
    if let Some(file) = opts.file {
        match FILE_NAME.lock() {
            Ok(mut filename) => {
                *filename = Some(file);
            },
            Err(e) => panic!("Unable to get lock FILE_NAME: {}", e)
        };
    }
    logger::initialize(opts.logging);
    match STOP_WATCH.lock() {
        Ok(mut stopwatch) => {
            *stopwatch = Some(Instant::now());
        },
        Err(e) => panic!("Unable to get lock STOP_WATCH: {}", e)
    };
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Deprecated
pub fn get_input() -> String {
    let mut input = String::new();
    match FILE_NAME.lock() {
        Ok(file) => {
            if let Some(ref filename) = *file {
                input = get_input_from_file(filename);
            } else {
                io::stdin().read_to_string(&mut input).unwrap();
            }
        }
        Err(e) => panic!("Unable to get lock FILE_NAME: {}", e)
    };

    input
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn get_input_from_file(filename: &str) -> String {
    let mut input = String::new();
    if let Ok(mut input_file) = File::open(filename) {
        let result = input_file.read_to_string(&mut input);
        if result.is_ok() {
            input
        } else {
            panic!("Unable to read from {}", filename);
        }
    } else {
        panic!("Unable to open {}", filename);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list<T: FromStr>(separator: &str) -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let input = get_input();

    let list: Vec<T> = read_list_from_string(&input, separator)?;
    Ok(list)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list_from_file<T: FromStr>(filename: &str, separator: &str) -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let input = read_file(filename)?;

    let list: Vec<T> = read_list_from_string(&input, separator)?;
    Ok(list)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list_from_string<T: FromStr>(input: &str, separator: &str) -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let list = input.trim().split(separator).map(|s| {
        if let Ok(value) = s.trim().parse() {
            value
        } else {
            panic!("Unable to parse {}", s);
        }
    });

    Ok(list.collect())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list_by_char<T: FromStr>() -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let input = get_input();

    let list: Vec<T> = read_list_by_char_from_string(&input)?;
    Ok(list)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list_by_char_from_file<T: FromStr>(filename: &str) -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let input = read_file(filename)?;

    let list: Vec<T> = read_list_by_char_from_string(&input)?;
    Ok(list)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_list_by_char_from_string<T: FromStr>(input: &str) -> Result<Vec<T>>
    where <T as FromStr>::Err: Debug {
    let list = input.trim().chars().map(|s| {
        if let Ok(value) = s.to_string().parse() {
            value
        } else {
            panic!("Unable to parse {}", s);
        }
    });

    Ok(list.collect())
}
////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_lists<T: FromStr>(separator: &str) -> Result<Vec<Vec<T>>>
    where <T as FromStr>::Err: Debug {
    let input = get_input();

    let lists: Vec<Vec<T>> = read_lists_from_string(&input, separator)?;
    Ok(lists)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_lists_from_file<T: FromStr>(filename: &str, separator: &str) -> Result<Vec<Vec<T>>>
    where <T as FromStr>::Err: Debug {
    let input = read_file(filename)?;

    let lists: Vec<Vec<T>> = read_lists_from_string(&input, separator)?;
    Ok(lists)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_lists_from_string<T: FromStr>(input: &str, separator: &str) -> Result<Vec<Vec<T>>>
    where <T as FromStr>::Err: Debug {

    let mut lists = Vec::new();
    for line in input.lines() {
        lists.push(line.trim().split(separator).map(|s| {
            if let Ok(value) = s.parse() {
                value
            } else {
                panic!("Unable to parse {}", s);
            }
        }).collect());
    }

    Ok(lists)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_grid<T: GridElement>() -> Result<Grid<T>> {
    let input = get_input();
    let grid = read_grid_from_string(&input)?;
    Ok(grid)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_grid_from_file<T: GridElement>(filename: &str) -> Result<Grid<T>> {
    let input = read_file(filename)?;

    let grid = read_grid_from_string(&input)?;
    Ok(grid)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn read_grid_from_string<T: GridElement>(input: &str) -> Result<Grid<T>> {
    Ok(input.parse()?)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn read_file(filename: &str) -> Result<String> {
    let mut input = String::new();
    let mut file = File::open(filename)?;
    file.read_to_string(&mut input)?;

    Ok(input)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn answer<T: Display>(part: &str, answer: &T) -> Result<()> {
    println!("{}: {}", part, answer);
    Ok(())
}

////////////////////////////////////////////////////////////////////////////////////////////////////
fn performance_checkpoint(message: &str) {
    if let Ok(mut stopwatch) = STOP_WATCH.lock() {
        if let Some(start) = *stopwatch {
            let duration = start.elapsed();
            print!("{}: Time: {: >6} μs", message, duration.as_micros());
        }
        *stopwatch = Some(Instant::now());
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn setup_finished() {
    performance_checkpoint("Setup ");
    println!();
}

pub fn min_max(values: &[i64]) -> (i64, i64) {
    let mut min = i64::MAX;
    let mut max = i64::MIN;
    values.iter().for_each(|n| {
        min = std::cmp::min(min, *n);
        max = std::cmp::max(max, *n);
    });

    (min, max)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug)]
pub struct Error {
    message: String
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Error {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(message: &str) -> Self {
        Error { message: message.into() }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Error {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> { None }
    fn description(&self) -> &str { &self.message }
    fn cause(&self) -> Option<&dyn std::error::Error> { None }
}
