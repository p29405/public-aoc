use std::fmt::{self, Debug, Display};
use std::str::FromStr;
use super::{Coord, Error};
use fnv::FnvHashMap;
use std::cmp::{min, max};
use std::io::Write;

static CLS: &str = "\x1B[2J\n";

////////////////////////////////////////////////////////////////////////////////////////////////////
pub trait GridElement: Copy + Default + Debug + Display + PartialEq {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn to_element(_c: char) -> Self { unimplemented!() }
    // Using default for now fn unknown() -> Self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub trait Map<T: GridElement>: Display {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set(&mut self, coord: &Coord, value: T);
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get(&self, coord: &Coord) -> T;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set_xy(&mut self,x: i64, y: i64, value: T);
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get_xy(&self, x: i64, y: i64) -> T;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn upper_left(&self) -> Coord;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn lower_right(&self) -> Coord;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Grid<T: GridElement> {
    pub elements: Vec<T>,
    pub rows: i64,
    pub columns: i64,
    pub default_override: Option<T>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> Grid<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(columns: i64, rows: i64) -> Self {
        let mut elements = vec![];
        elements.resize((rows * columns) as usize, T::default());
        Self { elements, rows, columns, default_override: None }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn default_element(&self) -> T {
        if let Some(t) = self.default_override {
            t
        } else {
            T::default()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn adjacent(&self, x: i64, y: i64) -> impl Iterator<Item=T> + '_{
        [(-1, 0), (0, 1), (1, 0), (0, -1)].iter().map(move |(dx, dy)| self.get_xy(x + dx, y + dy))
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn neighbors(&self, x: i64, y: i64) -> impl Iterator<Item=T> + '_{
        [
            (-1, 0), (-1, 1), (0, 1), (1, 1),
            (1, 0), (1, -1), (0, -1), (-1, -1)
        ].iter().map(move |(dx, dy)| self.get_xy(x + dx, y + dy))
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn count_neighbors<P>(&self, x: i64, y: i64, mut predicate: P) -> usize
        where P: FnMut(&T) -> bool {
        self.neighbors(x, y).fold(0, |accum, t| accum + predicate(&t) as usize)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn contains(&self, c: Coord) -> bool {
        c.x >= 0 && c.x < self.columns && c.y >= 0 && c.y < self.rows

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn flip_horizontal(&self) -> Grid<T> {
        let mut flipped = Grid::new(self.columns, self.rows);

        for coord in Coord::range(self.upper_left(), self.lower_right()) {
            flipped.set_xy(self.columns - 1 - coord.x, coord.y, self.get(&coord));
        }
        flipped
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn flip_vertical(&self) -> Grid<T> {
        let mut flipped = Grid::new(self.columns, self.rows);

        for coord in Coord::range(self.upper_left(), self.lower_right()) {
            flipped.set_xy(coord.x, self.rows - 1 - coord.y, self.get(&coord));
        }
        flipped
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn rotate_right(&self) -> Grid<T> {
        let mut rotated = Grid::new(self.rows, self.columns);

        for coord in Coord::range(self.upper_left(), self.lower_right()) {
            rotated.set_xy(self.rows - 1 - coord.y, coord.x, self.get(&coord));
        }
        rotated
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn rotate_left(&self) -> Grid<T> {
        let mut rotated = Grid::new(self.rows, self.columns);

        for coord in Coord::range(self.upper_left(), self.lower_right()) {
            rotated.set_xy(coord.y, self.columns - 1 - coord.x, self.get(&coord));
        }
        rotated
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn split(&self, width: i64, height: i64) -> Vec<Grid<T>> {
        let mut grids = vec![];

        if self.columns % width != 0 || self.rows % height != 0 {
            panic!("Unable to split a grid into {}x{} grids", width, height);
        }

        let x_split = self.columns / width;
        let y_split = self.rows / height;

        for grid_y in 0..y_split {
            for grid_x in 0..x_split {
                let mut current_grid = Grid::new(width, height);
                for current_x in 0..width {
                    for current_y in 0..height {
                        current_grid.set_xy(current_x, current_y, self.get_xy(grid_x * width + current_x, grid_y * height + current_y));
                    }
                }
                grids.push(current_grid)
            }
        }
        grids
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn combine(grids: &[Grid<T>], across: i64, down: i64) -> Grid<T> {
        if across * down != grids.len() as i64 {
            panic!("Cannot construct grid of {}x{} parts without {} parts", across, down, grids.len());
        }

        let width = grids[0].columns;
        let height = grids[0].rows;

        let mut grid = Grid::new(width * across, height * down);

        for grid_y in 0..down {
            for grid_x in 0..across {
                let current_grid = (grid_y * down + grid_x) as usize;
                if grids[current_grid].columns != width && grids[current_grid].rows != height {
                    panic!("Unable to combine grids of different dimensions");
                }
                for current_x in 0..width {
                    for current_y in 0..height {
                        grid.set_xy(grid_x * width + current_x, grid_y * height + current_y, grids[current_grid].get_xy(current_x, current_y));
                    }
                }
            }
        }
        grid
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn iter(&self) -> GridIterator<T> {

        GridIterator { position: Coord::new(0, 0), grid: self.clone() }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn coord_iter(&self) -> CoordIterator {

        CoordIterator { start: self.upper_left(), end: self.lower_right(), current: self.upper_left() }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> Map<T> for Grid<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set(&mut self, coord: &Coord, t: T) {
        self.set_xy(coord.x, coord.y, t);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get(&self, coord: &Coord) -> T {
        self.get_xy(coord.x, coord.y)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set_xy(&mut self, x: i64, y: i64, t: T) {
        self.elements[(y * self.columns + x) as usize] = t;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get_xy(&self, x: i64, y: i64) -> T {
        if x < 0 || x >= self.columns {
            return self.default_element();
        }
        if y < 0 || y >= self.rows {
            return self.default_element();
        }

        self.elements[(y * self.columns + x) as usize]
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn upper_left(&self) -> Coord {
        Coord::new(0, 0)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn lower_right(&self) -> Coord {
        Coord::new(self.columns, self.rows)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> FromStr for Grid<T> {
    type Err = Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut grid = Grid::<T>::default();
        for line in s.lines() {
            grid.columns = 0;
            for c in line.trim().chars() {
                grid.elements.push(T::to_element(c));
                grid.columns += 1;
            }
            grid.rows += 1
        }
        Ok(grid)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> Display for Grid<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for position in Coord::range(Coord::new(0, 0), Coord::new(self.columns, self.rows)) {
            if position.x == 0 && position.y > 0 {
                writeln!(f)?;
            }
            write!(f, "{}", self.get(&position))?;
        }
        writeln!(f)?;
        Ok(())
    }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct GridIterator<T: GridElement> {
    position: Coord,
    grid: Grid<T>
}

//////////////////////////////////////////////////////////////////////////////
impl<T> Iterator for GridIterator<T>
    where T: GridElement {
    type Item = (Coord, T);

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        if self.position.x < 0 || self.position.y < 0 || self.position.x >= self.grid.columns || self.position.y >= self.grid.rows {
            return None;
        }
        let next = (self.position, self.grid.get(&self.position));

        if self.position.x == self.grid.columns - 1 {
            self.position.x = 0;
            self.position.y += 1;
        } else {
            self.position.x += 1;
        }

        Some(next)
    }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct CoordIterator {
    start: Coord,
    end: Coord,
    current: Coord
}

//////////////////////////////////////////////////////////////////////////////
impl Iterator for CoordIterator {
    type Item = Coord;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        if self.current.y >= self.end.y {
            return None;
        }
        let next = self.current;

        if self.current.x == self.end.x - 1 {
            self.current.x = self.start.x;
            self.current.y += 1;
        } else {
            self.current.x += 1;
        }

        Some(next)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T:GridElement> From<CenteredMap<T>> for Grid<T> {
    fn from(map: CenteredMap<T>) -> Self {
        let rows = map.lower_right.y - map.upper_left.y + 1;
        let columns = map.lower_right.x - map.upper_left.x + 1;

        let x_offset = map.upper_left.x;
        let y_offset = map.upper_left.y;

        let mut grid = Grid::new(columns, rows);

        if map.inverted {
            for (grid_y, y) in (0..rows).rev().enumerate() {
                for x in 0..columns {
                    grid.set_xy(x, grid_y as i64, map.get_xy(x + x_offset, y + y_offset));
                }
            }
        } else {
            for y in 0..rows {
                for x in 0..columns {
                    grid.set_xy(x, y, map.get_xy(x + x_offset, y + y_offset));
                }
            }
        }

        grid
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct CenteredMap<T: GridElement> {
    pub elements: FnvHashMap<Coord, T>,
    pub upper_left: Coord,
    pub lower_right: Coord,
    pub inverted: bool,
    animate: bool,
    pub display_upper_left: Coord
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> CenteredMap<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn animate(&mut self, upper_left: Coord) -> &mut Self {
        self.animate = true;
        self.display_upper_left = upper_left;
        print!("{}", CLS);
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn stop_animation(&mut self) -> &mut Self {
        self.animate = false;
        print!("\x1B[{};1H", self.lower_right.y - self.display_upper_left.y + 5);
        self
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn invert(&mut self) -> &mut Self {
        self.inverted = true;
        self
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> Map<T> for CenteredMap<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set(&mut self, coord: &Coord, t: T) {
        let entry = self.elements.entry(*coord).or_insert_with(T::default);
        *entry = t;

        self.upper_left.x = min(self.upper_left.x, coord.x);
        self.upper_left.y = min(self.upper_left.y, coord.y);

        self.lower_right.x = max(self.lower_right.x, coord.x);
        self.lower_right.y = max(self.lower_right.y, coord.y);

        if self.animate {
            print!("\x1B[{};{}H{}", coord.y - self.display_upper_left.y + 1, coord.x -  self.display_upper_left.x + 1, t);
            std::thread::sleep(std::time::Duration::from_millis(5));
            std::io::stdout().flush().unwrap()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get(&self, coord: &Coord) -> T {
        *self.elements.get(coord).unwrap_or(&T::default())
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn set_xy(&mut self, x: i64, y: i64, t: T) {
        self.set(&Coord::new(x, y), t);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn get_xy(&self, x: i64, y: i64) -> T {
        self.get(&Coord::new(x, y))
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn upper_left(&self) -> Coord {
        self.upper_left
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn lower_right(&self) -> Coord {
        self.lower_right
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: GridElement> Display for CenteredMap<T> {

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let rows = self.lower_right.y - self.upper_left.y + 1;
        let columns = self.lower_right.x - self.upper_left.x + 1;

        let x_offset = self.upper_left.x;
        let y_offset = self.upper_left.y;
        writeln!(f, "area ({}), ({})", self.upper_left, self.lower_right)?;

        if self.inverted {
            for y in (0..rows).rev() {
                for x in 0..columns {
                    write!(f, "{}", self.get_xy(x + x_offset, y + y_offset))?;
                }
                writeln!(f)?;
            }
        } else {
            for y in 0..rows {
                for x in 0..columns {
                    write!(f, "{}", self.get_xy(x + x_offset, y + y_offset))?;
                }
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T:GridElement> From<Grid<T>> for CenteredMap<T> {
    fn from(grid: Grid<T>) -> Self {
        let mut map = CenteredMap::default();

        for (c, v) in grid.iter() {
            map.set(&c, v);
        }

        map
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Pixel {
    Off,
    On
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Pixel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn on(&self) -> bool {
        self == &Pixel::On
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn off(&self) -> bool {
        self == &Pixel::Off
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Pixel {
    fn default() -> Self {
        Self::Off
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Pixel {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Pixel::Off => write!(f, "."),
            Pixel::On => write!(f, "#")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Pixel {
    fn to_element(c: char) -> Self {
        match c {
            '#' => Pixel::On,
            '.' => Pixel::Off,
            _   => panic!("Unknown element: _{}_", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Pixel {
    type Err = Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "#" => Ok(Self::On),
            "." => Ok(Self::Off),
            _   => Err(Self::Err::new(&format!("Unknown element: _{}_", s)))
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod mapping_tests {
    use std::fmt;
    use crate::{GridElement, Grid, Map, Coord};

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub enum Square {
        Off,
        Letter(char)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    impl Default for Square {
        fn default() -> Self {
            Square::Off
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    impl fmt::Display for Square {
        ////////////////////////////////////////////////////////////////////////////////////////////
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Square::Off => write!(f, "."),
                Square::Letter(c) => write!(f, "{}", c)
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    impl GridElement for Square {
        fn to_element(c: char) -> Self {
            match c {
                '.' => Square::Off,
                letter => Square::Letter(letter)
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn parsing() {
        let input =
            "abcd
             e.g.";

        let grid: Grid<Square> = input.parse().unwrap();

        assert_eq!(grid.columns, 4);
        assert_eq!(grid.rows, 2);

        assert_eq!(grid.get_xy(0, 0), Square::Letter('a'));
        assert_eq!(grid.get_xy(1, 0), Square::Letter('b'));
        assert_eq!(grid.get_xy(2, 0), Square::Letter('c'));
        assert_eq!(grid.get_xy(3, 0), Square::Letter('d'));
        assert_eq!(grid.get_xy(0, 1), Square::Letter('e'));
        assert_eq!(grid.get_xy(1, 1), Square::Off);
        assert_eq!(grid.get_xy(2, 1), Square::Letter('g'));
        assert_eq!(grid.get_xy(3, 1), Square::Off);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn horizontal_flip() {
        let input =
            "abcd
             efgh
             ijkl
             mnop";

        let original: Grid<Square> = input.parse().unwrap();

        let flipped = original.flip_horizontal();

        assert_eq!(original.columns, flipped.columns);
        assert_eq!(original.rows, flipped.rows);

        assert_eq!(original.get_xy(0, 0), flipped.get_xy(3, 0));
        assert_eq!(original.get_xy(1, 0), flipped.get_xy(2, 0));
        assert_eq!(original.get_xy(2, 0), flipped.get_xy(1, 0));
        assert_eq!(original.get_xy(3, 0), flipped.get_xy(0, 0));
        assert_eq!(original.get_xy(0, 1), flipped.get_xy(3, 1));
        assert_eq!(original.get_xy(1, 1), flipped.get_xy(2, 1));
        assert_eq!(original.get_xy(2, 1), flipped.get_xy(1, 1));
        assert_eq!(original.get_xy(3, 1), flipped.get_xy(0, 1));
        assert_eq!(original.get_xy(0, 2), flipped.get_xy(3, 2));
        assert_eq!(original.get_xy(1, 2), flipped.get_xy(2, 2));
        assert_eq!(original.get_xy(2, 2), flipped.get_xy(1, 2));
        assert_eq!(original.get_xy(3, 2), flipped.get_xy(0, 2));
        assert_eq!(original.get_xy(0, 3), flipped.get_xy(3, 3));
        assert_eq!(original.get_xy(1, 3), flipped.get_xy(2, 3));
        assert_eq!(original.get_xy(2, 3), flipped.get_xy(1, 3));
        assert_eq!(original.get_xy(3, 3), flipped.get_xy(0, 3));
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn vertical_flip() {
        let input =
            "abcd
             efgh
             ijkl
             mnop";

        let original: Grid<Square> = input.parse().unwrap();

        let flipped = original.flip_vertical();

        assert_eq!(original.columns, flipped.columns);
        assert_eq!(original.rows, flipped.rows);

        assert_eq!(original.get_xy(0, 0), flipped.get_xy(0, 3));
        assert_eq!(original.get_xy(1, 0), flipped.get_xy(1, 3));
        assert_eq!(original.get_xy(2, 0), flipped.get_xy(2, 3));
        assert_eq!(original.get_xy(3, 0), flipped.get_xy(3, 3));
        assert_eq!(original.get_xy(0, 1), flipped.get_xy(0, 2));
        assert_eq!(original.get_xy(1, 1), flipped.get_xy(1, 2));
        assert_eq!(original.get_xy(2, 1), flipped.get_xy(2, 2));
        assert_eq!(original.get_xy(3, 1), flipped.get_xy(3, 2));
        assert_eq!(original.get_xy(0, 2), flipped.get_xy(0, 1));
        assert_eq!(original.get_xy(1, 2), flipped.get_xy(1, 1));
        assert_eq!(original.get_xy(2, 2), flipped.get_xy(2, 1));
        assert_eq!(original.get_xy(3, 2), flipped.get_xy(3, 1));
        assert_eq!(original.get_xy(0, 3), flipped.get_xy(0, 0));
        assert_eq!(original.get_xy(1, 3), flipped.get_xy(1, 0));
        assert_eq!(original.get_xy(2, 3), flipped.get_xy(2, 0));
        assert_eq!(original.get_xy(3, 3), flipped.get_xy(3, 0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn flip_symmetry() {
        let input =
            "abcd
             efgh";

        let original: Grid<Square> = input.parse().unwrap();

        assert_eq!(original, original.flip_horizontal().flip_horizontal());
        assert_eq!(original, original.flip_vertical().flip_vertical());

        let grid1 = original.flip_horizontal().flip_vertical();
        let grid2 = original.flip_vertical().flip_horizontal();

        assert_eq!(grid1, grid2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn rotate_right() {
        let input =
            "abcd
             efgh
             ijkl
             mnop";

        let original: Grid<Square> = input.parse().unwrap();

        let rotated = original.rotate_right();

        assert_eq!(original.columns, rotated.rows);
        assert_eq!(original.rows, rotated.columns);

        assert_eq!(original.get_xy(0, 0), rotated.get_xy(3, 0));
        assert_eq!(original.get_xy(1, 0), rotated.get_xy(3, 1));
        assert_eq!(original.get_xy(2, 0), rotated.get_xy(3, 2));
        assert_eq!(original.get_xy(3, 0), rotated.get_xy(3, 3));
        assert_eq!(original.get_xy(0, 1), rotated.get_xy(2, 0));
        assert_eq!(original.get_xy(1, 1), rotated.get_xy(2, 1));
        assert_eq!(original.get_xy(2, 1), rotated.get_xy(2, 2));
        assert_eq!(original.get_xy(3, 1), rotated.get_xy(2, 3));
        assert_eq!(original.get_xy(0, 2), rotated.get_xy(1, 0));
        assert_eq!(original.get_xy(1, 2), rotated.get_xy(1, 1));
        assert_eq!(original.get_xy(2, 2), rotated.get_xy(1, 2));
        assert_eq!(original.get_xy(3, 2), rotated.get_xy(1, 3));
        assert_eq!(original.get_xy(0, 3), rotated.get_xy(0, 0));
        assert_eq!(original.get_xy(1, 3), rotated.get_xy(0, 1));
        assert_eq!(original.get_xy(2, 3), rotated.get_xy(0, 2));
        assert_eq!(original.get_xy(3, 3), rotated.get_xy(0, 3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn rotate_right_rectangle() {
        let input =
            "abcd
             efgh";

        let original: Grid<Square> = input.parse().unwrap();

        let rotated = original.rotate_right();

        assert_eq!(original.columns, rotated.rows);
        assert_eq!(original.rows, rotated.columns);

        assert_eq!(original.get_xy(0, 0), rotated.get_xy(1, 0));
        assert_eq!(original.get_xy(1, 0), rotated.get_xy(1, 1));
        assert_eq!(original.get_xy(2, 0), rotated.get_xy(1, 2));
        assert_eq!(original.get_xy(3, 0), rotated.get_xy(1, 3));
        assert_eq!(original.get_xy(0, 1), rotated.get_xy(0, 0));
        assert_eq!(original.get_xy(1, 1), rotated.get_xy(0, 1));
        assert_eq!(original.get_xy(2, 1), rotated.get_xy(0, 2));
        assert_eq!(original.get_xy(3, 1), rotated.get_xy(0, 3));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn rotate_left() {
        let input =
            "abcd
             efgh
             ijkl
             mnop";

        let original: Grid<Square> = input.parse().unwrap();

        let rotated = original.rotate_left();
        assert_eq!(original.columns, rotated.rows);
        assert_eq!(original.rows, rotated.columns);

        assert_eq!(original.get_xy(0, 0), rotated.get_xy(0, 3));
        assert_eq!(original.get_xy(1, 0), rotated.get_xy(0, 2));
        assert_eq!(original.get_xy(2, 0), rotated.get_xy(0, 1));
        assert_eq!(original.get_xy(3, 0), rotated.get_xy(0, 0));
        assert_eq!(original.get_xy(0, 1), rotated.get_xy(1, 3));
        assert_eq!(original.get_xy(1, 1), rotated.get_xy(1, 2));
        assert_eq!(original.get_xy(2, 1), rotated.get_xy(1, 1));
        assert_eq!(original.get_xy(3, 1), rotated.get_xy(1, 0));
        assert_eq!(original.get_xy(0, 2), rotated.get_xy(2, 3));
        assert_eq!(original.get_xy(1, 2), rotated.get_xy(2, 2));
        assert_eq!(original.get_xy(2, 2), rotated.get_xy(2, 1));
        assert_eq!(original.get_xy(3, 2), rotated.get_xy(2, 0));
        assert_eq!(original.get_xy(0, 3), rotated.get_xy(3, 3));
        assert_eq!(original.get_xy(1, 3), rotated.get_xy(3, 2));
        assert_eq!(original.get_xy(2, 3), rotated.get_xy(3, 1));
        assert_eq!(original.get_xy(3, 3), rotated.get_xy(3, 0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn rotate_left_rectangle() {
        let input =
            "abcd
             efgh";

        let original: Grid<Square> = input.parse().unwrap();

        let rotated = original.rotate_left();

        assert_eq!(original.columns, rotated.rows);
        assert_eq!(original.rows, rotated.columns);

        assert_eq!(original.get_xy(0, 0), rotated.get_xy(0, 3));
        assert_eq!(original.get_xy(1, 0), rotated.get_xy(0, 2));
        assert_eq!(original.get_xy(2, 0), rotated.get_xy(0, 1));
        assert_eq!(original.get_xy(3, 0), rotated.get_xy(0, 0));
        assert_eq!(original.get_xy(0, 1), rotated.get_xy(1, 3));
        assert_eq!(original.get_xy(1, 1), rotated.get_xy(1, 2));
        assert_eq!(original.get_xy(2, 1), rotated.get_xy(1, 1));
        assert_eq!(original.get_xy(3, 1), rotated.get_xy(1, 0));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn rotate_symmetry() {
        let input =
            "abcd
             efgh";

        let original: Grid<Square> = input.parse().unwrap();

        assert_eq!(original, original.rotate_left().rotate_right());
        assert_eq!(original, original.rotate_right().rotate_left());

        let one_eighty_left = original.rotate_left().rotate_left();
        let one_eighty_right = original.rotate_right().rotate_right();

        assert_eq!(one_eighty_left, one_eighty_right);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn split() {
        let input =
            "abcdef
             ghijkl
             mnopqr
             stuvwx
             yz0123
             45689.";

        let grid: Grid<Square> = input.parse().unwrap();

        let four_grids = grid.split(3, 3);
        assert_eq!(four_grids.len(), 4);

        assert_eq!(four_grids[0].columns, 3);
        assert_eq!(four_grids[0].rows, 3);
        assert_eq!(four_grids[0].get_xy(0,0), grid.get_xy(0,0));
        assert_eq!(four_grids[0].get_xy(2,2), grid.get_xy(2,2));

        assert_eq!(four_grids[1].columns, 3);
        assert_eq!(four_grids[1].rows, 3);
        assert_eq!(four_grids[1].get_xy(0,0), grid.get_xy(3,0));
        assert_eq!(four_grids[1].get_xy(2,2), grid.get_xy(5,2));

        assert_eq!(four_grids[2].columns, 3);
        assert_eq!(four_grids[2].rows, 3);
        assert_eq!(four_grids[2].get_xy(0,0), grid.get_xy(0,3));
        assert_eq!(four_grids[2].get_xy(2,2), grid.get_xy(2,5));

        assert_eq!(four_grids[3].columns, 3);
        assert_eq!(four_grids[3].rows, 3);
        assert_eq!(four_grids[3].get_xy(0,0), grid.get_xy(3,3));
        assert_eq!(four_grids[3].get_xy(2,2), grid.get_xy(5,5));

        let nine_grids = grid.split(2, 2);
        assert_eq!(nine_grids.len(), 9);

        assert_eq!(nine_grids[0].columns, 2);
        assert_eq!(nine_grids[0].rows, 2);
        assert_eq!(nine_grids[0].get_xy(0,0), grid.get_xy(0,0));
        assert_eq!(nine_grids[0].get_xy(1,1), grid.get_xy(1,1));

        assert_eq!(nine_grids[1].columns, 2);
        assert_eq!(nine_grids[1].rows, 2);
        assert_eq!(nine_grids[1].get_xy(0,0), grid.get_xy(2,0));
        assert_eq!(nine_grids[1].get_xy(1,1), grid.get_xy(3,1));

        assert_eq!(nine_grids[2].columns, 2);
        assert_eq!(nine_grids[2].rows, 2);
        assert_eq!(nine_grids[2].get_xy(0,0), grid.get_xy(4,0));
        assert_eq!(nine_grids[2].get_xy(1,1), grid.get_xy(5,1));

        assert_eq!(nine_grids[3].columns, 2);
        assert_eq!(nine_grids[3].rows, 2);
        assert_eq!(nine_grids[3].get_xy(0,0), grid.get_xy(0,2));
        assert_eq!(nine_grids[3].get_xy(1,1), grid.get_xy(1,3));

        assert_eq!(nine_grids[4].columns, 2);
        assert_eq!(nine_grids[4].rows, 2);
        assert_eq!(nine_grids[4].get_xy(0,0), grid.get_xy(2,2));
        assert_eq!(nine_grids[4].get_xy(1,1), grid.get_xy(3,3));

        assert_eq!(nine_grids[5].columns, 2);
        assert_eq!(nine_grids[5].rows, 2);
        assert_eq!(nine_grids[5].get_xy(0,0), grid.get_xy(4,2));
        assert_eq!(nine_grids[5].get_xy(1,1), grid.get_xy(5,3));

        assert_eq!(nine_grids[6].columns, 2);
        assert_eq!(nine_grids[6].rows, 2);
        assert_eq!(nine_grids[6].get_xy(0,0), grid.get_xy(0,4));
        assert_eq!(nine_grids[6].get_xy(1,1), grid.get_xy(1,5));

        assert_eq!(nine_grids[7].columns, 2);
        assert_eq!(nine_grids[7].rows, 2);
        assert_eq!(nine_grids[7].get_xy(0,0), grid.get_xy(2,4));
        assert_eq!(nine_grids[7].get_xy(1,1), grid.get_xy(3,5));

        assert_eq!(nine_grids[8].columns, 2);
        assert_eq!(nine_grids[8].rows, 2);
        assert_eq!(nine_grids[8].get_xy(0,0), grid.get_xy(4,4));
        assert_eq!(nine_grids[8].get_xy(1,1), grid.get_xy(5,5));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn split_non_square() {
        let input =
            "abcdef
             ghijkl";

        let grid: Grid<Square> = input.parse().unwrap();

        let grids = grid.split(3, 2);
        assert_eq!(grids.len(), 2);

        assert_eq!(grids[0].columns, 3);
        assert_eq!(grids[0].rows, 2);
        assert_eq!(grids[0].get_xy(0, 0), grid.get_xy(0, 0));
        assert_eq!(grids[0].get_xy(2, 1), grid.get_xy(2, 1));

        assert_eq!(grids[1].columns, 3);
        assert_eq!(grids[1].rows, 2);
        assert_eq!(grids[1].get_xy(0, 0), grid.get_xy(3, 0));
        assert_eq!(grids[1].get_xy(2, 1), grid.get_xy(5, 1));

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    #[should_panic]
    fn invalid_split() {
        let input =
            "abcd
             efgh
             ijkl
             mnop";

        let grid: Grid<Square> = input.parse().unwrap();

        grid.split(3, 3);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn combine() {
        let input =
            "abcdef
             ghijkl
             mnopqr
             stuvwx
             yz0123
             45689.";

        let grid: Grid<Square> = input.parse().unwrap();

        let four_grids = grid.split(3, 3);
        let combined_four_grid = Grid::combine(&four_grids, 2, 2);

        assert_eq!(combined_four_grid, grid);

        let nine_grids = grid.split(2, 2);
        let combined_nine_grid = Grid::combine(&nine_grids, 3, 3);

        assert_eq!(combined_nine_grid, grid);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn combine_non_square() {
        let input =
            "abcdef
             ghijkl";

        let grid: Grid<Square> = input.parse().unwrap();

        let grids = grid.split(3, 2);

        let combined = Grid::combine(&grids, 2, 1);

        assert_eq!(combined, grid);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    #[should_panic]
    fn invalid_combine_grids() {
        let input =
            "abcdef
             ghijkl
             mnopqr
             stuvwx
             yz0123
             45689.";

        let grid: Grid<Square> = input.parse().unwrap();

        let four_grids = grid.split(3, 3);
        let _combined_four_grid = Grid::combine(&four_grids[0..3], 2, 2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    #[should_panic]
    fn invalid_combine_size() {
        let input =
            "abcdef
             ghijkl
             mnopqr
             stuvwx
             yz0123
             45689.";

        let grid: Grid<Square> = input.parse().unwrap();

        let mut four_grids = grid.split(3, 3);

        let wrong_size =
            "ab
             cd";
        four_grids[2] = wrong_size.parse().unwrap();
        let _combined_four_grid = Grid::combine(&four_grids, 2, 2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn iterator() {
        let input =
            "abc
             def
             ghi";

        let grid: Grid<Square> = input.parse().unwrap();
        let mut iter = grid.iter();

        assert_eq!(iter.next(), Some((Coord::new(0, 0), Square::Letter('a'))));
        assert_eq!(iter.next(), Some((Coord::new(1, 0), Square::Letter('b'))));
        assert_eq!(iter.next(), Some((Coord::new(2, 0), Square::Letter('c'))));
        assert_eq!(iter.next(), Some((Coord::new(0, 1), Square::Letter('d'))));
        assert_eq!(iter.next(), Some((Coord::new(1, 1), Square::Letter('e'))));
        assert_eq!(iter.next(), Some((Coord::new(2, 1), Square::Letter('f'))));
        assert_eq!(iter.next(), Some((Coord::new(0, 2), Square::Letter('g'))));
        assert_eq!(iter.next(), Some((Coord::new(1, 2), Square::Letter('h'))));
        assert_eq!(iter.next(), Some((Coord::new(2, 2), Square::Letter('i'))));
        assert_eq!(iter.next(), None);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    #[test]
    fn coord_iterator() {
        let input =
            "abc
             def
             ghi";

        let grid: Grid<Square> = input.parse().unwrap();
        let mut iter = grid.coord_iter();

        assert_eq!(iter.next(), Some(Coord::new(0, 0)));
        assert_eq!(iter.next(), Some(Coord::new(1, 0)));
        assert_eq!(iter.next(), Some(Coord::new(2, 0)));
        assert_eq!(iter.next(), Some(Coord::new(0, 1)));
        assert_eq!(iter.next(), Some(Coord::new(1, 1)));
        assert_eq!(iter.next(), Some(Coord::new(2, 1)));
        assert_eq!(iter.next(), Some(Coord::new(0, 2)));
        assert_eq!(iter.next(), Some(Coord::new(1, 2)));
        assert_eq!(iter.next(), Some(Coord::new(2, 2)));
        assert_eq!(iter.next(), None);
    }
}
