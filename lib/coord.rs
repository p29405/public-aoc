use std::hash::{Hash, Hasher};
use std::cmp::Ordering;
use std::fmt::{self, Display};
use std::str::FromStr;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Default, Debug, PartialEq, Eq)]
pub struct Slope {
    pub rise: i64,
    pub run: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Slope {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(p1: &Coord, p2: &Coord) -> Self {
        let mut rise = p2.y - p1.y;
        let mut run = p2.x - p1.x;
        if rise == 0 && run == 0 {
            panic!("p1 == p2");
        } else if rise == 0 {
            run = if p1.x > p2.x { -1 } else { 1 };
        } else if run == 0 {
            rise = if p1.y > p2.y { -1 } else { 1 };
        } else {
            let divisor = Self::gcd(rise.abs(), run.abs());
            rise /= divisor;
            run /= divisor;
        }
        Self { rise, run }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn next(&self, pos: Coord) -> Coord {
        Coord::new(pos.x + self.run, pos.y + self.rise)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn previous(&self, pos: &Coord) -> Coord {
        Coord::new(pos.x - self.run, pos.y - self.rise)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn angle(&self) -> f64 {
        (self.rise as f64).atan2(self.run as f64)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn gcd(x: i64, y: i64) -> i64 {
        let mut a = x;
        let mut b = y;
        while b != 0 {
            let tmp = a;
            a = b;
            b = tmp % b;
        }
        a
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Copy, Default, Debug, Eq)]
pub struct Coord {
    pub x: i64,
    pub y: i64,
    pub d: u64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(x: i64, y: i64) -> Self {
        Self { x, y, d: 0 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn distance(&self, target: &Self) -> i64 {
        (self.x - target.x).abs() + (self.y - target.y).abs()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn go(&self, direction: Direction) -> Self {
        let dx;
        let dy;
        match direction {
            Direction::North => { dx = 0; dy = 1; },
            Direction::East => { dx = 1; dy = 0; },
            Direction::South => { dx = 0; dy = -1; },
            Direction::West => { dx = -1; dy = 0; },
        };

        Self { x: self.x + dx, y: self.y + dy, d: self.d + 1 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn go_inverted(&self, direction: Direction) -> Self {
        let dx;
        let dy;
        match direction {
            Direction::North => { dx = 0; dy = -1; },
            Direction::East => { dx = 1; dy = 0; },
            Direction::South => { dx = 0; dy = 1; },
            Direction::West => { dx = -1; dy = 0; },
        };

        Self { x: self.x + dx, y: self.y + dy, d: self.d + 1 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn next(&self, velocity: &Velocity) -> Self {
        Self { x: self.x + velocity.dx, y: self.y + velocity.dy, d: 0 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn prev(&self, velocity: &Velocity) -> Self {
        Self { x: self.x - velocity.dx, y: self.y - velocity.dy, d: 0 }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn adjacent(&self) -> impl Iterator<Item=Self> {
        let x = self.x;
        let y = self.y;

        [(-1, 0), (0, 1), (1, 0), (0, -1)].iter()
            .map(move |(dx, dy)| Self { x: x + dx, y: y + dy, d: 0 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn neighbors(&self) -> impl Iterator<Item=Self> {
        let x = self.x;
        let y = self.y;

        [(-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)].iter()
            .map(move |(dx, dy)| Self { x: x + dx, y: y + dy, d: 0 })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn range(start: Coord, end: Coord) -> impl Iterator<Item=Self> {
        std::iter::successors(Some((start, end, start)), |&(start, end, current)| {
            if current.x < end.x - 1 {
                Some((start, end, Self { x: current.x + 1, y: current.y, d: 0 }))
            } else if current.y < end.y - 1 {
                Some((start, end, Self { x: start.x, y: current.y + 1, d: 0 }))
            } else {
                None
            }
        }).map(|(_, _,current)| current)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn range_inclusive(start: Coord, end: Coord) -> impl Iterator<Item=Self> {
        std::iter::successors(Some((start, end, start)), |&(start, end, current)| {
            if current.x < end.x {
                Some((start, end, Self { x: current.x + 1, y: current.y, d: 0 }))
            } else if current.y < end.y {
                Some((start, end, Self { x: start.x, y: current.y + 1, d: 0 }))
            } else {
                None
            }
        }).map(|(_, _,current)| current)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn from_pair(s: &str) -> Coord {
        let parts = s.split(',').collect::<Vec<&str>>();
        Coord::new(parts[0].parse().unwrap(), parts[1].parse().unwrap())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Ord for Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn cmp(&self, other: &Self) -> Ordering {
        if self.y != other.y {
            self.y.cmp(&other.y)
        } else {
            self.x.cmp(&other.x)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl PartialOrd for Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl PartialEq for Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Hash for Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Coord {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Coord {
    type Err = crate::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split(',');

        let x = if let Ok(x) = parts.next().unwrap().parse() {
            x
        } else {
            return Err(Self::Err::new(&format!("Invalid Coord {}", s)));
        };

        let y = if let Ok(y) = parts.next().unwrap().parse() {
            y
        } else {
            return Err(Self::Err::new(&format!("Invalid Coord {}", s)));
        };

        Ok(Self { x, y, d: 0 })
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug, Clone, Copy, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Velocity {
    pub dx: i64,
    pub dy: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Velocity {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(dx: i64, dy: i64) -> Self {
        Self { dx, dy }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Velocity {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.dx, self.dy)
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug, Clone, Copy, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Coord3D {
    pub x: i64,
    pub y: i64,
    pub z: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Coord3D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(x: i64, y: i64, z: i64) -> Self {
        Self { x, y, z }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn next(&self, velocity: &Velocity3D) -> Self {
        Self { x: self.x + velocity.dx, y: self.y + velocity.dy, z: self.z + velocity.dz }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn range_inclusive(start: Coord3D, end: Coord3D) -> impl Iterator<Item = Coord3D> {
        std::iter::successors(Some((start, end, start)), |&(start, end, current)| {
            if current.x < end.x {
                Some((start, end, Coord3D::new(current.x + 1, current.y, current.z)))
            } else if current.y < end.y {
                Some((start, end, Coord3D::new(start.x, current.y + 1, current.z)))
            } else if current.z < end.z {
                Some((start, end, Coord3D::new(start.x, start.y, current.z + 1)))
            } else {
                None
            }
        }).map(|(_, _,current)| current)
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Coord3D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug, Clone, Copy, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Velocity3D {
    pub dx: i64,
    pub dy: i64,
    pub dz: i64,
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Velocity3D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(dx: i64, dy: i64, dz: i64) -> Self {
        Self { dx, dy, dz }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Velocity3D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {})", self.dx, self.dy, self.dz)
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Default, Debug, Clone, Copy, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Coord4D {
    pub x: i64,
    pub y: i64,
    pub z: i64,
    pub w: i64
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Coord4D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn new(x: i64, y: i64, z: i64, w: i64) -> Self {
        Self { x, y, z, w }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn range_inclusive(start: Self, end: Self) -> impl Iterator<Item = Self> {
        std::iter::successors(Some((start, end, start)), |&(start, end, current)| {
            if current.x < end.x {
                Some((start, end, Self::new(current.x + 1, current.y, current.z, current.w)))
            } else if current.y < end.y {
                Some((start, end, Self::new(start.x, current.y + 1, current.z, current.w)))
            } else if current.z < end.z {
                Some((start, end, Self::new(start.x, start.y, current.z + 1, current.w)))
            } else if current.w < end.w {
                Some((start, end, Self::new(start.x, start.y, start.z, current.w + 1)))
            } else {
                None
            }
        }).map(|(_, _,current)| current)
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Display for Coord4D {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {}, {})", self.x, self.y, self.z, self.w)
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Direction {
    North,
    East,
    South,
    West
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl Direction {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn one_eighty(self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn turn_right(self) -> Self {
        match self {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn turn_left(self) -> Self {
        match self {
            Direction::North => Direction::West,
            Direction::East => Direction::North,
            Direction::South => Direction::East,
            Direction::West => Direction::South
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn from(c: char) -> Self {
        match c {
            'N'|'U'|'^' => Self::North,
            'E'|'R'|'>' => Self::East,
            'S'|'D'|'v' => Self::South,
            'W'|'L'|'<' => Self::West,
            _ => panic!("Unknown direction {}", c)
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl FromStr for Direction {
    type Err = crate::Error;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Direction::from(s.chars().next().unwrap()))
    }
}