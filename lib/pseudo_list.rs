use std::collections::VecDeque;

////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Clone, Debug, Default)]
pub struct CircularList<T> {
    front: Option<T>,
    list: VecDeque<T>
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl<T: Clone> CircularList<T> {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn from_slice(slice: &[T]) -> Self {
        Self { front: None, list: slice.to_vec().into() }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn normalize(&mut self) {
        if self.front.is_none() {
            self.front = self.list.pop_front();
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn pop_front(&mut self) -> Option<T> {
        if self.front.is_none() {
            self.list.pop_front()
        } else {
            self.front.take()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn front(&mut self) -> Option<&T> {
        if self.front.is_none() {
            self.list.front()
        } else {
            self.front.as_ref()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn pop_back(&mut self) -> Option<T> {
        if self.list.is_empty() {
            self.front.take()
        } else {
            self.list.pop_back()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn push_back(&mut self, t: T) {
        if self.front.is_none() {
            self.front = Some(t);
        } else {
            self.list.push_back(t);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn push_front(&mut self, t: T) {
        if let Some(front) = self.front.take() {
            self.list.push_front(front);
            self.front = Some(t)
        } else {
            self.front = Some(t)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn len(&self) -> usize {
        if self.front.is_none() {
            self.list.len()
        } else {
            self.list.len() + 1
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn is_empty(&self) -> bool {
        self.front.is_none() && self.list.is_empty()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn value(&self) -> &Option<T> {
        &self.front
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn move_left(&mut self) {
        if self.list.is_empty() {
            return;
        }

        if let Some(current) = self.front.take() {
            self.list.push_front(current);
            self.front = self.list.pop_back();
        } else {
            self.front = self.list.pop_back();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn move_right(&mut self) {
        if self.list.is_empty() {
            return;
        }

        if let Some(current) = self.front.take() {
            self.list.push_back(current);
            self.front = self.list.pop_front();
        } else {
            self.front = self.list.pop_front();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn insert_left(&mut self, t: T) {
        if self.front.is_some() {
            self.list.push_back(t);
        } else {
            self.front = Some(t);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn insert_right(&mut self, t: T) {
        if self.front.is_some() {
            self.list.push_front(t);
        } else {
            self.front = Some(t);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn remove(&mut self) -> Option<T>{
        self.front.take()
    }
}



#[cfg(test)]
mod circular_list_test {
    use crate::pseudo_list::CircularList;

    #[test]
    fn test_construction() {
        let mut list = CircularList::default();

        assert!(list.is_empty());
        assert_eq!(list.value(), &None);
        (0..100).for_each(|i| list.push_back(i));

        assert_eq!(list.len(), 100);
        assert_eq!(list.value(), &Some(0));
        assert_eq!(list.list.len(), 99);

        (-100..0).rev().for_each(|i| list.push_front(i));

        assert_eq!(list.len(), 200);
        assert_eq!(list.value(), &Some(-100));
    }

    #[test]
    fn test_moving() {
        let mut list = CircularList::default();

        assert!(list.is_empty());
        assert_eq!(list.value(), &None);

        list.move_left();

        assert!(list.is_empty());
        assert_eq!(list.value(), &None);

        list.move_right();

        assert!(list.is_empty());
        assert_eq!(list.value(), &None);

        (0..100).for_each(|i| list.push_back(i));
        (-100..0).rev().for_each(|i| list.push_front(i));

        list.move_left();
        assert_eq!(list.value(), &Some(99));

        (0..99).for_each(|_| list.move_left());
        assert_eq!(list.value(), &Some(0));

        list.move_left();
        assert_eq!(list.value(), &Some(-1));

        list.move_right();
        assert_eq!(list.value(), &Some(0));

        (0..99).for_each(|_| list.move_right());
        assert_eq!(list.value(), &Some(99));

        list.move_right();
        assert_eq!(list.value(), &Some(-100));

        list.move_left();
        assert_eq!(list.value(), &Some(99));
    }

    #[test]
    fn test_inserting() {
        let mut list = CircularList::default();

        list.insert_left(10);
        assert_eq!(list.len(), 1);
        assert_eq!(list.value(), &Some(10));

        list.insert_left(8);
        assert_eq!(list.len(), 2);
        assert_eq!(list.value(), &Some(10));

        list.insert_right(12);
        assert_eq!(list.len(), 3);
        assert_eq!(list.value(), &Some(10));

        list.insert_left(9);
        assert_eq!(list.len(), 4);
        assert_eq!(list.value(), &Some(10));

        list.insert_right(11);
        assert_eq!(list.len(), 5);
        assert_eq!(list.value(), &Some(10));

        list.move_left();
        assert_eq!(list.len(), 5);
        assert_eq!(list.value(), &Some(9));

        list.move_right();
        list.move_right();
        assert_eq!(list.len(), 5);
        assert_eq!(list.value(), &Some(11));
    }

    #[test]
    fn test_remove() {
        let mut list = CircularList::default();

        (0..10).for_each(|i| list.push_back(i));

        (0..5).for_each(|_| list.move_right());

        assert_eq!(list.len(), 10);
        assert_eq!(list.value(), &Some(5));

        let value = list.remove();

        assert_eq!(value, Some(5));
        assert_eq!(list.len(), 9);
        assert_eq!(list.value(), &None);

        list.move_right();

        assert_eq!(list.len(), 9);
        assert_eq!(list.value(), &Some(6));

        let value2 = list.remove();

        assert_eq!(value2, Some(6));
        assert_eq!(list.len(), 8);
        assert_eq!(list.value(), &None);

        list.move_left();

        assert_eq!(list.len(), 8);
        assert_eq!(list.value(), &Some(4));
    }
}