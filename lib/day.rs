use crate::Result;
use structopt::{clap::arg_enum};

///////////////////////////////////////////////////////////////////////////////
arg_enum! {
    #[derive(Debug, PartialEq, Eq)]
    pub enum Aoc {
        Day01,
        Day02,
        Day03,
        Day04,
        Day05,
        Day06,
        Day07,
        Day08,
        Day09,
        Day10,
        Day11,
        Day12,
        Day13,
        Day14,
        Day15,
        Day16,
        Day17,
        Day18,
        Day19,
        Day20,
        Day21,
        Day22,
        Day23,
        Day24,
        Day25,
        Bonus,
        All
    }
}

///////////////////////////////////////////////////////////////////////////////
macro_rules! timer {
    ($func:expr) => {{
        let now = std::time::Instant::now();
        let res = $func;
        let elapsed = now.elapsed();
        (res, elapsed)
    }}
}

///////////////////////////////////////////////////////////////////////////////
#[macro_export]
macro_rules! day_match {
    ($opts:tt, $enum:expr, $mod:tt, $day:tt, $i:tt) => {{
        if $enum == $opts.day || Aoc::All == $opts.day {
            let input = aoc::get_input_from_file(&input_file($i));
            println!("================ Day {:02} ====================", $i);
            aoc::run::<$mod::$day>(&input);
            println!();
        }
    }}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
pub trait Day {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    type Part1;
    type Part2;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn setup(_: &str) -> Result<Self> where Self: Sized { unimplemented!() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part1(&mut self) { unimplemented!() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part1_answer(&self) -> Option<Self::Part1> { unimplemented!() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn run_part2(&mut self) { unimplemented!() }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn part2_answer(&self) -> Option<Self::Part2> { unimplemented!() }

}

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn run<D: Day>(input: &str)
    where <D as Day>::Part1: std::fmt::Display,
          <D as Day>::Part2: std::fmt::Display,
{
    let (mut day, setup_time) = timer!(D::setup(input).unwrap());

    let part1_time = timer!(day.run_part1()).1;
    let part2_time = timer!(day.run_part2()).1;
    println!("Setup: Time {:>12}", format!("{:?}", setup_time));
    if let Some(answer) = day.part1_answer() {
        println!("Part1: Time {:>12}, answer {}", format!("{:?}", part1_time), answer);
    }
    if let Some(answer) = day.part2_answer() {
        println!("Part2: Time {:>12}, answer {}", format!("{:?}", part2_time), answer);
    }
}
