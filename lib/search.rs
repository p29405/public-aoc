use crate::{Coord, GridElement, Grid, Map};
use std::collections::HashSet;
use std::fmt;

//////////////////////////////////////////////////////////////////////////////
pub fn bfs<P>(start: Coord, predicate: P) -> Bfs<P>
    where P: FnMut(&Coord) -> bool {
    Bfs { visited: HashSet::new(), frontier: vec![start], predicate }
}

//////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
pub struct Bfs<P> {
    visited: HashSet<Coord>,
    frontier: Vec<Coord>,
    predicate: P
}

//////////////////////////////////////////////////////////////////////////////
impl<P> Bfs<P>
    where P: FnMut(&Coord) -> bool {

    //////////////////////////////////////////////////////////////////////////
    pub fn get_next(&mut self) -> &HashSet<Coord> {
        let mut next = vec![];
        for p in &self.frontier {
            for n in p.adjacent() {
                if !self.visited.contains(&n) && !self.frontier.contains(&n) &&
                    !next.contains(&n) && (self.predicate)(&n) {
                    next.push(n)
                }
            }
            self.visited.insert(*p);
        }
        self.frontier = next;

        &self.visited
    }
}

//////////////////////////////////////////////////////////////////////////////
impl<P> Iterator for Bfs<P>
    where P: FnMut(&Coord) -> bool {
    type Item = HashSet<Coord>;

    //////////////////////////////////////////////////////////////////////////
    fn next(&mut self) -> Option<Self::Item> {
        // I really want a streaming iterator
        Some(self.get_next().clone())
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// pub fn find_paths<P>(starts: &[Coord], ends: &[Coord], columns: i64, rows: i64, mut predicate: P) -> Grid<Path>
//     where P: FnMut(&Coord) -> bool {
//     let mut paths = Grid::new(columns, rows);
//     let mut frontier = vec![];
//     let mut depth = 0;
//
//     for start in starts {
//         frontier.push(*start);
//         paths.set(start, Path::Distance(depth));
//     }
//
//     loop {
//         let mut next = vec![];
//         depth += 1;
//
//         for p in &frontier {
//             for n in p.adjacent() {
//                 if paths.get(&n) == Path::Unreachable && !next.contains(&n) && (predicate)(&n) {
//                     paths.set(&n, Path::Distance(depth));
//                     next.push(n)
//                 }
//             }
//         }
//
//         frontier = next;
//
//         if frontier.is_empty() || ends.iter().all(|e| paths.get(e) != Path::Unreachable) {
//             break;
//         }
//     }
//
//     paths
// }

////////////////////////////////////////////////////////////////////////////////////////////////////
pub fn find_paths<P>(starts: &[Coord], ends: &[Coord], columns: i64, rows: i64, mut predicate: P) -> Grid<Path>
    where P: FnMut(&Coord, &Coord) -> bool {
    let mut paths = Grid::new(columns, rows);
    let mut frontier = vec![];
    let mut depth = 0;

    for start in starts {
        frontier.push(*start);
        paths.set(start, Path::Distance(depth));
    }

    loop {
        let mut next = vec![];
        depth += 1;

        for p in &frontier {
            for n in p.adjacent() {
                if paths.get(&n) == Path::Unreachable && !next.contains(&n) && (predicate)(p, &n) {
                    paths.set(&n, Path::Distance(depth));
                    next.push(n)
                }
            }
        }

        frontier = next;

        if frontier.is_empty() || ends.iter().all(|e| paths.get(e) != Path::Unreachable) {
            break;
        }
    }

    paths
}
////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Path {
    Distance(usize),
    Unreachable
}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    pub fn distance(&self) -> usize {
        if let Path::Distance(d) = self {
            *d
        } else {
            panic!("");
        }
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////
impl Default for Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn default() -> Self {
        Path::Unreachable
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl fmt::Display for Path {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Path::Distance(d) => write!(f, "{:02}|", d),
            Path::Unreachable => write!(f, "##|")
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
impl GridElement for Path {}

//////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test_bfs {
    use crate::{search::bfs, Coord};

    //////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_bfs() {
        let mut bfs_it = bfs(Coord::new(0, 0), |p| p.x >= p.y);

        let visited = bfs_it.next().unwrap();
        assert_eq!(visited.len(), 1);
        assert!(visited.contains(&Coord::new(0, 0)));

        let visited = bfs_it.next().unwrap();
        assert_eq!(visited.len(), 3);
        assert!(visited.contains(&Coord::new(0, 0)));

        assert!(visited.contains(&Coord::new(1, 0)));
        assert!(visited.contains(&Coord::new(0, -1)));

        let visited = bfs_it.next().unwrap();
        assert_eq!(visited.len(), 8);
        assert!(visited.contains(&Coord::new(0, 0)));

        assert!(visited.contains(&Coord::new(1, 0)));
        assert!(visited.contains(&Coord::new(0, -1)));

        assert!(visited.contains(&Coord::new(2, 0)));
        assert!(visited.contains(&Coord::new(1, 1)));
        assert!(visited.contains(&Coord::new(1, -1)));
        assert!(visited.contains(&Coord::new(0, -2)));
        assert!(visited.contains(&Coord::new(-1, -1)));
    }
}

//////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod test_path_finding {
    use crate::{search::find_paths, Coord, GridElement, read_grid_from_string, Map};
    use std::fmt;
    use crate::search::Path;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub enum Square {
        Wall,
        Open
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl Default for Square {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        fn default() -> Self {
            Square::Wall
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl fmt::Display for Square {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                Square::Wall => write!(f, "#"),
                Square::Open => write!(f, ".")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    impl GridElement for Square {
        fn to_element(c: char) -> Self {
            match c {
                '.' => Square::Open,
                '#' => Square::Wall,
                _ => panic!("Unknown element: {}", c)
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_find_path() {
        let input =
            "########
             #...#..#
             ###.#.##
             #...#..#
             #.####.#
             #......#
             ########";

        let map = read_grid_from_string::<Square>(input).unwrap();

        let paths = find_paths(&[Coord::new(1, 1)], &[Coord::new(6, 1)],
                               map.columns, map.rows,
                               |_, p| map.get(p) != Square::Wall);
        assert_eq!(paths.get_xy(1, 1), Path::Distance(0));
        assert_eq!(paths.get_xy(2, 1), Path::Distance(1));
        assert_eq!(paths.get_xy(3, 1), Path::Distance(2));
        assert_eq!(paths.get_xy(3, 2), Path::Distance(3));
        assert_eq!(paths.get_xy(3, 3), Path::Distance(4));
        assert_eq!(paths.get_xy(2, 3), Path::Distance(5));
        assert_eq!(paths.get_xy(1, 3), Path::Distance(6));
        assert_eq!(paths.get_xy(1, 4), Path::Distance(7));
        assert_eq!(paths.get_xy(1, 5), Path::Distance(8));
        assert_eq!(paths.get_xy(2, 5), Path::Distance(9));
        assert_eq!(paths.get_xy(3, 5), Path::Distance(10));
        assert_eq!(paths.get_xy(4, 5), Path::Distance(11));
        assert_eq!(paths.get_xy(5, 5), Path::Distance(12));
        assert_eq!(paths.get_xy(6, 5), Path::Distance(13));
        assert_eq!(paths.get_xy(6, 4), Path::Distance(14));
        assert_eq!(paths.get_xy(6, 3), Path::Distance(15));
        assert_eq!(paths.get_xy(5, 3), Path::Distance(16));
        assert_eq!(paths.get_xy(5, 2), Path::Distance(17));
        assert_eq!(paths.get_xy(5, 1), Path::Distance(18));
        assert_eq!(paths.get_xy(6, 1), Path::Distance(19));
    }

    //////////////////////////////////////////////////////////////////////////
    #[test]
    fn test_find_path_2() {
        let input =
            "########
             #...#..#
             ##.##.##
             #......#
             #.####.#
             #......#
             ########";

        let map = read_grid_from_string::<Square>(input).unwrap();

        let paths = find_paths(&[Coord::new(1, 1)], &[Coord::new(6, 1), Coord::new(1, 5)],
                               map.columns, map.rows,
                               |_, p| map.get(p) != Square::Wall);
        assert_eq!(paths.get_xy(1, 1), Path::Distance(0));
        assert_eq!(paths.get_xy(6, 1), Path::Distance(9));
        assert_eq!(paths.get_xy(1, 5), Path::Distance(6));

    }
}